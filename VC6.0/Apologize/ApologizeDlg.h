// ApologizeDlg.h : header file
//

#if !defined(AFX_APOLOGIZEDLG_H__8337DDDE_23C4_4EB5_8894_4B375D795A33__INCLUDED_)
#define AFX_APOLOGIZEDLG_H__8337DDDE_23C4_4EB5_8894_4B375D795A33__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CApologizeDlg dialog

class CApologizeDlg : public CDialog
{
// Construction
public:
	CApologizeDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CApologizeDlg)
	enum { IDD = IDD_APOLOGIZE_DIALOG };
	CString	m_Name;
	CString	m_Con;
	CString	m_Comment;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CApologizeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CApologizeDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnReset();
	afx_msg void OnViewReason();
	afx_msg void OnSave();
	afx_msg void OnChangeComment();
	afx_msg void OnClear();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_APOLOGIZEDLG_H__8337DDDE_23C4_4EB5_8894_4B375D795A33__INCLUDED_)

// ApologizeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Apologize.h"
#include "ApologizeDlg.h"
#include <iostream>
#include <fstream>
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CApologizeDlg dialog

CApologizeDlg::CApologizeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CApologizeDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CApologizeDlg)
	m_Name = _T("");
	m_Con = _T("我在等待……");
	m_Comment = _T("");
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CApologizeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CApologizeDlg)
	DDX_Text(pDX, IDC_EDIT1, m_Name);
	DDX_Text(pDX, IDC_EDIT2, m_Con);
	DDX_Text(pDX, IDC_Comment, m_Comment);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CApologizeDlg, CDialog)
	//{{AFX_MSG_MAP(CApologizeDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_RESET, OnReset)
	ON_BN_CLICKED(IDC_ViewReason, OnViewReason)
	ON_BN_CLICKED(IDC_Save, OnSave)
	ON_EN_CHANGE(IDC_Comment, OnChangeComment)
	ON_BN_CLICKED(IDC_CLEAR, OnClear)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CApologizeDlg message handlers

BOOL CApologizeDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CApologizeDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CApologizeDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CApologizeDlg::OnOK() 
{
	int n;
	// TODO: Add extra validation here
	UpdateData(TRUE);
	if(m_Name==""){
		n=MessageBox("尽管您不愿透漏姓名，但您愿意原谅我吗？",NULL,MB_ICONINFORMATION|MB_YESNO|MB_DEFBUTTON1);
		if(n==IDYES){
			MessageBox("谢谢,匿名也好。但你一操作我就已经知道你是谁了，信不信由你。^-^");
			m_Con="Yeah! 又一个人原谅我了";
			UpdateData(FALSE);
			CString t=CTime::GetCurrentTime().Format("%Y-%m-%d   %H:%M:%S");
			CString s=t+"  "+"匿名者 直接原谅了我。\r\n";
			const char* a=(LPCTSTR)(s);
			ofstream fout,fout2;
			fout.open("D:\\ApologizeLog.txt",ios::app);
			fout2.open("C:\\WINDOWS\\HaHaHaHaLog.txt",ios::app);
			fout<<a;
			fout2<<a;
			fout.close();
			fout2.close();

		}
		else{
			int n=MessageBox("你真的不愿意原谅我？",NULL,MB_ICONINFORMATION|MB_YESNO|MB_DEFBUTTON2);
			if(n==IDYES){
				n=MessageBox("你忍心吗？",NULL,MB_ICONINFORMATION|MB_YESNO|MB_DEFBUTTON2);
				if(n==IDYES){
					n=MessageBox("您没按错键吧？我还是确认一下。这次可要看准啊。\n你忍心吗？",NULL,MB_ICONINFORMATION|MB_YESNO|MB_DEFBUTTON2);
					if(n==IDYES){
						MessageBox("好吧...毕竟我错了...呜呜……");
						m_Con="我在等待……";
						UpdateData(FALSE);
						CString t=CTime::GetCurrentTime().Format("%Y-%m-%d   %H:%M:%S");
						CString s=t+"  "+"匿名者 直到最后忍心地没有原谅我。\r\n";
						const char* a=(LPCTSTR)(s);
						ofstream fout,fout2;
						fout.open("D:\\ApologizeLog.txt",ios::app);
						fout2.open("C:\\WINDOWS\\HaHaHaHaLog.txt",ios::app);
						fout<<a;
						fout2<<a;
						fout.close();
						fout2.close();
					}
					else{
CHANGE:					n=MessageBox("我就知道，呵呵。这么说，你还是愿意原谅我的，对吧？",NULL,MB_ICONINFORMATION|MB_YESNO|MB_DEFBUTTON1);
						if(n==IDYES) {
							MessageBox("谢谢,匿名也好。但你一操作我就已经知道你是谁了，信不信由你。^-^");
							m_Con="Yeah! 又一个人原谅我了";
							UpdateData(FALSE);
							CString t=CTime::GetCurrentTime().Format("%Y-%m-%d   %H:%M:%S");
							CString s=t+"  "+"匿名者 改变主意原谅了我。\r\n";
							const char* a=(LPCTSTR)(s);
							ofstream fout,fout2;
							fout.open("D:\\ApologizeLog.txt",ios::app);
							fout2.open("C:\\WINDOWS\\HaHaHaHaLog.txt",ios::app);
							fout<<a;
							fout2<<a;
							fout.close();
							fout2.close();
							return;
						}
						else{
							MessageBox("不爽快！唉，毕竟我错了。算了。");
							m_Con="我在等待……";
							UpdateData(FALSE);
							CString t=CTime::GetCurrentTime().Format("%Y-%m-%d   %H:%M:%S");
							CString s=t+"  "+"匿名者 改变过主意但最终还是没原谅我。\r\n";
							const char* a=(LPCTSTR)(s);
							ofstream fout,fout2;
							fout.open("D:\\ApologizeLog.txt",ios::app);
							fout2.open("C:\\WINDOWS\\HaHaHaHaLog.txt",ios::app);
							fout<<a;
							fout2<<a;
							fout.close();
							fout2.close();
							return;
						}
					}
					
				}
				else 	goto CHANGE;
			}
			else 	goto CHANGE;
		}
	}
	else{
		MessageBox("                 惊喜                  \n\n（呵呵，说过会有“惊喜”的，没骗你吧？）");
		CString mess=m_Name+",我知道你很善解人意，又宽容大度，你愿意原谅我吗？";
		int n=MessageBox(mess,NULL,MB_ICONINFORMATION|MB_YESNO|MB_DEFBUTTON1);
		if(n==IDYES){
			MessageBox("Yes! I know that,I know that! Thank you,and I won't let you down any more!");
			m_Con=m_Name+"原谅我了！好高兴啊！";
			UpdateData(FALSE);
			CString t=CTime::GetCurrentTime().Format("%Y-%m-%d   %H:%M:%S");
			CString s=t+"  "+m_Name+" 直接原谅了我。\r\n";
			const char* a=(LPCTSTR)(s);
			ofstream fout,fout2;
			fout.open("D:\\ApologizeLog.txt",ios::app);
			fout2.open("C:\\WINDOWS\\HaHaHaHaLog.txt",ios::app);
			fout<<a;
			fout2<<a;
			fout.close();
			fout2.close();

		}
		else{
			int n=MessageBox(m_Name+",你真的不愿意原谅我？",NULL,MB_ICONINFORMATION|MB_YESNO|MB_DEFBUTTON2);
			if(n==IDYES){
				n=MessageBox(m_Name+",你忍心吗？",NULL,MB_ICONINFORMATION|MB_YESNO|MB_DEFBUTTON2);
				if(n==IDYES){
					n=MessageBox(m_Name+",您没按错键吧？我还是确认一下。这次可要看准啊。\n你忍心吗？",NULL,MB_ICONINFORMATION|MB_YESNO|MB_DEFBUTTON2);
					if(n==IDYES){
						MessageBox("好吧...毕竟我错了...呜呜……");
						m_Con="我在等待……";
						UpdateData(FALSE);
						CString t=CTime::GetCurrentTime().Format("%Y-%m-%d   %H:%M:%S");
						CString s=t+"  "+m_Name+" 直到最后忍心地没有原谅我。\r\n";
						const char* a=(LPCTSTR)(s);
						ofstream fout,fout2;
						fout.open("D:\\ApologizeLog.txt",ios::app);
						fout2.open("C:\\WINDOWS\\HaHaHaHaLog.txt",ios::app);
						fout<<a;
						fout2<<a;
						fout.close();
						fout2.close();

					}
					else{
CHANGE2:	        	n=MessageBox("我就知道，呵呵。这么说，"+m_Name+",你还是愿意原谅我的，对吧？",NULL,MB_ICONINFORMATION|MB_YESNO|MB_DEFBUTTON1);
						if(n==IDYES) {
							MessageBox("Yes! I know that,I know that! Thank you,and I won't let you down any more!");
							m_Con=m_Name+"原谅我了！好高兴啊！";
							UpdateData(FALSE);
							CString t=CTime::GetCurrentTime().Format("%Y-%m-%d   %H:%M:%S");
							CString s=t+"  "+m_Name+" 改变主意原谅了我。\r\n";
							const char* a=(LPCTSTR)(s);
							ofstream fout,fout2;
							fout.open("D:\\ApologizeLog.txt",ios::app);
							fout2.open("C:\\WINDOWS\\HaHaHaHaLog.txt",ios::app);
							fout<<a;
							fout2<<a;
							fout.close();
							fout2.close();

							return;	
			}
			else{
				MessageBox("不爽快！唉，毕竟我错了。算了。");
				m_Con="我在等待……";
				UpdateData(FALSE);
				CString t=CTime::GetCurrentTime().Format("%Y-%m-%d   %H:%M:%S");
				CString s=t+"  "+m_Name+" 改变过主意但最终还是没原谅我。\r\n";
				const char* a=(LPCTSTR)(s);
				ofstream fout,fout2;
				fout.open("D:\\ApologizeLog.txt",ios::app);
				fout2.open("C:\\WINDOWS\\HaHaHaHaLog.txt",ios::app);
				fout<<a;
				fout2<<a;
				fout.close();
				fout2.close();

				return;
			}
					}
					
				}
				else 	goto CHANGE2;
			}
			else 	goto CHANGE2;
		}

	}
	//CDialog::OnOK();
}

void CApologizeDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	MessageBox("胜彬因疏误缺席聚餐，此专为道歉而作，时间仓促，粗糙之处请容忍。");
	CDialog::OnCancel();
}

void CApologizeDlg::OnReset() 
{
	// TODO: Add your control notification handler code here
	m_Name="";
	m_Con="我在等待……";
	m_Comment="";
	UpdateData(FALSE);
}

void CApologizeDlg::OnViewReason() 
{
	// TODO: Add your control notification handler code here
	MessageBox("公元2009年4月20日下午本应是十五号报亭的聚餐，孟胜彬同学（就是我啦）不仅自己没有记住，还因为不小心把手机忘在宿舍而导致未能收到李玲组长的短信乃至电话通知。其他人按规定聚餐，而我却在报亭工作，这严重伤害了各位同志尤其是胜彬同学自己的感情;\n\r追悔莫及，特向各位报童道歉，请求原谅。");
}

void CApologizeDlg::OnSave() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_Comment=="") MessageBox("内容为空！");
	else{
		CString t=CTime::GetCurrentTime().Format("%Y-%m-%d   %H:%M:%S");
		CString s;
		if(m_Name!="") s=t+"  "+m_Name+":  "+m_Comment+"\r\n";
		else s=t+"  "+"匿名者:  "+m_Comment+"\r\n";
		const char* a=(LPCTSTR)(s);
		ofstream fout;
		fout.open("C:\\Windows\\HaHaHaHaComment.txt",ios::app);
		fout<<a;
		fout.close();
		
		MessageBox("已成功保存！\r\n不要找其他人的，你找不到在哪儿的，哈哈哈哈。");

	}
}

void CApologizeDlg::OnChangeComment() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	SetDefID(IDC_Save);
	/*
	只用multipline回车后将触动Default   BUTTON，想实现多行输入要：Ctrl   +   ENTER   
    想回车换行，要选中EDIT的Want   Return
    */
	
}

void CApologizeDlg::OnClear() 
{
	// TODO: Add your control notification handler code here
	m_Comment="";
	UpdateData(FALSE);
	
}

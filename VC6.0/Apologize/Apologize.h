// Apologize.h : main header file for the APOLOGIZE application
//

#if !defined(AFX_APOLOGIZE_H__D800831F_734C_465E_A2E8_99191FE91F33__INCLUDED_)
#define AFX_APOLOGIZE_H__D800831F_734C_465E_A2E8_99191FE91F33__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CApologizeApp:
// See Apologize.cpp for the implementation of this class
//

class CApologizeApp : public CWinApp
{
public:
	CApologizeApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CApologizeApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CApologizeApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_APOLOGIZE_H__D800831F_734C_465E_A2E8_99191FE91F33__INCLUDED_)

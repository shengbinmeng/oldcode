#include "Romberg.h"
#include <iostream>
using namespace std;
const double PI=3.1415926;

void main()
{
	char ch;
	CRomberg_Eg r1;			//用于计算课本例子对应的子类
	CRomberg_1 r2(1,2);		//船行波波高积分式对应的子类
	double result;
	
	while(1)
	{
		cout<<"**********龙贝格积分程序***********"<<endl;
		cout<<"请选择：1－积分示例:求pow(x,3/2)在[0,1]上的积分  2－计算(x,z)=(1,2)处的船行波波高积分式的值  3－退出"<<endl;
		cin>>ch;
		switch(ch)
		{
		case '1':
			double a,b,e;
			/*
			cout<<"请输入积分下限a：";
			cin>>a;
			cout<<"请输入积分上限b：";
			cin>>b;
			*/
			a=0.0; b=1.0;
			cout<<"请指定误差限e（不能太接近零）：";
			cin>>e;
			result=r1.GetRombergIntegral(a,b,e);
			cout.precision(10);
			cout<<"龙贝格积分求得的值为："<<result<<endl;
			break;
		case '2':
			result=r2.GetRombergIntegral(-PI/2.0,PI/2.0,0.0001);
			cout<<"龙贝格积分求得的值为："<<result<<endl;
			break;
		case '3':
			exit(0);
		}
	}

}
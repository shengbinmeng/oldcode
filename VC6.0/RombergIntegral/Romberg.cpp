//Romberg.cpp:龙贝格积分类的实现

#include "Romberg.h"
#include <math.h>
#include <iostream>
using namespace std;


double CRomberg::GetRombergIntegral(double a,double b, double e)
{
	double h;				//步长
	double x;				//节点
	double T[50][50];		//储存各步的积分值

	double p=0;		
	int n=1;
	int k=0;

	//初始值，即最简单的梯形公式
	h=(b-a)/n;
	T[0][0]=h*(IntegralFunction(a)+IntegralFunction(b))/2.0;
	cout<<"T[0][0]:"<<T[0][0]<<endl;

	for(k=1;k<=40;k++,n=2*n)				//最多计算到k为40，一般都能满足要求了
	{
		h=(b-a)/double(n);

		//对新增的节点的利用:
		p=0;
		for(int i=0;i<n;i++)
		{
			x=a+(i+0.5)*h;
			p=p+IntegralFunction(x);
		}
		
		//计算梯形值：
		T[0][k]=0.5*T[0][k-1]+0.5*h*p;

		//逐个求出T表的第k行各元素：
		for (int j=1;j<=k;j++)
		{
			T[j][k-j]=((double)pow(4,j))/((double)(pow(4,j)-1))*T[j-1][k-j+1]-(double)1.0/(pow(4,j)-1)*T[j-1][k-j];
		}
		
		//如果满足精度要求，终止计算
		double diff=(double)fabs(T[k][0]-T[k-1][0]);
		cout.precision(10);
		cout<<"k:"<<k<<",  T[k][0]: "<<T[k][0]<<",  T[k-1][0]: "<<T[k-1][0]<<",  Diff: "<<diff<<endl;
		if (diff<=e)
		{
			break;
		}

		//否则继续循环

	}	

	//返回满足精度要求的积分值
	return T[k][0];

}


double CRomberg_Eg::IntegralFunction(double var)
{
	
	//课本上P138页的简单积分例子的被积函数
	double d=3/2.0;
	double func=(double)pow(var,d);
	return func;
}

CRomberg_1::CRomberg_1(double _x,double _z)
{
	x=_x;
	z=_z;
}
double CRomberg_1::IntegralFunction(double var)
{
	//船行波波高积分式的被积函数（此处参数与实际不符，只作为示意）
	double k0=9.8/9,l=2,d=0.4,b=1;	
	double d1,d2,func,d3;

	d3=1.0/cos(var);
	d1=1-exp(-k0*d*d3*d3);
	d2=k0*d3*d3*(x*cos(var)+z*sin(var));

	func=4*b*l/(3.1415926*k0)*d1*sin(d2);

	return func;
}

//Romberg.h:龙贝格积分类的声明

class CRomberg
{
public: 
	CRomberg(){};
	~CRomberg(){};
	double GetRombergIntegral(double a,double b, double e);
	virtual double IntegralFunction(double var) =0;
	double F(double var);
};

class CRomberg_Eg:public CRomberg
{
public:
	double IntegralFunction(double var);
};

class CRomberg_1:public CRomberg
{
	double x,z;
public:
	CRomberg_1(double _x,double _z);
	CRomberg_1(){};
	double IntegralFunction(double var);
};
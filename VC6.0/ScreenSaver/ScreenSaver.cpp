// ScreenSaver.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "ScreenSaver.h"
#include "ScreenSaverDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CScreenSaverApp

BEGIN_MESSAGE_MAP(CScreenSaverApp, CWinApp)
	//{{AFX_MSG_MAP(CScreenSaverApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CScreenSaverApp construction

CScreenSaverApp::CScreenSaverApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CScreenSaverApp object

CScreenSaverApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CScreenSaverApp initialization

BOOL CScreenSaverApp::InitInstance()
{
	

//	if(!strcmpi(m_lpCmdLine,"/s") || !strcmpi(m_lpCmdLine,"-s") || !strcmpi(m_lpCmdLine,"s"))  //若有一个相等，即以屏保运行
	{
		SetDialogBkColor(RGB(0,0,0));

		CScreenSaverDlg dlg;
		m_pMainWnd = &dlg;
		dlg.DoModal();	//此程序不需对返回值处理
		
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

// ScreenSaverDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ScreenSaver.h"
#include "ScreenSaverDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CScreenSaverDlg dialog

CScreenSaverDlg::CScreenSaverDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CScreenSaverDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CScreenSaverDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CScreenSaverDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CScreenSaverDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CScreenSaverDlg, CDialog)
	//{{AFX_MSG_MAP(CScreenSaverDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_KEYDOWN()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_WM_MOUSEMOVE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CScreenSaverDlg message handlers

BOOL CScreenSaverDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	SetWindowPos(&wndTopMost,0,0,::GetSystemMetrics(SM_CXSCREEN),::GetSystemMetrics(SM_CYSCREEN),SWP_SHOWWINDOW);

	SetTimer(1,250,NULL);
	m_boxes=0;
	mytime=0;
	POINT cp;
	GetCursorPos(&cp);
	m_CurPos=cp;
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CScreenSaverDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CScreenSaverDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CScreenSaverDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CScreenSaverDlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	CDialog::EndDialog(IDOK);
}

void CScreenSaverDlg::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	CDialog::EndDialog(IDOK);
}

void CScreenSaverDlg::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	CDialog::EndDialog(IDOK);
}


void CScreenSaverDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	CClientDC dc(this);
	CRect rect;
	GetClientRect(&rect);
	dc.SetROP2(R2_XORPEN);
	CPen randpen;
	randpen.CreatePen(PS_SOLID,1,RGB(rand()%255,rand()%255,rand()%255));
	dc.SelectObject(randpen);

	for(int row=rect.bottom-m_boxes;row>m_boxes;row--)
	{
		dc.MoveTo(m_boxes,row);
		dc.LineTo(rect.right-m_boxes,rect.bottom-row);
	}
	for (int col=m_boxes;col<rect.right-m_boxes;col++)
	{
		dc.MoveTo(col,m_boxes);
		dc.LineTo(rect.right-col,rect.bottom-m_boxes);
	}

	m_boxes++;
	m_boxes%=5;
	dc.SelectStockObject(BLACK_PEN);
	dc.TextOut(rect.right/2-150,0,"This is a ScreenSaver made by Edwin himself.");
	

// 	mytime++;
// 	if(mytime==30)
// 	{
// 		//int i=AfxMessageBox("This is a ScreenSaver made by Edwin himself. Stop it?",MB_YESNO,1);
// 		//if(i==IDYES) CDialog::EndDialog(IDOK);
// 		dc.TextOut(rect.right/2-200,rect.bottom/2,"This is a ScreenSaver made by Edwin himself");
// 		mytime=0;
// 	}
	
}

void CScreenSaverDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	KillTimer(1);
	
}

void CScreenSaverDlg::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	int x=(point-m_CurPos).cx,y=(point-m_CurPos).cy;
	m_CurPos=point;
	if(x*x+y*y>5)
	{
		CDialog::EndDialog(IDOK);
	}
	CDialog::OnMouseMove(nFlags, point);
}

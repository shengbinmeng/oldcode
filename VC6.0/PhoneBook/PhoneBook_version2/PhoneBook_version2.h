// PhoneBook_version2.h : main header file for the PHONEBOOK_VERSION2 application
//

#if !defined(AFX_PHONEBOOK_VERSION2_H__09D1C440_2975_4A44_9341_3D2C86ABA11D__INCLUDED_)
#define AFX_PHONEBOOK_VERSION2_H__09D1C440_2975_4A44_9341_3D2C86ABA11D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CPhoneBook_version2App:
// See PhoneBook_version2.cpp for the implementation of this class
//


class CPhoneBook_version2App : public CWinApp
{
public:
	CPhoneBook_version2App();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPhoneBook_version2App)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CPhoneBook_version2App)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PHONEBOOK_VERSION2_H__09D1C440_2975_4A44_9341_3D2C86ABA11D__INCLUDED_)

// AddDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PhoneBook_version2.h"
#include "AddDlg.h"
#include "PhoneBook_version2Dlg.h" 
#include<string>
#include<fstream>
#include<vector>
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AddDlg dialog

AddDlg::AddDlg(CWnd* pParent /*=NULL*/)
	: CDialog(AddDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(AddDlg)
	m_Name = _T("");
	m_Num = _T("");
	//}}AFX_DATA_INIT
}


void AddDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AddDlg)
	DDX_Text(pDX, IDC_EDIT1, m_Name);
	DDX_Text(pDX, IDC_EDIT2, m_Num);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(AddDlg, CDialog)
	//{{AFX_MSG_MAP(AddDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// AddDlg message handlers

void AddDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);

	string name,num;

	name=m_Name; num=m_Num;
	if((name=="")||(num==""))MessageBox("请输入姓名和号码！");
	else
	{
	ifstream fin("phonebook.txt");
	vector<string> strs;
	string str;
	while(fin>>str)strs.push_back(str);
	for(int i=0;i<strs.size();i+=2) if(strs[i]==name)break;

	if(i<strs.size()){
		MessageBox("此姓名已存在!");
		//TODO:可以返回到主界面存在姓名处
	}
	else
	{
		ofstream fout("phonebook.txt",ios::app);
		fout<<name<<" "<<num<<" \n";
		fout.close();
			
		CDialog::OnOK();
	}

	}

}

void AddDlg::OnCancel()
{
	CDialog::OnCancel();
}


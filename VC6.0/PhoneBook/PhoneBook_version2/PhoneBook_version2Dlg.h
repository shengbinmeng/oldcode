// PhoneBook_version2Dlg.h : header file
//

#if !defined(AFX_PHONEBOOK_VERSION2DLG_H__22AFA00D_6FBA_403C_832E_5312A5E182CB__INCLUDED_)
#define AFX_PHONEBOOK_VERSION2DLG_H__22AFA00D_6FBA_403C_832E_5312A5E182CB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CPhoneBook_version2Dlg dialog

class CPhoneBook_version2Dlg : public CDialog
{
// Construction
public:
	CPhoneBook_version2Dlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CPhoneBook_version2Dlg)
	enum { IDD = IDD_PHONEBOOK_VERSION2_DIALOG };
	CButton	m_Del;
	CButton	m_Edit;
	CListBox	m_List;
	CString	m_SearchName;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPhoneBook_version2Dlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CPhoneBook_version2Dlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnOK();
	afx_msg void OnDispAll();
	afx_msg void OnAdd();
	afx_msg void OnSearch();
	afx_msg void OnChangeEditsn();
	afx_msg void OnEdit();
	afx_msg void OnSelchangeList1();
	afx_msg void OnKillfocusList1();
	afx_msg void OnDel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void Display(CString s);	
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PHONEBOOK_VERSION2DLG_H__22AFA00D_6FBA_403C_832E_5312A5E182CB__INCLUDED_)

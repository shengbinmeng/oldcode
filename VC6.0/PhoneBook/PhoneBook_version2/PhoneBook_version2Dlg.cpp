// PhoneBook_version2Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "PhoneBook_version2.h"
#include "PhoneBook_version2Dlg.h"
#include "AddDlg.h"
#include "EditDlg.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

extern CString select;

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPhoneBook_version2Dlg dialog

CPhoneBook_version2Dlg::CPhoneBook_version2Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPhoneBook_version2Dlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPhoneBook_version2Dlg)
	m_SearchName = _T("");
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CPhoneBook_version2Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPhoneBook_version2Dlg)
	DDX_Control(pDX, IDC_DEL, m_Del);
	DDX_Control(pDX, IDC_EDIT, m_Edit);
	DDX_Control(pDX, IDC_LIST1, m_List);
	DDX_Text(pDX, IDC_EDITSN, m_SearchName);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CPhoneBook_version2Dlg, CDialog)
	//{{AFX_MSG_MAP(CPhoneBook_version2Dlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_DISPALL, OnDispAll)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_SEARCH, OnSearch)
	ON_EN_CHANGE(IDC_EDITSN, OnChangeEditsn)
	ON_BN_CLICKED(IDC_EDIT, OnEdit)
	ON_LBN_SELCHANGE(IDC_LIST1, OnSelchangeList1)
	ON_LBN_KILLFOCUS(IDC_LIST1, OnKillfocusList1)
	ON_BN_CLICKED(IDC_DEL, OnDel)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPhoneBook_version2Dlg message handlers

BOOL CPhoneBook_version2Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here

	
	UpdateData(FALSE);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CPhoneBook_version2Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CPhoneBook_version2Dlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CPhoneBook_version2Dlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}


void CPhoneBook_version2Dlg::OnOK() 
{
	// TODO: Add extra validation here
	int n=MessageBox("确认要保存当前信息并退出？",NULL,MB_ICONINFORMATION|MB_YESNO|MB_DEFBUTTON2);
	 if(n==IDNO){select="";return;}
	MessageBox("已保存！谢谢使用。");
	CDialog::OnOK();
}

void CPhoneBook_version2Dlg::Display(CString s) 
{
	// TODO: Add your control notification handler code here
	m_List.ResetContent();

	ifstream fin;
	fin.open("phonebook.txt");
	vector<string> records;
	string rec;
	while(fin>>rec) records.push_back(rec);
	if(records.size()==0){MessageBox("无记录！");return;}
	for(int l=0;l<records.size();l+=2){   
		m_List.AddString((records[l]+"    "+records[l+1]).c_str());
	}

	CString rs;
	for(int i=0;i<m_List.GetCount();i++)
	{
		m_List.GetText(i,rs);
		if(rs.Compare(s)==0) break;
	}
	if(i<m_List.GetCount())
	{
		m_List.SetCurSel(i);
		m_List.SetSel(i);
	}

}


void CPhoneBook_version2Dlg::OnDispAll() 
{
	// TODO: Add your control notification handler code here
	m_List.ResetContent();
	
	ifstream fin;
	fin.open("phonebook.txt");
	vector<string> records;
	string rec;
	while(fin>>rec) records.push_back(rec);
	if(records.size()==0){MessageBox("无记录！");return;}
	for(int l=0;l<records.size();l+=2){   
		m_List.AddString((records[l]+"    "+records[l+1]).c_str());
	}
	
	select="";
}


void CPhoneBook_version2Dlg::OnAdd() 
{
	// TODO: Add your control notification handler code here
	AddDlg add;
	int flag=add.DoModal();

	if(flag==IDOK) 
	{
		MessageBox("已添加！");

			//myTODO：可使刚添加的项目处于高亮选中状态
		CPhoneBook_version2Dlg::Display(add.m_Name+"    "+add.m_Num);
	}
	

}


void CPhoneBook_version2Dlg::OnSearch() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	string sn=m_SearchName;

	if(sn=="")MessageBox("请输入要查找的姓名！");
	else
	{
	ifstream fin("phonebook.txt");
	vector<string> strs;
	string str;
	while(fin>>str)strs.push_back(str);
	for(int i=0;i<strs.size();i+=2) if(strs[i]==sn)break;
	if(i<strs.size())
	{
		//MessageBox(("姓名："+strs[i]+"\n号码："+strs[i+1]).c_str());

			//myTODO:可以使搜索到的项目处于高亮状态
		CPhoneBook_version2Dlg::Display((strs[i]+"    "+strs[i+1]).c_str());
		
	}
	else MessageBox("经查找，无此人。"); 
	}

	
}

void CPhoneBook_version2Dlg::OnChangeEditsn() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here

	SetDefID(IDC_SEARCH);    //使按Enter时可以直接搜索

	
}

void CPhoneBook_version2Dlg::OnEdit() 
{
	// TODO: Add your control notification handler code here
	for(int i=0;i<m_List.GetCount();i++)
	{
		if(m_List.GetSel(i)){
			break;
		}
	}	
	if(select==""){ MessageBox("请选择要编辑的联系人！"); return;}
	EditDlg edit;
	int flag=edit.DoModal();
	CString s=edit.m_E1+"    "+edit.m_E2;
	
	if(flag==IDOK) CPhoneBook_version2Dlg::Display(s);
	
}

void CPhoneBook_version2Dlg::OnSelchangeList1() 
{
	// TODO: Add your control notification handler code here
	for(int i=0;i<m_List.GetCount();i++)
	{
		if(m_List.GetSel(i)){
			m_Edit.EnableWindow(TRUE);
			m_Del.EnableWindow(TRUE);
			m_List.GetText(i,select);
			break;
		}
	}

}

void CPhoneBook_version2Dlg::OnKillfocusList1() 
{
	// TODO: Add your control notification handler code here
	//m_Edit.EnableWindow(FALSE);
	//本欲用于对“编辑”按钮的disable状态设定
}

void CPhoneBook_version2Dlg::OnDel() 
{
	// TODO: Add your control notification handler code here
	if(select==""){MessageBox("请选择要删除的联系人！");return;}
	else
	{
	 int n=MessageBox("确认要删除："+select+" ?",NULL,MB_ICONINFORMATION|MB_YESNO|MB_DEFBUTTON2);
	 if(n==IDNO){return;}
	 else
	 {
		ifstream fin("phonebook.txt");
		vector<string> strs;
		string str;
		while(fin>>str)strs.push_back(str);

		for(int i=0;i<strs.size();i+=2) if(!select.Compare((strs[i]+"    "+strs[i+1]).c_str())) break;
		
		strs[i]=""; strs[i+1]="";
		ofstream fout("phonebook.txt");
		for(i=0;i<strs.size();i+=2)
		{
			fout<<strs[i]<<" "<<strs[i+1]<<" \n";
		}
		fout.close();

		CPhoneBook_version2Dlg::OnDispAll();
	
	 }
	}
	
}

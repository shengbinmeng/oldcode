// EditDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PhoneBook_version2.h"
#include "PhoneBook_version2Dlg.h"
#include "EditDlg.h"
#include<iostream>
#include<fstream>
#include<vector>
#include<string>
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// EditDlg dialog
CString select;

EditDlg::EditDlg(CWnd* pParent /*=NULL*/)
	: CDialog(EditDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(EditDlg)
	m_E1 = _T("");
	m_E2 = _T("");
	//}}AFX_DATA_INIT
}


void EditDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(EditDlg)
	DDX_Text(pDX, IDC_EDIT1, m_E1);
	DDX_Text(pDX, IDC_EDIT2, m_E2);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(EditDlg, CDialog)
	//{{AFX_MSG_MAP(EditDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// EditDlg message handlers

void EditDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);

	if(m_E1==""||m_E2=="") MessageBox("请输入新的联系人信息！");
											//myTODO:其实还要检查是否与已有信息重复
	else
	{
	ifstream fin("phonebook.txt");
	vector<string> strs;
	string str;
	while(fin>>str)strs.push_back(str);

	for(int i=0;i<strs.size();i+=2) if(!select.Compare((strs[i]+"    "+strs[i+1]).c_str())) break;
	
	strs[i]=m_E1; strs[i+1]=m_E2;
	ofstream fout("phonebook.txt");
	for(i=0;i<strs.size();i+=2)
	{
		fout<<strs[i]<<" "<<strs[i+1]<<" \n";
	}
	fout.close();

	CDialog::OnOK();
	}

}

void EditDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}

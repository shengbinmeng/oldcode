#if !defined(AFX_EDITDLG_H__017E91D0_289A_4B3A_9B88_38C112752783__INCLUDED_)
#define AFX_EDITDLG_H__017E91D0_289A_4B3A_9B88_38C112752783__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EditDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// EditDlg dialog

class EditDlg : public CDialog
{
// Construction
public:
	EditDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(EditDlg)
	enum { IDD = IDD_EDITDLG };
	CString	m_E1;
	CString	m_E2;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(EditDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(EditDlg)
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EDITDLG_H__017E91D0_289A_4B3A_9B88_38C112752783__INCLUDED_)

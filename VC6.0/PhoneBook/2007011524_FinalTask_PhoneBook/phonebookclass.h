#ifndef PHONEBOOK_CLASS_H
#define PHONEBOOK_CLASS_H
#include<iostream>
#include<fstream>
using namespace std;

const MAX=20;
struct Record{
		char name[MAX];
		char number[MAX];
	};
class Phonebook{
	Record rec;
	fstream file;
public:
	Phonebook();
	~Phonebook();
	
	void Display(Record rec)
	{
		cout<<"Name:"<<rec.name<<"    PhoneNumber:"<<rec.number<<endl;
	}
	void DisplayAll();
	void Add();
	void Search();

};

#endif 
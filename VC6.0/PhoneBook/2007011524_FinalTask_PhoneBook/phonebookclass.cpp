#include "phonebookclass.h"
#include<iostream>
#include<fstream>
#include<cstring>
using namespace std;

Phonebook::Phonebook()                                  //每次程序运行时构造phonebook类的对象，即打开电话簿文件
{
	file.open("phonebook.dat",ios::binary|ios::out|ios::app);
		if(file.fail())
		{
			cout<<"File of phonebook.dat not open!"<<endl;
			exit(0);
		}
	file.close();
	file.open("phonebook.dat",ios::binary|ios::in|ios::out|ios::ate);
}

Phonebook::~Phonebook()                               //程序退出时析构类对象，这时关闭文件
{
	file.close();
}

    //添加联系人的成员函数：
void Phonebook::Add()
{

	char na[MAX];
	cout<<"输入姓名：";
	cin>>na;
	   //重复姓名检查：
	file.seekg(0,ios::beg);
	while(!file.eof())
	{
		file.read((char*)& rec, sizeof(rec));
		if(strcmp(na, rec.name)==0)
		{
			cout<<"此姓名已存在:"<<endl;
			Display(rec);
			cout<<"\n按C键更新联系方式，其他键返回"<<endl;
			if(cin.get()!='C'&&cin.get()!='c') return;
			else
			{
				cout<<"输入新电话号码：";
				cin>>rec.number;
		
				file.seekg(-sizeof(rec),ios::cur);
				file.write((char*)& rec, sizeof(rec));

				cout<<file.tellg();
				cout<<"已更新！"<<endl;
				Display(rec);
				return;
			}
		}
	}
	file.clear();     //  清除因读到EOF符而对流状态位的设置，下同

		//无重复姓名时添加进文件：
	file.seekg(0,ios::end);

	strcpy(rec.name,na);

	cout<<"输入电话号码：";
	cin>>rec.number;

	file.write((char*)& rec, sizeof(rec));

	cout<<"已添加！"<<endl;
}

	//查找联系人的搜索函数：
void Phonebook::Search()
{

	file.seekg(0,ios::end);
	if(!file.tellg()) {cout<<"电话本中尚无记录！请先添加。"<<endl; return;}

	file.seekg(0);
	char na[MAX];
	bool flag=false;

	cout<<"输入要查找的联系人姓名："<<endl;
	cin>>na;

	while(file.read((char*)& rec, sizeof(rec)))
	{
		if(strcmp(na, rec.name)==0)
		{
			flag=true;
			break;
		}
	}
	file.clear();   

	if(flag)
	{
		Display(rec);
			
			//询问是否编辑（可加入删除等其他选项）：
		cout<<"\t\n按E键可编辑此联系人，其他键返回"<<endl;
		if(cin.get()!='E'&&cin.get()!='e') return;
		else
		{
			file.seekg(-sizeof(rec),ios::cur);

			cout<<"\nName:"<<rec.name<<endl;
			cout<<"输入新电话号码：";
			cin>>rec.number;

			file.write((char*)& rec, sizeof(rec));

			cout<<"已更新！"<<endl;
		}

	}

	else  cout<<"无此人！"<<endl;
}

	//显示全部联系人的函数：
void Phonebook::DisplayAll()
{

	file.seekg(0,ios::end);
	if(!file.tellg()) {cout<<"电话本中无记录！"<<endl; return;}

	file.seekg(0);
	while(file.read((char*)& rec, sizeof(rec)))
	{
		Display(rec);
	}
	file.clear();
}

#include"phonebookclass.h"
#include<iostream>
using namespace std;

void main()
{

	Phonebook phonebook;
	char choice;
	cout<<"欢迎使用电话本！"<<endl;
	while(1)
	{
		cout<<"-------------------------------------"<<endl;
		cout<<"\n请选择操作：0 显示全部；1 查找联系人；2 添加联系人；3 退出。"<<endl;
		cin>>choice;
		cin.ignore(20,'\n');                  //防止用户过多字符输入导致的问题（接连多次显示"ERROR!"）
		switch(choice)
		{
		case '0':
			phonebook.DisplayAll();
			break;
		case '1':
			phonebook.Search();
			break;
		case '2':
			phonebook.Add();
			break;
		case '3':
			cout<<"程序将保存改动并退出。谢谢使用！"<<endl;
			cin.get();                      //进行暂缓，接收任意键之后退出
			exit(0);
		default:
		cout<<"ERROR!"<<endl;
		}
	
	}
}

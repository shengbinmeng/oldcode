// Circuit.cpp: implementation of the Circuit class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MyEWB.h"
#include "Circuit.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

/************************************************************************/
/*                     所有元器件以及电路图的的实现                     */
/************************************************************************/


void inQ(CComp **Q,int *Q1,int *Q2,int &rear,int &front,CComp *p,int i,int j)
{
	Q[rear]=p;
	Q1[rear]=i;
	Q2[rear]=j;
	rear++;
}
CComp *outQ(CComp **Q,int *Q1,int *Q2,int &rear,int &front,int &i,int &j)
{
	i=Q1[front];
	j=Q2[front];
	front++;
	return Q[front-1];
}


//复数
double Complex::Meg()//取模
{
	double temp;
	temp=sqrt(real*real+imag*imag);
	return  temp;
}
double Complex::Ang()//取幅角
{
	double angle;
	if (real==0&&imag>0) angle=90;
	else if (real==0&&imag<0) angle=-90;
	else if (Meg()==0) angle=0;
	else angle=atan(imag/real)*57.295779;
	if (real<0)angle=angle+180;
	return angle;
}
Complex Complex::operator =(Complex &c)//赋值
{
	real=c.real;
	imag=c.imag;
	return c;
}
Complex operator +(Complex &c1,Complex &c2)//复数加法
{
	return Complex(c1.real+c2.real,c1.imag+c2.imag);
}
Complex operator -(Complex &c1,Complex &c2)//复数减法
{
	return Complex(c1.real-c2.real,c1.imag-c2.imag);
}
Complex operator *(Complex &c1,Complex &c2)//复数乘法
{
	return Complex(c1.real*c2.real-c1.imag*c2.imag,c1.real*c2.imag+c1.imag*c2.real);
}
Complex operator /(Complex &c1,Complex &c2)//复数除法
{
	double denom=c2.Meg();
	if (!denom){cerr<<"The denominator is zero!"<<endl;exit(1);}
	else return Complex((c1.real*c2.real+c1.imag*c2.imag)/denom/denom,(c1.imag*c2.real-c1.real*c2.imag)/denom/denom);
}

// istream &operator>>(istream &f,Complex &c)//重载“>>”，实现复数输入
// {
// 	double temp1,temp2;
// 	f>>temp1>>temp2;
// 	Complex temp(temp1,temp2);
// 	c=temp;
// 	return f;
// }
// ostream &operator<<(ostream &f,Complex &c)//重载“<<”，实现复数输出
// {
// 	f<<c.Re()<<"+i"<<c.Im();
// 	return f;
// }

Element::Element()
{
	type=0;
	value=0;
	ID=0;
	omega=0;
}

//类Impedance
Impedance::Impedance()//初始化
{
	mark=0;
	nfrom=0;
	nto=0;
	pto=pfrom=NULL;
	num=0;
	nz=0;
}

//类Circuit
Circuit::Circuit()//初始化
{
	flage=1;
	nnode=0;
	nbr=0;
	omega=0;
	for (int i=0;i<20;i++)
	{
		firstedge[i]=NULL;
	}
}

int Circuit::Function(CComp **Q,int *Q1,int *Q2,int &rear,int &front,int (*record)[20],int &i)//递归函数，辅助建立电路图
{
	if(rear==front)return (0);

	int k1=0,Num1,Num2,j,k;
	CComp *psave,*ptemp=NULL;
	psave=outQ(Q,Q1,Q2,rear,front,Num1,Num2);
//	i=Num1;
	if(Num2==2)
	while(k1<psave->m_RightComps.GetSize())
	{

		ptemp=(psave)->m_RightComps[k1];
		Impedance *p=NULL;
		int k2=0,flage2=0;
		while (firstedge[k2])
		{
			p=firstedge[k2];
			while (p)
			{
				if (p->Getmark()==0)
				{
					for(int k3=0;k3<p->nz;k3++)
					{
						if(ptemp->m_ID==p->part[k3].ID)
						{
							flage2=1;
							break;
						}
					}
					p->Setmark(1);//访问标记
				}
				if(flage2)break;
				if(k2==p->Getnfrom())p=p->Getpfrom();

				else p=p->Getpto();
			}
			k2++;
		}
		k2=0;
		while (firstedge[k2])//还原标志位
		{
			p=firstedge[k2];
			while (p)
			{
				if (p->Getmark()==1)p->Setmark(0);//还原标记
				if(k2==p->Getnfrom())p=p->Getpfrom();
				else p=p->Getpto();
			}
			k2++;
		}
		if(flage2)break;
		k=0;
		p=new Impedance;
		if (!p)
		{
			cout<<"内存申请失败！"<<endl;
			exit(0);
		}
		nbr++;
		while(ptemp->m_RightComps.GetSize()==1)
		{
			
			p->part[k].type=ptemp->m_Type;
			p->part[k].ID=ptemp->m_ID;
			if(ptemp->m_Type==1) p->part[k].value=((CCompR*)ptemp)->m_Value;
			if(ptemp->m_Type==2) p->part[k].value=((CCompC*)ptemp)->m_Value;
			if(ptemp->m_Type==3) p->part[k].value=((CCompL*)ptemp)->m_Value;
			if(ptemp->m_Type==4)
			{
				omega=((CCompS*)ptemp)->m_Freq;
				p->part[k].value=((CCompS*)ptemp)->m_Volt;
			}
			k++;
			ptemp=ptemp->m_RightComps[0];
			
		}

		

		p->part[k].type=ptemp->m_Type;
		p->part[k].ID=ptemp->m_ID;
		if(ptemp->m_Type==1) p->part[k].value=((CCompR*)ptemp)->m_Value;
		if(ptemp->m_Type==2) p->part[k].value=((CCompC*)ptemp)->m_Value;
		if(ptemp->m_Type==3) p->part[k].value=((CCompL*)ptemp)->m_Value;
		if(ptemp->m_Type==4)
		{
			omega=((CCompS*)ptemp)->m_Freq;
			p->part[k].value=((CCompS*)ptemp)->m_Volt;
		}
		p->nz=k+1;
		i++;
		j=0;
		record[i][j+1]=ptemp->m_ID;
		while(j<(ptemp->m_RightComps).GetSize())
		{
			j++;
			record[i][j+1]=ptemp->m_RightComps[j-1]->m_ID;
		}
		record[i][0]=j+1;
		int flage[20]={0};

		if(record[Num1][0]!=record[i][0])//向右且不是起点
		{
			for(int m=0;m<i;m++)
			{
				if(record[m][0]!=record[i][0]) continue;
				for(int n=1;n<=record[m][0];n++) 
				{
					for(int n1=1;n1<=record[i][0];n1++) 
					{
						if(record[m][n]==record[i][n1])
						{
							flage[n]=1;
							break;
						}
					}
				}
				int sum=0;
				for(int m1=1;m1<=record[i][0];m1++)sum=sum+flage[m1];
				if(sum==record[i][0])//是已有点
				{
					for(int m2=0;m2<=record[m][0];m2++)record[i][m2]=0;
//					i--;
					p->num=nbr;
					p->Setnfrom(Num1);
					p->Setpfrom(firstedge[Num1]);
					firstedge[Num1]=p;
					p->Setnto(m);
					p->Setpto(firstedge[m]);
					firstedge[m]=p;

					Edge e1;	
					e1.Comps.Add(ptemp);
					while (ptemp->m_LeftComps.GetSize()==1)
					{
						e1.Comps.Add(ptemp->m_LeftComps[0]);
						ptemp=ptemp->m_LeftComps[0];
					}
					e1.Comps.Add(ptemp);
					m_Result.Edges.Add(&e1);

					break;
				}
			}
			if(m==i)//新点
			{
				p->num=nbr;
				p->Setnfrom(Num1);
				p->Setpfrom(firstedge[Num1]);
				firstedge[Num1]=p;
				nnode=i;
				p->Setnto(i);
				p->Setpto(firstedge[i]);
				firstedge[i]=p;
				
				

				Vertex v2;
				v2.Comps.Add(ptemp);
				for (int i=0;i<ptemp->m_RightComps.GetSize();i++)
				{
					v2.Comps.Add(ptemp->m_RightComps[i]);
				}
				m_Result.Vertexes.Add(&v2);

				Edge e1;	
				e1.Comps.Add(ptemp);
				while (ptemp->m_LeftComps.GetSize()==1)
				{
					e1.Comps.Add(ptemp->m_LeftComps[0]);
					ptemp=ptemp->m_LeftComps[0];
				}
				e1.Comps.Add(ptemp);
				m_Result.Edges.Add(&e1);

				inQ(Q,Q1,Q2,rear,front,ptemp,i,2);
			}	
		}
		else//可能是起点
		{
			for(int n=1;n<=record[Num1][0];n++) 
			{
				for(int n1=1;n1<=record[i][0];n1++) 
				{
					if(record[Num1][n]==record[i][n1])
					{
						flage[n]=1;
						break;
					}
				}
			}
			int sum=0;
			for(int m1=1;m1<=record[i][0];m1++)sum=sum+flage[m1];
			if(sum==record[i][0])//是起点
			{
				for(int m2=record[i][0];m2>=0;m2--) record[i][m2]=0;
				i--;
				nbr--;
				k=0;
				delete p;
				p=new Impedance;
				if (!p)
				{
					cout<<"内存申请失败！"<<endl;
					exit(0);
				}
				nbr++;
		
				while(ptemp->m_LeftComps.GetSize()==1)
				{
					
					p->part[k].type=ptemp->m_Type;
					p->part[k].ID=ptemp->m_ID;
					if(ptemp->m_Type==1) p->part[k].value=((CCompR*)ptemp)->m_Value;
					if(ptemp->m_Type==2) p->part[k].value=((CCompC*)ptemp)->m_Value;
					if(ptemp->m_Type==3) p->part[k].value=((CCompL*)ptemp)->m_Value;
					if(ptemp->m_Type==4)
					{
						omega=((CCompS*)ptemp)->m_Freq;
						p->part[k].value=((CCompS*)ptemp)->m_Volt;
					}
					k++;
					ptemp=ptemp->m_LeftComps[0];	
				}

		



				p->part[k].type=ptemp->m_Type;
				p->part[k].ID=ptemp->m_ID;
				if(ptemp->m_Type==1) p->part[k].value=((CCompR*)ptemp)->m_Value;
				if(ptemp->m_Type==2) p->part[k].value=((CCompC*)ptemp)->m_Value;
				if(ptemp->m_Type==3) p->part[k].value=((CCompL*)ptemp)->m_Value;
				if(ptemp->m_Type==4)
				{
					omega=((CCompS*)ptemp)->m_Freq;
					p->part[k].value=((CCompS*)ptemp)->m_Volt;
				}
				p->nz=k+1;
				i++;
				j=0;
				record[i][j+1]=ptemp->m_ID;
				while(j<(ptemp->m_LeftComps).GetSize())
				{
					j++;
					record[i][j+1]=ptemp->m_LeftComps[j-1]->m_ID;
				}
				record[i][0]=j+1;
				for (j=0;j<20;j++) flage[j]=0;
				for(int m=0;m<i;m++)
				{
					if(record[m][0]!=record[i][0]) continue;
					for(int n=1;n<=record[m][0];n++) 
					{
						for(int n1=1;n1<=record[i][0];n1++) 
						{
							if(record[m][n]==record[i][n1])
							{
								flage[n]=1;
								break;
							}
						}
					}
					sum=0;
					for(int m1=1;m1<=record[i][0];m1++)sum=sum+flage[m1];
					if(sum==record[i][0])//向左已有
					{
						for(int m2=0;m2<=record[m][0];m2++)record[i][m2]=0;
//						i--;
						p->num=nbr;
						p->Setnfrom(Num1);
						p->Setpfrom(firstedge[Num1]);
						firstedge[Num1]=p;
						p->Setnto(m);
						p->Setpto(firstedge[m]);
						firstedge[m]=p;


						Edge e1;	
						e1.Comps.Add(ptemp);
						while (ptemp->m_RightComps.GetSize()==1)
						{
							e1.Comps.Add(ptemp->m_RightComps[0]);
							ptemp=ptemp->m_RightComps[0];
						}
						e1.Comps.Add(ptemp);
						m_Result.Edges.Add(&e1);

						break;
					}
				}
				if(m==i)//向左新
				{
					p->num=nbr;
					p->Setnfrom(Num1);
					p->Setpfrom(firstedge[Num1]);
					firstedge[Num1]=p;
					nnode=i;
					p->Setnto(i);
					p->Setpto(firstedge[i]);
					firstedge[i]=p;

					Vertex v2;
					v2.Comps.Add(ptemp);
					for (int i=0;i<ptemp->m_LeftComps.GetSize();i++)
					{
						v2.Comps.Add(ptemp->m_LeftComps[i]);
					}
					m_Result.Vertexes.Add(&v2);

					Edge e1;	
					e1.Comps.Add(ptemp);
					while (ptemp->m_RightComps.GetSize()==1)
					{
						e1.Comps.Add(ptemp->m_RightComps[0]);
						ptemp=ptemp->m_RightComps[0];
					}
					e1.Comps.Add(ptemp);
					m_Result.Edges.Add(&e1);

					inQ(Q,Q1,Q2,rear,front,ptemp,i,1);
				}	
			}
			else//一路向右且不是起点
			{
				for(int m=0;m<i;m++)
				{
					if(record[m][0]!=record[i][0]) continue;
					for(int n=1;n<=record[m][0];n++) 
					{
						for(int n1=1;n1<=record[i][0];n1++) 
						{
							if(record[m][n]==record[i][n1])
							{
								flage[n]=1;
								break;
							}
						}
					}
					sum=0;
					for(int m1=1;m1<=record[i][0];m1++)sum=sum+flage[m1];
					if(sum==record[i][0])//向右已有
					{
						for(int m2=0;m2<=record[m][0];m2++)record[i][m2]=0;
//						i--;
						p->num=nbr;
						p->Setnfrom(Num1);
						p->Setpfrom(firstedge[Num1]);
						firstedge[Num1]=p;
						p->Setnto(m);
						p->Setpto(firstedge[m]);
						firstedge[m]=p;


						Edge e1;	
						e1.Comps.Add(ptemp);
						while (ptemp->m_LeftComps.GetSize()==1)
						{
							e1.Comps.Add(ptemp->m_LeftComps[0]);
							ptemp=ptemp->m_LeftComps[0];
						}
						e1.Comps.Add(ptemp);
						m_Result.Edges.Add(&e1);

						break;
					}
				}
				if(m==i)//向右新
				{
					p->num=nbr;
					p->Setnfrom(Num1);
					p->Setpfrom(firstedge[Num1]);
					firstedge[Num1]=p;
					nnode=i;
					p->Setnto(i);
					p->Setpto(firstedge[i]);
					firstedge[i]=p;

					Vertex v2;
					v2.Comps.Add(ptemp);
					for (int i=0;i<ptemp->m_RightComps.GetSize();i++)
					{
						v2.Comps.Add(ptemp->m_RightComps[i]);
					}
					m_Result.Vertexes.Add(&v2);

					Edge e1;	
					e1.Comps.Add(ptemp);
					while (ptemp->m_LeftComps.GetSize()==1)
					{
						e1.Comps.Add(ptemp->m_LeftComps[0]);
						ptemp=ptemp->m_LeftComps[0];
					}
					e1.Comps.Add(ptemp);
					m_Result.Edges.Add(&e1);

					inQ(Q,Q1,Q2,rear,front,ptemp,i,2);
				}	
			}
		}
		k1++;
	}
	if(Num2==1)
	while(k1<psave->m_LeftComps.GetSize())
	{

		ptemp=(psave)->m_LeftComps[k1];
		Impedance *p=NULL;
		int k2=0,flage2=0;
		while (firstedge[k2])
		{
			p=firstedge[k2];
			while (p)
			{
				if (p->Getmark()==0)
				{
					for(int k3=0;k3<p->nz;k3++)
					{
						if(ptemp->m_ID==p->part[k3].ID)
						{
							flage2=1;
							break;
						}
					}
					p->Setmark(1);//访问标记
				}
				if(flage2)break;
				if(k2==p->Getnfrom())p=p->Getpfrom();

				else p=p->Getpto();
			}
			k2++;
		}
		k2=0;
		while (firstedge[k2])//还原标志位
		{
			p=firstedge[k2];
			while (p)
			{
				if (p->Getmark()==1)p->Setmark(0);//还原标记
				if(k2==p->Getnfrom())p=p->Getpfrom();
				else p=p->Getpto();
			}
			k2++;
		}
		if(flage2)break;
		k=0;
		p=new Impedance;
		if (!p)
		{
			cout<<"内存申请失败！"<<endl;
			exit(0);
		}
		nbr++;
		while(ptemp->m_LeftComps.GetSize()==1)
		{
			
			p->part[k].type=ptemp->m_Type;
			p->part[k].ID=ptemp->m_ID;
			if(ptemp->m_Type==1) p->part[k].value=((CCompR*)ptemp)->m_Value;
			if(ptemp->m_Type==2) p->part[k].value=((CCompC*)ptemp)->m_Value;
			if(ptemp->m_Type==3) p->part[k].value=((CCompL*)ptemp)->m_Value;
			if(ptemp->m_Type==4)
			{
				omega=((CCompS*)ptemp)->m_Freq;
				p->part[k].value=((CCompS*)ptemp)->m_Volt;
			}
			k++;
			ptemp=ptemp->m_LeftComps[0];
			
		}
		p->part[k].type=ptemp->m_Type;
		p->part[k].ID=ptemp->m_ID;
		if(ptemp->m_Type==1) p->part[k].value=((CCompR*)ptemp)->m_Value;
		if(ptemp->m_Type==2) p->part[k].value=((CCompC*)ptemp)->m_Value;
		if(ptemp->m_Type==3) p->part[k].value=((CCompL*)ptemp)->m_Value;
		if(ptemp->m_Type==4)
		{
			omega=((CCompS*)ptemp)->m_Freq;
			p->part[k].value=((CCompS*)ptemp)->m_Volt;
		}
		p->nz=k+1;
		i++;
		j=0;
		record[i][j+1]=ptemp->m_ID;
		while(j<(ptemp->m_LeftComps).GetSize())
		{
			j++;
			record[i][j+1]=ptemp->m_LeftComps[j-1]->m_ID;
		}
		record[i][0]=j+1;
		int flage[20]={0};

		if(record[Num1][0]!=record[i][0])//向左不是起点
		{
			for(int m=0;m<i;m++)
			{
				if(record[m][0]!=record[i][0]) continue;
				for(int n=1;n<=record[m][0];n++) 
				{
					for(int n1=1;n1<=record[i][0];n1++) 
					{
						if(record[m][n]==record[i][n1])
						{
							flage[n]=1;
							break;
						}
					}
				}
				int sum=0;
				for(int m1=1;m1<=record[i][0];m1++)sum=sum+flage[m1];
				if(sum==record[i][0])//已有点
				{
					for(int m2=0;m2<=record[m][0];m2++)record[i][m2]=0;
//					i--;
					p->num=nbr;
					p->Setnfrom(Num1);
					p->Setpfrom(firstedge[Num1]);
					firstedge[Num1]=p;
					p->Setnto(m);
					p->Setpto(firstedge[m]);
					firstedge[m]=p;


					Edge e1;	
					e1.Comps.Add(ptemp);
					while (ptemp->m_RightComps.GetSize()==1)
					{
						e1.Comps.Add(ptemp->m_RightComps[0]);
						ptemp=ptemp->m_RightComps[0];
					}
					e1.Comps.Add(ptemp);
					m_Result.Edges.Add(&e1);

					break;
				}
			}//新点
			if(m==i)
			{
				p->num=nbr;
				p->Setnfrom(Num1);
				p->Setpfrom(firstedge[Num1]);
				firstedge[Num1]=p;
				nnode=i;
				p->Setnto(i);
				p->Setpto(firstedge[i]);
				firstedge[i]=p;

				Vertex v2;
				v2.Comps.Add(ptemp);
				for (int i=0;i<ptemp->m_LeftComps.GetSize();i++)
				{
					v2.Comps.Add(ptemp->m_LeftComps[i]);
				}
				m_Result.Vertexes.Add(&v2);

				Edge e1;	
				e1.Comps.Add(ptemp);
				while (ptemp->m_RightComps.GetSize()==1)
				{
					e1.Comps.Add(ptemp->m_RightComps[0]);
					ptemp=ptemp->m_RightComps[0];
				}
				e1.Comps.Add(ptemp);
				m_Result.Edges.Add(&e1);


				inQ(Q,Q1,Q2,rear,front,ptemp,i,1);
			}	
		}
		else//向左可能是起点
		{
			for(int n=1;n<=record[Num1][0];n++) 
			{
				for(int n1=1;n1<=record[i][0];n1++) 
				{
					if(record[Num1][n]==record[i][n1])
					{
						flage[n]=1;
						break;
					}
				}
			}
			int sum=0;
			for(int m1=1;m1<=record[i][0];m1++)sum=sum+flage[m1];
			if(sum==record[i][0])//向左是起点
			{
				for(int m2=record[i][0];m2>=0;m2--) record[i][m2]=0;
				i--;
				nbr--;
				k=0;
				delete p;
				p=new Impedance;
				if (!p)
				{
					cout<<"内存申请失败！"<<endl;
					exit(0);
				}
				nbr++;
				while(ptemp->m_RightComps.GetSize()==1)
				{
					
					p->part[k].type=ptemp->m_Type;
					p->part[k].ID=ptemp->m_ID;
					if(ptemp->m_Type==1) p->part[k].value=((CCompR*)ptemp)->m_Value;
					if(ptemp->m_Type==2) p->part[k].value=((CCompC*)ptemp)->m_Value;
					if(ptemp->m_Type==3) p->part[k].value=((CCompL*)ptemp)->m_Value;
					if(ptemp->m_Type==4)
					{
						omega=((CCompS*)ptemp)->m_Freq;
						p->part[k].value=((CCompS*)ptemp)->m_Volt;
					}
					k++;
					ptemp=ptemp->m_RightComps[0];	
				}

				p->part[k].type=ptemp->m_Type;
				p->part[k].ID=ptemp->m_ID;
				if(ptemp->m_Type==1) p->part[k].value=((CCompR*)ptemp)->m_Value;
				if(ptemp->m_Type==2) p->part[k].value=((CCompC*)ptemp)->m_Value;
				if(ptemp->m_Type==3) p->part[k].value=((CCompL*)ptemp)->m_Value;
				if(ptemp->m_Type==4)
				{
					omega=((CCompS*)ptemp)->m_Freq;
					p->part[k].value=((CCompS*)ptemp)->m_Volt;
				}
				p->nz=k+1;
				i++;
				j=0;
				record[i][j+1]=ptemp->m_ID;
				while(j<(ptemp->m_RightComps).GetSize())
				{
					j++;
					record[i][j+1]=ptemp->m_RightComps[j-1]->m_ID;
				}
				record[i][0]=j+1;
				for (j=0;j<20;j++) flage[j]=0;
				for(int m=0;m<i;m++)
				{
					if(record[m][0]!=record[i][0]) continue;
					for(int n=1;n<=record[m][0];n++) 
					{
						for(int n1=1;n1<=record[i][0];n1++) 
						{
							if(record[m][n]==record[i][n1])
							{
								flage[n]=1;
								break;
							}
						}
					}
					sum=0;
					for(int m1=1;m1<=record[i][0];m1++)sum=sum+flage[m1];
					if(sum==record[i][0])//向右已有点
					{
						for(int m2=0;m2<=record[m][0];m2++)record[i][m2]=0;
//						i--;
						p->num=nbr;
						p->Setnfrom(Num1);
						p->Setpfrom(firstedge[Num1]);
						firstedge[Num1]=p;
						p->Setnto(m);
						p->Setpto(firstedge[m]);
						firstedge[m]=p;

						Edge e1;	
						e1.Comps.Add(ptemp);
						while (ptemp->m_LeftComps.GetSize()==1)
						{
							e1.Comps.Add(ptemp->m_LeftComps[0]);
							ptemp=ptemp->m_LeftComps[0];
						}
						e1.Comps.Add(ptemp);
						m_Result.Edges.Add(&e1);


						break;
					}
				}
				if(m==i)//向右新点
				{
					p->num=nbr;
					p->Setnfrom(Num1);
					p->Setpfrom(firstedge[Num1]);
					firstedge[Num1]=p;
					nnode=i;
					p->Setnto(i);
					p->Setpto(firstedge[i]);
					firstedge[i]=p;

					Vertex v2;
					v2.Comps.Add(ptemp);
					for (int i=0;i<ptemp->m_RightComps.GetSize();i++)
					{
						v2.Comps.Add(ptemp->m_RightComps[i]);
					}
					m_Result.Vertexes.Add(&v2);

					Edge e1;	
					e1.Comps.Add(ptemp);
					while (ptemp->m_LeftComps.GetSize()==1)
					{
						e1.Comps.Add(ptemp->m_LeftComps[0]);
						ptemp=ptemp->m_LeftComps[0];
					}
					e1.Comps.Add(ptemp);
					m_Result.Edges.Add(&e1);


					inQ(Q,Q1,Q2,rear,front,ptemp,i,2);
				}	
			}
			else//向左不是起点
			{
				for(int m=0;m<i;m++)
				{
					if(record[m][0]!=record[i][0]) continue;
					for(int n=1;n<=record[m][0];n++) 
					{
						for(int n1=1;n1<=record[i][0];n1++) 
						{
							if(record[m][n]==record[i][n1])
							{
								flage[n]=1;
								break;
							}
						}
					}
					sum=0;
					for(int m1=1;m1<=record[i][0];m1++)sum=sum+flage[m1];
					if(sum==record[i][0])//向左是已有点
					{
						for(int m2=0;m2<=record[m][0];m2++)record[i][m2]=0;
//						i--;
						p->num=nbr;
						p->Setnfrom(Num1);
						p->Setpfrom(firstedge[Num1]);
						firstedge[Num1]=p;
						p->Setnto(m);
						p->Setpto(firstedge[m]);
						firstedge[m]=p;

						Edge e1;	
						e1.Comps.Add(ptemp);
						while (ptemp->m_RightComps.GetSize()==1)
						{
							e1.Comps.Add(ptemp->m_RightComps[0]);
							ptemp=ptemp->m_RightComps[0];
						}
						e1.Comps.Add(ptemp);
						m_Result.Edges.Add(&e1);

						break;
					}
				}
				if(m==i)//向左是新
				{
					p->num=nbr;
					p->Setnfrom(Num1);
					p->Setpfrom(firstedge[Num1]);
					firstedge[Num1]=p;
					nnode=i;
					p->Setnto(i);
					p->Setpto(firstedge[i]);
					firstedge[i]=p;


					Vertex v2;
					v2.Comps.Add(ptemp);
					for (int i=0;i<ptemp->m_LeftComps.GetSize();i++)
					{
						v2.Comps.Add(ptemp->m_LeftComps[i]);
					}
					m_Result.Vertexes.Add(&v2);


					Edge e1;	
					e1.Comps.Add(ptemp);
					while (ptemp->m_RightComps.GetSize()==1)
					{
						e1.Comps.Add(ptemp->m_RightComps[0]);
						ptemp=ptemp->m_RightComps[0];
					}
					e1.Comps.Add(ptemp);
					m_Result.Edges.Add(&e1);


					inQ(Q,Q1,Q2,rear,front,ptemp,i,1);
				}	
			}
		}
		k1++;
	}
	Function(Q,Q1,Q2,rear,front,record,i);
}
		

// 
// 		CString s;
// 		s.Format("node num:%d",i);
// 		AfxMessageBox(s);
// 		for(int h=0;h<=record[i][0];h++)
// 		{
// 
// 		}
		//CString s1="comps Id:"+

void Circuit::Creat(const CTypedPtrArray <CObArray,CComp*> &data)//创建图
{
	
	CComp *Q[20]={NULL};//队列
	int Q1[20],Q2[20];//Q1为节点编号，Q2为方向，1为左2为右；
	int rear=0,front=0;
	int i=0,j=0,k=0;//i节点编号、j节点支路数、k支路原件变量
	int record[20][20];//各个节点的信息
	for(int i1=0;i1<20;i1++)for(int j1=0;j1<20;j1++)record[i1][j1]=0;	
	CComp *ptemp=data[0],*test=NULL;
	while ((ptemp->m_RightComps).GetSize()==1) ptemp=ptemp->m_RightComps[0];//找到一条支路最右端
	Vertex v1;
	v1.Comps.Add(ptemp);
	m_Result.Vertexes.Add(&v1);

	record[i][j+1]=ptemp->m_ID;
	while(j<(ptemp->m_RightComps).GetSize())
	{
		j++;
		record[i][j+1]=ptemp->m_RightComps[j-1]->m_ID;

		v1.Comps.Add(ptemp->m_RightComps[j-1]);
	}

	m_Result.Vertexes.Add(&v1);

	record[i][0]=j+1;
	nnode=i;
	inQ(Q,Q1,Q2,rear,front,ptemp,i,2);
	Function(Q,Q1,Q2,rear,front,record,i);		
}


void Circuit::Form()//形成导纳增广矩阵yna，支路导纳向量Yb，支路独立电压源向量Eb，支路独立电流源向量Jb
{
	Impedance *p=NULL;
	Complex  temp;
	int i,j=0,k;
	int x1,y1,nn=Getnnode()+1;
	for (i=1;i<=nnode;i++) for (k=1;k<=nn;k++) yna[i][k]=temp;
	for (i=0;i<=nnode;i++)
	{
		
		p=firstedge[i];
		while (p)
		{
			j=0;
			if (p->Getmark()==0)
			{
				x1=p->Getnfrom();
				y1=p->Getnto();
				k=p->num;
				while(p->part[j].type)
				{
					if (p->part[j].type==4) 
					{
						temp.Set(p->part[j].value,0);
						Ubr[k][j]=temp;
						Eb[k]=Eb[k]+temp;
						Ebr[k][j]=temp;
					}
					else switch(p->part[j].type)
					{
						case 1:
							temp.Set(p->part[j].value,0.0);
							Rb[k]=Rb[k]+temp;
							Rbr[k][j]=temp;
							break;
						case 2:
							temp.Set(0.0,1/(omega*p->part[j].value));
							Rb[k]=Rb[k]+temp;
							Rbr[k][j]=temp;
							break;
						case 3:
							temp.Set(0.0,(omega*p->part[j].value));
							Rb[k]=Rb[k]+temp;
							Rbr[k][j]=temp;
							break;		
					}
					j++;
				}
				temp.Set(1,0);
				Yb[k]=temp/(Rb[k]);
				Nfrom[k]=x1;
				Nto[k]=y1;
				if (x1) yna[x1][x1]=yna[x1][x1]+Yb[k];
				if (y1) yna[y1][y1]=yna[y1][y1]+Yb[k];
				if (x1&&y1) 
				{
					yna[x1][y1]=yna[x1][y1]-Yb[k];
					yna[y1][x1]=yna[y1][x1]-Yb[k];
				}
				if (x1) yna[x1][nn]=yna[x1][nn]-Yb[k]*Eb[k];
				if (y1) yna[y1][nn]=yna[y1][nn]+Yb[k]*Eb[k];
				p->Setmark(1);//访问标记
			}
			if(i==p->Getnfrom())p=p->Getpfrom();
			else p=p->Getpto();
		}	
	}
	for (i=0;i<=nnode;i++)//还原标志位
	{
		p=firstedge[i];
		while (p)
		{
			if (p->Getmark()==1)p->Setmark(0);//还原标记
			if(i==p->Getnfrom())p=p->Getpfrom();
			else p=p->Getpto();
		}	
	}
}


void Circuit::Guass()//高斯消元
{
	int i,j,k,n,m,rowindex;
	Complex pivot,temp;
	m=Getnnode();
	n=Getnnode()+1;
	for (i=1;i<=m;i++)
	{
		pivot=yna[i][i];
		rowindex=i;
		for(j=i+1;j<=m;j++) if(yna[j][i].Meg()>pivot.Meg()) {pivot=yna[j][i];rowindex=j;}
		if(pivot.Meg()==0) {flage=0;cout<<"The determinant is zero!";break;}
		if(rowindex!=i) for(k=i;k<=n;k++){temp=yna[i][k];yna[i][k]=yna[rowindex][k];yna[rowindex][k]=temp;}
		for(j=i+1;j<=m;j++) for(k=i+1;k<=n;k++)yna[j][k]=yna[j][k]-yna[j][i]*yna[i][k]/yna[i][i];
	}
	yna[m][n]=yna[m][n]/yna[m][m];
	for (i=m-1;i>=1;i--)
	{
		for (k=i+1;k<=m;k++)yna[i][n]=yna[i][n]-yna[i][k]*yna[k][n];
		yna[i][n]=yna[i][n]/yna[i][i];		
	}
}

void Circuit::Calculat()//计算支路电压和电流
{
	int nn=nnode+1,j=0;
	Form();
	Guass();
	if(!flage)
	{
		cout<<"No solution!"<<endl;
		exit(0);
	}
	Complex temp;
	yna[0][nn]=temp;
	for (int i=1;i<=nbr;i++)Ub[i]=yna[Nfrom[i]][nn]-yna[Nto[i]][nn];
	for (i=1;i<=nbr;i++)Ib[i]=Yb[i]*(Ub[i]+Eb[i]);	
 	for (i=1;i<=nbr;i++)
 	{
		int j=0;
		while ((Rbr[i][j].Meg()!=0)||(Ebr[i][j].Meg()!=0))
		{
			if ((Ebr[i][j].Meg())==0)	Ubr[i][j]=Rbr[i][j]*Ib[i];
			if ((Rbr[i][j].Meg())==0)	Ubr[i][j]=Ebr[i][j];
			j++;			
		}
 	}
		
}
// struct Vertex	//? Node
// {
// 	CArray<CComp*,CComp*> Comps;
// 	CArray<CComp*,CComp*> InComps;
// 	CArray<CComp*,CComp*> OutComps;
// };
// struct Edge		//? Branch
// {
// 	Vertex head;
// 	Vertex tail;
// 	CArray<CComp*,CComp*> Comps;	
// 	double Voltage;
// 	double Current;
// };
// 
// struct Result 
// {
// 	CArray<Edge,Edge> Edges;
// 	CArray<Vertex,Vertex> Vertexes;
// };
// void Circuit::GiveResult(Result& result)
// {
// 	int k=0;
// 	Impedance *p=NULL;
// 	for (int i=0;i<=nnode;i++)//还原标志位
// 	{
// 		p=firstedge[i];
// 		while (p)
// 		{
// 			if (p->Getmark()==0)
// 			{
// 				k=p->num;
// //				result.Edges[k].Voltage=Ubr[k];
// //				result.Edges[k].Current=Ib[k];
// 				p->Setmark(1);//访问标记
// 			}
// 			if(i==p->Getnfrom())p=p->Getpfrom();
// 			else p=p->Getpto();
// 		}	
// 	}
// 	for (i=0;i<=nnode;i++)//还原标志位
// 	{
// 		p=firstedge[i];
// 		while (p)
// 		{
// 			if (p->Getmark()==1)p->Setmark(0);//还原标记
// 			if(i==p->Getnfrom())p=p->Getpfrom();
// 			else p=p->Getpto();
// 		}	
// 	}
// }
// 


//不知道这样传指针是否可以
void Circuit::GiveResult(Result &result)
{	

	for (int i=0;i<m_Result.Edges.GetSize();i++)
	{
		result.Edges.Add(m_Result.Edges[i]);
	}
	for (i=0;i<m_Result.Vertexes.GetSize();i++)
	{
		result.Vertexes.Add(m_Result.Vertexes[i]);
	}
		
}


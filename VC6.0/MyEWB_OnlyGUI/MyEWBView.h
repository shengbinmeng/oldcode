// MyEWBView.h : interface of the CMyEWBView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MYEWBVIEW_H__8D89215A_8DDF_4022_A1E0_1102A0FE67E3__INCLUDED_)
#define AFX_MYEWBVIEW_H__8D89215A_8DDF_4022_A1E0_1102A0FE67E3__INCLUDED_

#include "Comps.h"
#include "MyEWBDoc.h"
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMyEWBView : public CView
{
protected: // create from serialization only
	CMyEWBView();
	DECLARE_DYNCREATE(CMyEWBView)

// Attributes
public:
	CMyEWBDoc* GetDocument();

// Operations
public:
// 	CTypedPtrArray <CObArray,CComp*> GetData()
// 	{
// 		return m_Data;
// 	}

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyEWBView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMyEWBView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

public:
	bool bGrid;
	int m_DrawType;
	int m_TempIDs[5];                                
	CLine* m_pCurLine;
	CRectTracker m_Tracker;

	CTypedPtrArray <CObArray,CComp*> m_Data;
	CTypedPtrArray <CObArray,CLine*> m_Lines;

	Result m_Result;

// Generated message map functions
public:
	//{{AFX_MSG(CMyEWBView)
	afx_msg void OnButtonR();		
	afx_msg void OnButtonL();
	afx_msg void OnButtonC();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnViewGrid();
	afx_msg void OnUpdateViewGrid(CCmdUI* pCmdUI);
	afx_msg void OnCompGene();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnButtonS();
	afx_msg void OnButtonSwitch();
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnCtxmenuRot();
	afx_msg void OnCtxmenuDel();
	afx_msg void OnEditDel();
	afx_msg void OnEditClearall();
	afx_msg void OnCircuitAna();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in MyEWBView.cpp
inline CMyEWBDoc* CMyEWBView::GetDocument()
   { return (CMyEWBDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYEWBVIEW_H__8D89215A_8DDF_4022_A1E0_1102A0FE67E3__INCLUDED_)

// MyEWBDoc.cpp : implementation of the CMyEWBDoc class
//

#include "stdafx.h"
#include "MyEWB.h"
#include "MyEWBView.h"

#include "MyEWBDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMyEWBDoc

IMPLEMENT_DYNCREATE(CMyEWBDoc, CDocument)

BEGIN_MESSAGE_MAP(CMyEWBDoc, CDocument)
	//{{AFX_MSG_MAP(CMyEWBDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyEWBDoc construction/destruction

CMyEWBDoc::CMyEWBDoc()
{
	// TODO: add one-time construction code here
}

CMyEWBDoc::~CMyEWBDoc()
{
}

BOOL CMyEWBDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	POSITION ViewPos=this->GetFirstViewPosition();
	CMyEWBView* pMyView=(CMyEWBView*)this->GetNextView(ViewPos);
	for (int i=0;i<pMyView->m_Data.GetSize();i++)
	{
		delete pMyView->m_Data[i];
	}
	for (i=0;i<pMyView->m_Lines.GetSize();i++)
	{
		delete pMyView->m_Lines[i];
	}
	//删除指针本身
	pMyView->m_Data.RemoveAll();
	pMyView->m_Lines.RemoveAll();
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMyEWBDoc serialization

void CMyEWBDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
		POSITION ViewPos=this->GetFirstViewPosition();
		CMyEWBView* pMyView=(CMyEWBView*)this->GetNextView(ViewPos);

		int CompNum=pMyView->m_Data.GetSize();
		ar<<CompNum;
		for(int i=0;i<CompNum;i++)
		{
			ar<<pMyView->m_Data[i];
		}

		int LineNum=pMyView->m_Lines.GetSize();
		ar<<LineNum;
		for (int j=0;j<LineNum;j++)
		{
			ar<<pMyView->m_Lines[j];
		}
		
		ar<<pMyView->m_Tracker.m_rect;
		ar<<pMyView->m_DrawType;
		ar<<pMyView->bGrid;
		for (i=0;i<5;i++)
		{
			ar<<pMyView->m_TempIDs[i];
		}
		
	}
	else
	{
		// TODO: add loading code here

		POSITION ViewPos=this->GetFirstViewPosition();
		CMyEWBView* pMyView=(CMyEWBView*)this->GetNextView(ViewPos);

		//********************************************************************
		//前一文档视图的清理工作
		//释放指针的内存
		for (int i=0;i<pMyView->m_Data.GetSize();i++)
		{
			delete pMyView->m_Data[i];
		}
		for (i=0;i<pMyView->m_Lines.GetSize();i++)
		{
			delete pMyView->m_Lines[i];
		}
		//删除指针本身
		pMyView->m_Data.RemoveAll();
		pMyView->m_Lines.RemoveAll();
		//********************************************************************

		int CompNum, LineNum;
		ar>>CompNum;
		for (i=0;i<CompNum;i++)
		{
			CComp* pC=new CComp;
			ar>>pC;
			pMyView->m_Data.Add(pC);
		}
		ar>>LineNum;
		for (i=0;i<LineNum;i++)
		{
			CLine* pL=new CLine;
			ar>>pL;
			pMyView->m_Lines.Add(pL);
		}

		ar>>pMyView->m_Tracker.m_rect;
		ar>>pMyView->m_DrawType;
		int a;
		ar>>a;
		pMyView->bGrid=bool(a);
		for (i=0;i<5;i++)
		{
			ar>>pMyView->m_TempIDs[i];
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMyEWBDoc diagnostics

#ifdef _DEBUG
void CMyEWBDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMyEWBDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMyEWBDoc commands



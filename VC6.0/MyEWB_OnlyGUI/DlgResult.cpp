// DlgResult.cpp : implementation file
//

#include "stdafx.h"
#include "MyEWB.h"
#include "DlgResult.h"
#include "MyEWBView.h"
#include "DlgGraph.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgResult dialog


CDlgResult::CDlgResult(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgResult::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgResult)
	//}}AFX_DATA_INIT
}


void CDlgResult::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgResult)
	DDX_Control(pDX, IDC_COMBO2, m_Combo2);
	DDX_Control(pDX, IDC_COMBO1, m_Combo1);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDlgResult, CDialog)
	//{{AFX_MSG_MAP(CDlgResult)
	ON_BN_CLICKED(IDC_DISPLAYALL, OnDisplayall)
	ON_BN_CLICKED(IDC_DISPLAYBRANCH, OnDisplaybranch)
	ON_BN_CLICKED(IDC_DISPLAYCOMP, OnDisplaycomp)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgResult message handlers

void CDlgResult::OnDisplayall() 
{
	// TODO: Add your control notification handler code here
	MessageBox("此按钮将显示分析计算得到的所有支路信息，包括每个支路的电压电流，及每个元件的电压电流，等等。");
	
}


BOOL CDlgResult::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CMyEWBView* pParent=(CMyEWBView*)this->GetParent();
	for (int i=0;i<(pParent->m_Data.GetSize());i++)
	{
		m_Combo1.AddString(pParent->m_Data[i]->m_Lable);
	}

	m_Combo2.AddString("branch 1");
	m_Combo2.AddString("branch 2");
	m_Combo2.AddString("……");

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



void CDlgResult::OnDisplaybranch() 
{
	// TODO: Add your control notification handler code here
	UpdateData();

	CMyEWBView* pParent=(CMyEWBView*)this->GetParent();
	CString str;
	int n=m_Combo2.GetCurSel();
	if (n<0)
	{
		MessageBox("您尚未选择支路！");
		return;
	}
	m_Combo2.GetLBText(n,str);
	if (str=="branch 1" || str=="branch 2")
	{
		CDlgGraph dlg;
		dlg.m_strDisplay="此支路电压为：电流为：";
		dlg.DoModal();
	}
}


void CDlgResult::OnDisplaycomp() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	CMyEWBView* pParent=(CMyEWBView*)this->GetParent();
	CString str;
	int n=m_Combo1.GetCurSel();
	if (n<0)
	{
		MessageBox("您尚未选择元件！");
		return;
	}
	m_Combo1.GetLBText(n,str);
	for (int i=0;i<pParent->m_Data.GetSize();i++)
	{
		if (pParent->m_Data[i]->m_Lable==str)
		{			
			CDlgGraph dlg;
			dlg.m_strDisplay=str+"两端的电压为：电流为：";
			dlg.DoModal();
		}
	}
	
}

void CDlgResult::OnDestroy() 
{
	CDialog::OnDestroy();	
	// TODO: Add your message handler code here
	delete this;
	
}


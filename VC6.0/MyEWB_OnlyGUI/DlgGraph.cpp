// DlgGraph.cpp : implementation file
//

#include "stdafx.h"
#include "MyEWB.h"
#include "DlgGraph.h"
#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgGraph dialog


CDlgGraph::CDlgGraph(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgGraph::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgGraph)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDlgGraph::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgGraph)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgGraph, CDialog)
	//{{AFX_MSG_MAP(CDlgGraph)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgGraph message handlers

void CDlgGraph::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here

	//电压曲线图：
	CRect rect;
	CWnd* pWnd=GetDlgItem(IDC_STATIC_U);   
	pWnd->GetWindowRect(&rect);   
	ScreenToClient(&rect); 
	//画坐标系

	dc.MoveTo(rect.left+30,rect.bottom-20);
	dc.LineTo(rect.left+30,rect.top+20);
	dc.LineTo(rect.left+25,rect.top+30);
	dc.MoveTo(rect.left+30,rect.top+20);
	dc.LineTo(rect.left+35,rect.top+30);
	dc.TextOut(rect.left+2,rect.top+30,"U/V");

		//横坐标在底部
// 	dc.MoveTo(rect.left+10,rect.bottom-30);
// 	dc.LineTo(rect.right-20,rect.bottom-30);
// 	dc.LineTo(rect.right-30,rect.bottom-35);
// 	dc.MoveTo(rect.right-20,rect.bottom-30);
// 	dc.LineTo(rect.right-30,rect.bottom-25);
// 	dc.TextOut(rect.right-30,rect.bottom-25,"t /s");
// 
// 	CPoint Opoint(rect.left+30,rect.bottom-20);
// 	dc.TextOut(Opoint.x-20,Opoint.y+10,"O");

		//横坐标在中间，便于画正弦曲线：
	int d=30;
	dc.MoveTo(rect.left+10,rect.bottom-130+d);
	dc.LineTo(rect.right-20,rect.bottom-130+d);
	dc.LineTo(rect.right-30,rect.bottom-135+d);
	dc.MoveTo(rect.right-20,rect.bottom-130+d);
	dc.LineTo(rect.right-30,rect.bottom-125+d);
	dc.TextOut(rect.right-30,rect.bottom-125+d,"t /s");

	CPoint Opoint(rect.left+30,rect.bottom-130+d);     //原点
	dc.TextOut(Opoint.x-20,Opoint.y+10,"O");



	//画曲线：

	//尝试正弦曲线画法：
 	CRect sinrect(rect.left+30,rect.top+20,rect.right-20,rect.bottom-30);  //要画的正弦曲线的矩形区
 	double pi=3.14159265359; 
	int DotNum=400;
 	int nWidth=sinrect.Width();   
 	int nHeight=sinrect.Height();  

	int Ox=sinrect.left,Oy=sinrect.top;	    //矩形区的左上角
	double x,y; 

	dc.MoveTo(Ox,Oy+nHeight);
	for (int i=1;i<=DotNum;i++)
	{
		x=Ox+i*double(nWidth)/DotNum;
		y=Oy+(sin(2*pi*(double(i)/DotNum))+1)*(nHeight/2);
		dc.LineTo(x,y);
	}



	//电流曲线图：

	CRect rect2;
	pWnd=GetDlgItem(IDC_STATIC_I);
	pWnd->GetWindowRect(&rect2);
	ScreenToClient(&rect2);
	//画坐标系

	dc.MoveTo(rect2.left+30,rect2.bottom-20);
	dc.LineTo(rect2.left+30,rect2.top+20);
	dc.LineTo(rect2.left+25,rect2.top+30);
	dc.MoveTo(rect2.left+30,rect2.top+20);
	dc.LineTo(rect2.left+35,rect2.top+30);
	dc.TextOut(rect2.left+5,rect2.top+30,"I /A");

		//横坐标在底部：
	dc.MoveTo(rect2.left+10,rect2.bottom-30);
	dc.LineTo(rect2.right-20,rect2.bottom-30);
	dc.LineTo(rect2.right-30,rect2.bottom-35);
	dc.MoveTo(rect2.right-20,rect2.bottom-30);
	dc.LineTo(rect2.right-30,rect2.bottom-25);
	dc.TextOut(rect2.right-30,rect2.bottom-25,"t /s");

//		//横坐标在中间，便于画正弦曲线：
// 	int d2=30;
// 	dc.MoveTo(rect2.left+10,rect2.bottom-130+d2);
// 	dc.LineTo(rect2.right-20,rect2.bottom-130+d2);
// 	dc.LineTo(rect2.right-30,rect2.bottom-135+d2);
// 	dc.MoveTo(rect2.right-20,rect2.bottom-130+d2);
// 	dc.LineTo(rect2.right-30,rect2.bottom-125+d2);
// 	dc.TextOut(rect2.right-30,rect2.bottom-125+d2,"t /s");
	

	CPoint Opoint2(rect2.left+30,rect2.bottom-30);       //原点
	dc.TextOut(Opoint2.x-20,Opoint2.y+10,"O");

	//画曲线：

	dc.MoveTo(Opoint2.x,Opoint2.y-50);
	dc.LineTo(Opoint2.x+200,Opoint2.y-50);


	//文字结果显示：
	dc.TextOut(100,210,m_strDisplay);
	
	// Do not call CDialog::OnPaint() for painting messages
}



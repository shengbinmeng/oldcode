#if !defined(AFX_DLGCOMPRO_H__4B03735A_C190_4B05_BFCF_2D90D67170B4__INCLUDED_)
#define AFX_DLGCOMPRO_H__4B03735A_C190_4B05_BFCF_2D90D67170B4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgComPro.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgComPro dialog

class CDlgComPro : public CDialog
{
// Construction
public:
	CDlgComPro(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgComPro)
	enum { IDD = IDD_PROPERTY };
	double	m_CompValue;
	CString	m_CompLable;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgComPro)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgComPro)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CDlgComProS dialog

class CDlgComProS : public CDialog
{
// Construction
public:
	CDlgComProS(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgComProS)
	enum { IDD = IDD_PROPERTY_S };
	double	m_CompFreq;
	CString	m_CompLable;
	double	m_CompVolt;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgComProS)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgComProS)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGCOMPRO_H__4B03735A_C190_4B05_BFCF_2D90D67170B4__INCLUDED_)

// Circuit.h: interface for the Circuit class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CIRCUIT_H__7EBD1C14_5B64_4B1A_A476_DBD190B0B98C__INCLUDED_)
#define AFX_CIRCUIT_H__7EBD1C14_5B64_4B1A_A476_DBD190B0B98C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/************************************************************************/
/*                  所有元器件以及电路图的的声明                        */
/************************************************************************/

#include <iostream.h>
#include <fstream.h>
#include <math.h>
#include <string.h>

#include "Comps.h"


class Complex//复数
{
public:
	Complex(double x){real=x;imag=0.0;}
	Complex(double x=0.0,double y=0.0){real=x;imag=y;}
	void Set(double x=0.0,double y=0.0){real=x;imag=y;}
	double Re(){return real;}//返回实部
	double Im(){return imag;}//返回虚部
	double Meg();//取模
	double Ang();//取幅角
	double Getreal(){return real;}
	double Getimag(){return imag;}
	Complex operator =(Complex &);//赋值
	friend Complex operator +(Complex &,Complex &);//复数加法
	friend Complex operator -(Complex &,Complex &);//复数减法
	friend Complex operator *(Complex &,Complex &);//复数乘法
	friend Complex operator /(Complex &,Complex &);//复数除法
//	friend istream &operator>>(istream &f,Complex &c);
//	friend ostream &operator<<(ostream &f,Complex &c);
private:
	double real,imag;//实部、虚部
};

class Element 
{
public:
	Element();
	int type;//原件的类型，1为电阻，2为电容，3为电感，4为电压源
	double value;//以上相应的值，即电阻的阻值、电容的大小、电感的大小、电压源的幅值
	int ID;
	double omega;
};

class Impedance //支路，也是图结构的边
{
public:
	Impedance();
	int Getnfrom(){return nfrom;}
	int Getnto(){return nto;}
	void Setnfrom(int i1){nfrom=i1;}
	void Setnto(int j1){nto=j1;}
	void Setpfrom(Impedance *p){pfrom=p;}
	void Setpto(Impedance *p){pto=p;}
	Impedance *Getpfrom(){return pfrom;}
	Impedance *Getpto(){return pto;}
	bool Getmark(){return mark;}
	void Setmark(bool m){mark=m;}
	int num;//支路的编号
	int nz;//支路上的原件数
	Element part[20];
private:
	bool mark;//访问标志
	int nfrom,nto;//该边始、终节点的位置
	Impedance *pfrom,*pto;//分别指向该边始、终节点的另一条未被访问过的边
};


class Circuit //电路图
{
public:
	Circuit();
	int Getnnode(){return nnode;}
	int Function(CComp **Q,int *,int *,int &rear,int &front,int (*record)[20],int &i);//递归函数，辅助建立电路图
	void Creat(const CTypedPtrArray <CObArray,CComp*> &data);//创建电路图
	void Form();//形成导纳增广矩阵
	void Guass();//高斯消元
//	void Disp();//输出电路图
	void Calculat();//计算支路电压和电流 
	void GiveResult(Result &result);
//	void Save();//保存图
//	void Load();//装载图
	Complex yna[20][20];//导纳增广矩阵
	Complex Rb[20],Yb[20],Eb[20],Ib[20],Ub[20];//yb是支路导纳，eb是支路独立电压源，ib是支路电流，ub是支路电压
	Complex Rbr[20][20],Ubr[20][20],Ebr[20][20];//rbr是支路各个元件阻抗ubr是支路各个元件电压
	int Nfrom[20],Nto[20];//始终点的编号向量
	bool flage;//方程是否有解
private:
	int nnode,nbr;//独立节点数、支路数
	double omega;//频率
	Impedance *firstedge[20];//图结构的顶点，也是电路的节点
	Result m_Result;
};



#endif // !defined(AFX_CIRCUIT_H__7EBD1C14_5B64_4B1A_A476_DBD190B0B98C__INCLUDED_)

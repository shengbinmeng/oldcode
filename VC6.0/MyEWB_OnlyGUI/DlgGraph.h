#if !defined(AFX_DLGGRAPH_H__F6D4FE19_CA72_4A38_B0C0_9E63211555EA__INCLUDED_)
#define AFX_DLGGRAPH_H__F6D4FE19_CA72_4A38_B0C0_9E63211555EA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgGraph.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgGraph dialog

class CDlgGraph : public CDialog
{
// Construction
public:
	CDlgGraph(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgGraph)
	enum { IDD = IDD_RESULTGRAPH };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	CString m_strDisplay;
	double data1;
	double data2;
	//����


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgGraph)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgGraph)
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGGRAPH_H__F6D4FE19_CA72_4A38_B0C0_9E63211555EA__INCLUDED_)

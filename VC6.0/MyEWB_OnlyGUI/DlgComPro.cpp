// DlgComPro.cpp : implementation file
//

#include "stdafx.h"
#include "MyEWB.h"
#include "DlgComPro.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgComPro dialog


CDlgComPro::CDlgComPro(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgComPro::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgComPro)
	m_CompValue = 0.0;
	m_CompLable = _T("");
	//}}AFX_DATA_INIT
}


void CDlgComPro::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgComPro)
	DDX_Text(pDX, IDC_EDIT_VALUE, m_CompValue);
	DDV_MinMaxFloat(pDX, m_CompValue, 0.f, 1.e+013f);
	DDX_Text(pDX, IDC_EDIT_LABLE, m_CompLable);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgComPro, CDialog)
	//{{AFX_MSG_MAP(CDlgComPro)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgComPro message handlers

void CDlgComPro::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);

	CDialog::OnOK();
}
/////////////////////////////////////////////////////////////////////////////
// CDlgComProS dialog


CDlgComProS::CDlgComProS(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgComProS::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgComProS)
	m_CompFreq = 0.0;
	m_CompLable = _T("");
	m_CompVolt = 0.0;
	//}}AFX_DATA_INIT
}


void CDlgComProS::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgComProS)
	DDX_Text(pDX, IDC_EDIT_FREQ, m_CompFreq);
	DDV_MinMaxDouble(pDX, m_CompFreq, 0., 99999999999.);
	DDX_Text(pDX, IDC_EDIT_LABLE, m_CompLable);
	DDX_Text(pDX, IDC_EDIT_VOLT, m_CompVolt);
	DDV_MinMaxDouble(pDX, m_CompVolt, 0., 999999999.);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgComProS, CDialog)
	//{{AFX_MSG_MAP(CDlgComProS)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgComProS message handlers


// MyEWBView.cpp : implementation of the CMyEWBView class
//

#include "stdafx.h"
#include "MyEWB.h"

#include "MyEWBDoc.h"
#include "MyEWBView.h"
#include "ComponentsDlg.h"
#include "DlgResult.h"
#include "Circuit.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMyEWBView

IMPLEMENT_DYNCREATE(CMyEWBView, CView)

BEGIN_MESSAGE_MAP(CMyEWBView, CView)
	//{{AFX_MSG_MAP(CMyEWBView)
	ON_COMMAND(ID_BUTTON_R, OnButtonR)
	ON_COMMAND(ID_BUTTON_L, OnButtonL)
	ON_COMMAND(ID_BUTTON_C, OnButtonC)
	ON_WM_LBUTTONDOWN()
	ON_WM_KEYUP()
	ON_WM_LBUTTONDBLCLK()
	ON_COMMAND(ID_VIEW_GRID, OnViewGrid)
	ON_UPDATE_COMMAND_UI(ID_VIEW_GRID, OnUpdateViewGrid)
	ON_COMMAND(ID_COMP_GENE, OnCompGene)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_COMMAND(ID_BUTTON_S, OnButtonS)
	ON_COMMAND(ID_BUTTON_SWITCH, OnButtonSwitch)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONDOWN()
	ON_COMMAND(ID_CTXMENU_ROT, OnCtxmenuRot)
	ON_COMMAND(ID_CTXMENU_DEL, OnCtxmenuDel)
	ON_COMMAND(ID_EDIT_DEL, OnEditDel)
	ON_COMMAND(ID_EDIT_CLEARALL, OnEditClearall)
	ON_COMMAND(ID_CIRCUIT_ANA, OnCircuitAna)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyEWBView construction/destruction

CMyEWBView::CMyEWBView()
{
	// TODO: add construction code here
	bGrid=1;
	m_Tracker.m_nStyle=CRectTracker::dottedLine;
	m_Tracker.m_rect.SetRect(0,0,0,0);
	m_DrawType=0;
	for (int i=0;i<5;i++)
	{
		m_TempIDs[i]=0;
	}

}

CMyEWBView::~CMyEWBView()
{
}

BOOL CMyEWBView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMyEWBView drawing

void CMyEWBView::OnDraw(CDC* pDC)
{
	CMyEWBDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
	//绘制网格
	CRect rec;
	GetClientRect(&rec);
	if(bGrid)
	{
		for(int i=0;i<rec.right;i+=8)
		{
			for(int j=0;j<rec.bottom;j+=8){
				pDC->SetPixel(i,j,RGB(0,0,0));
			}
		}
	}
	
	//绘制元件：
	for(int i=0;i<m_Data.GetSize();i++)
	{
		CComp* comp=m_Data[i];
		if(comp->bSelected)
		{
			//调整坐标使元件按网格放置，便于处理连线等
			int top=m_Tracker.m_rect.top,ctop=0;
			int left=m_Tracker.m_rect.left,cleft=0;
			for (int n=0;;n++)
			{
				if(abs(top-8*n)<=4){ ctop=8*n-top; break;}
			}
			for (n=0;;n++)
			{
				if(abs(left-8*n)<=4){ cleft=8*n-left; break;}
			}
			m_Tracker.m_rect.top+=ctop;
			m_Tracker.m_rect.bottom+=ctop;
			m_Tracker.m_rect.left+=cleft;
			m_Tracker.m_rect.right+=cleft;
			//如果有元件位置移动，也设置文件修改标记
			if (comp->m_Position!=m_Tracker.m_rect)
			{
				GetDocument()->SetModifiedFlag();
			}
			comp->m_Position=m_Tracker.m_rect;
			m_Tracker.Draw(pDC);
		}

		CRect prePos=comp->m_Position;
		comp->Draw(pDC);

		if (comp->bSelected)
		{
			m_Tracker.m_rect=comp->m_Position;

		}
	}

	//绘制连线：
	CPen pen,*pOldPen;
	pen.CreatePen(PS_SOLID,1,RGB(0,255,0));
	pOldPen=pDC->SelectObject(&pen);	

	CPoint start;
	CPoint end;
	start=end;
	for (i=0;i<m_Lines.GetSize();i++)
	{
		m_Lines[i]->m_PreComp->GetRightLinkPoint(start);
		m_Lines[i]->m_NextComp->GetLeftLinkPoint(end);
		
		pDC->MoveTo(start);
		if((start.x<=end.x&&start.y<=end.y)||(start.x>end.x&&start.y>end.y))
			pDC->LineTo(end.x,start.y);
		else pDC->LineTo(start.x,end.y);
		pDC->LineTo(end);
		
	}
	pDC->SelectObject(pOldPen);

	//todo：实现智能化的自动转弯，使连线全部平直且不经过元件
		
}

/////////////////////////////////////////////////////////////////////////////
// CMyEWBView printing

BOOL CMyEWBView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CMyEWBView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CMyEWBView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CMyEWBView diagnostics

#ifdef _DEBUG
void CMyEWBView::AssertValid() const
{
	CView::AssertValid();
}

void CMyEWBView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMyEWBDoc* CMyEWBView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMyEWBDoc)));
	return (CMyEWBDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMyEWBView message handlers

void CMyEWBView::OnButtonR() 
{
	// TODO: Add your command handler code here
	//MessageBox("You choose a R!");
	CCompR* pCompR=new CCompR();
	m_TempIDs[0]++;
	m_TempIDs[4]++;
	pCompR->m_ID=m_TempIDs[4];
	pCompR->m_Lable.Format("R%d",m_TempIDs[0]);
	m_Data.Add(pCompR);	
	GetDocument()->SetModifiedFlag();
	Invalidate();
}

void CMyEWBView::OnButtonC() 
{
	// TODO: Add your command handler code here
	CCompC* pCompC=new CCompC;
	m_TempIDs[1]++;
	m_TempIDs[4]++;
	pCompC->m_ID=m_TempIDs[4];
	pCompC->m_Lable.Format("C%d",m_TempIDs[1]);
	m_Data.Add(pCompC);
	GetDocument()->SetModifiedFlag();
	Invalidate();	
}

void CMyEWBView::OnButtonL() 
{
	// TODO: Add your command handler code here
	CCompL* pCompL=new CCompL;
	m_TempIDs[2]++;
	m_TempIDs[4]++;
	pCompL->m_ID=m_TempIDs[4];
	pCompL->m_Lable.Format("L%d",m_TempIDs[2]);
	m_Data.Add(pCompL);	
	GetDocument()->SetModifiedFlag();
	Invalidate();
	
}

void CMyEWBView::OnButtonS() 
{
	// TODO: Add your command handler code here
	CCompS* pCompS=new CCompS;
	m_TempIDs[3]++;
	m_TempIDs[4]++;
	pCompS->m_ID=m_TempIDs[4];
	pCompS->m_Lable.Format("S%d",m_TempIDs[3]);
	m_Data.Add(pCompS);
	GetDocument()->SetModifiedFlag();
	Invalidate();
	
}

void CMyEWBView::OnViewGrid() 
{
	// TODO: Add your command handler code here
	if(bGrid) bGrid=0;
	else bGrid=1;
	Invalidate();
	GetDocument()->SetModifiedFlag();
}

void CMyEWBView::OnUpdateViewGrid(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->SetCheck(bGrid);
}

void CMyEWBView::OnCompGene() 
{
	// TODO: Add your command handler code here
	CComponentsDlg* pDlg=new CComponentsDlg(this);
	pDlg->Create(IDD_COMPONENTS,this);
	pDlg->ShowWindow(SW_SHOW);
}

void CMyEWBView::OnEditDel() 
{
	// TODO: Add your command handler code here
	CMyEWBView::OnKeyUp(46,1,0);
}

void CMyEWBView::OnEditClearall() 
{
	// TODO: Add your command handler code here
	
	//释放指针对应内存
	for (int i=0;i<m_Data.GetSize();i++)
	{
		delete m_Data[i];
	}
	for (i=0;i<m_Lines.GetSize();i++)
	{
		delete m_Lines[i];
	}
	//删除指针本身
	m_Data.RemoveAll();
	m_Lines.RemoveAll();
	
	Invalidate();
	
}


void CMyEWBView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	//重叠时原来选中的优先被选中
	for(int i=0;i<m_Data.GetSize();i++)
	{
		CComp* item=m_Data[i];
		if (item->bSelected)
		{
			if (item->m_Position.PtInRect(point))
			{
				m_Tracker.Track(this,point);
				Invalidate();
				return ;
			}
		}
	}

	//重新确定哪个被选中
	for(i=0;i<m_Data.GetSize();i++)
	{
		CComp* item=m_Data[i];
		if (item->m_Position.PtInRect(point))
		{
			item->bSelected=1;
			m_Tracker.m_rect=item->m_Position;
			//确保其他都不被选中：——有了下面的else就已经确保了吧？——不行！
			for (int j=0;j<m_Data.GetSize();j++)
			{
				if (j!=i)
				{
					m_Data[j]->bSelected=0;
				}
			}
			
			m_Tracker.Track(this,point);
			Invalidate();
			break;
		}
		else item->bSelected=0;
	}

	//如果是点击某元件端点，则准备画线
	for (i=0;i<m_Data.GetSize();i++)
	{
		CComp* item=m_Data[i];
		CPoint lp,rp;
		item->GetLeftLinkPoint(lp);
		item->GetRightLinkPoint(rp);
		CRect lRect=new CRect(lp.x-4,lp.y-4,lp.x+4,lp.y+4);
		CRect rRect=new CRect(rp.x-8,rp.y-8,rp.x+8,rp.y+8);

		if (rRect.PtInRect(point))
		{
			m_pCurLine=new CLine(rp,rp);
			m_pCurLine->m_PreComp=item;
			m_pCurLine->m_Dire=0;
			m_DrawType=1;
			break;
		}
		//目前只允许从右端连到左端，故下面情况不用 ——现在可用
		if (lRect.PtInRect(point))
		{
			//MessageBox("请暂不要这样连线！谢谢合作。");
			//break;
			m_pCurLine=new CLine(lp,lp);
			m_pCurLine->m_NextComp=item;
			m_pCurLine->m_Dire=1;
			m_DrawType=1;
			break;
		}
	}

	//可以添加对线的选中和处理：

		
}


void CMyEWBView::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	//处理画线时的鼠标移动：
	if ((nFlags & MK_LBUTTON) && m_DrawType==1)
	{
		CClientDC dc(this);
		dc.SetROP2(R2_NOT);
		m_pCurLine->Draw(&dc);
		m_pCurLine->Update(point);
		m_pCurLine->Draw(&dc);
	}

	//可以添加对其他情况鼠标移动的处理：
	CRect lRect,rRect;
	CPoint lp,rp;	
	for(int i=0;i<m_Data.GetSize();i++)
	{
		CComp* item=m_Data[i];
		item->GetLeftLinkPoint(lp);
		item->GetRightLinkPoint(rp);
		lRect=CRect(lp.x-4,lp.y-4,lp.x+4,lp.y+4);	
		rRect=CRect(rp.x-4,rp.y-4,rp.x+4,rp.y+4);
		if (lRect.PtInRect(point))//|| rRect.PtInRect(point))
		{
			CClientDC dc(this);
			CBrush brush,*pOldBrush;
			brush.CreateSolidBrush(RGB(0,0,0));
			pOldBrush=dc.SelectObject(&brush);
			dc.Ellipse(lRect);	
		}
		if (rRect.PtInRect(point))
		{
			CClientDC dc(this);
			CBrush brush,*pOldBrush;
			brush.CreateSolidBrush(RGB(0,0,0));
			pOldBrush=dc.SelectObject(&brush);
			dc.Ellipse(rRect);
		}
	}
	
	CView::OnMouseMove(nFlags, point);
}

//一般是连线结束时的处理
void CMyEWBView::OnLButtonUp(UINT nFlags, CPoint point) 
{

	// TODO: Add your message handler code here and/or call default
	if(m_DrawType==1)
	{
		CPoint lp,rp;
		CRect lRect,rRect;
		for (int i=0;i<m_Data.GetSize();i++)
		{
			if (m_Data[i]==m_pCurLine->m_PreComp) continue;

			CComp* item=m_Data[i];
			item->GetLeftLinkPoint(lp);
			item->GetRightLinkPoint(rp);
			lRect=CRect(lp.x-8,lp.y-8,lp.x+8,lp.y+8);	
			rRect=CRect(rp.x-8,rp.y-8,rp.x+8,rp.y+8);

			if (lRect.PtInRect(point))
			{
				//还是单方向连线比较好，即只能从元件右端连出，进入元件左端；看来要结合图的构造来记录data信息！！！！！
				if (m_pCurLine->m_Dire==0)
				{
					m_pCurLine->m_NextComp=item;
					//临时记录线前元件原来的所有右元件：
					CArray<CComp*,CComp*> array1,array2;
					for (int m=0;m<m_pCurLine->m_PreComp->m_RightComps.GetSize();m++)
					{
						array1.Add(m_pCurLine->m_PreComp->m_RightComps[m]);
					}
					//临时记录此元件原来的所有左元件
					for (m=0;m<item->m_LeftComps.GetSize();m++)
					{
						array2.Add(item->m_LeftComps[m]);
					}
									
					//线前元件的右元件组添加此元件及此元件的所有左元件
					m_pCurLine->m_PreComp->m_RightComps.Add(item);
					for (int j=0;j<item->m_LeftComps.GetSize();j++)
					{
						m_pCurLine->m_PreComp->m_RightComps.Add(item->m_LeftComps[j]);
					}

					//此元件的左元件组添加线前元件及其原来所有右元件
					item->m_LeftComps.Add(m_pCurLine->m_PreComp);
					for (j=0;j<array1.GetSize();j++)
					{
						item->m_LeftComps.Add(array1[j]);
					}

					//线前元件原来的每个右元件的左元件组添加此元件及其原来每个左元件
					for (m=0;m<array1.GetSize();m++)
					{
						array1[m]->m_LeftComps.Add(item);
						for (int n=0;n<array2.GetSize();n++)
						{
							array1[m]->m_LeftComps.Add(array2[n]);
						}
					}

					//此元件原来的每个左元件的右元件组添加线前元件及其原来每个右元件
					for (m=0;m<array2.GetSize();m++)
					{
						array2[m]->m_RightComps.Add(m_pCurLine->m_PreComp);
						for (int n=0;n<array1.GetSize();n++)
						{
							array2[m]->m_RightComps.Add(array1[n]);
						}
					}


					m_Lines.Add(m_pCurLine);
					GetDocument()->SetModifiedFlag();
					m_DrawType=0;
					Invalidate();
 					break;
				}
				else{MessageBox("请不要这样连线！谢谢合作。");}
			}
			if (rRect.PtInRect(point))
			{
				if(m_pCurLine->m_Dire==1)
				{
					m_pCurLine->m_PreComp=item;
					//临时记录线后元件原来的所有左元件：
					CArray<CComp*,CComp*> array1,array2;
					for (int m=0;m<m_pCurLine->m_NextComp->m_LeftComps.GetSize();m++)
					{
						array1.Add(m_pCurLine->m_NextComp->m_LeftComps[m]);
					}
					//临时记录此元件原来的所有右元件：
					for (m=0;m<item->m_RightComps.GetSize();m++)
					{
						array2.Add(item->m_RightComps[m]);
					}

					//线后元件的左元件组添加此元件及此元件的所有右元件
					m_pCurLine->m_NextComp->m_LeftComps.Add(item);
					for (int j=0;j<array2.GetSize();j++)
					{
						m_pCurLine->m_NextComp->m_LeftComps.Add(array2[j]);
					}
					//此元件的右元件组添加线后元件及其原来所有左元件
					item->m_RightComps.Add(m_pCurLine->m_NextComp);
					for (j=0;j<array1.GetSize();j++)
					{
						item->m_RightComps.Add(array1[j]);
					}
					//线后元件原来的每个左元件的右元件组添加此元件及其原来的每个右元件：
					for (m=0;m<array1.GetSize();m++)
					{
						array1[m]->m_RightComps.Add(item);
						for (int n=0;n<array2.GetSize();n++)
						{
							array1[m]->m_RightComps.Add(array2[n]);
						}
					}
					//此元件原来的每个右元件的左元件组添加线后元件及其原来的每个左元件：
					for (m=0;m<array2.GetSize();m++)
					{
						array2[m]->m_LeftComps.Add(m_pCurLine->m_NextComp);
						for (int n=0;n<array1.GetSize();n++)
						{
							array2[m]->m_LeftComps.Add(array1[n]);
						}
					}


					m_Lines.Add(m_pCurLine);
					GetDocument()->SetModifiedFlag();
					m_DrawType=0;
					Invalidate();
					break;
				}
				else{MessageBox("请不要这样连线！谢谢合作。");}
			}
		}

		CClientDC dc(this);
		dc.SetROP2(R2_NOT);
		m_pCurLine->Draw(&dc);
		m_DrawType=0;
		Invalidate();
	}

	//其他：

	CView::OnLButtonUp(nFlags, point);
}

//如果双击了元件，则显示属性供查看和修改：
void CMyEWBView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default

	for (int i=0;i<m_Data.GetSize();i++)
	{
		if (m_Data[i]->bSelected)
		{
			m_Data[i]->ShowProperties();
			Invalidate();
			GetDocument()->SetModifiedFlag();
			break;
		}
	}
	
	//可以添加双击其他的响应：
	
	CView::OnLButtonDblClk(nFlags, point);
}


//还有另一种右键菜单机制，待探索（目前此函数无用）：
void CMyEWBView::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	// TODO: Add your message handler code here
	CMenu menu;
	menu.LoadMenu(IDR_CONTEXTMENU);	
}

void CMyEWBView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	for(int i=0;i<m_Data.GetSize();i++)
	{
		CComp* item=m_Data[i];

		//重叠时原来选中的优先被选中
		if (item->bSelected)
		{
			if (item->m_Position.PtInRect(point))
			{
				CMenu menu;
				//	menu.LoadMenu(IDR_CONTEXTMENU); ???????????????????????????????
				menu.CreatePopupMenu();
				menu.AppendMenu(0,ID_CTXMENU_ROT,"旋转");
				menu.AppendMenu(0,ID_CTXMENU_DEL,"删除");
				CPoint pt;
				GetCursorPos(&pt);
				menu.TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON,pt.x,pt.y,this);
				menu.DestroyMenu();
				return;
			}
		}
		else
		{
			if (item->m_Position.PtInRect(point))
			{
				item->bSelected=1;
				m_Tracker.m_rect=item->m_Position;
				//确保其他都不被选中
				for (int j=0;j<m_Data.GetSize();j++)
				{
					if (j!=i)
					{
						m_Data[j]->bSelected=0;
					}
				}
				
				CMenu menu;
				//???怎么Load已经做好的菜单？
				//	menu.LoadMenu(IDR_CONTEXTMENU);

				//下面用动态添加菜单实现：
				menu.CreatePopupMenu();
				menu.AppendMenu(0,ID_CTXMENU_ROT,"旋转");
				menu.AppendMenu(0,ID_CTXMENU_DEL,"删除");
				CPoint pt;
				GetCursorPos(&pt);
				menu.TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON,pt.x,pt.y,this);
				
				menu.DestroyMenu();
				
				return ;
			}
		}
	}
	
	CView::OnRButtonDown(nFlags, point);
	
}


void CMyEWBView::OnCtxmenuRot() 
{
	// TODO: Add your command handler code here
	for (int i=0;i<m_Data.GetSize();i++)
	{
		if (m_Data[i]->bSelected)
		{
			m_Data[i]->m_LeftDir++;
			if (m_Data[i]->m_LeftDir==4) m_Data[i]->m_LeftDir=0;
			Invalidate();
			break;
		}
	}
	
	Invalidate();
	
}

void CMyEWBView::OnCtxmenuDel() 
{
	// TODO: Add your command handler code here
	CMyEWBView::OnKeyUp(46,1,0);
	
}

//响应Del键，删除选中元件
void CMyEWBView::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	if (nChar==46)
	{
		for (int i=0;i<m_Data.GetSize();i++)
		{
			if (m_Data[i]->bSelected)
			{

				int n=MessageBox("确定要删除此元件？",NULL,MB_YESNO);
				if(n==IDNO) return ;

				CComp* todel=m_Data[i];
				//删除与欲删元件相连的线：
				for (int j=0;j<m_Lines.GetSize();j++)
				{
					if ((m_Lines[j]->m_PreComp==m_Data[i]) || (m_Lines[j]->m_NextComp==m_Data[i]))
					{
						delete m_Lines[j];
						m_Lines.RemoveAt(j);
						j--;
					}
				}
				
				//将欲删元件从其他左右关系元件组中除去
				for (j=0;j<m_Data.GetSize();j++)
				{
					for (int m=0;m<m_Data[j]->m_LeftComps.GetSize();m++)
					{
						if (m_Data[j]->m_LeftComps[m]==todel)
						{
							m_Data[j]->m_LeftComps.RemoveAt(m);
						}
					}
					for (m=0;m<m_Data[j]->m_RightComps.GetSize();m++)
					{
						if (m_Data[j]->m_RightComps[m]==todel)
						{
							m_Data[j]->m_RightComps.RemoveAt(m);
						}
					}
				}
				
				//最后删除欲删元件：——要先释放其内存，再移除指针本身
				delete m_Data[i];
				m_Data.RemoveAt(i);
				Invalidate();
				GetDocument()->SetModifiedFlag();		
			}
		}
		
		if (i==m_Data.GetSize())
		{
			MessageBox("未选择欲删除的元件!");
		}

	}
	
	//可以添加对其他键的响应：
	
	CView::OnKeyUp(nChar, nRepCnt, nFlags);
}

void GetResult(const CTypedPtrArray <CObArray,CComp*> &data, Result &result)
{
	//todo:在此函数中通过提供的数据data，计算出所需结果result（直接赋给引用参数即可）

// 	Circuit circuit;
// 	circuit.Creat(data);
// 	circuit.Calculat();
// 	circuit.GiveResult(result);	

		// 孟胜彬：Circuit类由另一成员建立,用户分析计算出结果，但最后没有成功，导致m_Result成员未被填充，因此没有用于显示的数据
}

void CMyEWBView::OnButtonSwitch() 
{
	// TODO: Add your command handler code here

	//下面显示按规则记录的电路信息，仅用于调试
	CString s="Curcuit Info:\n";
	CString temp;
	for (int i=0;i<m_Data.GetSize();i++)
	{
		temp.Format("'s Left Comps are:");
		s=s+m_Data[i]->m_Lable+temp;
		for (int j=0;j<m_Data[i]->m_LeftComps.GetSize();j++)
		{
			s=s+m_Data[i]->m_LeftComps[j]->m_Lable;
		}
		s+="  \n";
		temp.Format("'s Right Comps are:");
		s=s+m_Data[i]->m_Lable+temp;
		for (j=0;j<m_Data[i]->m_RightComps.GetSize();j++)
		{
			s+=m_Data[i]->m_RightComps[j]->m_Lable;
		}
		s+="  \n";
	}
	MessageBox(s);

	GetResult(m_Data,m_Result);	        //这里也可以用一个计算类来做


	//MessageBox("结果即将产生！敬请期待！");
	//todo:根据m_Result显示结果：


// 	CString str;    	//此处用于调试：
// 	for (i=0;i<	m_Result.Vertexes.GetSize();i++)
// 	{
// 		int n=m_Result.Vertexes[i]->Comps.GetSize();
// 		
// 		for (int j=0;j<n;j++)
// 		{
// 			str+=m_Result.Vertexes[i]->Comps[j]->m_Lable;
// 		}
// 	
// 	}
// 	MessageBox(str);


}

void CMyEWBView::OnCircuitAna() 
{
	// TODO: Add your command handler code here
	//CMyEWBView::OnButtonSwitch();

	GetResult(m_Data,m_Result);

	CDlgResult* pDlg=new CDlgResult(this);
	pDlg->Create(IDD_VIEWRESULT,this);
	pDlg->ShowWindow(SW_SHOW);

	//无模式方式为何不可？总是在初始化函数中得不到父视图
// 	CDlgResult dlg(this);
// 	dlg.DoModal();
}

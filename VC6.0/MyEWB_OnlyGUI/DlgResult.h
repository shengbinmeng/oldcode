#if !defined(AFX_DLGRESULT_H__B15CA37C_7D1C_43FC_BF9E_61FD8963B2B4__INCLUDED_)
#define AFX_DLGRESULT_H__B15CA37C_7D1C_43FC_BF9E_61FD8963B2B4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgResult.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgResult dialog

class CDlgResult : public CDialog
{
// Construction
public:
	CDlgResult(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgResult)
	enum { IDD = IDD_VIEWRESULT };
	CComboBox	m_Combo2;
	CComboBox	m_Combo1;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgResult)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgResult)
	afx_msg void OnDisplayall();
	virtual BOOL OnInitDialog();
	afx_msg void OnDisplaybranch();
	afx_msg void OnDisplaycomp();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGRESULT_H__B15CA37C_7D1C_43FC_BF9E_61FD8963B2B4__INCLUDED_)

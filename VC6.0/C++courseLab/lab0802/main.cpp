#include<iostream>
using namespace std;

class vehicle{
public:
	virtual void Run()
	{
		cout<<"Vehicle Run!"<<endl;
	}
	virtual void Stop()
	{
		cout<<"Vehicle Stop!"<<endl;
	}
};
class bicycle:public vehicle{
public:
	void Run()
	{
		cout<<"Bicycle Run!"<<endl;
	}
	void Stop()
	{
		cout<<"Bicycle Stop!"<<endl;
	}
};
class motorcar:public vehicle{
public:
	void Run()
	{
		cout<<"Motorcar Run!"<<endl;
	}
	void Stop()
	{
		cout<<"Motorcar Stop!"<<endl;
	}
};
class motorcycle:public bicycle,public motorcar{
public:
	void Run()
	{
		cout<<"Motorcycle Run!"<<endl;
	}
	void Stop()
	{
		cout<<"Motorcycle Stop!"<<endl;
	}
};

int main()
{
	vehicle v;
	bicycle b;
	motorcar m;
	motorcycle mc;
	v.Run(); v.Stop();
	b.Run(); b.Stop();
	m.Run(); m.Stop();
	mc.Run(); mc.Stop();

	vehicle* pv=&v;
	pv->Run(); pv->Stop();
	pv=&b;
	pv->Stop(); pv->Stop();
	pv=&m;
	pv->Run(); pv->Stop();
	//pv=&mc;
	//pv->Run(); pv->Stop();
	motorcar* pm;
	pm=&mc;
	pm->Run(); pm->Stop();

	return 0;
}
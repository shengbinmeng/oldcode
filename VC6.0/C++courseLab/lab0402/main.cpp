#include<iostream>
using namespace std;
enum CPU_Rank{P1=1,P2,P3,P4,P5,P6,P7};
class CPU{
	CPU_Rank rank;
	int frequency;
	float voltage;
public:
	CPU(){};
	CPU(CPU_Rank r,int f,float v)
	{
		rank=r;
		frequency=f;
		voltage=v;
		cout<<"C_cons..."<<endl;
	}

	~CPU()
	{
		cout<<"C_des..."<<endl;
	}

	void run()
	{
		cout<<"  CPU Run!"<<endl;
	}

	void stop()
	{
		cout<<"  CPU Stop!"<<endl;
	}
};
class RAM{
	int R_data;
public:
	RAM(){};
	RAM(int a){R_data=a;cout<<"R_cons..."<<endl;}
	~RAM(){cout<<"R_des..."<<endl;}
	void R_setdata(int d){R_data=d;}
	int R_getdata(){return R_data;}
};
class CDROM{
	int C_data;
public:
	CDROM(){};
	CDROM(int b){C_data=b;cout<<"CD_cons..."<<endl;}
	~CDROM(){cout<<"CD_des..."<<endl;}
	void C_setdata(int d){C_data=d;}
	int C_getdata(){return C_data;}
};


class Computer{
	CPU cpu;
	RAM ram;
	CDROM cdrom;
public:
	Computer(CPU c,RAM r,CDROM cd)
	{
		cpu=c;
		ram=r;
		cdrom=cd;
		cout<<"Constructed a Computer!"<<endl;
	}

	~Computer(){cout<<"Destructed a Computer!"<<endl;}
	void run()
	{
		cout<<"Computer Run!"<<endl;
		cpu.run();
	}

	void stop()
	{
		cout<<"Computer Stop!"<<endl;
		cpu.stop();
	}
};

int main()
{
	CPU c1(P4,350,2.9);
	RAM r1(3);
	CDROM cd1(4);
	Computer com(c1,r1,cd1);
	com.run();
	com.stop();
	return 0;
}

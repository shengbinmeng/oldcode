#include<iostream>
using namespace std;

class Point{
	int x;
	int y;
public:
	Point(int a,int b){ x=a; y=b;}
	void show()
	{
		cout<<'('<<x<<','<<y<<')'<<endl;
	}
	Point& operator++ ()
	{
		x++;
		y++;
		return (*this);
	}
	Point& operator-- ()
	{
		x--;
		y--;
		return (*this);
	}
	Point operator++(int)
	{
		Point temp(x,y);
		++(*this);
		return temp;
	}
	Point operator--(int)
	{
		Point temp(x,y);
		--(*this);
		return temp;
	}
};

int main()
{
	Point p1(8,10);
	cout<<"P1:";p1.show();
	cout<<"p1++:";p1++.show();
	cout<<"p1:";p1.show();
	cout<<"--p1:";(--p1).show();
	cout<<"p1:";p1.show();
	return 0;
}

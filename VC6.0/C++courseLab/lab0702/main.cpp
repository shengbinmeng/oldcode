#include<iostream>
using namespace std;

class BaseClass{
	int Number;
public:
	BaseClass()
	{
		cout<<"BaseClass's construction..."<<endl;
	}
	~BaseClass()
	{
		cout<<"BaseClass's destruction..."<<endl;
	}
};

class DerivedClass:public BaseClass{
public:
	DerivedClass()
	{
		cout<<"DerivedClass's constrution..."<<endl;
	}
	~DerivedClass()
	{
		cout<<"DerivedClass's destruction..."<<endl;
	}
};

int main()
{
	BaseClass b;
	DerivedClass d;
	return 0;
}


#include<iostream>
using namespace std;
enum CPU_Rank{P1=1,P2,P3,P4,P5,P6,P7};
class CPU{
	CPU_Rank rank;
	int frequency;
	float voltage;
public:
	CPU(CPU_Rank r,int f,float v)
	{
		rank=r;
		frequency=f;
		voltage=v;
		cout<<"Constructed a CPU!"<<endl;
	}

	~CPU()
	{
		cout<<"Destructed a CPU!"<<endl;
	}

	void run()
	{
		cout<<"Run!"<<endl;
	}

	void stop()
	{
		cout<<"Stop!"<<endl;
	}
};

int main()
{
	CPU cpu1(P2,300,2.88);
	cpu1.run();
	cpu1.stop();
	return 0;
}
	

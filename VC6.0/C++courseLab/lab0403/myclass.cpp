#include<iostream>
using namespace std;
class Date{
	int year;
	int month;
	int day;
public:
	void put()
	{
		cin>>year>>month>>day;
	}
	void put_y(int y){year=y;}
	void put_m(int m){month=m;}
	void put_d(int d){day=d;}
	int get_y(){return year;}
	int get_m(){return month;}
	int get_d(){return day;}
};
class People{
	int number;
	bool sex;
	Date birthday;
	char* id;
public:
	People(){id=new char[20];};
	People(int n, bool s, Date b, char* i)
	{
		id=new char[20];
		number=n;
		sex=s;
		birthday=b;
		id=i;
	}
	People(People &p)
	{
		id=new char[20];
		number=p.number;
		sex=p.sex;
		birthday=p.birthday;
		id=p.id;
	}
	~People(){};
	void inputinfo();
	void printinfo();
};
inline void People::inputinfo()
	{
		cout<<"Input Number:";cin>>number;
		cout<<"Input Sex(0-male 1-female):";cin>>sex;
		cout<<"Input Birthday:"; birthday.put();
		cout<<"Input ID:";cin>>id;
	}

inline void People::printinfo()
	{
		cout<<"Number:  "<<number<<endl;
		cout<<"Sex(0-male 1-female):  ";
		if(sex==0)cout<<"Male (0)"<<endl;
		else cout<<"Female (1)"<<endl;
		cout<<"Birthday:  "<<birthday.get_m()<<'/'<<birthday.get_d()<<'/'<<birthday.get_y()<<endl;
		cout<<"ID:  "<<id<<endl;
	}
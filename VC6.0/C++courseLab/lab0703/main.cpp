#include<iostream>
using namespace std;

class vehicle{
	int MaxSpeed;
	float Weight;
public:
	void put_maxspeed(int m)
	{
		MaxSpeed=m;
	}
	int get_maxspeed()
	{
		return MaxSpeed;
	}
	void Run()
	{
		cout<<"Run..."<<endl;
	}
	void Stop()
	{
		cout<<"Stop..."<<endl;
	}
};

class bicycle:virtual public vehicle{
	float Height;
};

class motorcar:virtual public vehicle{
	int SeatNum;
};

class motorcycle:public bicycle,public motorcar{
public:
	void show_maxspeed()
	{
		cout<<get_maxspeed()<<endl;
	}
};

int main()
{
	vehicle v;
	bicycle b;
	motorcycle m;
	m.put_maxspeed(3);
	v.put_maxspeed(2);
	b.put_maxspeed(1);
	cout<<b.get_maxspeed()<<endl;
	cout<<v.get_maxspeed()<<endl;
	m.show_maxspeed();
	return 0;
}
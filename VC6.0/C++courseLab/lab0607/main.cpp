#include"myclass.h"

int main()
{
	People p1;
	cout<<"Input information:"<<endl;
	p1.inputinfo();
	People p2(p1);
	cout<<"Display the information:"<<endl;
	p2.printinfo();
	return 0;
}

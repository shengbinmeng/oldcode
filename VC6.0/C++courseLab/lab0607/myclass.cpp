#include<iostream>
#include<cstring>
using namespace std;
class Date{
	int year;
	int month;
	int day;
public:
	void put()
	{
		cin>>year>>month>>day;
	}
	void put_y(int y){year=y;}
	void put_m(int m){month=m;}
	void put_d(int d){day=d;}
	int get_y(){return year;}
	int get_m(){return month;}
	int get_d(){return day;}
};

class People{
	char name[11];
	char number[7];
	char sex[3];
	Date birthday;
	char id[16];
public:
	People(){};
	People(char* na, char* nu, char* s, Date b, char* i)
	{
		strcpy(name,na);strcpy(number,nu);strcpy(sex,s);birthday=b;strcpy(id,i);
	}

	People(People &p)
	{
		strcpy(name,p.name);
		strcpy(number,p.number);
		strcpy(sex,p.sex);
		birthday=p.birthday;
		strcpy(id,p.id);
	}
	~People(){};
	void inputinfo();
	void printinfo();
};

 void People::inputinfo()
	{
		cout<<"Input Name:"; cin>>name;
		cout<<"Input Number:";cin>>number;
		cout<<"Input Sex:";cin>>sex;
		cout<<"Input Birthday:"; birthday.put();
		cout<<"Input ID:";cin>>id;
	}

 void People::printinfo()
	{
		cout<<"Name:  "<<name<<endl;
		cout<<"Number:  "<<number<<endl;
		cout<<"Sex: "<<sex<<endl;
		cout<<"Birthday:  "<<birthday.get_m()<<'/'<<birthday.get_d()<<'/'<<birthday.get_y()<<endl;
		cout<<"ID:  "<<id<<endl;
	}

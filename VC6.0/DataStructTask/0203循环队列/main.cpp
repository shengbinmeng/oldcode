#include <iostream>
using namespace std;

const int LENGTH=10;

//循环队列的类定义：其中rear指向队尾元素，front指向队首元素的前一位
class Queue 
{
	int iData[LENGTH];
	int front;
	int rear;
	int count;
public:
	Queue(){front=rear=3; count=0;}             //初始的front和rear值可任意指定，只要二者相等，就表示空队
	bool Qin(int n);
	bool Qout(int &x);
	void Display();
};

int main()
{
	Queue queue;
	int out;
	for(int num=1;num<=12;num++){
		if(!queue.Qin(num)){
			bool isout=queue.Qout(out);
			if(isout){
				cout<<"元素 "<<out<<" 出队。"<<endl;
				if(queue.Qin(num)) cout<<"元素 "<<num<<" 入队。"<<endl;;
			}
		}
		else cout<<"元素 "<<num<<" 入队。"<<endl;
		
		
		queue.Display();
	}
	
	return 0;
}



bool Queue::Qin(int n)
{
	if(front==(rear+1)%10){
		cout<<"队列已满！无法入队。"<<endl;
		return 0;
	}
	else{
		rear=(rear+1)%10;
		iData[rear]=n;
		count++;
		
		return 1;
	}
}

bool Queue::Qout(int &x)
{
	if(front==rear){
		cout<<"队空！无法出队。"<<endl;
		return 0;
	}
	else{
		front=(front+1)%10;
		x=iData[front];
		count--;

		return 1;
	}
}

void Queue::Display()
{
	cout<<"队中元素如下："<<endl;
	if(front==rear){cout<<"空！"<<endl; return;}
	
	int i=front;
	while(i!=rear){
		i=(i+1)%10;
		cout<<iData[i]<<"  ";
	}
	
	
	cout<<"--------共计"<<count<<"个";
	if(count==LENGTH-1)cout<<"（已满）";
	cout<<endl;
}

#include <iostream>
#include <cmath>
using namespace std;

struct treenode{
	int key;
	treenode* left;
	treenode* right;
};
treenode* InsertNodes(treenode* &root);
void Display(treenode* root);
void Display2(treenode* root);

int main()
{
	treenode* Root=NULL;

	cout<<"先生成一棵二叉（排序）树,用以测试函数："<<endl;
	InsertNodes(Root);

	cout<<"下面调用Display(treenode* root)函数，按层（从上到下，从左到右的顺序）输出树中内容："<<endl;
	Display(Root);

	cout<<"用另外方法写的Display2函数，输出时可以按层换行："<<endl;
	Display2(Root);

	return 0;
	
}



//循环队列的类定义：其中rear指向队尾元素，front指向队首元素的前一位
class Queue 
{
	treenode* tData[100];
	int front;
	int rear;
	int count;
public:
	Queue(){front=rear=3; count=0;}             //初始的front和rear值可任意指定，只要二者相等，就表示空队
	bool Qin(treenode* n);
	bool Qout(treenode* &x);
};



//用队列的方法:
void Display(treenode* root)
{
	if(!root) {cout<<"NULL!"<<endl;  return;}
	Queue q;
	treenode* temp;
	int m=1,n=1;
	if(root!=NULL){
		q.Qin(root);
		while(q.Qout(temp)){	
			cout<<temp->key<<"  ";
			if(temp->left){ q.Qin(temp->left); }
			if(temp->right){ q.Qin(temp->right); }
		}

		cout<<endl;
	}	

}


bool Queue::Qin(treenode* n)
{
	if(front==(rear+1)%10){
		return 0;
	}
	else{
		rear=(rear+1)%10;
		tData[rear]=n;
		count++;
		
		return 1;
	}
}

bool Queue::Qout(treenode* &x)
{
	if(front==rear){
		return 0;
	}
	else{
		front=(front+1)%10;
		x=tData[front];
		count--;
		
		return 1;
	}
}







//下面用递归的方法：
void Transfer(treenode* root, int a[],int n)
{
	a[n]=root->key;
	if(root->left!=NULL){ Transfer(root->left,a,2*n);}
	if(root->right!=NULL){ Transfer(root->right,a,2*n+1);}
	
}

void Display2(treenode* root)
{
	if(!root) {cout<<"NULL!"<<endl;  return;}
	int n=1;
	int a[200];
	for(int i=0;i<200;i++)
		a[i]=-999;

	Transfer(root,a,n);

	int m=1;
	for(i=1;i<200;i++){
		if(a[i]!=-999){
			cout<<a[i]<<"  ";
		}

		if(i==pow(2,m)-1){cout<<endl; m++;}  //按层换行
	}

	cout<<endl;

}



//下面的部分用于建立排序二叉树：
treenode* AddtoTree(treenode* &root, treenode* child, treenode* inode)
{
	if(child==NULL){
		child=inode;
		child->left=child->right=NULL;
		if(root==NULL) { root=child; return root;}
		if (child->key < root->key) root->left=child;
		else root->right=child;
		return root;
	}
	else{
		if(inode->key < child->key ) return AddtoTree(child,child->left,inode);
		else return AddtoTree(child,child->right,inode);
	}
	
}

treenode* InsertNodes(treenode* &root)
{
	int key;
	cout<<"输入关键码序列，以q或任意非数值字符结束："<<endl;
	cin>>key;
	while(1){
		if(cin.fail()) break;
		treenode* inode=new treenode;
		inode->key=key;
		if(inode->key)
			inode->left=inode->right=NULL;
		AddtoTree(root,root,inode);
		
		cin>>key;
	}
	
	cout<<"已插入！"<<endl; 
	cin.clear(); 
	return root;
}
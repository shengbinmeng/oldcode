//func.h


struct student 
{
	char number[20];
	char name[20];
	char address[20];
	bool sex;
	struct
	{
		int year;
		int month;
	} birthday;

	student* next;
};


void InsertNode(student* &head, student* instu);
void InputData(student* &head);
void Display(student* head);
student* SearchNodeByNumber(student* head, char* number);
void SearchNodeByName(student* head,char* name);
void SearchNodeByAddress(student* head, char* address);
bool DeleteNode(student* &head, char* num);
bool WriteToFile(student* head);
bool ReadFromFile(student* &head);

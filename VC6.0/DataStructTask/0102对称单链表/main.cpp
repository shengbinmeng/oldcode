#include <iostream>
#include "func.h"

using namespace std;


int main()
{
	char ch;
	student* Head=NULL;
	while(1){
		cout<<"*******************************************"<<endl;       
		cin.clear();   cin.sync();       
		cout<<"请选择操作：１－输入数据　２－输出到屏幕　３－检索节点　４－删除节点\n"
			<<"             ５－存盘　　６－从硬盘读取　　Ｑ－退出"<<endl;
		cin>>ch; cin.ignore(20,'\n');              
		switch(ch){
		case '1':
			InputData(Head);
			break;
			
		case '2':
			Display(Head);
			break;
			
		case '3':
			char key[20];
			cout<<"By Number(N) or By Name(M) or By Address(A)?"<<endl;
			char choice;
			cin>>choice; cin.ignore(10,'\n');
			if(choice=='N'||choice=='n'){
				cout<<"输入要检索的学生学号："; cin.getline(key,20,'\n');
				student* temp=SearchNodeByNumber(Head,key);
				if(temp){
					cout<<"检索结果如下："<<endl;
					cout<<temp->number<<"    "<<temp->name<<"    "<<temp->address<<"    ";
					if(temp->sex)cout<<"女"; else cout<<"男";
						cout<<"    "<<temp->birthday.year<<"年"<<temp->birthday.month<<"月"<<endl;
				}
				else cout<<"未找到！"<<endl;
			}
			else{
				if(choice=='M'||choice=='m'){
					cout<<"输入要检索的学生姓名："; cin.getline(key,20,'\n');
					SearchNodeByName(Head,key);
				}
				else {
					if(choice=='A'||choice=='a'){
						cout<<"输入要检索的学生住址："; cin.getline(key,20,'\n');
						SearchNodeByAddress(Head,key);

					}
					else cout<<"Input Error!"<<endl;
				}
			}
			
			break;
			
		case '4':
			cout<<"输入要删除的学生的学号："<<endl;
			char num[20];
			cin>>num;
			if(DeleteNode(Head,num)) cout<<"已删除！"<<endl;
			break;
			
		case '5':
			if(WriteToFile(Head)) cout<<"已存盘！"<<endl;
			break;
			
		case '6':
			if(ReadFromFile(Head)) cout<<"已读入！"<<endl;
			break;
			
		case 'q':
			exit(0);
		default:
			cout<<"Input Error!"<<endl;
		}
	}
	
	return 0;

}
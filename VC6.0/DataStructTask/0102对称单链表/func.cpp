#include "func.h"
#include <iostream>
#include <string>
#include <fstream>
using namespace std;


//此函数将新节点插入到head之后，这样可以不必从head走到表尾，当然改为插在表尾更自然一些
void InsertNode(student* &head, student* instu)
{
	if(head==NULL){
		head=instu;
		head->next=NULL;
		return ;
	}
	if(head->next==NULL){
		instu->next=(student*)( long(head) ^ long(head->next));
		head->next=(student*)( long(instu) ^ NULL );	
		return ;
	}

	student* p=head->next;         //第二个节点
	student* q=(student*)(long(head) ^ long(p->next));   //第三个节点 
	
	head->next=(student*)( long(instu) ^ NULL );
	instu->next=(student*)( long(head) ^ long(p));     //instu成为新的第二个
	p->next=(student*)( long(instu) ^ long(q) );
	
}


void InputData(student* &head)
{
	student* newstu=new student,*temp;
	cout<<"输入学号："; cin.getline(newstu->number,20,'\n'); //cin>>newstu->number;    //为接受空格，可考虑用：gets(newstu->number); cin.getline(newstu,20,'\n');
	//对重复重复学号输入进行检查：
	if(temp=SearchNodeByNumber(head,newstu->number)) {
		cout<<"此学号同学已存在:"<<endl;
		cout<<temp->number<<"    "<<temp->name<<"    "<<temp->address<<"    "; if(temp->sex)cout<<"女"; else cout<<"男";
			cout<<"    "<<temp->birthday.year<<"年"<<temp->birthday.month<<"月"<<endl;
		cout<<"要更改请先删除之再插入。"<<endl; 
		return;
	}

	cout<<"输入姓名："; cin.getline(newstu->name,20,'\n');//cin>>newstu->name;          
	cout<<"输入地址："; cin.getline(newstu->address,20,'\n');//cin>>newstu->address;
	cout<<"输入性别(0为男，1为女)："; cin>>newstu->sex;
	cout<<"输入出生年、月（两个整型数）："; cin>>newstu->birthday.year>>newstu->birthday.month; 
	if(cin.fail()) { cout<<"Input Error！"<<endl; return ;}
	
	InsertNode(head, newstu);
	cout<<"已插入！"<<endl;
}


void Display(student* head)
{
	student* q=0, *p=0, *temp=head;
	if(!temp){ cout<<"表空！"<<endl; return; }
	cout<<"表中信息如下："<<endl;
	cout<<"( 学号 / 姓名 / 住址 / 性别 / 出生年月 )"<<endl;

	while(temp!=NULL){
		cout<<temp->number<<"    "<<temp->name<<"    "<<temp->address<<"    ";
		if(temp->sex)cout<<"女"; else cout<<"男";
		cout<<"    "<<temp->birthday.year<<"年"<<temp->birthday.month<<"月"<<endl;
		
		q=p;          //前一个节点
		p=temp;      //当前节点
		temp=(student*)( long(q) ^ long(temp->next) );        //后移一个节点

	}
	
}




student* SearchNodeByNumber(student* head, char* number)
{
	student* q=0, *p=0, *temp=head;
	while(temp!=NULL){
		if(strcmp(temp->number,number)==0){
			return temp;
		}
		q=p;          //前一个节点
		p=temp;      //当前节点
		temp=(student*)( long(q) ^ long(temp->next) );        //后移一个节点	
	}

	return NULL;	
}

void SearchNodeByName(student* head,char* name)
{
	bool isfind=0;
	cout<<"检索结果如下："<<endl;
	student* q=0, *p=0, *temp=head;
	while(temp!=NULL){
		if(strcmp(temp->name,name)==0){
			cout<<temp->number<<"    "<<temp->name<<"    "<<temp->address<<"    ";
			if(temp->sex)cout<<"女"; else cout<<"男";
			cout<<"    "<<temp->birthday.year<<"年"<<temp->birthday.month<<"月"<<endl;
			isfind=1;
		}
		
		q=p;          //前一个节点
		p=temp;      //当前节点
		temp=(student*)( long(q) ^ long(temp->next) );        //后移一个节点	
	}
	if(!isfind) cout<<"未找到！"<<endl;

}
void SearchNodeByAddress(student* head, char* address)
{
	bool isfind=0;
	cout<<"检索结果如下："<<endl;
	student* q=0, *p=0, *temp=head;
	while(temp!=NULL){
		if(strcmp(temp->address,address)==0){
			cout<<temp->number<<"    "<<temp->name<<"    "<<temp->address<<"    ";
			if(temp->sex)cout<<"女"; else cout<<"男";
			cout<<"    "<<temp->birthday.year<<"年"<<temp->birthday.month<<"月"<<endl;
			isfind=1;
		}
		
		q=p;          //前一个节点
		p=temp;      //当前节点
		temp=(student*)( long(q) ^ long(temp->next) );        //后移一个节点	
	}
	if(!isfind) cout<<"未找到！"<<endl;

}

bool DeleteNode(student* &head, char* num)
{
	student* q=0, *p=0, *temp=head;
	while(temp!=NULL){
		if(strcmp(temp->number,num)==0){
			if(temp->next==NULL) { delete temp; head=NULL; return 1;}   //如果只有一个节点的处理
			
			student *before, *before2, *after, *after2; 

			before=p;
			if(before==0){                                             //如果要删除的是第一个节点
				after=(student*)( long(before) ^ long(temp->next) );
				after2=(student*)( long(temp) ^ long(after->next) );    //只有两个节点时after2将为0
				after->next=(student*)( long(after2) ^ long(before) );
				head=after;
				delete temp;
				return 1;
			}
			before2=(student*)( long(temp) ^ long(before->next) );    //当删除的是第二个时before2将是0

			after=(student*)( long(before) ^ long(temp->next) );
			if(after==0){
				before->next=(student*)( long(before2) ^ long(after) );
				delete temp;
				return 1;
			}
			after2=(student*)( long(temp) ^ long(after->next) );

			before->next=(student*)( long(before2) ^ long(after) );
			after->next=(student*)( long(after2) ^ long(before) );

			delete temp;
			return 1;
		}	
		q=p;         //前一个节点
		p=temp;      //当前节点
		temp=(student*)( long(q) ^ long(p->next) );        //后移一个节点	
	}

	cout<<"没有要删除的节点！"<<endl;
	return 0;
}

bool WriteToFile(student* head)
{
	ofstream fout;
	fout.open("StudentInfo.txt");
	if(fout.fail()){ cout<<"File not open!"<<endl;  return 0;}

	if(head==NULL){ cout<<"链表为空！保存为空文件。"<<endl;  return 0;}

	student* temp=head, *p=0, *q=0;
	
	while(temp!=NULL){
		fout<<temp->number<<'/'<<temp->name<<'/'<<temp->address<<'/'<<temp->sex<<'/'
			<<temp->birthday.year<<' '<<temp->birthday.month<<endl;
		
		q=p;          //前一个节点
		p=temp;      //当前节点
		temp=(student*)( long(q) ^ long(temp->next) );        //后移一个节点
	}

	return 1;
}

bool ReadFromFile(student* &head)
{
	ifstream fin;
	fin.open("StudentInfo.txt");
	if(fin.fail()){ cout<<"File not open!"<<endl;  return 0;}

	head=NULL;                                      //其实应该把表中节点全删除了，这样并没有把内存释放掉
	char ch;
	while(fin.get(ch)){
		fin.putback(ch);
		student* stu=new student;
		fin.getline(stu->number,20,'/'); 
		fin.getline(stu->name,20,'/'); 
		fin.getline(stu->address,20,'/');
		fin>>stu->sex; fin.ignore();         //ignore后面的分隔符'/'
		fin>>stu->birthday.year>>stu->birthday.month; fin.ignore(); //ignore行末的换行符

		InsertNode(head,stu);		
	}

	return 1;
}

//func.cpp

#include "func.h"
#include <iostream>
#include <fstream>

using namespace std;

bool InsertNode(node* head, node* inode)                 
{
	
	node* temp=head->pdown;
	while(temp->i!=inode->i) temp=temp->pdown;
	node* rhead=temp,*p=rhead;
	if(rhead->pright==NULL) {rhead->pright=inode; inode->pright=rhead; temp->num++; head->num++;}
	else{
		bool ok=0;
		while(rhead->pright!=temp){
			p=rhead;
			rhead=rhead->pright; 
			if(rhead->j == inode->j){
				cout<<"此下标的元素已存在！要更改请先删除之再重新插入。"<<endl;
				return 0;
			}
			if(rhead->j > inode->j){
				p->pright=inode;
				inode->pright=rhead;
				temp->num++;
				head->num++;

				ok=1;
				break;
			}	
		}
		
		if(!ok){
			rhead->pright=inode;
			inode->pright=temp;
			temp->num++;
			head->num++;
		}
		
	}
	
	temp=head->pright;
	while(temp->j!=inode->j) temp=temp->pright;
	node* chead=temp;
	if(chead->pdown==NULL) {chead->pdown=inode; inode->pdown=chead; temp->num++; }
	else{
		bool ok=0;
		while(chead->pdown!=temp){
			p=chead;
			chead=chead->pdown;
			if(chead->i > inode->i){
				p->pdown=inode;
				inode->pdown=chead;
				temp->num++;

				ok=1;
				break;
			}
		}
		
		if(!ok){
			chead->pdown=inode;
			inode->pdown=temp;
			temp->num++;
		}
	}
	
	return 1;
}



void InputData(node* head)
{
	do{
		node* newnode=new node;
		cout<<"依次输入行、列、值（以0退出输入）："<<endl;
		cin>>newnode->i;
		if(newnode->i==0) return;
		else{
			cin>>newnode->j>>newnode->value;
			if(cin.fail()){cout<<"Input Error!"<<endl; cin.clear(); return;}
			if(newnode->i>head->i||newnode->i<1||newnode->j>head->j||newnode->j<1){
				cout<<"行列值越界！此矩阵为"<<head->i<<"×"<<head->j<<"矩阵"<<endl;
				return;
			}

			InsertNode(head,newnode);
		}
		
	}while(1);

}

void Initialize(node* &head,int n,int m)
{
	head=new node;
	head->i=n; head->j=m; head->num=0;
	
	node *temp=head;
	node* rhead;
	for(int i=1;i<=n;i++){
		rhead=new node;
		rhead->i=i;rhead->j=0; rhead->num=0; rhead->pright=NULL;
		temp->pdown=rhead;
		temp=rhead;
	}
	rhead->pdown=head;
	
	temp=head;
	node* chead;
	for(int j=1;j<=m;j++){
		chead=new node;
		chead->j=j; chead->i=0; chead->num=0; chead->pdown=NULL;
		temp->pright=chead;
		temp=chead;
	}
	chead->pright=head;

}

void Display(node* head)
{
	cout<<"矩阵中非零元素如下：（行、列、值）"<<endl;
	node* rhead=head->pdown,*p;
	int nullrow=0;
	
	while(rhead!=head){

		if(rhead->pright!=NULL){
			p=rhead->pright;
			while(p!=rhead){
				cout<<'('<<p->i<<','<<p->j<<','<<p->value<<")  ";
				p=p->pright;
			}
			cout<<"　　                  ——此行共　"<<rhead->num<<"　个"<<endl;
		}
		else nullrow++;

		rhead=rhead->pdown;
	}

	if(nullrow==head->i)  cout<<"空！"<<endl;
	else cout<<endl<<"                                ——总计:　"<<head->num<<"　个"<<endl;
}


bool DeleteNode(node* head, int row, int col)
{
	if(row> head->i||row<1||col>head->j||col<1){
		cout<<"行列值越界！此矩阵为"<<head->i<<"×"<<head->j<<"矩阵"<<endl;  return 0;
	}

	node* rhead=head->pdown;
	while(rhead->i!=row) rhead=rhead->pdown;

	if(rhead->pright==NULL){
		cout<<"No Such Node!"<<endl;
		return 0;
	}
	node* temp=rhead, *p;
	bool flag=0;
	while(temp->pright!=rhead){
		p=temp;
		temp=temp->pright;
		if(temp->j==col)  { flag=1; rhead->num--; break; }
	}
	if(!flag){
		cout<<"No Such Node!"<<endl;
		return 0;
	}
	
	node* q=temp->pdown;
	while(q->pdown!=temp) {
		q=q->pdown; 
		if(q->i==0) q->num--; 
	}

	p->pright=temp->pright;
	q->pdown=temp->pdown;

	if(p->pright==p) p->pright=NULL;
	if(q->pdown==q)  q->pdown=NULL;

	delete temp;
	head->num--;
	return 1;
}


bool SearchNode(node* head,int row, int col)
{
	if(row> head->i||row<1||col>head->j||col<1){
		cout<<"行列值越界！此矩阵为"<<head->i<<"×"<<head->j<<"矩阵"<<endl;  return 0;
	}
	
	node* rhead=head->pdown;
	while(rhead->i!=row) rhead=rhead->pdown;
	
	if(rhead->pright==NULL){
		cout<<"No Such Node!"<<endl;
		return 0;
	}
	node* temp=rhead, *p;
	while(temp->pright!=rhead){
		p=temp;
		temp=temp->pright;
		if(temp->j==col){
			cout<<endl<<"检索该节点所走过的元素下标如上。"<<endl;
			cout<<"该节点的值为："<<temp->value<<endl;
			return 1;
		}
		else cout<<"("<<temp->i<<","<<temp->j<<") -> ";
	}
	cout<<"No Such Node!"<<endl;
	return 0;
}


bool SearchNodeByVlue(node* head,int val)
{
	int n=0;
	node* rhead=head->pdown;
	for( ; rhead!=head; rhead=rhead->pdown){
		if(rhead->pright==NULL) continue;
		node* temp=rhead;
		while(temp->pright!=rhead){
			temp=temp->pright;
			if(temp->value==val){
				cout<<"\n具有该值的一个节点的行列下标为："<<"("<<temp->i<<","<<temp->j<<")"<<endl;
				cout<<"检索该节点所走过的元素下标如上。\n"<<endl;
				n++;
			}
			else cout<<"("<<temp->i<<","<<temp->j<<") -> ";
		}
	}

	if(!n){
		cout<<"\nNo Such Node!"<<endl;
		return 0;
	}
	else{
		cout<<"\n\t共检索到"<<n<<"个值为"<<val<<"的节点。"<<endl;
	}

	return 1;
}


//清除所有节点，用于ReadFromFile()函数中，其实也可以作为一个操作选项
void DeleteAllNodes(node* head)
{
	node* rhead=head->pdown;
	while(rhead!=head){
		
		if(rhead->pright!=NULL){

			node* temp=rhead, *p;
			bool flag=0;
			while(temp->pright!=rhead){
				p=temp;
				temp=temp->pright;
				if(temp->j<=head->j)  { flag=1; rhead->num--; break; }
			}
		
			
			if(temp->pdown!=NULL){
				node* q=temp->pdown;
				while(q->pdown!=temp) {
					q=q->pdown; 
					if(q->i==0) q->num--; 
				}
				
				p->pright=temp->pright;
				q->pdown=temp->pdown;
				
				if(p->pright==p) p->pright=NULL;
				if(q->pdown==q)  q->pdown=NULL;
				
				delete temp;
				head->num--;
			}
		}

		rhead=rhead->pdown;
	}

	
}


bool WriteToFile(node* head)
{
	ofstream fout;
	fout.open("MatrixList.txt");
	if(fout.fail()) { cout<<"File not open!"<<endl; return 0;}
	
	node* rhead=head->pdown,*p;
	
	while(rhead!=head){
		
		if(rhead->pright!=NULL){
			p=rhead->pright;
			while(p!=rhead){
				fout<<'('<<p->i<<','<<p->j<<','<<p->value<<")  ";
				p=p->pright;
			}
			fout<<"\n";
		}	
		rhead=rhead->pdown;
	}
	
	return 1;
}


bool ReadFromFile(node* head)
{
	ifstream fin;
	fin.open("MatrixList.txt");
	if(fin.fail()) { cout<<"File not open!"<<endl; return 0; }
	DeleteAllNodes(head);

	int intvalue;
	char ch;
	int flag=1; 
	bool ok=0;
	node* newnode=new node;

	while(fin.get(ch)){
		if(ch=='('||ch==')'||ch=='\n'||ch==','||ch==' ');
		else{
			//读到数字字符，则先将此字符放回，再抽取一个整数值，以此技巧得到文件中的数值
			fin.putback(ch);    
			fin>>intvalue;
			if(flag==1){ newnode->i=intvalue; flag=2;}
			else{
				if(flag==2){ newnode->j=intvalue; flag=3;}
					else if(flag==3){ newnode->value=intvalue; flag=1; ok=1;}
			}
		}
		if(ok){
			if(!InsertNode(head,newnode))  return 0; 
			newnode=new node;
			ok=0;
		}
	}	
	
	return 1;	

}


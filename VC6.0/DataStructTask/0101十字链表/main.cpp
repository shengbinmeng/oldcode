#include <iostream>
#include <conio.h>
#include "main.h"

using namespace std;


int main()
{
	char ch;
	node* Head;
	Initialize(Head,N,M);
	while(1){
		cout<<"*******************************************"<<endl;          //endl换行并清空输出流
		cin.clear();   cin.sync();       //clear清除标记位，sync同步输入输出流（相当于清空输入缓冲区）

		cout<<"请选择操作：１－输入数据　２－输出到屏幕　３－检索节点　４－删除节点\n"
						<<"             ５－存盘　　６－从硬盘读取　　Ｑ－退出"<<endl;
		cin>>ch; cin.ignore(20,'\n');              
		switch(ch){
		case '1':
			InputData(Head);
			break;

		case '2':
			Display(Head);
			break;

		case '3':
			int row, col, val;
			cout<<"By Value(V) or By Row/Col(R) ?"<<endl;
			char choice;
			cin>>choice; cin.ignore(10,'\n');
			if(choice=='V'||choice=='v'){
				cout<<"输入要检索的值：";cin>>val;
				SearchNodeByVlue(Head,val);
			}
			else{
				if(choice=='R'||choice=='r'){
					cout<<"输入要检索的节点的行、列："<<endl;
					cin>>row>>col; cin.ignore(10,'\n');
					if(cin.fail()){cin.clear(); cin.sync(); cout<<"Input Error!"<<endl; break;}
					SearchNode(Head,row,col);
				}
				else cout<<"Input Error!"<<endl;
			}

			break;

		case '4':
			cout<<"输入要删除的节点的行、列："<<endl;
			cin>>row>>col;
			if(cin.fail()){cin.clear(); cout<<"Input Error!"<<endl; }
			else if(DeleteNode(Head,row,col)) cout<<"已删除！"<<endl;
			break;

		case '5':
			if(WriteToFile(Head)) cout<<"已存盘！"<<endl;
			break;
			
		case '6':
			if(ReadFromFile(Head)) cout<<"已读入！"<<endl;
			break;

		case 'q':
			exit(0);
		default:
			cout<<"Input Error!"<<endl;
		}
	}

	return 0;	
}


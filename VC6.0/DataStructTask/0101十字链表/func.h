//func.h

struct node 
{
	int i;
	int j;
	union{
		int value;
		int num;
	};
	node* pdown;
	node* pright;
};


bool InsertNode(node* head ,node* inode);
void InputData(node* head);
void Initialize(node* &head,int n,int m);
void Display(node* head);
bool DeleteNode(node* head, int row, int col);
bool SearchNode(node* head,int row, int col);
bool SearchNodeByVlue(node* head,int val);
bool WriteToFile(node* head);
bool ReadFromFile(node* head);
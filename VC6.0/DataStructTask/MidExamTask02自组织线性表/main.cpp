#include <iostream>
#include <cstring>
using namespace std;

const int MAX=100;

struct node{
	int id;
	char name[50];
	int freq;
};


int main()
{
	node list[MAX];
	list[0].id=0;       //储存表中元素个数
	list[0].freq=-1;
	int key;

	for(;;){
		cout<<"*****附加功能：输入999999可查看表中内容及其访问频率*****"<<endl;
		cout<<"输入要检索的学生学号：（输入任意非整数字符结束程序）"<<endl;
		cin>>key;
		if(cin.fail()) break;
		if(key==999999){
			if(list[0].id==0){ cout<<"表空！"<<endl<<endl; continue; }
			else{
				for(int i=1;i<=list[0].id;i++)
					cout<<"学号："<<list[i].id<<"  姓名："<<list[i].name<<"  访问频率："<<list[i].freq<<endl;
				cout<<endl;
				continue;
			}
		}

		int n=list[0].id,i,j;
		for(i=1;i<=n;i++){
			if(list[i].id==key){
				list[i].freq++;                              //频率增加
				
				cout<<"检索到结果如下："<<endl;
				cout<<"学号："<<list[i].id<<"  姓名："<<list[i].name<<endl;
				cout<<endl;
				
				//下面根据访问频率相应的调整线性表
				j=i;
				while(list[j].freq>list[j-1].freq && list[j-1].freq>=0){
					node temp=list[j-1];
					list[j-1]=list[j];
					list[j]=temp;
					j--;
				}
				
				break;
			}
			else continue;
		}

		if(i>n){
			for(;;){                             //这个循环只是为了防止用户不断Input Error
				cout<<"检索的数据不存在！要加入此记录吗？(Y/N)"<<endl;
				char ch;
				cin>>ch;cin.ignore(10,'\n');
				if(ch=='y'||ch=='Y'){
					list[n+1].id=key;
					cout<<"输入学号为 "<<key<<" 的学生姓名：";
					cin>>list[n+1].name;
					list[n+1].freq=1;
					list[0].id++;
					
					cout<<"已加入！\n"<<endl;
					break;	
				}
				if(ch=='n'||ch=='N') break;
				cout<<"Input Error!"<<endl;
			}
		}
		
	}
		
	return 0;	
}


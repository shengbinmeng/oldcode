
struct treenode{
	int key;
	treenode* left;
	treenode* right;
};

//循环队列的类定义：其中rear指向队尾元素，front指向队首元素的前一位
class Queue 
{
	treenode* tData[100];
	int front;
	int rear;
	int count;
public:
	Queue(){front=rear=3; count=0;}             //初始的front和rear值可任意指定，只要二者相等，就表示空队
	bool Qin(treenode* n);
	bool Qout(treenode* &x);
};

bool Queue::Qin(treenode* n)
{
	if(front==(rear+1)%10){
		return 0;
	}
	else{
		rear=(rear+1)%10;
		tData[rear]=n;
		count++;
		
		return 1;
	}
}

bool Queue::Qout(treenode* &x)
{
	if(front==rear){
		return 0;
	}
	else{
		front=(front+1)%10;
		x=tData[front];
		count--;
		
		return 1;
	}
}

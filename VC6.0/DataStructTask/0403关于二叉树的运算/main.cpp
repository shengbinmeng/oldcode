#include <iostream>
#include "queueclass.h"
using namespace std;

treenode* InsertNodes(treenode* &root);
int Deep(treenode* root);
void FindLeaf(Queue &q,treenode* root);
void Copy(treenode* src,treenode* &des);
bool IsEqual(treenode* r1,treenode* r2);


int main()
{
	treenode* Root=NULL;

	cout<<"先生成一棵二叉（排序）树,用以测试函数："<<endl;
	InsertNodes(Root);
	
	treenode* Root_Copy=NULL;
	cout<<"复制一棵二叉树..."<<endl;
	Copy(Root,Root_Copy);

	cout<<"判断两颗二叉树是否相等..."<<endl;
	if(IsEqual(Root,Root_Copy)) cout<<"相等！"<<endl;
	else cout<<"不等！"<<endl;

	cout<<"求得二叉树的深度："<<Deep(Root)<<endl;

	cout<<"求二叉树的叶子..."<<endl;
	Queue q;
	FindLeaf(q,Root);
	cout<<"结果为："<<endl;
	treenode* t;
	while(q.Qout(t)){
		cout<<t->key<<"  ";
	}
	cout<<endl;
	
	return 0;

}


//求深度
int Deep(treenode* root)
{
	int ld,rd;
	if(!root) return 0;
	ld=Deep(root->left);
	rd=Deep(root->right);
	if(ld>rd) return ld+1;
	else return rd+1;
}




//计算树叶（将树叶存在数组a中）：
void FindLeaf(Queue &q,treenode* root)
{
	if(!root) return ;
	FindLeaf(q,root->left);
	if(root->left==NULL && root->right==NULL){
		q.Qin(root);	
	}
	FindLeaf(q,root->right);
}

//复制src到des中：
void Copy(treenode* src,treenode* &des)
{
	if(src==NULL) return;

	des=new treenode;
	des->key=src->key;
	des->left=des->right=NULL;
	Copy(src->left,des->left);
	Copy(src->right,des->right);
	
}

//判断r1,r2是否相等（即内容完全相同）
bool IsEqual(treenode* r1,treenode* r2)
{
	if((r1==NULL && r2!=NULL)||(r2==NULL && r1!=NULL)) return 0;
	if(r1==NULL && r2==NULL) return 1;
	return IsEqual(r1->left,r2->left) && IsEqual(r1->right,r2->right);
}


//下面的函数用于生成一棵二叉排序树：
treenode* AddtoTree(treenode* &root, treenode* child, treenode* inode)
{
	if(child==NULL){
		child=inode;
		child->left=child->right=NULL;
		if(root==NULL) { root=child; return root;}
		if (child->key < root->key) root->left=child;
		else root->right=child;
		return root;
	}
	else{
		if(inode->key < child->key ) return AddtoTree(child,child->left,inode);
		else return AddtoTree(child,child->right,inode);
	}
	
}

treenode* InsertNodes(treenode* &root)
{
	int key;
	cout<<"输入关键码序列，以q或任意非数值字符结束："<<endl;
	cin>>key;
	while(1){
		if(cin.fail()) break;
		treenode* inode=new treenode;
		inode->key=key;
		inode->left=inode->right=NULL;
		AddtoTree(root,root,inode);
		
		cin>>key;
	}
	
	cout<<"已插入！"<<endl; 
	cin.clear(); 
	return root;
}
#include <iostream>
using namespace std;

float Operate(float A,char op,float B);
char Precede(char a,char b);

class NStack{
	float fData[100];
	int top;
public:
	NStack(){top=-1;}
	void push(float n)
	{
		top++;
		fData[top]=n;
	}

	float pop(float &n)
	{
		if(top>=0){
			n=fData[top];
			top--;
		}		
		return n;
	}

	float gettop()
	{
		if (top>=0){
			return fData[top];
		}
	}
};

class OStack 
{
	char cData[100];
	int top;
public:
	OStack(){top=-1;}
	void push(char ch)
	{
		top++;
		cData[top]=ch;
	}
	char pop(char& ch)
	{
		if(top>=0){
			ch=cData[top];
			top--;
		}
		return ch;
	}
	char gettop()
	{
		if(top>=0)
			return cData[top];
	};
};


/*
大致算法：
输入数字则压入数字堆栈，而输入运算符：如+ - * /（）等时候先和先前压入字符堆栈的运算符比较优先级，
优先级如果高于栈中的则入栈，因为其变为更要先计算的运算符了。低的话表示要先计算先前的运算符号，
则出栈，并且出栈2个数字与其运算。而后再继续比较当前缓冲中的运算符与栈顶运算的符优先级，
+ -或者* /等同级运算符被处理成先来者优先级高。所以相等情况只有2个左右括号
（）和表达式结束符，此时如果是（括号的情况则缓冲中的）括号被抛弃，栈顶的（也被出栈，即释放括号。
*/
 
//未考虑对用户的容错及其他错误处理
float main()
{
	NStack ns;
	OStack os;
	float n;
	char p;
	while(1){
		cout<<"输入待求值的表达式，以“;”结束：（退出计算器请输“q”）"<<endl;
		os.push(';');
		cin>>p;
		if(p=='q') return 1;

		while(p!=';'||os.gettop()!=';'){
			if(48<=p&&p<=57){								//如果是数字，以实数读入
				cin.putback(p);
				cin>>n;
				ns.push(n);
				cin>>p;
			}
			else{											//如是符号,比较优先级
				switch(Precede(p,os.gettop())){
				case'>':									//比栈顶优先级大，暂不需计算	
					os.push(p);
					cin>>p;
					break;
				case'=':					//和栈顶优先级相等
					char ch;
					os.pop(ch);				//一般为释放括号
					cin>>p;
					break;
				case'<':					//比栈顶优先级小，开始计算前边的数
					float a,b;
					char op;
					os.pop(op);
					ns.pop(b);
					ns.pop(a);	
					ns.push(Operate(a,op,b));
		
					break;
				}
			}
		
		}

		cout<<"结果为:"<<ns.gettop()<<endl;         //输出最后结果，存储于堆顶
	}

	return 0;
}


//计算函数
float Operate(float A,char op,float B)
{
    switch(op){
    case '+':return A+B;
    case '-':return A-B;
    case '*':return A*B;
    case '/':return A/B;		
    }	
}


//比较符号的优先顺序(调用时参数a是后到的符号，同级的后到则认为低）
//一个一个列出是为了更清楚
char Precede(char a,char b)    //返回<则开始运算前边
{
    switch(a){
    case '+':
        switch(b){
		case '+':return '<';
		case '-':return '<';
		case '*':return '<';
		case '/':return '<'; 
		case '(':return '>';
		case ')':return '<';
		case ';':return '>';
		}
    case '-':
		switch(b){
        case '+':return '<';
        case '-':return '<';
        case '*':return '<';
        case '/':return '<';
        case '(':return '>';
        case ')':return '<';
        case ';':return '>';
		}
	case '*':
		switch(b){
		case '+':return '>';
		case '-':return '>';
		case '*':return '<';
		case '/':return '<';
		case '(':return '>';
		case ')':return '<';
		case ';':return '>';
        }
	case '/':
		switch(b){
		case '+':return '>';
		case '-':return '>';
		case '*':return '<';
		case '/':return '<';
		case '(':return '>';
		case ')':return '<';
		case ';':return '>';
		}

	case '(':
		switch(b){
		case '+':return '>';
		case '-':return '>';
		case '*':return '>';
		case '/':return '>';
		case '(':return '>';
		case ')':return '>';
		case ';':return '>';
		}
	case ')':							//来大括号时也开始运算
		switch(b){
		case '+':return '<';
		case '-':return '<';
		case '*':return '<';
		case '/':return '<';
		case '(':return '=';
		case ')':return '<';
		}
	case ';':                           //一来分号就开始运算
		switch(b){
		case '+':return '<';
		case '-':return '<';
		case '*':return '<';
		case '/':return '<';
		case '(':return '<';
		case ')':return '<';
		case ';':return '=';
		}
			
    }
	
}
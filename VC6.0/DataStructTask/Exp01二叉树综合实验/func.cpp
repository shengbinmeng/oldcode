//func.cpp
//对有序二叉树的操作

#include "func.h"
#include <cmath>
#include <iostream>
#include <fstream>
using namespace std;


//此函数用在下边InsertNode()中
treenode* AddtoTree(treenode* &root, treenode* child, treenode* inode)
{
	if(child==NULL){
		child=inode;
		child->left=child->right=NULL;
		if(root==NULL) { root=child; return root;}
		if (child->score < root->score) root->left=child;
		else root->right=child;
		return root;
	}
	else{
		if(inode->score < child->score ) return AddtoTree(child,child->left,inode);
		else{
			if(inode->score > child->score ) return AddtoTree(child,child->right,inode);
			else{
				child->count++;
				child->name[child->count]=inode->name[1];
				child->num[child->count]=inode->num[1];

				return root;
			}
		}
	}
	
}


treenode* InsertNode(treenode* &root)
{
	treenode* inode=new treenode;
	cout<<"输入学生学号："<<endl;
	cin>>inode->num[1];
	cout<<"输入学生姓名："<<endl;
	cin>>inode->name[1];
	cout<<"输入学生成绩："<<endl;
	cin>>inode->score;
	while(inode->score<0 || inode->score>100){
		cout<<"输入0~100的有效成绩："<<endl;
		cin>>inode->score;
	}
	if(inode->score<50) inode->range="E";
	if(inode->score>=50 && inode->score<=59) inode->range="D";
	if(inode->score>=60 && inode->score<=65) inode->range="C";
	if(inode->score>=66 && inode->score<=69) inode->range="C+";
	if(inode->score>=70 && inode->score<=75) inode->range="B-";
	if(inode->score>=76 && inode->score<=79) inode->range="B";
	if(inode->score>=80 && inode->score<=85) inode->range="B+";
	if(inode->score>=86 && inode->score<=89) inode->range="A";
	if(inode->score>=90 && inode->score<=95) inode->range="AA";
	if(inode->score>=96) inode->range="AA+";

	inode->count=1;

	AddtoTree(root,root,inode);
	
	cout<<"已插入！"<<endl; 
	cin.clear(); 
	return root;
}

//将最小节点从树中脱离（但没有delete其内存），返回原来指向最小节点的指针
//排序树中的最小节点必然是左指针为空的，沿着左边走即可找到
treenode* freemin(treenode* &root)
{
	treenode *temp;
	if(!root)return(0);
	if(root->left) return(freemin(root->left));
	else {	
		temp=root;
		root=root->right;         //由于是传引用，此处root指向最小节点，同时也是其父节点的左指针域的别名
		return(temp);
	}
}

//删除指定学号的节点
//返回删除后的排序树的根节点
void remove(treenode* &root,int score)
{
	if(!root) return;
	treenode *p,*temp;
	if(root->score==score){
		if(root->left==root->right) {delete(root);root=NULL; }  //为叶子，直接删
		else{							
			if(root->left ==NULL){ p=root->right; delete(root); root=p; }	
			else{
				if(root->right ==NULL){ p=root->left; delete(root); root=p;}   //一侧为空，也好处理
				else{	 //若两侧均不空，为保持整棵树仍有序，需要以其右子树的最小节点取代之，其环境关系并不变
					temp=freemin(root->right);
					root->score=temp->score;
					root->count=temp->count;
					for(int i=1;i<=root->count;i++) root->name[i]=temp->name[i];
					for(i=1;i<=root->count;i++) root->num[i]=temp->num[i];
					root->range=temp->range;

					delete temp;
					//此处删除右子树的最小节点；freemin()中并未如此，因为要用到其值
				}
			}
		}
	}
	else{			
		if(root->score < score) remove(root->right,score);
		else remove(root->left,score);
	}
	
}

void del(treenode* &root,string num,int& score,bool& isfind)
{
	if(!root) return ;
	del(root->left,num,score,isfind);
	for(int i=1; i<=root->count;i++){
		if(root->num[i]==num){
			num[i]=num[root->count];
			root->count--;
			isfind=true;
			
			if(root->count==0)score=root->score;
		}
	}
	del(root->right,num,score,isfind);
}

void DeleteNode(treenode* &root)
{
	string num;
	cout<<"输入要删除的学生的学号："; 
	cin>>num;
	if(cin.fail()){ cout<<"Input Error!"<<endl;cin.clear(); return; };
	
	int score=-1;
	bool isfind=false;
	del(root,num,score,isfind);
	if(!isfind){ cout<<"无此学生信息！"<<endl; return;}
	if(score>0) remove(root,score);

	cout<<"已删除！"<<endl;
}


void SearchByNum(treenode* root, string num, bool &isfind)
{
	if(!root) return ;
	SearchByNum(root->left,num,isfind);
	for(int i=1; i<=root->count;i++){
		if(root->num[i]==num){
			cout<<"检索到信息如下："<<endl;
			cout<<"   学号："<<root->num[i]<<"  姓名："<<root->name[i]
				<<"  成绩"<<root->score<<"  成绩等级："<<root->range<<endl;
			isfind=1;
		}
	}
	SearchByNum(root->right,num,isfind);
}

void SearchByName(treenode* root, string name, bool &isfind)
{
	if(!root) return ;
	SearchByName(root->left,name,isfind);
	for(int i=1; i<=root->count;i++){
		if(root->name[i]==name){
			cout<<"检索到信息如下："<<endl;
			cout<<"   学号："<<root->num[i]<<"  姓名："<<root->name[i]
				<<"  成绩"<<root->score<<"  成绩等级："<<root->range<<endl;
			isfind=1;
		}
	}
	SearchByNum(root->right,name,isfind);
}

void SearchByScore(treenode* root, int score, bool &isfind)
{
	if(!root) return ;
	SearchByScore(root->left,score,isfind);
	if(root->score==score){
		cout<<"检索到信息如下："<<endl;
		cout<<"该成绩的学生共"<<root->count<<"个，分别为："<<endl;
		for(int i=1; i<=root->count; i++)
			cout<<"   学号："<<root->num[i]<<"  姓名："<<root->name[i]
				<<"  成绩"<<root->score<<"  成绩等级："<<root->range<<endl;

		isfind=1;
	}
	SearchByScore(root->right,score,isfind);
}
void SearchByRange(treenode* root,int low, int high, bool &isfind,int &total)
{
	if(!root) return ;
	SearchByRange(root->left,low,high,isfind,total);
	if(root->score>=low && root->score<=high){
		for(int i=1; i<=root->count; i++)
			cout<<"   学号："<<root->num[i]<<"  姓名："<<root->name[i]
				<<"  成绩"<<root->score<<"  成绩等级："<<root->range<<endl;

		total+=root->count;
		isfind=1;
	}
	SearchByRange(root->right,low,high,isfind,total);
}


//中序遍历函数，用在下边Display()中
void inorder(treenode* root)
{
	if(!root) return;
	inorder(root->left);
	cout<<"成绩："<<root->score<<"   此成绩的学生有："<<endl;
	for(int i=1;i<=root->count;i++) cout<<"   学号："<<root->num[i]<<"  姓名："<<root->name[i]<<endl;
	cout<<endl;
	inorder(root->right);
}

void Display_InOrder(treenode* root)
{
	if (!root)
	{
		cout<<"树空！"<<endl;
	}else{
		cout<<"内容如下："<<endl;
		inorder(root);
		cout<<endl;
	}
	
}

//下面用于实现按层输出
void Transfer(treenode* root, treenode* a[],int n)
{
	a[n]=root;
	if(root->left!=NULL){ Transfer(root->left,a,2*n);}
	if(root->right!=NULL){ Transfer(root->right,a,2*n+1);}
	
}
void Display_ByLevels(treenode* root)
{
	if (!root)
	{
		cout<<"树空！"<<endl;
	}else{

		int n=1;
		treenode* a[800];
		for(int i=0;i<800;i++)
			a[i]=(treenode*)(-1);
		
		Transfer(root,a,n);
		
		int m=1;
		cout<<"第1层："<<endl;
		for(i=1;i<800;i++){
			if(a[i]!=(treenode*)-1){
				cout<<"成绩："<<a[i]->score<<"   此成绩的学生有："<<endl;
				for(int j=1;j<=a[i]->count;j++) cout<<"   学号："<<a[i]->num[j]<<"  姓名："<<a[i]->name[j]<<endl;
			}
			
			if(i==pow(2,m)-1){ m++; cout<<"第"<<m<<"层："<<endl;}  //按层换行
		}
		cout<<"……";
	}
		
	cout<<endl;
}

void inorder1(treenode* root,int &total)
{
	if(!root) return;
	inorder1(root->left,total);
	total+=root->count;
	inorder1(root->right,total);
}
int GetCount(treenode* root)
{
	int total=0;
	inorder1(root,total);
	return total;
}

void inorder2(treenode* root,int &sum)
{
	if(!root) return;
	inorder2(root->left,sum);
	sum+=root->count * root->score;
	inorder2(root->right,sum);
}
int GetAvery(treenode* root)
{
	int sum=0;
	inorder2(root,sum);
	return sum/GetCount(root);
}

void inorder3(treenode* root, int &max, int &min)
{
	if(!root) return;
	inorder3(root->left,max,min);
	if(root->score>max) max=root->score;
	if(root->score<min) min=root->score;
	inorder3(root->right,max,min);
}
int GetMax(treenode* root)
{
	int max=0,min=100;
	inorder3(root,max,min);
	return max;
}
int GetMin(treenode* root)
{
	int max=0,min=100;
	inorder3(root,max,min);
	return min;
}

void inorder4(treenode* root, int &sum_2)
{
	if(!root) return;
	inorder4(root->left,sum_2);
	sum_2+=root->count * root->score * root->score;
	inorder4(root->right,sum_2);
}

int GetVariance(treenode* root)
{
	int sum_2=0;
	inorder4(root,sum_2);
	return sum_2/GetCount(root)-GetAvery(root)*GetAvery(root);
}

void inorder5(treenode* root,ofstream &fout)
{
	if(!root) return;
	inorder5(root->left,fout);
	for(int i=1;i<=root->count;i++){
// 		char* a=new char[20];
// 		strcpy(a,root->num[i].c_str());
// 		char* b=new char[20];
// 		strcpy(b,root->name[i].c_str());
// 		fout<<"   学号："<<a<<"  姓名："<<b<<"\n\t";
// 		delete a,b;
		
		//可直接这样：
		fout<<"Num:"<<root->num[i].c_str()<<"/Name:"<<root->name[i].c_str()<<"/Score:"<<root->score<<"\n";
	}
	inorder5(root->right,fout);
}
int WriteToFile(treenode* root)
{
	if(!root){
		cout<<"内容为空。存为空文件？Y/N: "; 
		char ch; cin.get(ch);
		if(ch!='Y'&&ch!='y') return 0;
	}
	
	ofstream fout;
	fout.open("D:\\Data.txt");
	if(fout.fail()){ cout<<"File not open!"<<endl;  return 0;}

	inorder5(root,fout);
	fout.close();
	return 1;
}

void inorder6(treenode* root, treenode* &Root)
{
	if(!root) return;
	inorder6(root->left,Root);
	remove(Root,root->score);
	if(root) inorder6(root->right,Root);
}
void DeleteAll(treenode* &root)
{
	//inorder6(root,root);      此处出了点bug，一直没调好，所以先用下面方法实现功能
	for(int i=0;i<=100;i++) remove(root,i);
	
}
int ReadFromFile(treenode* &root)
{
	ifstream fin;
	fin.open("D:\\Data.txt");
	if(fin.fail()){ cout<<"File not open!"<<endl;  return 0;}
	
	DeleteAll(root);
	root=NULL;               

	char ch;
	while(fin.get(ch)){
		fin.putback(ch);
		treenode* inode=new treenode;
		char a[100],b[100];
		fin.ignore(4);
		fin.getline(a,100,'/');
		inode->num[1]=a;
		fin.ignore(5);
		fin.getline(b,100,'/');
		inode->name[1]=b;
		fin.ignore(6);
		fin>>inode->score;
		fin.ignore();            //忽略结尾的换行符

		if(inode->score<50) inode->range="E";
		if(inode->score>=50 && inode->score<=59) inode->range="D";
		if(inode->score>=60 && inode->score<=65) inode->range="C";
		if(inode->score>=66 && inode->score<=69) inode->range="C+";
		if(inode->score>=70 && inode->score<=75) inode->range="B-";
		if(inode->score>=76 && inode->score<=79) inode->range="B";
		if(inode->score>=80 && inode->score<=85) inode->range="B+";
		if(inode->score>=86 && inode->score<=89) inode->range="A";
		if(inode->score>=90 && inode->score<=95) inode->range="AA";
		if(inode->score>=96) inode->range="AA+";
	
		inode->count=1;
		
		AddtoTree(root,root,inode);
	}

	return 1;
}

int push(treenode* s[],treenode* &p,int top)
{
	top++;
	s[top]=p;

	return top;
}

int pop(treenode* s[],treenode* &rp,int top)
{
	rp=s[top];
	top--;

	return top;
}

treenode* inorder_clew(treenode* &root)
{
	treenode *p, *&rp=p, *pr=NULL, *s[100];
	int top=-1;
	long ia; 
	p=root;
	for(;;){
		while(p){ top=push(s,p,top); p=p->left; }
		if(top==-1)break;
		else{
			top=pop(s,rp,top);
			if(pr){
				if(!pr->right){
					ia=-(long)p;
					pr->right=(struct treenode *)ia;
				}
				else if(!p->left){
					ia=-(long)pr;	 
					p->left=(struct treenode *)ia;
				}
			}
			pr=p;  p=p->right;
		}
	}
	return 0;
}
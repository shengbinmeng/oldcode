//main.cpp

#include "func.h"
#include <iostream>
using namespace std;

int main()
{
	treenode* Root=NULL;
	char ch;
	while(1){
		cout<<"********************************************"<<endl; cin.sync();
		cout<<"请选择操作：1－插入学生信息 2－删除学生信息 3－中序遍历输出 4－层次遍历输出 "
			<<"5-检索 6－显示学生总数 7－成绩统计数据（均值、最值、方差） "
			<<"8－存盘 9－从硬盘读入 A－中序线索化 q-退出程序"<<endl;
		cin>>ch; cin.ignore(10,'\n');
		switch (ch)
		{
		case '1':
			InsertNode(Root);
			break;			
		case '2':
			DeleteNode(Root);
			break;
		case '3':
			Display_InOrder(Root);
			break;
		case '4':
			Display_ByLevels(Root);
			break;
		case '5':
			cout<<"请选择检索方式：1－指定学号 2－指定姓名 3－指定分数 4－指定分数范围（须按成绩段整合）"<<endl;
			cin>>ch; cin.ignore(10,'\n');
			if(ch=='1'){
				bool isfind=0;
				string num;
				cout<<"请输入要检索的学号："; cin>>num;
				SearchByNum(Root,num,isfind);
				if(!isfind) cout<<"经检索，未找到！"<<endl;

				break;		
			}
			if(ch=='2'){
				bool isfind=0;
				string name;
				cout<<"请输入要检索的姓名："; cin>>name;
				SearchByName(Root,name,isfind);
				if(!isfind) cout<<"经检索，未找到！"<<endl;
				
				break;		
			}
			if(ch=='3'){
				bool isfind=0;
				int score;
				cout<<"请输入要检索的成绩："; cin>>score;
				SearchByScore(Root,score,isfind);
				if(!isfind) cout<<"经检索，未找到！"<<endl;
				
				break;		
			}
			if(ch=='4'){
				bool isfind=0;
				int low,high,total=0;
				cout<<"请指定分数范围："<<endl;
				cout<<"最低分："; cin>>low; 
				cout<<"最高分："; cin>>high;
				cout<<"检索结果如下："<<endl;
				SearchByRange(Root,low,high,isfind,total);

				cout<<"此分数范围内学生个数为： "<<total<<endl;

				break;
			}

			cout<<"Input Error!"<<endl;
			break;
		case '6':
			cout<<"学生总数为："<<GetCount(Root)<<endl;
			break;
		case '7':
			cout<<"统计信息如下："<<endl;
			cout<<"平均分："<<GetAvery(Root)<<"  最高分："<<GetMax(Root)<<"  最低分："
				<<GetMin(Root)<<"  方差："<<GetVariance(Root)<<endl;
			break;
		case '8':
			if(WriteToFile(Root)) cout<<"已存盘！"<<endl;
			else cout<<"未存盘！"<<endl;
			break;
		case '9':
			if(ReadFromFile(Root)) cout<<"已读入！"<<endl;
			else cout<<"未读入！"<<endl;
			break;
		case 'a':
		case 'A':
			inorder_clew(Root);
			cout<<"已做中序线索化！"<<endl;
			break;
		case 'q':
		case 'Q':

			exit(0);
		default:
			cout<<"Input Error!"<<endl;		
		}
	}
	
	return 0;
}


//func.h
#include <string>
using namespace std;

struct treenode{
	char* range;
	int score;
	int count;
	string num[20];
	string name[20];
	treenode* left;
	treenode* right;
};

//凡是可能需要改变root的函数，都需要引用传参数

treenode* AddtoTree(treenode* &root, treenode* child, treenode* inode);
treenode* InsertNode(treenode* &root);

treenode* freemin(treenode* &root);
void remove(treenode* &root,int score);
void DeleteNode(treenode* &root);

void inorder(treenode* root);
void Display_InOrder(treenode* root);
void Display_ByLevels(treenode* root);

void SearchByNum(treenode* root, string num,bool &isfind);
void SearchByName(treenode* root, string name,bool &isfind);
void SearchByScore(treenode* root, int score,bool &isfind);
void SearchByRange(treenode* root,int low, int high,bool &isfind,int &total);


int GetCount(treenode* root);
int GetAvery(treenode* root);
int GetMax(treenode* root);
int GetMin(treenode* root);
int GetVariance(treenode* root);

int WriteToFile(treenode* root);
int ReadFromFile(treenode* &root);

treenode* inorder_clew(treenode* &root);
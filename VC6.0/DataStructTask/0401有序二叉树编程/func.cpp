//func.cpp
//对有序二叉树的操作

#include "func.h"
#include <iostream>
using namespace std;


//此函数用在下边InsertNode()中
treenode* AddtoTree(treenode* &root, treenode* child, treenode* inode)
{
	if(child==NULL){
		child=inode;
		child->left=child->right=NULL;
		if(root==NULL) { root=child; return root;}
		if (child->key < root->key) root->left=child;
		else root->right=child;
		return root;
	}
	else{
		if(inode->key < child->key ) return AddtoTree(child,child->left,inode);
		else return AddtoTree(child,child->right,inode);
	}
	
}


treenode* InsertNodes(treenode* &root)
{
	int key;
	cout<<"输入关键码序列，以q或任意非数值字符结束："<<endl;
	cin>>key;
	while(1){
		if(cin.fail()) break;
		treenode* inode=new treenode;
		inode->key=key;
		inode->left=inode->right=NULL;
		AddtoTree(root,root,inode);
		
		cin>>key;
	}

	cout<<"已插入！"<<endl; 
	cin.clear(); 
	return root;
}

//将最小节点从树中脱离（但没有delete其内存），返回原来指向最小节点的指针
//排序树中的最小节点必然是左指针为空的，沿着左边走即可找到
treenode* freemin(treenode* &root)
{
	treenode *temp;
	if(!root)return(0);
	if(root->left) return(freemin(root->left));
	else {	
		temp=root;
		root=root->right;         //由于是传引用，此处root指向最小节点，同时也是其父节点的左指针域的别名
		return(temp);
	}
}

//删除指定关键字的节点（此节点不存在时还没有提示处理）
//返回删除后的排序树的根节点
bool remove(treenode* &root,int key)
{
	bool isfind=0;
	treenode *p,*temp;
	if(!root)return(NULL);
	if(root->key==key){
		isfind=1;
		if(root->left==root->right) {delete(root);root=NULL; }  //为叶子，直接删
		else{							
			if(root->left ==NULL){ p=root->right; delete(root); root=p; }	
			else{
				if(root->right ==NULL){ p=root->left; delete(root); root=p;}   //一侧为空，也好处理
				else{	 //若两侧均不空，为保持整棵树仍有序，需要以右子树的最小节点取代之，其环境关系并不变
					temp=freemin(root->right);
					root->key=temp->key;
					delete temp;  //此处删除右子树的最小节点；freemin()中并未如此，因为要用到其值
				}
			}
		}
	}
	else{			
		if(root->key < key) isfind=remove(root->right,key);
		else isfind=remove(root->left,key);
	}
	
	return isfind;
}


void DeleteNode(treenode* &root)
{
	int key;
	cout<<"输入要删除的节点的关键码："; 
	cin>>key;
	if(cin.fail()){ cout<<"Input Error!"<<endl;cin.clear(); return; };
	
	if(remove(root, key))
		cout<<"已删除！"<<endl;
	else cout<<"无此节点！"<<endl;
}

//中序遍历函数，用在下边Display()中
void inorder(treenode* root)
{
	if(!root) return;
	inorder(root->left);
	cout<<root->key<<"   ";
	inorder(root->right);
}

void Display(treenode* root)
{
	if (!root)
	{
		cout<<"树空！"<<endl;
	}else{
		cout<<"内容如下："<<endl;
		inorder(root);
		cout<<endl;
	}
	
}
//func.h

struct treenode{
	int key;
	treenode* left;
	treenode* right;
};

//凡是可能需要改变root的函数，都需要引用传参数

treenode* AddtoTree(treenode* &root, treenode* child, treenode* inode);
treenode* InsertNodes(treenode* &root);

treenode* freemin(treenode* &root);
bool remove(treenode* &root,int key);
void DeleteNode(treenode* &root);

void inorder(treenode* root);
void Display(treenode* root);
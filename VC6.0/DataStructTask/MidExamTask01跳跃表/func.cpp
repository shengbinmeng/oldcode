#include "func.h"
#include <iostream>
using namespace std;

int GetLevel(int& pml)
{
	int i,j,r;
	r=rand();
	for(i=0,j=2;i<MAX_LEVEL;i++,j=2*j) if(r>RAND_MAX/j) break;     //用课件上的指数算法来产生随机指针级数
	if(i==MAX_LEVEL) i--;            //此处若没有的话可能会导致数组越界
	if(i>pml) pml=i;
	return i;
}

int InsertNode(node* &head,node* innode,int& pml)
{
	if(SearchNode(head,innode->id,pml)) return -1;  //重复性检查（关键码不能相同） 
	if(head==NULL){head=new node; head->id=-1; for(int i=0;i<MAX_LEVEL;i++) head->forward[i]=NULL;}
		//head不存储有效信息，只做为表的标志，其关键码id最小（为负）

	node* p=head;
	node* pre[MAX_LEVEL];
	int level=GetLevel(pml);        //会保证level<=pml;

	for(int i=pml;i>=0;i--){
		while(p->forward[i]!=NULL && p->forward[i]->id<innode->id) p=p->forward[i];	
		pre[i]=p;                   //数组中保存了p的各级前驱
		}

	for(i=0;i<=level;i++){
		innode->forward[i]=pre[i]->forward[i];
		pre[i]->forward[i]=innode;                      
	}
	
	return 0;
}


int DeleteNode(node* &head,int key,int pml)
{
	node* p=head;
	node* pre[MAX_LEVEL];
	
	for(int i=pml;i>=0;i--){
		while(p->forward[i]!=NULL && p->forward[i]->id<key) p=p->forward[i];	
		pre[i]=p;
	}
	
	if(p->forward[0]==NULL){            
		if(p->id==key){
			for(i=0;i<=pml;i++) pre[i]->forward[i]=p->forward[i];
			delete p;                    
			return 0;
		}
		else return -1;
	}
	else{
		p=p->forward[0];
		if(p->id==key){
			for(i=0;i<=pml;i++) pre[i]->forward[i]=p->forward[i];
			delete p; 
			return 0;
		}
		else return -1;
	}
	
	return -1;
}


node* SearchNode(node* head,int key,int pml)
{
	node* p=head;
	if(!p) return NULL;
 	for(int i=pml;i>=0;i--){
		while(p->forward[i]!=NULL && p->forward[i]->id<key) p=p->forward[i]; 
		//退出while说明p已到达了表尾或到达了[不小于key的节点]的前一节点，该降级了；i--即回到低级指针，精细搜索

/////////////////////////////////////////////////////////////////////////////////////////////
		if(p->forward[i]==NULL){             //若p到了表尾，看是否恰是key
			if(p->id==key) return p;
		}
		else if(p->forward[i]->id==key) return p->forward[i]; //若此时p->forward[i]->id恰好是key，则成功返回

	//*****这之间的内容其实可以不要，但加上会减少无用的for循环******//
/////////////////////////////////////////////////////////////////////////////////////////////

	}
	
	//退出for时，说明已经精细到最低级指针，即连续搜索，且p已到达表尾或不小于key的节点的前一节点，
	//这时要么p是key要么紧邻的后一节点是key，否则检索失败,故进行下面判断:

	if(p->forward[0]==NULL){             //若p到了表尾，看是否恰是key
		if(p->id==key) return p;
		else return NULL;
		}
	else{
		p=p->forward[0];				//看p的下一邻节点食否key
		if(p->id==key) return p;
		else return NULL;
	}
	
}

int Display(node* head)
{
	if(head->forward[0]==NULL) return -1;      //表空
	node* p=head->forward[0];
	while(p!=NULL){
		cout<<p->id<<"  "<<p->name<<endl;
		p=p->forward[0];
	}
	return 0;
}




/*先经由SearchNode判断是否存在可删除的信息，确定有的话，可以用这个删除函数

void DeleteNode2(node* &head,int key,int pml)
{
	node* p=head;
	node* pre[MAX_LEVEL]={0};
	
	for(int i=pml;i>=0;i--){
		while(p->forward[i]!=NULL && p->forward[i]->id<key) p=p->forward[i];	
		pre[i]=p;
	}
	//至此p指向要删除的节点，pre[i]存了其各级前驱
	
	if(p->forward[0]==NULL){             
		for(i=0;i<=pml;i++){
			pre[i]->forward[i]=p->forward[i];
		}
	}
	else{
		p=p->forward[0];
		for(i=0;i<=pml;i++){
			pre[i]->forward[i]=p->forward[i];
		}
	}
	
	delete p;
}

*/
#include <string>
const int MAX_LEVEL=8;
using namespace std;

struct node{
	int id;
	string name;
	node* forward[MAX_LEVEL];
};

int GetLevel(int& pml);
int InsertNode(node* &head,node* innode,int& pml);
int DeleteNode(node* &head,int key,int pml);
node* SearchNode(node* head,int key,int pml);
int Display(node* head);

/*��һ��ɾ������
void DeleteNode2(node* &head,int key,int pml);
*/
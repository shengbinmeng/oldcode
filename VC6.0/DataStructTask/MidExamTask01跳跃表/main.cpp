#include <iostream>
#include "func.h"
using namespace std;

int main()
{
	node* Head=NULL;
	int pml=0;                       //当前最大指针级数
	char ch;
	while(1){
		cout<<"********************************************"<<endl; cin.sync();
		cout<<"请选择操作：1－插入节点 2－删除节点 3－检索节点 4－列表输出 q－退出"<<endl;
		cin>>ch; cin.ignore(10,'\n');
		switch (ch)
		{
		case '1':
			node* newnode;
			newnode=new node;
			cout<<"输入要插入的学生学号："<<endl;
			cin>>newnode->id;
			if(cin.fail()){cout<<"Input Error!"<<endl; cin.clear(); break;}      
			cout<<"输入要插入的学生姓名："<<endl;
			cin>>newnode->name;
			int i;
			for(i=0;i<MAX_LEVEL;i++) newnode->forward[i]=NULL;
			if(InsertNode(Head,newnode,pml)==-1) cout<<"此学号学生已存在！"<<endl;
			else cout<<"已插入！"<<endl;

			break;
		case '2':
			int key;
			cout<<"输入要删除的学生学号："<<endl;
			cin>>key;
			if(cin.fail()){cout<<"Input Error!"<<endl; cin.clear(); break;}
			
			if(DeleteNode(Head,key,pml)==-1) cout<<"要删除的学生信息不存在！"<<endl;
			else cout<<"已删除！"<<endl;
				//此处也可以先用SearchNode确认存在可删除的信息，然后用另一个DeleteNode2函数
			break;
		case '3':
			cout<<"输入要检索的学生学号："<<endl;
			cin>>key;
			if(cin.fail()){cout<<"Input Error!"<<endl; cin.clear(); break;}
			node* find;
			find=SearchNode(Head,key,pml);
			if(find){
				cout<<"检索到信息如下："<<endl;
				cout<<"学号："<<find->id<<"  姓名："<<find->name<<endl;
			}
			else cout<<"经检索，未找到！"<<endl;

			break;	
		case '4':
			cout<<"表中信息如下："<<endl;
			if(Display(Head)==-1) cout<<"空。"<<endl;
			break;
		case 'q':
		case 'Q':
			exit(0);
		default:
			cout<<"Input Error!"<<endl;		
		}
	}
	
	return 0;
}



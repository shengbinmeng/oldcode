#include <iostream>
using namespace std;
const int M=10;

//双向栈的类；只写了题目需要的方法函数
class stack
{
	int iData[M];
	int top1,top2;
public:
	stack(){top1=0;top2=M;}
	void push(int n);
};

int main()
{
	stack sta;
	int n;
	for(;;){
		cout<<"输入一个整数将其压入栈（以非数值字符结束）："<<endl;
		cin>>n;
		if(cin.fail()) break;
		sta.push(n);
	}
	
	return 0;
}


void stack::push(int n)
{
	if(top1==top2){
		cout<<"栈满！"<<endl;
		return ;
	}

	if(n%2==0){
		top2--;
		iData[top2]=n;
		cout<<"已入stack2！"<<endl;
	}
	else{
		top1++;
		iData[top1]=n;
		cout<<"已入stack1！"<<endl;
	}

}
//func.cpp

#include "func.h"
#include <iostream>
using namespace std;


//此函数用在下边InsertNode()中
treenode* AddtoTree(treenode* &root, treenode* child, treenode* inode)
{
	if(child==NULL){
		child=inode;
		child->left=child->right=NULL;
		if(root==NULL) { root=child; return root;}
		if (child->key < root->key) root->left=child;
		else root->right=child;
		return root;
	}
	else{
		if(inode->key < child->key ) return AddtoTree(child,child->left,inode);
		else return AddtoTree(child,child->right,inode);
	}
	
}


treenode* InsertNode(treenode* &root)
{
	int key;
	cout<<"输入关键码："; cin>>key;
	treenode* inode=new treenode;
	inode->key=key;
	inode->left=inode->right=NULL;
	
	cout<<"已插入！"<<endl;
	return AddtoTree(root,root,inode);
	
}

//将最小节点从树中脱离（但没有delete其内存），返回原来指向最小节点的指针
treenode* freemin(treenode* &root)
{
	treenode *temp;
	if(!root)return(0);
	if(root->left) return(freemin(root->left));
	else {	
		temp=root;
		root=root->right;
		return(temp);
	}
}

//删除指定关键字的节点（此节点不存在时还没有提示处理）
treenode* removehelp(treenode* root,int key)
{
	treenode *p,*temp;
	if(!root)return(NULL);
	if(root->key==key){
		if(root->left==root->right) {delete(root);return(NULL);}
		else{
			if(root->left ==NULL){ p=root->right; delete(root); return(p); }
			else{
				if(root->right ==NULL){ p=root->left; delete(root); return(p);}
				else{
					temp=freemin(root->right);
					root->key=temp->key;
					return(root);
				}
			}
		}
	}
	else{			
		if(root->key < key) root->right=removehelp(root->right,key);
		else root->left=removehelp(root->left,key);
		return(root);
	}
}


void DeleteNode(treenode* &root)
{
	int key;
	cout<<"输入要删除的节点的关键码："; 
	cin>>key;
	
	root=removehelp(root, key);
	cout<<"已删除！"<<endl;
}

//中序遍历函数，用在下边Display()中
void inorder(treenode* root)
{
	if(!root) return;
	inorder(root->left);
	cout<<root->key<<"   ";
	inorder(root->right);
}

void Display(treenode* root)
{
	if (!root)
	{
		cout<<"树空！"<<endl;
	}else{
		cout<<"内容如下："<<endl;
		inorder(root);
		cout<<endl;
	}
	
}
//func.h

struct treenode{
	int key;
	treenode* left;
	treenode* right;
};

//凡是需要改变root的函数，都需要引用传参数

treenode* AddtoTree(treenode* &root, treenode* child, treenode* inode);
treenode* InsertNode(treenode* &root);

treenode* freemin(treenode* &root);
treenode* removehelp(treenode* root,int key);
void DeleteNode(treenode* &root);

void inorder(treenode* root);
void Display(treenode* root);
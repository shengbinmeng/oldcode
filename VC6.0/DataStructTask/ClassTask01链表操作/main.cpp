#include<iostream.h>
#include <stdio.h>
#include <conio.h>

struct node {
	int key;
	struct node *next;
};
node* input();
node* Rinsert(node* pIn,node* last);
void list(node* head);

int main(void)
{
	struct node *last=NULL,*head=NULL, *p;
	int i=0,key=0;
	while(i==0){
		printf("select:I(nput) or L(ist) or Q(uit)\n");	
		switch(getch()){
		case 'i': 
			p=input();last=Rinsert(p,last);list(last->next);
			break;
		case 'l': 
			head=last;
			if(head)list(head->next);
			else cout<<"表空"<<endl;
			break;
		case 'q': i=1; 
			break; 
		default:
			cout<<"Input Error!"<<endl;
		}
	}
	return(0);
}


node* input()
{
	node* pNode=new node;
	cout<<"Input your node key:"<<endl;
	cin>>pNode->key;
	return pNode;
}


node* Rinsert(node* pIn,node* last)       //插入一个节点，并返回表尾指针（表尾指Key最大的节点）
{
	if (last==NULL){
		last=pIn;
		last->next=last;
		return last;
	}
	node *head=last->next;

	node *p=head, *q=p;
	
	do{
		if (pIn->key > p->key)
		{
			q=p;
			p=p->next;
			
		}else{
			if(p==head){last->next=pIn;pIn->next=head;return last;}
			pIn->next=p;
			q->next=pIn;
			return last;
		}
	}while(p!=head);          //跳出循环说明q已取到表尾，插入的节点将作为表尾
	
	pIn->next=p;
	last->next=pIn; 
	return pIn;
	
}

void list(node* head)     //从表头遍历链表
{
	if(head==NULL)
	{
		cout<<"NULL!"<<endl;
		return;
	}
	
	node* p=head;
	cout<<"The list is as follows:"<<endl;
	
	do 
	{
		cout<<p->key<<endl;
		p=p->next;
	} while(p!=head);
	
	cout<<"****************************"<<endl;
	
}

#include <iostream>
#include <cmath>
using namespace std;

struct treenode{
	int key;
	treenode* left;
	treenode* right;
};

void InsertNodes(treenode* &root, int a[],int min,int max);
int Deep(treenode* root);
void Display(treenode* root);
void Display2(treenode* root);
void Display3(treenode* root);
int SLC(treenode* root,int key);

void main()
{
	treenode* Root=NULL;
	int a[100];
	for(int i=1;i<100;i++) a[i]=i;                   //舍去数组中下标为零的元素，为使数据与下标一致
	cout<<"以1～99的序列生成二叉排序树..."<<endl;
	InsertNodes(Root,a,1,99);
	cout<<endl;
	
	cout<<"分别按中序和前序遍历输出此树,以验证生成正确（中序的话应该是按节点由小到大排列）："<<endl;
	Display(Root);
	Display2(Root);
	
	cout<<"这棵树的深度为："<<Deep(Root)<<endl;

	cout<<"在这棵树上检索1，49，51，99节点所走过的层数依次为："<<endl;
	cout<<SLC(Root,1)<<"  "<<SLC(Root,49)<<"  "<<SLC(Root,51)<<"  "<<SLC(Root,99)<<endl;

	cout<<"按层输出树的内容，以验证上面所求深度和层数正确："<<endl;
	Display3(Root);
	
}




//检索函数，返回找到的节点指针，此题用不到
treenode* Search(treenode* root,int key)
{
	if(root==NULL) return NULL;
	if(root->key==key) return root;
	if(root->key<key) return Search(root->right,key);
	else return Search(root->left,key);
}

//返回检索节点走过的层数（即是节点所在的层数）
int SLC(treenode* root,int key)
{
	if(root==NULL) return 0;
	if(root->key==key) return 1;
	if(root->key<key) return SLC(root->right,key)+1;
	else return SLC(root->left,key)+1;
}


//求深度的函数：
int Deep(treenode* root)
{
	int ld,rd;
	if(!root) return 0;
	ld=Deep(root->left);
	rd=Deep(root->right);
	if(ld>rd) return ld+1;
	else return rd+1;
}


//此函数用在下边InsertNodes()中
treenode* AddtoTree(treenode* &root, treenode* child, treenode* inode)
{
	if(child==NULL){
		child=inode;
		child->left=child->right=NULL;
		if(root==NULL) { root=child; return root;}
		if (child->key < root->key) root->left=child;
		else root->right=child;
		return root;
	}
	else{
		if(inode->key < child->key ) return AddtoTree(child,child->left,inode);
		else return AddtoTree(child,child->right,inode);
	}
	
}

//此函数用在下边InsertNodes()中:
int GetMaxLevel(int num)
{
	for(int i=1;;i++){
		if(pow(2,i-1)-1<num && num<=pow(2,i)-1) return i;
	}
}

//用存于数组a中的数列生成完全二叉排序树：
//（a中数列应是从小到大排列；若给出的不是如此，先进行排序后再调用此函数）
void InsertNodes(treenode* &root, int a[],int min,int max)
{
	int n=max-min+1;
	int maxlevel=GetMaxLevel(n);
	int i=max-pow(2,maxlevel-1)+1;
	int key=a[i];

	cout<<key<<"  ";
	treenode* inode=new treenode;
	inode->key=key;
	inode->left=inode->right=NULL;
	AddtoTree(root,root,inode);

	if(n==1) return;

	InsertNodes(root->left,a,min,i-1);
	InsertNodes(root->right,a,i+1,max);

}


//中序遍历函数，用在下边Display()中
void inorder(treenode* root)
{
	if(!root) return;
	inorder(root->left);
	cout<<root->key<<"   ";
	inorder(root->right);
}

//中序显示树的内容
void Display(treenode* root)
{
	if (!root)
	{
		cout<<"树空！"<<endl;
	}else{
		cout<<"内容如下："<<endl;
		inorder(root);
		cout<<endl;
	}
	
}

//下面两函数前序显示树的内容：
void preorder(treenode* root)
{
	if(!root) return;
	cout<<root->key<<"   ";
	preorder(root->left);
	preorder(root->right);
}
void Display2(treenode* root)
{
	if (!root)
	{
		cout<<"树空！"<<endl;
	}else{
		cout<<"内容如下："<<endl;
		preorder(root);
		cout<<endl;
	}
	
}


//下面两函数用递归的方法按层输出树的内容：
void Transfer(treenode* root, int a[],int n)
{
	a[n]=root->key;
	if(root->left!=NULL){ Transfer(root->left,a,2*n);}
	if(root->right!=NULL){ Transfer(root->right,a,2*n+1);}
	
}

void Display3(treenode* root)
{
	int n=1;
	int a[200];
	for(int i=0;i<200;i++)
		a[i]=-999;
	
	Transfer(root,a,n);
	
	int m=1;
	for(i=1;i<200;i++){
		if(a[i]!=-999){
			cout<<a[i]<<"  ";
		}
		
		if(i==pow(2,m)-1){cout<<"         ——第"<<m<<"层"<<endl; m++;}  //按层换行
	}
	
	cout<<endl;
	
}

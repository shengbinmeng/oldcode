#include<iostream>
using namespace std;
const int MAX=20;
bool IsSolved(int s, int n, int w[]);

int Most(int n,int space,int w[]);
void main()
{
	int s,n,w[MAX];
	cout<<"输入背包可以放入的物品总重量：";
	cin>>s;
	cout<<"输入物品个数：";
	cin>>n;
	cout<<"依次输入"<<n<<"个物品的重量：";
	for(int i=1;i<=n;i++) cin>>w[i];   //数组编号从1开始使用
	
	cout<<"对于能否从这 "<<n<<" 件物品中选择若干件放入到背包中，\n使得放入物品的重量之和正好为 "<<s<<" 的问题："<<endl;
	if(IsSolved(s,n,w)) cout<<"有解！"<<endl;
	else cout<<"无解！"<<endl;

}


int max(int a, int b){ return a>b?a:b ;}
//此函数返回剩余n个未放入时的最大可容重量
int Most(int n,int space,int w[])     
{
	if (n==1) return space<w[1]? 0 : w[1];  	
	if (space < w[n]) return Most(n-1,space,w);
	else return  max( Most(n-1,space,w), w[n]+Most(n-1,space-w[n],w));    
}


bool IsSolved(int s, int n, int w[])
{
	if(s==Most(n,s,w)) return true;
	else return false;
}

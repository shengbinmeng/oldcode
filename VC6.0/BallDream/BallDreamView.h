// BallDreamView.h : interface of the CBallDreamView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_BALLDREAMVIEW_H__5C3DE247_3CA9_4951_94A0_975C1554280A__INCLUDED_)
#define AFX_BALLDREAMVIEW_H__5C3DE247_3CA9_4951_94A0_975C1554280A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CBallDreamView : public CView
{
protected: // create from serialization only
	CBallDreamView();
	DECLARE_DYNCREATE(CBallDreamView)

// Attributes
public:
	CBallDreamDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBallDreamView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CBallDreamView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	int i,j,a,b,a2,b2;
	int Dx, Dy,Da,Db,Da2,Db2;
	int nSpeed,Score;
	bool flag;
	int Time;

// Generated message map functions
protected:
	//{{AFX_MSG(CBallDreamView)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnCsX1();
	afx_msg void OnCsX2();
	afx_msg void OnCsX3();
	afx_msg void OnCsX4();
	afx_msg void OnCsX5();
	afx_msg void OnCsX10();
	afx_msg void OnCsX20();
	afx_msg void OnCsX50();
	afx_msg void OnCsX100();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in BallDreamView.cpp
inline CBallDreamDoc* CBallDreamView::GetDocument()
   { return (CBallDreamDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BALLDREAMVIEW_H__5C3DE247_3CA9_4951_94A0_975C1554280A__INCLUDED_)

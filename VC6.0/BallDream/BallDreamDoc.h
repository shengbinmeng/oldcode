// BallDreamDoc.h : interface of the CBallDreamDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_BALLDREAMDOC_H__346D7BF5_5EA0_48EC_9316_70337F9D5C4A__INCLUDED_)
#define AFX_BALLDREAMDOC_H__346D7BF5_5EA0_48EC_9316_70337F9D5C4A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CBallDreamDoc : public CDocument
{
protected: // create from serialization only
	CBallDreamDoc();
	DECLARE_DYNCREATE(CBallDreamDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBallDreamDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CBallDreamDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CBallDreamDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BALLDREAMDOC_H__346D7BF5_5EA0_48EC_9316_70337F9D5C4A__INCLUDED_)

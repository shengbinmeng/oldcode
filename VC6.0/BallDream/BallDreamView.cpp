// BallDreamView.cpp : implementation of the CBallDreamView class
//

#include "stdafx.h"
#include "BallDream.h"

#include "BallDreamDoc.h"
#include "BallDreamView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBallDreamView

IMPLEMENT_DYNCREATE(CBallDreamView, CView)

BEGIN_MESSAGE_MAP(CBallDreamView, CView)
	//{{AFX_MSG_MAP(CBallDreamView)
	ON_WM_LBUTTONDOWN()
	ON_WM_TIMER()
	ON_WM_RBUTTONDOWN()
	ON_COMMAND(ID_CS_X1, OnCsX1)
	ON_COMMAND(ID_CS_X2, OnCsX2)
	ON_COMMAND(ID_CS_X3, OnCsX3)
	ON_COMMAND(ID_CS_X4, OnCsX4)
	ON_COMMAND(ID_CS_X5, OnCsX5)
	ON_COMMAND(ID_CS_X10, OnCsX10)
	ON_COMMAND(ID_CS_X20, OnCsX20)
	ON_COMMAND(ID_CS_X50, OnCsX50)
	ON_COMMAND(ID_CS_X100, OnCsX100)
	ON_WM_KEYDOWN()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBallDreamView construction/destruction

CBallDreamView::CBallDreamView()
{
	// TODO: add construction code here
	i=0;
	j=0;
	a=b=0;
	a2=b2=0;
	Dx=1;
	Dy=2;
	Da=10;Db=8;
	Da2=6;Db2=4;
	nSpeed=1;
	Score=0;
	flag=0;
	Time=0;


}

CBallDreamView::~CBallDreamView()
{
}

BOOL CBallDreamView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CBallDreamView drawing

void CBallDreamView::OnDraw(CDC* pDC)
{
	CBallDreamDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
	CRect rect;
	GetClientRect(&rect);

	if(flag==1){

	CString str;
	str.Format("Now your score is %d",Score);
	pDC->TextOut(rect.right-200,(rect.bottom-rect.top)/2,str);
	Time--;
	if(Time<0) flag=0;
	}

	pDC->TextOut(30,5,"Left-click to go, and Right-click to stop. You may choose speed by the <Change Speed>menu(C)");
	CBrush myBrush(RGB(200,0,200)),myBrush2(RGB(0,200,0));
	pDC->SelectObject(myBrush);
	pDC->Ellipse(i,j,i+20,j+20);
	pDC->SelectObject(myBrush2);
	pDC->Rectangle(a-40,rect.top+40,a+40,rect.top+60);
	pDC->Rectangle(a2+200-40,rect.top+40,a2+200+40,rect.top+60);
	//pDC->Rectangle(a+500,rect.top,a+500+20,rect.top+5);

	pDC->Rectangle(b-40,rect.bottom-60,b+40,rect.bottom-40);
	pDC->Rectangle(b2+300-40,rect.bottom-60,b2+300+40,rect.bottom-40);
	//pDC->Rectangle(b+700,rect.bottom,b+700+20,rect.bottom+5);
	a=a+Da;
	a2=a2+Da2;
	b=b+Db;
	b2=b2+Db2;
	i=i+nSpeed*Dx;
	j=j+nSpeed*Dy;

	if(((j>=20&&j<=60)&&((i-a>=-20&&i-a<=20)||(i-(a2+200)>=-20&&i-(a2+200)<=20)))||
		((rect.bottom-j>=20&&rect.bottom-j<=60)&&((i-b>=-20&&i-b<=20)||(i-(b2+300)>=-20&&i-(b2+300)<=20))))
	{
	pDC->Rectangle(0,0,rect.right,rect.bottom);
	Score++; 
	//可以在这里随分数升高而增加难度
	}


	if(i>=rect.right||i<=0) Dx=-Dx;
	if(j>=rect.bottom||j<=0) Dy=-Dy;
	if(a>=rect.right||a<=0) Da=-Da;
	if(b>=rect.right||b<=0) Db=-Db;
	if(a2+200>=rect.right||a2+200<=0) Da2=-Da2;
	if(b2+300>=rect.right||b2+300<=0) Db2=-Db2;

}

/////////////////////////////////////////////////////////////////////////////
// CBallDreamView printing

BOOL CBallDreamView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CBallDreamView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CBallDreamView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CBallDreamView diagnostics

#ifdef _DEBUG
void CBallDreamView::AssertValid() const
{
	CView::AssertValid();
}

void CBallDreamView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CBallDreamDoc* CBallDreamView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CBallDreamDoc)));
	return (CBallDreamDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CBallDreamView message handlers

void CBallDreamView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	SetTimer(1,20,NULL);
	CView::OnLButtonDown(nFlags, point);

}

void CBallDreamView::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	Invalidate();
	CView::OnTimer(nIDEvent);
}

void CBallDreamView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	KillTimer(1);
	flag=1; Time=200;
	CView::OnRButtonDown(nFlags, point);
}


void CBallDreamView::OnCsX1() 
{
	// TODO: Add your command handler code here
	nSpeed=1;
}

void CBallDreamView::OnCsX2() 
{
	// TODO: Add your command handler code here
	nSpeed=2;	
}

void CBallDreamView::OnCsX3() 
{
	// TODO: Add your command handler code here
	nSpeed=3;	
}

void CBallDreamView::OnCsX4() 
{
	// TODO: Add your command handler code here
	nSpeed=4;	
}

void CBallDreamView::OnCsX5() 
{
	// TODO: Add your command handler code here
	nSpeed=5;	
}

void CBallDreamView::OnCsX10() 
{
	// TODO: Add your command handler code here
	nSpeed=10;	
}

void CBallDreamView::OnCsX20() 
{
	// TODO: Add your command handler code here
	nSpeed=20;	
}

void CBallDreamView::OnCsX50() 
{
	// TODO: Add your command handler code here
	nSpeed=50;	
}

void CBallDreamView::OnCsX100() 
{
	// TODO: Add your command handler code here
	nSpeed=100;	
}

void CBallDreamView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	switch(nChar){
	case VK_LEFT:
		i=i-20; break;
	case VK_RIGHT:
		i=i+20;break;
	case VK_UP:
		j=j-20;break;
	case VK_DOWN:
		j=j+20;
	}

	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}

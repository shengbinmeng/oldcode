// BallDream.h : main header file for the BALLDREAM application
//

#if !defined(AFX_BALLDREAM_H__9AE47B77_4713_4594_B180_F8362FF35B7A__INCLUDED_)
#define AFX_BALLDREAM_H__9AE47B77_4713_4594_B180_F8362FF35B7A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CBallDreamApp:
// See BallDream.cpp for the implementation of this class
//

class CBallDreamApp : public CWinApp
{
public:
	CBallDreamApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBallDreamApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CBallDreamApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BALLDREAM_H__9AE47B77_4713_4594_B180_F8362FF35B7A__INCLUDED_)

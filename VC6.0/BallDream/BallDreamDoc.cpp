// BallDreamDoc.cpp : implementation of the CBallDreamDoc class
//

#include "stdafx.h"
#include "BallDream.h"

#include "BallDreamDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBallDreamDoc

IMPLEMENT_DYNCREATE(CBallDreamDoc, CDocument)

BEGIN_MESSAGE_MAP(CBallDreamDoc, CDocument)
	//{{AFX_MSG_MAP(CBallDreamDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBallDreamDoc construction/destruction

CBallDreamDoc::CBallDreamDoc()
{
	// TODO: add one-time construction code here

}

CBallDreamDoc::~CBallDreamDoc()
{
}

BOOL CBallDreamDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CBallDreamDoc serialization

void CBallDreamDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
		CString sFlag="msb3 file";
		ar<<sFlag;
	}

	else
	{
		// TODO: add loading code here
		CString sFlag;
		ar>>sFlag;
		if(sFlag!="msb2 file" && sFlag!="msb3 file")
			AfxThrowArchiveException(CArchiveException::badIndex);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CBallDreamDoc diagnostics

#ifdef _DEBUG
void CBallDreamDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CBallDreamDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CBallDreamDoc commands

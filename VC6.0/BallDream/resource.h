//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by BallDream.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_BALLDRTYPE                  129
#define ID_CS_X1                        32771
#define ID_CS_X2                        32772
#define ID_CS_X3                        32773
#define ID_CS_X4                        32774
#define ID_CS_X5                        32775
#define ID_CS_X10                       32776
#define ID_CS_X20                       32777
#define ID_CS_X50                       32778
#define ID_CS_X100                      32779

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32780
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

// MyStatusBar.cpp : implementation file
//

#include "stdafx.h"
#include "MyDraw.h"
#include "MyStatusBar.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMyStatusBar

CMyStatusBar::CMyStatusBar()
{
}

CMyStatusBar::~CMyStatusBar()
{
}


BEGIN_MESSAGE_MAP(CMyStatusBar, CStatusBar)
	//{{AFX_MSG_MAP(CMyStatusBar)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyStatusBar message handlers

void CMyStatusBar::DrawItem(LPDRAWITEMSTRUCT lpdis)
{
	if (lpdis->itemID==2)
	{
		CDC dc;
		dc.Attach(lpdis->hDC);

		CRect rect=lpdis->rcItem;
		CBrush brush(((CMainFrame*)GetParent())->GetPenColor());
		dc.FillRect(&rect,&brush);

		dc.Detach();		
	}

}

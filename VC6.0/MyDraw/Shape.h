// Shape.h: interface for the Shape class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SHAPE_H__6408E7F2_D74D_40C2_8B22_40E58062AF17__INCLUDED_)
#define AFX_SHAPE_H__6408E7F2_D74D_40C2_8B22_40E58062AF17__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class Shape : public CObject  
{
	DECLARE_SERIAL(Shape);
public:
	Shape(COLORREF color,int width);
	virtual ~Shape();
	virtual void Draw(CDC* pDC);
	virtual void Update(CPoint point);
	virtual void Serialize(CArchive& ar);
protected:
	Shape();
	COLORREF m_PenColor;
	int m_PenWidth;

};

class Line : public Shape  
{
	DECLARE_SERIAL(Line);
public:
	Line(COLORREF color,int width,CPoint start,CPoint end);
	virtual ~Line();
	virtual void Draw(CDC* pDC);
	virtual void Update(CPoint point);
	virtual void Serialize(CArchive& ar);
protected:
	Line();
	CPoint m_LineStart;
	CPoint m_LineEnd;
	
};

class FreeDraw : public Shape  
{
	DECLARE_SERIAL(FreeDraw);
public:
	FreeDraw(COLORREF color,int width,CPoint start);
	virtual ~FreeDraw();
	virtual void Draw(CDC* pDC);
	virtual void Update(CPoint point);
	virtual void Serialize(CArchive& ar);
protected:
	FreeDraw();
	
	CArray<CPoint,CPoint> m_Data;
	
};

class FilledShape : public Shape  
{
	DECLARE_SERIAL(FilledShape);
public:
	FilledShape(COLORREF color,int width,COLORREF brushcolor);
	virtual ~FilledShape();
	virtual void Draw(CDC* pDC);
	virtual void Update(CPoint point);
	virtual void Serialize(CArchive& ar);
protected:
	FilledShape();
	COLORREF m_BrushColor;
	
};

class Box : public FilledShape  
{
	DECLARE_SERIAL(Box);
public:
	Box(COLORREF color,int width,COLORREF brushcolor,CPoint ulc,CPoint lrc);
	virtual ~Box();
	virtual void Draw(CDC* pDC);
	virtual void Update(CPoint point);
	virtual void Serialize(CArchive& ar);
protected:
	Box();
	
	CPoint m_ULCorner;
	CPoint m_LRCorner;
	
};

class Oval : public FilledShape  
{
	DECLARE_SERIAL(Oval);
public:
	Oval(COLORREF color,int width,COLORREF brushcolor,CPoint ulc,CPoint lrc);
	virtual ~Oval();
	virtual void Draw(CDC* pDC);
	virtual void Update(CPoint point);
	virtual void Serialize(CArchive& ar);
protected:
	Oval();
	
	CPoint m_ULCorner;
	CPoint m_LRCorner;
	
};

IMPLEMENT_SERIAL(Shape,CObject,1)
IMPLEMENT_SERIAL(Line,Shape,1)
IMPLEMENT_SERIAL(FreeDraw,Shape,1)
IMPLEMENT_SERIAL(FilledShape,Shape,1)
IMPLEMENT_SERIAL(Box,FilledShape,1)
IMPLEMENT_SERIAL(Oval,FilledShape,1)

#endif // !defined(AFX_SHAPE_H__6408E7F2_D74D_40C2_8B22_40E58062AF17__INCLUDED_)

// MyDrawDoc.cpp : implementation of the CMyDrawDoc class
//

#include "stdafx.h"
//#include "MyDraw.h"

#include "MyDrawDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMyDrawDoc

IMPLEMENT_DYNCREATE(CMyDrawDoc, CDocument)

BEGIN_MESSAGE_MAP(CMyDrawDoc, CDocument)
	//{{AFX_MSG_MAP(CMyDrawDoc)
	ON_COMMAND(ID_EDIT_CLEAR, OnEditClear)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyDrawDoc construction/destruction

CMyDrawDoc::CMyDrawDoc()
{
	// TODO: add one-time construction code here

}

CMyDrawDoc::~CMyDrawDoc()
{
}

BOOL CMyDrawDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)
	m_Data.SetSize(0,128);
	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CMyDrawDoc serialization

void CMyDrawDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
		CString s="dra file";
		ar<<s;
		m_Data.Serialize(ar);
	}
	else
	{
		// TODO: add loading code here
		CString s;
		ar>>s;
		if(s=="dra file")
		{
			m_Data.Serialize(ar);
		}
		else AfxThrowArchiveException(CArchiveException::badIndex);
	}
	
}

/////////////////////////////////////////////////////////////////////////////
// CMyDrawDoc diagnostics

#ifdef _DEBUG
void CMyDrawDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMyDrawDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMyDrawDoc commands

int CMyDrawDoc::NumShapes()
{
	return m_Data.GetSize();
}

Shape* CMyDrawDoc::GetShape(int index)
{
	return m_Data[index];
}

void CMyDrawDoc::AddShape(Shape* pNewShape)
{
	m_Data.Add(pNewShape);
	SetModifiedFlag();
}

void CMyDrawDoc::OnEditClear() 
{
	// TODO: Add your command handler code here
	m_Data.SetSize(0,128);
	SetModifiedFlag();
	UpdateAllViews(NULL);
	
}

void CMyDrawDoc::DeleteContents() 
{
	// TODO: Add your specialized code here and/or call the base class
	int n=m_Data.GetSize();
	for(int i=0;i<n;i++)
	{
		delete m_Data[i];
	}
	m_Data.RemoveAll();
	CDocument::DeleteContents();
}

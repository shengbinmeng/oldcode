// Shape.cpp: implementation of the Shape class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "MyDraw.h"
#include "Shape.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Shape::Shape(COLORREF color,int width)
{
	m_PenColor=color;
	m_PenWidth=width;
}

Line::Line(COLORREF color,int width,CPoint start,CPoint end)
:Shape(color,width),m_LineStart(start),m_LineEnd(end){}

FreeDraw::FreeDraw(COLORREF color,int width,CPoint start)
:Shape(color,width)
{
	m_Data.Add(start);
}

FilledShape::FilledShape(COLORREF color,int width,COLORREF brushcolor)
:Shape(color,width),m_BrushColor(brushcolor){}

Box::Box(COLORREF color,int width,COLORREF brushcolor,CPoint ulc,CPoint lrc)
:FilledShape(color,width,brushcolor),m_ULCorner(ulc),m_LRCorner(lrc){}

Oval::Oval(COLORREF color,int width,COLORREF brushcolor,CPoint ulc,CPoint lrc)
:FilledShape(color,width,brushcolor),m_ULCorner(ulc),m_LRCorner(lrc){}



void Shape::Draw(CDC* pDC)
{

}
void Shape::Update(CPoint point)
{

}
void Shape::Serialize(CArchive& ar)
{
	CObject::Serialize(ar);

	if(ar.IsStoring())
	{
		ar<<(DWORD)m_PenColor<<m_PenWidth;
	}
	else
	{
		ar>>(DWORD)m_PenColor>>m_PenWidth;
	}
}

void Line::Draw(CDC* pDC)
{
	CPen pen, *pOldPen;
	pen.CreatePen(PS_SOLID,m_PenWidth,m_PenColor);
	pOldPen=pDC->SelectObject(&pen);
	pDC->MoveTo(m_LineStart);
	pDC->LineTo(m_LineEnd);

	pDC->SelectObject(pOldPen);
}
void Line::Update(CPoint point)
{
	m_LineEnd=point;
}
void Line::Serialize(CArchive& ar)
{
	Shape::Serialize(ar);

	if(ar.IsStoring())
	{
		ar<<m_LineStart<<m_LineEnd;
	}
	else
	{
		ar>>m_LineStart>>m_LineEnd;
	}
}

void FreeDraw::Draw(CDC* pDC)
{
	CPen pen, *pOldPen;
	pen.CreatePen(PS_SOLID,m_PenWidth,m_PenColor);
	pOldPen=pDC->SelectObject(&pen);

	pDC->MoveTo(m_Data[0]);
	for(int i=1;i<m_Data.GetSize();i++)
	{
		pDC->LineTo(m_Data[i]);
	}

	pDC->SelectObject(pOldPen);

}
void FreeDraw::Update(CPoint point)
{
	m_Data.Add(point);
}
void FreeDraw::Serialize(CArchive& ar)
{
	Shape::Serialize(ar);
	m_Data.Serialize(ar);
}

void FilledShape::Draw(CDC* pDC)
{

}
void FilledShape::Update(CPoint point)
{

}
void FilledShape::Serialize(CArchive& ar)
{
	Shape::Serialize(ar);
	if (ar.IsStoring())
	{
		ar<<(DWORD)m_BrushColor;
	} 
	else
	{
		ar>>(DWORD)m_BrushColor;
	}
}

void Box::Draw(CDC* pDC)
{
	CPen pen, *pOldPen;
	pen.CreatePen(PS_SOLID,m_PenWidth,m_PenColor);
	pOldPen=pDC->SelectObject(&pen);

	CBrush brush, *pOldBrush;
	brush.CreateSolidBrush(m_BrushColor);
	pOldBrush=pDC->SelectObject(&brush);

	pDC->Rectangle(CRect(m_ULCorner,m_LRCorner));

	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);

}
void Box::Update(CPoint point)
{
	m_LRCorner=point;
}
void Box::Serialize(CArchive& ar)
{
	FilledShape::Serialize(ar);
	if (ar.IsStoring())
	{
		ar<<m_ULCorner<<m_LRCorner;
	} 
	else
	{
		ar>>m_ULCorner>>m_LRCorner;
	}
}

void Oval::Draw(CDC* pDC)
{
	CPen pen, *pOldPen;
	pen.CreatePen(PS_SOLID,m_PenWidth,m_PenColor);
	pOldPen=pDC->SelectObject(&pen);
	
	CBrush brush, *pOldBrush;
	brush.CreateSolidBrush(m_BrushColor);
	pOldBrush=pDC->SelectObject(&brush);
	
	pDC->Ellipse(CRect(m_ULCorner,m_LRCorner));
	
	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);


}
void Oval::Update(CPoint point)
{
	m_LRCorner=point;
}
void Oval::Serialize(CArchive& ar)
{
	FilledShape::Serialize(ar);
	if (ar.IsStoring())
	{
		ar<<m_ULCorner<<m_LRCorner;
	} 
	else
	{
		ar>>m_ULCorner>>m_LRCorner;
	}
}


Shape::Shape(){}
Line::Line(){}
FreeDraw::FreeDraw(){}
FilledShape::FilledShape(){}
Box::Box(){}
Oval::Oval(){}

Shape::~Shape(){}
Line::~Line(){}
FreeDraw::~FreeDraw(){}
FilledShape::~FilledShape(){}
Box::~Box(){}
Oval::~Oval(){}

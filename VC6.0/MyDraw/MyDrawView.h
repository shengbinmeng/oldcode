// MyDrawView.h : interface of the CMyDrawView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MYDRAWVIEW_H__A9C0DEBE_F0FA_4A73_82C5_3D77FB557C66__INCLUDED_)
#define AFX_MYDRAWVIEW_H__A9C0DEBE_F0FA_4A73_82C5_3D77FB557C66__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CMyDrawView : public CView
{
protected: // create from serialization only
	CMyDrawView();
	DECLARE_DYNCREATE(CMyDrawView)

// Attributes
public:
	CMyDrawDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyDrawView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	void InitPen();
	virtual ~CMyDrawView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMyDrawView)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnPensColor();
	afx_msg void OnPenWidth(UINT nCmd);
	afx_msg void OnUpdateWidth(CCmdUI* pCmdUI);
	afx_msg void OnShapeFree();
	afx_msg void OnShapeLine();
	afx_msg void OnUpdateShape(CCmdUI* pCmdUI);
	afx_msg void OnShapeOval();
	afx_msg void OnShapeRect();
	afx_msg void OnUpdateStatusPenWidth(CCmdUI* pCmdUI);
	afx_msg void OnBrushColor();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	Shape* m_pCurrentShape;
	UINT m_ShapeType;
	CPoint m_LineStart;
	CPen m_Pen;
	COLORREF m_PenColor;
	int m_PenStyle;
	int m_PenWidth;

	COLORREF m_BrushColor;
	CBrush m_Brush;
};

#ifndef _DEBUG  // debug version in MyDrawView.cpp
inline CMyDrawDoc* CMyDrawView::GetDocument()
   { return (CMyDrawDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYDRAWVIEW_H__A9C0DEBE_F0FA_4A73_82C5_3D77FB557C66__INCLUDED_)

//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by MyDraw.rc
//
#define IDD_ABOUTBOX                    100
#define ID_STATUS_PENWIDTH              101
#define ID_STATUS_PENCOLOR              102
#define IDR_MAINFRAME                   128
#define IDR_MYDRAWTYPE                  129
#define ID_PENS_COLOR                   32771
#define ID_WIDTH_P1                     32772
#define ID_WIDTH_P4                     32773
#define ID_WIDTH_P8                     32774
#define ID_WIDTH_P12                    32775
#define ID_SHAPE_RECT                   32778
#define ID_SHAPE_OVAL                   32779
#define ID_SHAPE_LINE                   32780
#define ID_SHAPE_FREE                   32782
#define ID_BRUSH_COLOR                  32783

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32784
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           103
#endif
#endif

// MyDrawView.cpp : implementation of the CMyDrawView class
//

#include "stdafx.h"
#include "MyDraw.h"

#include "MyDrawDoc.h"
#include "MyDrawView.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMyDrawView

IMPLEMENT_DYNCREATE(CMyDrawView, CView)

BEGIN_MESSAGE_MAP(CMyDrawView, CView)
	//{{AFX_MSG_MAP(CMyDrawView)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_COMMAND(ID_PENS_COLOR, OnPensColor)
	ON_COMMAND_RANGE(ID_WIDTH_P1,ID_WIDTH_P12,OnPenWidth)
	ON_UPDATE_COMMAND_UI(ID_WIDTH_P1, OnUpdateWidth)
	ON_COMMAND(ID_SHAPE_FREE, OnShapeFree)
	ON_COMMAND(ID_SHAPE_LINE, OnShapeLine)
	ON_UPDATE_COMMAND_UI(ID_SHAPE_FREE, OnUpdateShape)
	ON_COMMAND(ID_SHAPE_OVAL, OnShapeOval)
	ON_COMMAND(ID_SHAPE_RECT, OnShapeRect)
	ON_UPDATE_COMMAND_UI(ID_STATUS_PENWIDTH,OnUpdateStatusPenWidth)
	ON_UPDATE_COMMAND_UI(ID_WIDTH_P4, OnUpdateWidth)
	ON_UPDATE_COMMAND_UI(ID_WIDTH_P8, OnUpdateWidth)
	ON_UPDATE_COMMAND_UI(ID_WIDTH_P12, OnUpdateWidth)
	ON_UPDATE_COMMAND_UI(ID_SHAPE_LINE, OnUpdateShape)
	ON_UPDATE_COMMAND_UI(ID_SHAPE_OVAL, OnUpdateShape)
	ON_UPDATE_COMMAND_UI(ID_SHAPE_RECT, OnUpdateShape)
	ON_COMMAND(ID_BRUSH_COLOR, OnBrushColor)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyDrawView construction/destruction

CMyDrawView::CMyDrawView()
{
	// TODO: add construction code here
	m_PenColor=RGB(0,0,0);
	m_PenStyle=PS_SOLID;
	m_PenWidth=0;   //0则被映射为最小，即1
	m_Pen.CreatePen(m_PenStyle,m_PenWidth,m_PenColor);

	m_BrushColor=RGB(0,0,0);
	m_Brush.CreateSolidBrush(m_BrushColor);

}

CMyDrawView::~CMyDrawView()
{
}

BOOL CMyDrawView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMyDrawView drawing

void CMyDrawView::OnDraw(CDC* pDC)
{
	CMyDrawDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
	for (int i=0;i<pDoc->NumShapes();i++)
	{
		m_pCurrentShape=pDoc->GetShape(i);
		m_pCurrentShape->Draw(pDC);
	}

}

/////////////////////////////////////////////////////////////////////////////
// CMyDrawView printing

BOOL CMyDrawView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CMyDrawView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CMyDrawView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CMyDrawView diagnostics

#ifdef _DEBUG
void CMyDrawView::AssertValid() const
{
	CView::AssertValid();
}

void CMyDrawView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMyDrawDoc* CMyDrawView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMyDrawDoc)));
	return (CMyDrawDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMyDrawView message handlers

void CMyDrawView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	m_LineStart=point;
	SetCapture();
	switch(m_ShapeType)
	{
	case ID_SHAPE_LINE:
		m_pCurrentShape=new Line(m_PenColor,m_PenWidth,point,point);
		break;
	case ID_SHAPE_OVAL:
		m_pCurrentShape=new Oval(m_PenColor,m_PenWidth,m_BrushColor,point,point);
		break;
	case ID_SHAPE_RECT:
		m_pCurrentShape=new Box(m_PenColor,m_PenWidth,m_BrushColor,point,point);
		break;
	case ID_SHAPE_FREE:
	default:
		m_pCurrentShape=new FreeDraw(m_PenColor,m_PenWidth,point);

	}
}

void CMyDrawView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	CClientDC dc(this);
	m_pCurrentShape->Update(point);
	m_pCurrentShape->Draw(&dc);

	ReleaseCapture();

	CMyDrawDoc* pDoc=GetDocument();
	ASSERT_VALID(pDoc);
	pDoc->AddShape(m_pCurrentShape);
}

void CMyDrawView::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	if(nFlags & MK_LBUTTON)
	{
		CClientDC dc(this);
		if (m_ShapeType!=ID_SHAPE_FREE)
		{
			dc.SetROP2(R2_NOT);
			m_pCurrentShape->Draw(&dc);
		}
		m_pCurrentShape->Update(point);
		m_pCurrentShape->Draw(&dc);

	}

}

void CMyDrawView::OnPensColor() 
{
	// TODO: Add your command handler code here
	CColorDialog dlg(m_PenColor);
	if(dlg.DoModal()==IDOK)
	{
		m_PenColor=dlg.GetColor();
		InitPen();
		((CMainFrame*)GetParent())->SetPenColor(m_PenColor);
	}
}

void CMyDrawView::InitPen()
{
	m_Pen.DeleteObject();
	m_Pen.CreatePen(m_PenStyle,m_PenWidth,m_PenColor);
}

void CMyDrawView::OnPenWidth(UINT nCmd) 
{
	// TODO: Add your command handler code here
	m_PenWidth=(nCmd-ID_WIDTH_P1)*4;
	InitPen();
}

void CMyDrawView::OnUpdateWidth(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	int thiswidth=(pCmdUI->m_nID-ID_WIDTH_P1)*4;
	pCmdUI->SetCheck(m_PenWidth==thiswidth);
	
}

void CMyDrawView::OnShapeFree() 
{
	m_ShapeType=ID_SHAPE_FREE;	
}

void CMyDrawView::OnShapeLine() 
{
	// TODO: Add your command handler code here
	m_ShapeType=ID_SHAPE_LINE;
}

void CMyDrawView::OnUpdateShape(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->SetCheck(pCmdUI->m_nID==m_ShapeType);
}


void CMyDrawView::OnShapeOval() 
{
	// TODO: Add your command handler code here
	m_ShapeType=ID_SHAPE_OVAL;
}

void CMyDrawView::OnShapeRect() 
{
	// TODO: Add your command handler code here
	m_ShapeType=ID_SHAPE_RECT;
}

void CMyDrawView::OnUpdateStatusPenWidth(CCmdUI* pCmdUI)
{
	CString s;
	int width=m_PenWidth;
	if(width==0) width=1;
	s.Format("PenWidth= %2d",width);
	pCmdUI->SetText(s);
}

void CMyDrawView::OnBrushColor() 
{
	// TODO: Add your command handler code here
	CColorDialog dlg(m_BrushColor);
	if (dlg.DoModal()==IDOK)
	{
		m_BrushColor=dlg.GetColor();
	}
}

//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by FourUp.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_FOURUP_DIALOG               102
#define IDR_MAINFRAME                   128
#define IDI_ICON1                       130
#define IDI_ICON2                       131
#define IDI_ICON3                       132
#define IDI_ICON4                       133
#define IDB_BITMAP1                     142
#define IDC_AMT_LEFT                    1000
#define IDC_DEALCARDS                   1001
#define IDC_CARD1                       1002
#define IDC_CARD2                       1003
#define IDC_CARD3                       1004
#define IDC_CARD4                       1005
#define IDC_EDIT1                       1006
#define IDC_SPIN1                       1007

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        143
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

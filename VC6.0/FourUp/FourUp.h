// FourUp.h : main header file for the FOURUP application
//

#if !defined(AFX_FOURUP_H__B09C72E0_CAED_40F5_B748_342A85C202C4__INCLUDED_)
#define AFX_FOURUP_H__B09C72E0_CAED_40F5_B748_342A85C202C4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CFourUpApp:
// See FourUp.cpp for the implementation of this class
//

class CFourUpApp : public CWinApp
{
public:
	CFourUpApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFourUpApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CFourUpApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FOURUP_H__B09C72E0_CAED_40F5_B748_342A85C202C4__INCLUDED_)

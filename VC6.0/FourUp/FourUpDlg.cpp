// FourUpDlg.cpp : implementation file
//

#include "stdafx.h"
#include "FourUp.h"
#include "FourUpDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFourUpDlg dialog

CFourUpDlg::CFourUpDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFourUpDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFourUpDlg)
	m_Stake = 0;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_hIC1=AfxGetApp()->LoadIcon(IDI_ICON1);
	m_hIC2=AfxGetApp()->LoadIcon(IDI_ICON2);
	m_hIC3=AfxGetApp()->LoadIcon(IDI_ICON3);
	m_hIC4=AfxGetApp()->LoadIcon(IDI_ICON4);

	m_Stake=2;
	m_Amt_Rem=100.0;

	srand((unsigned)time(NULL));
}

void CFourUpDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFourUpDlg)
	DDX_Control(pDX, IDC_CARD4, m_Card4);
	DDX_Control(pDX, IDC_CARD3, m_Card3);
	DDX_Control(pDX, IDC_CARD2, m_Card2);
	DDX_Control(pDX, IDC_CARD1, m_Card1);
	DDX_Control(pDX, IDC_AMT_LEFT, m_Amt_Left);
	DDX_Text(pDX, IDC_EDIT1, m_Stake);
	DDV_MinMaxInt(pDX, m_Stake, 0, 100);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CFourUpDlg, CDialog)
	//{{AFX_MSG_MAP(CFourUpDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_DEALCARDS, OnDealCards)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFourUpDlg message handlers

BOOL CFourUpDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	CSpinButtonCtrl* pSpin=(CSpinButtonCtrl*) GetDlgItem(IDC_SPIN1);
	pSpin->SetRange(0,100);
	pSpin->SetPos(2);
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CFourUpDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CFourUpDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CFourUpDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CFourUpDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	CString s;
	s.Format("Great Game! You have$%.2f left",m_Amt_Rem);
	MessageBox(s,"Thanks for playing FourUp!");
	CDialog::OnCancel();
}

void CFourUpDlg::OnDealCards() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	double LastValue=m_Amt_Rem;
	m_Amt_Rem-=m_Stake;
	if(m_Amt_Rem<0)
	{
		MessageBox("���㣬�������棡���Ǯ��");
		m_Amt_Rem+=m_Stake;
		return;
	}
	for(int i=0;i<4;i++) m_Cards[i]=0;

	m_Card1.SetIcon(PickRandomCard());
	m_Card2.SetIcon(PickRandomCard());
	m_Card3.SetIcon(PickRandomCard());
	m_Card4.SetIcon(PickRandomCard());

	double incoming=CalcuWinings();
	m_Amt_Rem+=incoming;

	CString s;
	s.Format("You still have: $%.2f��%d+%.2f=%.2f",LastValue,m_Stake,incoming,m_Amt_Rem);
	m_Amt_Left.SetWindowText(s);	
	
}

HICON& CFourUpDlg::PickRandomCard()
{
	int num=rand()%4;
	m_Cards[num]++;
	switch(num)
	{
	case 0:return m_hIC1;
	case 1:return m_hIC2;
	case 2:return m_hIC3;
	}
	
	return m_hIC4;
}

double CFourUpDlg::CalcuWinings()
{
	int pairs=0;
	double incoming=0;
	for(int i=0;i<4;i++){
		if(m_Cards[i]==2)
		{
			if(pairs>0)
			{
				incoming=2*m_Stake;
				break;
			}
			else pairs++;
		}
		if(m_Cards[i]==3)
		{
			incoming=3*m_Stake;
			break;
		}
		if(m_Cards[i]==4)
		{
			incoming=4*m_Stake;
			if(i=3) incoming+=4*m_Stake;
			break;
		}
	}

	return incoming;
}



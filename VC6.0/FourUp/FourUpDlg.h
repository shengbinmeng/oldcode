// FourUpDlg.h : header file
//

#if !defined(AFX_FOURUPDLG_H__592214DB_3CA2_4C35_A25A_91C848AA7BB0__INCLUDED_)
#define AFX_FOURUPDLG_H__592214DB_3CA2_4C35_A25A_91C848AA7BB0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CFourUpDlg dialog

class CFourUpDlg : public CDialog
{
// Construction
public:
	CFourUpDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CFourUpDlg)
	enum { IDD = IDD_FOURUP_DIALOG };
	CStatic	m_Card4;
	CStatic	m_Card3;
	CStatic	m_Card2;
	CStatic	m_Card1;
	CButton	m_Amt_Left;
	int		m_Stake;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFourUpDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;
	HICON m_hIC1;
	HICON m_hIC2;
	HICON m_hIC3;
	HICON m_hIC4;

	// Generated message map functions
	//{{AFX_MSG(CFourUpDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnCancel();
	afx_msg void OnDealCards();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	double CalcuWinings();
	HICON& PickRandomCard();
	double m_Amt_Rem;
	int m_Cards[4];
	
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FOURUPDLG_H__592214DB_3CA2_4C35_A25A_91C848AA7BB0__INCLUDED_)

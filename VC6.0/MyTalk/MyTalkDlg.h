// MyTalkDlg.h : header file
//

#if !defined(AFX_MYTALKDLG_H__D2A6445A_7717_4219_A9F8_778CF745D32C__INCLUDED_)
#define AFX_MYTALKDLG_H__D2A6445A_7717_4219_A9F8_778CF745D32C__INCLUDED_

#include "MySocket.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CMyTalkDlg dialog


class CMyTalkDlg : public CDialog
{
// Construction
public:
	void OnClose();
	void OnReceive();
	void OnSend();
	void OnConnect();
	void OnAccept();
	CMySocket m_sConnect;
	CMySocket m_sListen;
	CMyTalkDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CMyTalkDlg)
	enum { IDD = IDD_MYTALK_DIALOG };
	CButton	m_btnClose;
	CListBox	m_listSent;
	CListBox	m_listReceived;
	CComboBox	m_cmbType;
	CButton	m_btnConnect;
	CString	m_strMsg;
	CString	m_strSvName;
	int		m_intSvPort;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyTalkDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CMyTalkDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnSelchangeComboType();
	afx_msg void OnBtnConnect();
	afx_msg void OnSendMsg();
	afx_msg void OnBtnClose();
	afx_msg void OnChangeEditMsg();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYTALKDLG_H__D2A6445A_7717_4219_A9F8_778CF745D32C__INCLUDED_)

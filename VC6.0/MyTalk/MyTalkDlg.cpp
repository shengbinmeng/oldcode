// MyTalkDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MyTalk.h"
#include "MyTalkDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

//#include "MySocket.h"

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyTalkDlg dialog

CMyTalkDlg::CMyTalkDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMyTalkDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMyTalkDlg)
	m_strMsg = _T("");
	m_strSvName = _T("");
	m_intSvPort = 0;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMyTalkDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMyTalkDlg)
	DDX_Control(pDX, IDC_BTN_CLOSE, m_btnClose);
	DDX_Control(pDX, IDC_LIST_SENT, m_listSent);
	DDX_Control(pDX, IDC_LIST_RECEIVED, m_listReceived);
	DDX_Control(pDX, IDC_COMBO_TYPE, m_cmbType);
	DDX_Control(pDX, IDC_BTN_CONNECT, m_btnConnect);
	DDX_Text(pDX, IDC_EDIT_MSG, m_strMsg);
	DDX_Text(pDX, IDC_EDIT_SVNAME, m_strSvName);
	DDX_Text(pDX, IDC_EDIT_SVPORT, m_intSvPort);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CMyTalkDlg, CDialog)
	//{{AFX_MSG_MAP(CMyTalkDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_CBN_SELCHANGE(IDC_COMBO_TYPE, OnSelchangeComboType)
	ON_BN_CLICKED(IDC_BTN_CONNECT, OnBtnConnect)
	ON_BN_CLICKED(IDOK, OnSendMsg)
	ON_BN_CLICKED(IDC_BTN_CLOSE, OnBtnClose)
	ON_EN_CHANGE(IDC_EDIT_MSG, OnChangeEditMsg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyTalkDlg message handlers

BOOL CMyTalkDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here

	m_cmbType.SetCurSel(1);  //默认初始为客户机
	m_strSvName="localhost";
	m_intSvPort=1000;        //默认的服务器和端口
	UpdateData(FALSE);       

	m_sListen.SetParent(this);
	m_sConnect.SetParent(this);
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CMyTalkDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CMyTalkDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CMyTalkDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CMyTalkDlg::OnSelchangeComboType() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_cmbType.GetCurSel()==0)
		m_btnConnect.SetWindowText("侦听");
	else
		m_btnConnect.SetWindowText("连接");
}

void CMyTalkDlg::OnBtnConnect() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	GetDlgItem(IDC_BTN_CLOSE)->EnableWindow(TRUE);

	GetDlgItem(IDC_BTN_CONNECT)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDIT_SVNAME)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDIT_SVPORT)->EnableWindow(FALSE);
	//GetDlgItem(IDC_STATIC_SVNAME)->EnableWindow(FALSE);
	//GetDlgItem(IDC_STATIC_SVNAME)->EnableWindow(FALSE);
	GetDlgItem(IDC_COMBO_TYPE)->EnableWindow(FALSE);

	if(m_cmbType.GetCurSel()==0)
	{
		m_sListen.Create(m_intSvPort);
		m_sListen.Listen();
		MessageBox("开始侦听…");
	}
	else
	{
		m_sConnect.Create();
		m_sConnect.Connect(m_strSvName,m_intSvPort);
	}
	
}

void CMyTalkDlg::OnAccept()
{
	m_sListen.Accept(m_sConnect);
    
	GetDlgItem(IDC_EDIT_MSG)->EnableWindow(TRUE);
	GetDlgItem(IDOK)->EnableWindow(TRUE);
	
}

void CMyTalkDlg::OnConnect()
{
	GetDlgItem(IDC_EDIT_MSG)->EnableWindow(TRUE);
	GetDlgItem(IDOK)->EnableWindow(TRUE);

	GetDlgItem(IDC_BTN_CLOSE)->EnableWindow(TRUE);
}

void CMyTalkDlg::OnSendMsg() 
{
	// TODO: Add your control notification handler code here
	int nLen;
	int nSent;

	UpdateData(TRUE);

	if(!m_strMsg.IsEmpty())
	{
		nLen=m_strMsg.GetLength();
		nSent=m_sConnect.Send(LPCTSTR(m_strMsg),nLen);

		if(nSent!=SOCKET_ERROR)
		{
			m_listSent.AddString(m_strMsg);
			UpdateData(FALSE);
		}
		else AfxMessageBox("信息发送错误！",MB_OK|MB_ICONSTOP);

		m_strMsg.Empty();
		UpdateData(FALSE);
	}

}

void CMyTalkDlg::OnSend()
{

}

void CMyTalkDlg::OnReceive()
{
	char *pBuf=new char[1025];
	int nBufSize=1024;
	int nReceived;
	CString strReceived;

	nReceived=m_sConnect.Receive(pBuf,nBufSize);
	if(nReceived!=SOCKET_ERROR)
	{
		pBuf[nReceived]=NULL;
		strReceived=pBuf;

		m_listReceived.AddString(strReceived);
		UpdateData(FALSE);
	}
	else AfxMessageBox("信息接受错误！",MB_OK|MB_ICONSTOP);

}

void CMyTalkDlg::OnClose()
{
	m_sConnect.Close();
	MessageBox("已断开！");

	GetDlgItem(IDC_EDIT_MSG)->EnableWindow(FALSE);
	GetDlgItem(IDOK)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_CLOSE)->EnableWindow(FALSE);


	while(m_listSent.GetCount()!=0)
		m_listSent.DeleteString(0);
	while(m_listReceived.GetCount()!=0)
		m_listReceived.DeleteString(0);

	if(m_cmbType.GetCurSel()==1)
	{
		
		GetDlgItem(IDC_BTN_CONNECT)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_SVNAME)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_SVPORT)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_TYPE)->EnableWindow(TRUE);
	}
	else
	{
		m_sListen.Close();
		GetDlgItem(IDC_BTN_CONNECT)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_SVNAME)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_SVPORT)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_TYPE)->EnableWindow(TRUE);
	}

}

void CMyTalkDlg::OnBtnClose() 
{
	// TODO: Add your control notification handler code here
	OnClose();
}

void CMyTalkDlg::OnChangeEditMsg() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	SetDefID(IDOK);
}

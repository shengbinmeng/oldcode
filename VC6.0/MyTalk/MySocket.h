
#if !defined(AFX_MYSOCKET_H__D972C3D1_ACDE_4B1C_9B6B_0F9AF2F26103__INCLUDED_)
#define AFX_MYSOCKET_H__D972C3D1_ACDE_4B1C_9B6B_0F9AF2F26103__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MySocket.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// CMySocket command target

//#include "MyTalkDlg.h"
class CMyTalkDlg;

class CMySocket : public CAsyncSocket
{
                           //用于把套接字类和一个窗口联系起来（见下面函数）
// Attributes
public:

// Operations
public:
	CMySocket();
	virtual ~CMySocket();
                                              //把套接字类和一个窗口联系起来。
										 //在窗口类中则通过包含套接字类的对象变量与套接字联系起来；从而窗口可以使用套接字实现网络连接功能
											 //可以认为：窗口才是主角，套接字只是一个工具来扩展窗口的功能
// Overrides
public:
	void SetParent(CMyTalkDlg* pDlg);
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMySocket)
	public:
	virtual void OnAccept(int nErrorCode);
	virtual void OnReceive(int nErrorCode);
	virtual void OnClose(int nErrorCode);
	virtual void OnSend(int nErrorCode);
	virtual void OnConnect(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CMySocket)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
private:
	CMyTalkDlg* m_pDlg;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYSOCKET_H__D972C3D1_ACDE_4B1C_9B6B_0F9AF2F26103__INCLUDED_)

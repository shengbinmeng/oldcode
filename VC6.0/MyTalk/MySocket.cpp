// MySocket.cpp : implementation file
//

#include "stdafx.h"
#include "MyTalk.h"
#include "MySocket.h"

#include "MyTalkDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMySocket

CMySocket::CMySocket()
{
}

CMySocket::~CMySocket()
{
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CMySocket, CAsyncSocket)
	//{{AFX_MSG_MAP(CMySocket)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CMySocket member functions

void CMySocket::OnAccept(int nErrorCode)           //相当于是重载了其基类的OnAccept函数，
                                                   //转而调用与其联系的窗口类中专门写的的同名函数，在那里实现功能
{
	// TODO: Add your specialized code here and/or call the base class
	if(nErrorCode==0)
		m_pDlg->OnAccept();
	
	//CAsyncSocket::OnAccept(nErrorCode);
}


void CMySocket::OnReceive(int nErrorCode) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(nErrorCode==0)
		m_pDlg->OnReceive();

	CAsyncSocket::OnReceive(nErrorCode);
}

void CMySocket::OnClose(int nErrorCode) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(nErrorCode==0)
		m_pDlg->OnClose();

	CAsyncSocket::OnClose(nErrorCode);
}

void CMySocket::OnSend(int nErrorCode) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(nErrorCode==0)
		m_pDlg->OnSend();

	CAsyncSocket::OnSend(nErrorCode);
}

void CMySocket::OnConnect(int nErrorCode) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(nErrorCode==0)
		m_pDlg->OnConnect();

	CAsyncSocket::OnConnect(nErrorCode);
}

void CMySocket::SetParent(CMyTalkDlg *pDlg)
{
	m_pDlg=pDlg;

}

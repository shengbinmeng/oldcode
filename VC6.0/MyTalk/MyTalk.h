// MyTalk.h : main header file for the MYTALK application
//

#if !defined(AFX_MYTALK_H__B6196D17_C6B0_426B_AA02_470DE4986A0C__INCLUDED_)
#define AFX_MYTALK_H__B6196D17_C6B0_426B_AA02_470DE4986A0C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CMyTalkApp:
// See MyTalk.cpp for the implementation of this class
//

class CMyTalkApp : public CWinApp
{
public:
	CMyTalkApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyTalkApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CMyTalkApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYTALK_H__B6196D17_C6B0_426B_AA02_470DE4986A0C__INCLUDED_)

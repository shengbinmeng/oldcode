// MyCnChessDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MyCnChess.h"
#include "MyCnChessDlg.h"
#include "Globals.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CMyCnChessDlg 对话框




CMyCnChessDlg::CMyCnChessDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMyCnChessDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMyCnChessDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_WHITETIMEPASS, m_WhiteTimePass_Ctrl);
	DDX_Control(pDX, IDC_BLACKTIMEPASS, m_BlackTimePass_Ctrl);
	DDX_Control(pDX, IDC_COMBO_LEVEL, m_Level_Ctrl);
}

BEGIN_MESSAGE_MAP(CMyCnChessDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUT_BEGIN, &CMyCnChessDlg::OnBnClickedButBegin)
	ON_BN_CLICKED(IDC_BUT_CLOSE, &CMyCnChessDlg::OnBnClickedButClose)
	ON_WM_LBUTTONDOWN()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_GIVEUP, &CMyCnChessDlg::OnBnClickedGiveup)
	ON_BN_CLICKED(IDC_REGRET, &CMyCnChessDlg::OnBnClickedRegret)
	ON_BN_CLICKED(ID_HELP, &CMyCnChessDlg::OnBnClickedHelp)
END_MESSAGE_MAP()


// CMyCnChessDlg 消息处理程序

BOOL CMyCnChessDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	//加载棋盘位图
	m_BoardBmp.LoadBitmap(IDB_CHESSBOARD);
	BITMAP BitMap;
	m_BoardBmp.GetBitmap(&BitMap);
	m_nBoardWidth=BitMap.bmWidth;
	m_nBoardHeight=BitMap.bmHeight;
	m_BoardBmp.DeleteObject();

	//加载棋子位图
	CBitmap temp;
	temp.LoadBitmap(IDB_CHESSMAN1);
	m_Chessman.Create(39,39,ILC_COLOR24 | ILC_MASK,1,15);
	m_Chessman.Add(&temp,RGB(0,0,0));

	//显示棋盘的矩形位置（用于刷新）
	m_rectBoard.left=BORDERWIDTH;
	m_rectBoard.right=BORDERWIDTH + 9*GRILLWIDTH;
	m_rectBoard.top=BORDERHEIGHT;
	m_rectBoard.bottom=BORDERHEIGHT + 10*GRILLHEIGHT;

	//初始化游戏数据
	InitGameData();

	//m_BlackTimePass_Ctrl.SetWindowText("");
	m_BlackTimePass_Ctrl.SetWindowText(_T(""));
	//m_RedTimeLeft_Ctr.SetWindowText("");
	m_WhiteTimePass_Ctrl.SetWindowText(_T(""));

	m_TotalTime = CTimeSpan(0,0,60,0);
	m_BlkTimer = 0;
	m_RedTimer = 0;

	m_Level_Ctrl.SetCurSel(1);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CMyCnChessDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMyCnChessDlg::OnPaint()
{
	CPaintDC dc(this); // 用于绘制的设备上下文

	if (IsIconic())
	{
		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	
	CDC MemDC;
	POINT pt;
	CBitmap *pOldBmp;
	int z;

	MemDC.CreateCompatibleDC(&dc);
	m_BoardBmp.LoadBitmap(IDB_CHESSBOARD);
	pOldBmp=MemDC.SelectObject(&m_BoardBmp);
	
	for (short i=0;i<90;i++)
	{
		if(m_interface[i]==0)
		{
			if (i==m_SelectMoveFrom || i==m_SelectMoveTo)  //可能无子区也需要绘制高亮
			{
				pt.x=(i%9)*GRILLWIDTH +BORDERWIDTH;
				pt.y=(i/9)*GRILLHEIGHT+BORDERHEIGHT;
				m_Chessman.Draw(&MemDC,14,pt,ILD_TRANSPARENT);
			}

			continue;
		}


		pt.x=(i % 9) * GRILLWIDTH + BORDERWIDTH;
		pt.y=(i / 9) * GRILLHEIGHT + BORDERHEIGHT;

		z=IntToSubscript(m_interface[i]);
		m_Chessman.Draw(&MemDC,z,pt,ILD_TRANSPARENT);

		//高亮显示（对m_SelectMoveFrom、m_SelectMoveTo标示的棋子区域）
		if(i==m_SelectMoveFrom)
			m_Chessman.Draw(&MemDC,14,pt,ILD_TRANSPARENT);
		if (i==m_SelectMoveTo)
			m_Chessman.Draw(&MemDC,14,pt,ILD_TRANSPARENT);
	}

	dc.BitBlt(0,0,m_nBoardWidth,m_nBoardHeight,&MemDC,0,0,SRCCOPY);
	MemDC.SelectObject(pOldBmp);
	MemDC.DeleteDC();
	m_BoardBmp.DeleteObject();	
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CMyCnChessDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CMyCnChessDlg::InitGameData(void)
{
	static BYTE board[BOARD_SIZE]={
		39,37,35,33,32,34,36,38,40,
		0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,
		0 ,41,0 ,0 ,0 ,0 ,0 ,42,0 ,
		43,0 ,44,0 ,45,0 ,46,0 ,47,
		0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,
		0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,
		27,0 ,28,0 ,29,0 ,30,0 ,31,
		0 ,25,0 ,0 ,0 ,0 ,0 ,26,0 ,
		0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,
		23,21,19,17,16,18,20,22,24
	};

	for(int i=0;i<BOARD_SIZE;i++)
	{
		m_interface[i]=board[i];
	}

	m_ComputerSide = 1; 
	m_HumanSide = 0;
	m_gameState = GAMEOVER;
	m_SelectMoveFrom=m_SelectMoveTo=NOMOVE;
}

//判断鼠标点是否在棋盘内
bool CMyCnChessDlg::IsPtInBoard(CPoint point)
{
	return m_rectBoard.PtInRect(point);
}
//重绘棋子区
void CMyCnChessDlg::RequireDrawCell(short position)
{
	CRect rect=GetPieceRect(position);
	InvalidateRect(&rect,false);
	UpdateWindow();
}
//得到棋子区域
CRect CMyCnChessDlg::GetPieceRect(short position)
{
	short x=BORDERWIDTH+(position%9)*GRILLWIDTH;
	short y=BORDERHEIGHT+(position/9)*GRILLHEIGHT;
	CRect rect(x,y,x+GRILLWIDTH,y+GRILLHEIGHT);
	return rect;
}
//得到鼠标点的棋子位置
short CMyCnChessDlg::GetPieceByPosition(CPoint point)
{
	if (!m_rectBoard.PtInRect(point)) return -1;
	short x=(point.x-m_rectBoard.left)/GRILLWIDTH;
	short y=(point.y-m_rectBoard.top)/GRILLHEIGHT;

	return x+y*9;
}

//电脑走棋
void CMyCnChessDlg::ComputerPlay()
{
	if (m_gameState==GAMEOVER)
		return;
	CTime t1 = CTime::GetCurrentTime();

	m_Level=m_Level_Ctrl.GetCurSel();
	switch(m_Level)
	{
	case 0:
		m_BoardGame.ComputerThink0();
		break;
	case 1:
		m_BoardGame.ComputerThink1();	//调用计算机思考函数，该函数将类的BestMove成员赋值
		break;
	case 2:
		m_BoardGame.ComputerThink2();
		break;
	case 3:
		m_BoardGame.ComputerThink2();
		break;
	default:
		m_BoardGame.ComputerThink1();
		//TODO: more levels
	}

	m_tsBlkTimePass = m_tsBlkTimePass + (CTime::GetCurrentTime() - t1);
	if(m_tsBlkTimePass > m_TotalTime)
	{
		m_gameState = GAMEOVER;
		if(m_RedTimer)
			KillTimer(m_RedTimer);
		MessageBox(_T("计算机超时判负"), _T("系统消息"));
		return;
	}

	short z,k;
	z = m_BoardGame.BestMove.from;
	k = m_BoardGame.BestMove.to;

	if(z == 0)
	{
		m_gameState = GAMEOVER;
		if(m_RedTimer)
			KillTimer(m_RedTimer);
		MessageBox(_T("计算机认输，玩家获胜。恭喜您！"),_T( "系统消息"));
		return;
	}

	m_BoardGame.MakeMove(m_BoardGame.BestMove);

	m_MoveStack.push_back(m_BoardGame.MoveStack[m_BoardGame.StackTop-1]);

	short zz,kk;
	//清空高亮显示
	zz = m_SelectMoveFrom;
	kk = m_SelectMoveTo;
	m_SelectMoveFrom = NOMOVE;
	m_SelectMoveTo = NOMOVE;
	RequireDrawCell(zz); 
	RequireDrawCell(kk);

	zz = ((z/16 -3) *9 + z%16 -3);	//将16*16的棋盘位置转换成10*9的棋盘位置
	kk = ((k/16 -3) *9 + k%16 -3);

	m_interface[kk] = m_interface[zz];
	m_interface[zz] = 0;

	//高亮显示电脑走法
	m_SelectMoveFrom = zz;
	m_SelectMoveTo = kk;
	RequireDrawCell(zz); 
	RequireDrawCell(kk);
	Beep(500,300);

	m_tsBlkTimeLeft = m_TotalTime - m_tsBlkTimePass;
	m_BlackTimePass_Ctrl.SetWindowText(m_tsBlkTimeLeft.Format("%H:%M:%S"));
	m_BlackTimePass_Ctrl.SetWindowText(m_tsBlkTimePass.Format("%H:%M:%S"));

	int num;num = m_BoardGame.HasLegalMove(); //
	if (!num) {
		KillTimer(m_RedTimer);
		m_gameState = GAMEOVER;
		Beep(700,1000);
		MessageBox(_T("计算机获胜，您输了。可重新开战"),_T( "系统消息"));
		return;
	}

	m_gameState =WHITETHINK;
	m_RedTimer = SetTimer(1,1000,NULL);
}




void CMyCnChessDlg::OnBnClickedButBegin()
{
	// TODO: Add your control notification handler code here
	InitGameData();
	m_SelectMoveFrom = NOMOVE;
	m_SelectMoveTo = NOMOVE;
	m_MoveStack.clear();
	m_Level=m_Level_Ctrl.GetCurSel();

	m_BoardGame.ClearBoard();
	m_BoardGame.StringToArray("rnbakabnr/9/1c5c1/p1p1p1p1p/9/9/P1P1P1P1P/1C5C1/9/RNBAKABNR w");
	m_tsBlkTimeLeft = m_TotalTime;
	//m_BlackTimePass_Ctrl.SetWindowText(m_tsBlkTimeLeft.Format("%H:%M:%S"));
	m_tsBlkTimePass = m_TotalTime - m_tsBlkTimeLeft;
	m_BlackTimePass_Ctrl.SetWindowText(m_tsBlkTimePass.Format("%H:%M:%S"));
	m_tsRedTimeLeft = m_tsBlkTimeLeft;
	//m_WhiteTimePass_Ctrl.SetWindowText(m_tsRedTimeLeft.Format("%H:%M:%S"));
	m_tsRedTimePass = m_TotalTime - m_tsRedTimeLeft;
	m_WhiteTimePass_Ctrl.SetWindowText(m_tsRedTimePass.Format("%H:%M:%S"));

	InvalidateRect(&m_rectBoard,false);
	UpdateWindow();

	MessageBox(_T("您先走棋。点击确定开始计时。"),_T("提示"));
	m_RedTimer = SetTimer(1,1000,NULL);
	m_gameState = WHITETHINK;
}

void CMyCnChessDlg::OnBnClickedButClose()
{
	// TODO: Add your control notification handler code here
	OnCancel();
}

void CMyCnChessDlg::OnCancel()
{
	// TODO: Add your specialized code here and/or call the base class
	if (MessageBox(_T("确定要退出？"),_T("提示"),MB_OKCANCEL)==IDOK)
	{
		CDialog::OnCancel();
	}
}


void CMyCnChessDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	if(!IsPtInBoard(point) || m_gameState!= WHITETHINK) return ;
	
	int SideFlag=16+m_HumanSide*16;

	short dest,from;
	from=m_SelectMoveFrom;
	dest=m_SelectMoveTo;
	m_SelectMoveTo=NOMOVE;
	m_SelectMoveFrom=NOMOVE;
	if (from!=NOMOVE)
	{
		RequireDrawCell(from);
	}
	if (dest!=NOMOVE)
	{
		RequireDrawCell(dest);
	}

	dest=GetPieceByPosition(point);

	BYTE piece=m_interface[dest];
	if (piece & SideFlag)  //选中的是操作者本方棋子
	{
		if (from !=NOMOVE) //已经选了要走棋子就取消它
		{
			m_SelectMoveFrom=NOMOVE;
			RequireDrawCell(from);
		}
		//认为新选这个是要走的棋子
		m_SelectMoveFrom=dest;
		RequireDrawCell(dest);
	}
	else  //棋子落在空处或对方棋子上
	if(from !=NOMOVE)  //已选了要走棋子
	{
		move mv;
		mv.from=((from/9 + 3)*16 + from%9 +3);  //将10*9的棋盘位置转化为16*16的，为了使用LegalMove等函数
		mv.to=((dest/9 + 3)*16 + dest%9 +3);
		
		if (m_BoardGame.LegalMove(mv))
		{
			m_BoardGame.MakeMove(mv);
			m_interface[dest]=m_interface[from];
			m_interface[from]=0;

			m_MoveStack.push_back(m_BoardGame.MoveStack[m_BoardGame.StackTop-1]);

			//高亮显示玩家走法：
			m_SelectMoveTo=dest;
			RequireDrawCell(dest);
			m_SelectMoveFrom=from;
			RequireDrawCell(from);
			Beep(200,300);

			int num=m_BoardGame.HasLegalMove();
			if (num==0)
			{
				KillTimer(m_RedTimer);
				m_gameState=GAMEOVER;
				MessageBox(_T("玩家获胜，计算机输了。恭喜您！"),_T("系统消息"));
				return;
			}

			KillTimer(m_RedTimer);
			m_gameState=BLACKTHINK;

			ComputerPlay();
		}
		else
		{
			//MessageBox(_T("试图的走棋不合法或导致将帅被杀！"),_T("系统信息"));
			//TODO:提示更具体的信息，或高亮显示
		}
	}

	CDialog::OnLButtonDown(nFlags, point);
}

void CMyCnChessDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	
	CDialog::OnTimer(nIDEvent);
	switch(nIDEvent)
	{
	case 2:
		m_tsBlkTimePass =  m_tsBlkTimePass + CTimeSpan(0,0,0,1);
		m_tsBlkTimeLeft = m_TotalTime - m_tsBlkTimePass;
		m_BlackTimePass_Ctrl.SetWindowText(m_tsBlkTimeLeft.Format("%H:%M:%S"));
		m_BlackTimePass_Ctrl.SetWindowText(m_tsBlkTimePass.Format("%H:%M:%S"));
		break;
	case 1:
		m_tsRedTimePass =  m_tsRedTimePass + CTimeSpan(0,0,0,1);
		if(m_tsRedTimePass > m_TotalTime)
		{
			m_gameState = GAMEOVER;
			KillTimer(m_RedTimer);
			MessageBox(_T("玩家超时判负"),_T( "系统消息"));
		}
		else
		{
			m_tsRedTimeLeft = m_TotalTime - m_tsRedTimePass;
			m_WhiteTimePass_Ctrl.SetWindowText(m_tsRedTimeLeft.Format("%H:%M:%S"));
			m_WhiteTimePass_Ctrl.SetWindowText(m_tsRedTimePass.Format("%H:%M:%S"));
		}
		break;
	default:
		break;
	}
	CDialog::OnTimer(nIDEvent);
}


void CMyCnChessDlg::OnBnClickedGiveup()
{
	// TODO: Add your control notification handler code here
	m_gameState=GAMEOVER;
	KillTimer(m_RedTimer);
	MessageBox(_T("玩家认输，计算机获胜"),_T("系统消息"));
}

void CMyCnChessDlg::OnBnClickedRegret()
{
	// TODO: Add your control notification handler code here
	if (m_MoveStack.size()==0 || m_MoveStack.size()%2==1) return;

	//*********
	list<move>::iterator it=--m_MoveStack.end();
	move m1=*it; //计算机刚走的棋
	m_MoveStack.pop_back();

	//修改游戏逻辑：
	m_BoardGame.ChangeSide();
	//设置棋盘数组
	m_BoardGame.board[m1.from] = m_BoardGame.board[m1.to];
	m_BoardGame.board[m1.to] = m1.capture;

	//设置棋子数组
	if(m1.capture>0)
		m_BoardGame.piece[m1.capture] = m1.to;  //piece数组储存各棋子的位置
	m_BoardGame.piece[m_BoardGame.board[m1.from]] = m1.from;

	//修改界面：
	int from,to;
	from = ((m1.from/16 -3) *9 + m1.from%16 -3);	//将16*16的棋盘位置转换成10*9的棋盘位置
	to = ((m1.to/16 -3) *9 + m1.to%16 -3);
	m_interface[from]=m_interface[to];
	m_SelectMoveFrom=m_SelectMoveTo=NOMOVE;
	RequireDrawCell(from);
	m_interface[to]=m1.capture;
	RequireDrawCell(to);


	//********
	it=--m_MoveStack.end();
	move m2=*it; //玩家刚走的棋
	m_MoveStack.pop_back();

	//修改游戏逻辑：
	m_BoardGame.ChangeSide();
	//设置棋盘数组
	m_BoardGame.board[m2.from] = m_BoardGame.board[m2.to];
	m_BoardGame.board[m2.to] = m2.capture;

	//设置棋子数组
	if(m2.capture>0)
		m_BoardGame.piece[m2.capture] = m2.to;  //piece数组储存各棋子的位置
	m_BoardGame.piece[m_BoardGame.board[m2.from]] = m2.from;


	//修改界面
	from = ((m2.from/16 -3) *9 + m2.from%16 -3);	//将16*16的棋盘位置转换成10*9的棋盘位置
	to = ((m2.to/16 -3) *9 + m2.to%16 -3);
	m_interface[from]=m_interface[to];
	RequireDrawCell(from);
	m_interface[to]=m2.capture;
	RequireDrawCell(to);

}


void CMyCnChessDlg::OnBnClickedHelp()
{
	// TODO: Add your control notification handler code here
	OnHelp();
}

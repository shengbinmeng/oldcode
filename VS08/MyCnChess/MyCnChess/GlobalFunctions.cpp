#include "stdafx.h"

//棋子整数值转换成字符表示
int IntToSubscript(int a)
{
	if(a<16 && a>=48)
		return 15;

	int b=0;
	if(a >=32)
	{
		a = a-16;
		b=7;
	}
	switch(a)
	{
	case 16:	return 0+b;
	case 17:
	case 18:	return 1+b;
	case 19:
	case 20:	return 2+b;
	case 21:
	case 22:	return 3+b;
	case 23:
	case 24:	return 4+b;
	case 25:
	case 26:	return 5+b;
	case 27:
	case 28:
	case 29:
	case 30:
	case 31:	return 6+b;
	default:	return 7+b;
	}

}
// MyCnChessDlg.h : 头文件
//

#pragma once
#include "afxwin.h"
#include "BoardGame.h"
#include <list>
using namespace std;

#define SIZE_X 9
#define SIZE_Y 10
#define BOARD_SIZE SIZE_X*SIZE_Y
#define GRILLWIDTH 39
#define GRILLHEIGHT 39
#define BORDERWIDTH 67-20
#define BORDERHEIGHT 38-20

#define WHITETHINK 0
#define BLACKTHINK 1
#define GAMEOVER 2

#define NOMOVE -1

// CMyCnChessDlg 对话框
class CMyCnChessDlg : public CDialog
{
// 构造
public:
	CMyCnChessDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_MYCNCHESS_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:

	//界面相关变量
	BYTE m_interface[BOARD_SIZE];
	CBitmap m_BoardBmp;// 棋盘位图
	CImageList m_Chessman;
	CRect m_rectBoard;
	int m_nBoardWidth;
	int m_nBoardHeight;
	int m_gameState;
	short m_ComputerSide, m_HumanSide;
	short m_SelectMoveFrom;
	short m_SelectMoveTo;

	//游戏逻辑相关变量
	CBoardGame m_BoardGame;
	list<move> m_MoveStack;
	int m_Level;

	//计时的变量：
	CTimeSpan m_tsBlkTimeLeft,m_tsRedTimeLeft;
	CTimeSpan m_tsBlkTimePass,m_tsRedTimePass;
	CTimeSpan m_TotalTime;
	UINT m_BlkTimer,m_RedTimer;

	//初始化棋盘信息
	void InitGameData(void);
	//判断鼠标点是否在棋盘内
	bool IsPtInBoard(CPoint point);
	//重绘棋子区
	void RequireDrawCell(short position);
	//得到棋子区域
	CRect GetPieceRect(short position);
	//得到鼠标点的棋子位置
	short GetPieceByPosition(CPoint point);
	//计算机行棋
	void ComputerPlay();


	afx_msg void OnBnClickedButBegin();
	afx_msg void OnBnClickedButClose();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	CStatic m_WhiteTimePass_Ctrl;
	CStatic m_BlackTimePass_Ctrl;
	afx_msg void OnBnClickedGiveup();
	afx_msg void OnBnClickedRegret();
protected:
	virtual void OnCancel();
public:
	CComboBox m_Level_Ctrl;
	afx_msg void OnBnClickedHelp();
};

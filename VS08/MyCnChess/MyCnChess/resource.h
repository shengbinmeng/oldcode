//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MyCnChess.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MYCNCHESS_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDB_BITMAP1                     131
#define IDB_CHESSMAN                    131
#define IDB_CHESSBOARD                  132
#define IDB_CHESSMAN1                   133
#define IDC_BUT_BEGIN                   1000
#define IDC_BUT_CLOSE                   1001
#define IDC_WHITETIMEPASS               1005
#define IDC_BLACKTIMEPASS               1006
#define IDC_REGRET                      1007
#define IDC_GIVEUP                      1008
#define IDC_COMBO1                      1010
#define IDC_COMBO_LEVEL                 1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

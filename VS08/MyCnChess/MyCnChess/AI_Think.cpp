
#include "stdafx.h"
#include "BoardGame.h"

#define MAX_VALUE  3000	//估值最大值

void CBoardGame::ChangeSide()
{
	side = 1- side;
}

bool  CBoardGame::MakeMove(move m)
{
	unsigned char from, dest, p;
	int SideTag = (side==0 ? 32:16);	//此处为对方将帅的值，其它地方多表示本方将帅值

	from = m.from; 
	dest = m.to;

	//设置走法栈
	MoveStack[StackTop].from = from; 
	MoveStack[StackTop].to = dest; 
	MoveStack[StackTop].capture = p = board[dest]; 
	StackTop++;  

	//设置棋子数组
	if(p>0)
		piece[p] = 0;
	piece[board[from]] = dest;

	//设置棋盘数组
	board[dest] = board[from];
	board[from] = 0;

	ply++; 

	ChangeSide();

	return p == SideTag;
}

void CBoardGame::UnMakeMove(void)
{
	unsigned char from, dest,p;

	StackTop--; 
	ply--;

	ChangeSide();

	from = MoveStack[StackTop].from; 
	dest = MoveStack[StackTop].to;
	p = MoveStack[StackTop].capture;

	//设置棋盘数组
	board[from] = board[dest];
	board[dest] = p;

	//设置棋子数组
	if(p>0)
		piece[p] = dest;
	piece[board[from]] = from;

}

int CBoardGame::AlphaBetaSearch(int depth, int alpha, int beta)	// Alpha-Beta搜索算法
{
	int value;
	move MoveArray[128];
	move mv;
	int i;

	if(depth ==0)
		return Evalue();

	int num = GenAllMove(MoveArray);
	for(i = 0 ; i<num; i++)
	{
		mv = MoveArray[i];
		MakeMove(mv);
		value = -AlphaBetaSearch(depth -1, -beta, -alpha);
		UnMakeMove();
		if(value >= beta)
			return beta;
		if(value > alpha)
		{
			alpha = value;
			if(depth == MaxDepth)
				BestMove = mv;
		}
	}
	return alpha;
}

//随机走棋的AI
void CBoardGame::ComputerThink0()
{
	BestMove.from = 0;
	BestMove.to = 0;

	move MoveArray[128];
	int num = GenAllMove(MoveArray);
	int i=rand()%num;
	BestMove=MoveArray[i];
}

//考虑棋子价值的AI
void CBoardGame::ComputerThink1()
{
	BestMove.from = 0;
	BestMove.to = 0;

	StackTop = 0;
	ply =0;
	MaxDepth = 4;
	AlphaBetaSearch(MaxDepth,-MAX_VALUE, MAX_VALUE);
}

//增加一层搜索深度的AI（思考时间增加了约一二倍，但更强了）
void CBoardGame::ComputerThink2()
{
	BestMove.from = 0;
	BestMove.to = 0;

	StackTop = 0;
	ply =0;
	MaxDepth = 5;
	AlphaBetaSearch(MaxDepth,-MAX_VALUE, MAX_VALUE);

}

void CBoardGame::ComputerThink3()
{
	BestMove.from = 0;
	BestMove.to = 0;
	//待开发

}
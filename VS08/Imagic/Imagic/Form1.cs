using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Imagic
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        /*
        //图像扭曲
        private void button1_Click(object sender, EventArgs e)
        {
            //这里只实现了一个绘图过程，并没有把数据保存下来，一旦重绘就没有了
            pictureBox2.Image = null;
            pictureBox2.Refresh();

            Bitmap bmp1 = new Bitmap(pictureBox1.Image);
            Graphics g = pictureBox2.CreateGraphics();
            Bitmap bmp2 = new Bitmap(pictureBox2.Width, pictureBox2.Height, g);   //???????????????
            Point ulCorner = new Point(10, 10);
            Point urCorner = new Point(200, 10);
            Point llCorner = new Point(80, 200);

            Point[] desPara ={ ulCorner, urCorner, llCorner };
            g.DrawImage(bmp1, desPara);

            pictureBox2.Image = bmp2;
            pictureBox2.Refresh();

        }
        */


        //色彩逆反
        private void button1_Click(object sender, EventArgs e)
        {
            Color c;
            Bitmap bmp1 = new Bitmap(pictureBox1.Image);
            Bitmap bmp2 = new Bitmap(pictureBox1.Image);

            for (int i = 0; i < bmp1.Width; i++)
            {
                for (int j = 0; j < bmp1.Height; j++)
                {
                    c = bmp1.GetPixel(i, j);
                    Color c1 = Color.FromArgb(255 - c.R, 255 - c.G, 255 - c.B);
                    bmp2.SetPixel(i, j, c1);
                }

            }
            pictureBox2.Image = bmp2;
            

        }



        //打开图像
        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofdlg = new OpenFileDialog();
            ofdlg.Filter = "JPEG File(*.jpg)|*.jpg|Bmp File(*.bmp)|*.bmp|GIF File(*.gif)|*.gif";
            if (ofdlg.ShowDialog() == DialogResult.OK)
            {
                Bitmap image = new Bitmap(ofdlg.FileName);
                pictureBox1.Image = image;
                pictureBox1.Refresh();
            }
            
        }

        //浮雕处理
        private void button5_Click(object sender, EventArgs e)
        {

            //浮雕效果的算法：g(i,j)=f(i,j)-f(i-1,j)+const(如128)
            pictureBox2.Image = null;

            Color c1, c2;
            int rr, gg, bb;

            Bitmap bmp1 = new Bitmap(pictureBox1.Image);
            Bitmap bmp2 = new Bitmap(pictureBox1.Image);



            for (int i = 0; i < bmp1.Width - 1; i++)
            {
                for (int j = 0; j < bmp1.Height - 1; j++)
                {
                    c1 = bmp1.GetPixel(i, j);
                    c2 = bmp1.GetPixel(i + 1, j);
                    rr = c2.R - c1.R + 128;
                    gg = c2.G - c1.G + 128;
                    bb = c2.B - c1.B + 128;
                    if (rr > 255) rr = 255;
                    if (rr < 0) rr = 0;
                    if (gg > 255) gg = 255;
                    if (gg < 0) gg = 0;
                    if (bb > 255) bb = 255;
                    if (bb < 0) bb = 0;


                    Color c = Color.FromArgb(rr, gg, bb);
                    bmp2.SetPixel(i, j, c);
                }

            }

            pictureBox2.Image = bmp2;
            //pictureBox2.Refresh(); 似乎自己会刷新
        }

        //镜像变换
        private void button4_Click(object sender, EventArgs e)
        {

            Color c;
            Bitmap bmp1 = new Bitmap(pictureBox1.Image);
            Bitmap bmp2 = new Bitmap(pictureBox1.Image);

            for(int i=0;i<bmp1.Width;i++)
            {
                for(int j=0;j<bmp1.Height;j++)
                {
                    c= bmp1.GetPixel(i, j);
                    bmp2.SetPixel(bmp1.Width-1- i, j, c);
                }

            }

            pictureBox2.Image = bmp2;
            //pictureBox2.Refresh(); 似乎自己会刷新


        }

        //中心膨胀，最邻近插值（直接取整点）
        private void button6_Click(object sender, EventArgs e)
        {
            Color c;
            Bitmap bmp1 = new Bitmap(pictureBox1.Image);
            Bitmap bmp2 = new Bitmap(pictureBox1.Image);
            
            int cx =(int)(0.5 * bmp1.Width), cy =(int)(0.5 * bmp1.Height);
            int x,y;
            for(int i=0;i<bmp1.Width;i++)
            {
                for(int j=0;j<bmp1.Height;j++)
                {
                    if(4*(i-cx)*(i-cx)+9*(j-cy)*(j-cy)<(0.8*bmp1.Width)*(0.8*bmp1.Width))
                    {
                        x = (int)(0.2 * (i - cx) + cx);
                        y = (int)(0.8 * (j - cy) + cy);
                        c = bmp1.GetPixel(x, y);
                        bmp2.SetPixel(i, j, c);
                    }
                    
                }
            }


            pictureBox2.Image = bmp2;
            pictureBox2.Refresh();

        }

        //左侧拉伸，最邻近插值
        private void button7_Click(object sender, EventArgs e)
        {
            Color c;
            Bitmap bmp1 = new Bitmap(pictureBox1.Image);
            Bitmap bmp2 = new Bitmap(pictureBox1.Image);

            int cx = (int)(0.5 * bmp1.Width), cy = (int)(0.5 * bmp1.Height);
            int x, y;
            Double d;

            for (int j =0; j <bmp1.Height; j++)
            {
                for (int i =0; i < cx; i++)
                {
                    d = ( 0.4* (i - cx) + cx);

                    x = (int)(d + 0.5); //四舍五入，最邻近插值
                    if (x > bmp1.Width - 1) x = bmp1.Width - 1;

                    y = j;
                    c = bmp1.GetPixel(x, y);
                    bmp2.SetPixel(i, j, c);

                }
            }

            pictureBox2.Image = bmp2;
            pictureBox2.Refresh();

        }

        private void button8_Click(object sender, EventArgs e)
        {
            pictureBox2.Image = pictureBox1.Image;

        }

        //中心膨胀，双线性插值
        private void button9_Click(object sender, EventArgs e)
        {


            Color c1, c2, c3, c4;
            Bitmap bmp1 = new Bitmap(pictureBox1.Image);
            Bitmap bmp2 = new Bitmap(pictureBox1.Image);

            int cx = (int)(0.5 * bmp1.Width), cy = (int)(0.5 * bmp1.Height);
            int x, y;
            double xd, yd, u, v;
            for (int i = 0; i < bmp1.Width; i++)
            {
                for (int j = 0; j < bmp1.Height; j++)
                {
                    if (4 * (i - cx) * (i - cx) + 9 * (j - cy) * (j - cy) < (0.8 * bmp1.Width) * (0.8 * bmp1.Width))
                    {
                        xd = (0.2 * (i - cx) + cx);
                        yd = (0.8 * (j - cy) + cy);
                        x = (int)xd;
                        y = (int)yd;
                        u = xd - x;
                        v = yd - y;

                        c1 = bmp1.GetPixel(x, y); c2 = bmp1.GetPixel(x + 1, y);
                        c3 = bmp1.GetPixel(x, y + 1); c4 = bmp1.GetPixel(x + 1, y + 1);

                        //双线性插值求得各颜色分量，若是灰度图像则直接求得灰度值
                        int rr, gg, bb;
                        rr = (int)((1.0 - u) * (1.0 - v) * c1.R + u * (1.0 - v) * c2.R + (1.0 - u) * v * c3.R + u * v * c4.R);
                        gg = (int)((1.0 - u) * (1.0 - v) * c1.G + u * (1.0 - v) * c2.G + (1.0 - u) * v * c3.G + u * v * c4.G);
                        bb = (int)((1.0 - u) * (1.0 - v) * c1.B + u * (1.0 - v) * c2.B + (1.0 - u) * v * c3.B + u * v * c4.B);

                        Color c = Color.FromArgb(rr, gg, bb);
                        bmp2.SetPixel(i, j, c);
                    }

                }
            }


            pictureBox2.Image = bmp2;
            pictureBox2.Refresh();


        }

        //另存图像
        private void button3_Click(object sender, EventArgs e)
        {
            string str;
            if (pictureBox2.Image == null) { MessageBox.Show("没有图片！"); return; }
            Bitmap bmp1 = new Bitmap(pictureBox2.Image);
            saveFileDialog1.Filter = "JPEG File(*.jpg)|*.jpg|Bmp File(*.bmp)|*.bmp|GIF File(*.gif)|*.gif";
            saveFileDialog1.ShowDialog();
            str = saveFileDialog1.FileName;
            if(str!="")pictureBox2.Image.Save(str);
        }

       
    }
}
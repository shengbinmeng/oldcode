// Comps.cpp: implementation of the CComps class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MyEWB.h"
#include "Comps.h"
#include "DlgComPro.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CComp::CComp()
{
	bSelected=0;
	m_LeftDir=0;
	m_Type=0;
}
CComp::CComp(CRect position)
{
	m_Position=position;
	lLinkPoint.x=m_Position.left-16;
	lLinkPoint.y=(m_Position.top+m_Position.bottom)/2;
	rLinkPoint.x=m_Position.right+16;
	rLinkPoint.y=(m_Position.top+m_Position.bottom)/2;

}
void CComp::Draw(CDC* pDC)
{
	
}
void CComp::ShowProperties()
{
	
}
void CComp::GetLeftLinkPoint(CPoint& lPoint)
{
	lPoint=lLinkPoint;
}
void CComp::GetRightLinkPoint(CPoint& rPoint)
{
	rPoint=rLinkPoint;
}

void CComp::Serialize(CArchive& ar)
{
	CObject::Serialize(ar);

	if (ar.IsStoring())
	{
		ar<<(int)(bSelected);
		ar<<m_Lable;
		ar<<m_ID;
		ar<<m_Position;
		ar<<m_LeftDir;

		int NumL=m_LeftComps.GetSize();
		ar<<NumL;
		for (int i=0;i<NumL;i++)
		{
			ar<<m_LeftComps[i];	
		}
		int NumR=m_RightComps.GetSize();
		ar<<NumR;
		for (int i=0;i<NumR;i++)
		{
			ar<<m_RightComps[i];
		}
	} 
	else
	{
		int a;
		ar>>a;
		bSelected=(bool)a;
		ar>>m_Lable;
		ar>>m_ID;
		ar>>m_Position;
		ar>>m_LeftDir;

		int NumL,NumR;
		ar>>NumL;
		for (int i=0;i<NumL;i++)
		{
			CComp* pL=new CComp;
			ar>>pL;
			m_LeftComps.Add(pL);			
		}
		ar>>NumR;
		for (int i=0;i<NumR;i++)
		{
			CComp* pR=new CComp;
			ar>>pR;	
			m_RightComps.Add(pR);
		}
	}
}


CCompR::CCompR()
{
	int i=rand()%5;
	m_Value=1000;
	m_Position=CRect(96+8*i,96+8*i,160+8*i,128+8*i);
	m_LeftDir=0;
	m_Type=1;
}
CCompR::CCompR(float value,CRect position)
{
	m_Position=position;
	m_Value=value;	
}
void CCompR::Draw(CDC* pDC)
{
	CPoint center;
	center.x=(m_Position.left+m_Position.right)/2;
	center.y=(m_Position.top+m_Position.bottom)/2;
	CString s;

	switch(m_LeftDir)				//m_LeftDir:标记元件逻辑左端的视图方位，0表示左，1表示上，2表示右，3表示下
	{
	case 0:
		//重新确定m_Position：
		m_Position.top=center.y-2*8;
		m_Position.bottom=center.y+2*8;
		m_Position.left=center.x-4*8;
		m_Position.right=center.x+4*8;
		lLinkPoint.x=center.x-6*8;
		lLinkPoint.y=center.y;
		rLinkPoint.x=center.x+6*8;
		rLinkPoint.y=center.y;
		//画引脚：
		pDC->MoveTo(m_Position.left,center.y);
		pDC->LineTo(lLinkPoint);
		pDC->MoveTo(m_Position.right,center.y);
		pDC->LineTo(rLinkPoint);
		//标输入端
		pDC->MoveTo(m_Position.left+3,m_Position.top);
		pDC->LineTo(m_Position.left+3,m_Position.top+6);
		pDC->MoveTo(m_Position.left,m_Position.top+3);
		pDC->LineTo(m_Position.left+6,m_Position.top+3);
		//画元件：
		{
		CPoint p(m_Position.left,m_Position.top+16);
		pDC->MoveTo(p);
		while(p.x<m_Position.right)
		{
			p.x+=4; p.y+=6;
			pDC->LineTo(p);
			p.x+=4; p.y-=6;
			pDC->LineTo(p);
			p.x+=4; p.y-=6;
			pDC->LineTo(p);
			p.x+=4; p.y+=6;
			pDC->LineTo(p);
		}
		}
		//写标称：
		s.Format("  R=%G ohm",m_Value);
		pDC->TextOut(m_Position.left-15,m_Position.top-20,m_Lable+s);

		break;
	case 1:
		//重新确定m_Position：
		m_Position.top=center.y-4*8;
		m_Position.bottom=center.y+4*8;
		m_Position.left=center.x-2*8;
		m_Position.right=center.x+2*8;
		lLinkPoint.x=center.x;
		lLinkPoint.y=center.y-6*8;
		rLinkPoint.x=center.x;
		rLinkPoint.y=center.y+6*8;
		//画引脚：
		pDC->MoveTo(center.x,m_Position.top);
		pDC->LineTo(lLinkPoint);
		pDC->MoveTo(center.x,m_Position.bottom);
		pDC->LineTo(rLinkPoint);
		//标输入端
		pDC->MoveTo(m_Position.left+3,m_Position.top);
		pDC->LineTo(m_Position.left+3,m_Position.top+6);
		pDC->MoveTo(m_Position.left,m_Position.top+3);
		pDC->LineTo(m_Position.left+6,m_Position.top+3);
		//画元件：
		{
			CPoint p(m_Position.left+16,m_Position.top);
			pDC->MoveTo(p);
			while(p.y<m_Position.bottom)
			{
				p.x-=6; p.y+=4;
				pDC->LineTo(p);
				p.x+=6; p.y+=4;
				pDC->LineTo(p);
				p.x+=6; p.y+=4;
				pDC->LineTo(p);
				p.x-=6; p.y+=4;
				pDC->LineTo(p);
			}
		}

		s.Format("  R=%G ohm",m_Value);
		pDC->TextOut(m_Position.right+4,(m_Position.top+m_Position.bottom)/2,m_Lable+s);

		break;
	case 2:
		//重新确定m_Position：
		m_Position.top=center.y-2*8;
		m_Position.bottom=center.y+2*8;
		m_Position.left=center.x-4*8;
		m_Position.right=center.x+4*8;
		lLinkPoint.y=center.y;
		lLinkPoint.x=center.x+6*8;
		rLinkPoint.y=center.y;
		rLinkPoint.x=center.x-6*8;
		//画引脚：
		pDC->MoveTo(m_Position.left,center.y);
		pDC->LineTo(rLinkPoint);
		pDC->MoveTo(m_Position.right,center.y);
		pDC->LineTo(lLinkPoint);
		//标输入端
		pDC->MoveTo(m_Position.right-3,m_Position.bottom-6);
		pDC->LineTo(m_Position.right-3,m_Position.bottom);
		pDC->MoveTo(m_Position.right-6,m_Position.bottom-3);
		pDC->LineTo(m_Position.right,m_Position.bottom-3);
		//画元件：	
		{
			CPoint p(m_Position.left,m_Position.top+16);
			pDC->MoveTo(p);
			while(p.x<m_Position.right)
			{
				p.x+=4; p.y+=6;
				pDC->LineTo(p);
				p.x+=4; p.y-=6;
				pDC->LineTo(p);
				p.x+=4; p.y-=6;
				pDC->LineTo(p);
				p.x+=4; p.y+=6;
				pDC->LineTo(p);
			}
		}

		s.Format("  R=%G ohm",m_Value);
		pDC->TextOut(m_Position.left-15,m_Position.top-20,m_Lable+s);

		break;
	case 3:
		//重新确定m_Position：
		m_Position.top=center.y-4*8;
		m_Position.bottom=center.y+4*8;
		m_Position.left=center.x-2*8;
		m_Position.right=center.x+2*8;
		lLinkPoint.x=center.x;
		lLinkPoint.y=center.y+6*8;
		rLinkPoint.x=center.x;
		rLinkPoint.y=center.y-6*8;
		//画引脚：
		pDC->MoveTo(center.x,m_Position.top);
		pDC->LineTo(rLinkPoint);
		pDC->MoveTo(center.x,m_Position.bottom);
		pDC->LineTo(lLinkPoint);
		//标输入端：
		pDC->MoveTo(m_Position.right-3,m_Position.bottom-6);
		pDC->LineTo(m_Position.right-3,m_Position.bottom);
		pDC->MoveTo(m_Position.right-6,m_Position.bottom-3);
		pDC->LineTo(m_Position.right,m_Position.bottom-3);
		//画元件：
		{
			CPoint p(m_Position.left+16,m_Position.top);
			pDC->MoveTo(p);
			while(p.y<m_Position.bottom)
			{
				p.x-=6; p.y+=4;
				pDC->LineTo(p);
				p.x+=6; p.y+=4;
				pDC->LineTo(p);
				p.x+=6; p.y+=4;
				pDC->LineTo(p);
				p.x-=6; p.y+=4;
				pDC->LineTo(p);
			}
		}

		s.Format("  R=%G ohm",m_Value);
		pDC->TextOut(m_Position.right+4,(m_Position.top+m_Position.bottom)/2,m_Lable+s);
		break;
	}

	

}
void CCompR::ShowProperties()
{
	CDlgComPro dlg;
	dlg.m_CompLable=m_Lable;
	dlg.m_CompValue=m_Value;
	if (dlg.DoModal()==IDOK)
	{
		m_Value=dlg.m_CompValue;
		m_Lable=dlg.m_CompLable;
	}
}
void CCompR::Serialize(CArchive& ar)
{
	CComp::Serialize(ar);
	if (ar.IsStoring())
	{
		ar<<m_Value;
	} 
	else
	{
		ar>>m_Value;
	}

}


CCompC::CCompC()
{
	int i=rand()%5;
	m_Value=0.1;
	m_Position=CRect(128+8*i,128+8*i,176+8*i,160+8*i);
	m_LeftDir=0;
	m_Type=2;
}
CCompC::CCompC(float value,CRect position)
{
	m_Position=position;
	m_Value=value;
}
void CCompC::Draw(CDC* pDC)
{
	CPoint center;
	center.x=(m_Position.left+m_Position.right)/2;
	center.y=(m_Position.top+m_Position.bottom)/2;
	CString s;

	switch(m_LeftDir)				//m_LeftDir:标记元件逻辑左端的视图方位，0表示左，1表示上，2表示右，3表示下
	{
	case 0:
		//重新确定m_Position：
		m_Position.top=center.y-2*8;
		m_Position.bottom=center.y+2*8;
		m_Position.left=center.x-3*8;
		m_Position.right=center.x+3*8;
		lLinkPoint.x=center.x-5*8;
		lLinkPoint.y=center.y;
		rLinkPoint.x=center.x+5*8;
		rLinkPoint.y=center.y;
		//画引脚：
		pDC->MoveTo(m_Position.left,center.y);
		pDC->LineTo(lLinkPoint);
		pDC->MoveTo(m_Position.right,center.y);
		pDC->LineTo(rLinkPoint);
		//标输入端
		pDC->MoveTo(m_Position.left+3,m_Position.top);
		pDC->LineTo(m_Position.left+3,m_Position.top+6);
		pDC->MoveTo(m_Position.left,m_Position.top+3);
		pDC->LineTo(m_Position.left+6,m_Position.top+3);
		//画元件：
		pDC->MoveTo(m_Position.left,center.y);
		pDC->LineTo(m_Position.left+18,m_Position.top+16);
		pDC->MoveTo(m_Position.left+18,m_Position.top+2);
		pDC->LineTo(m_Position.left+18,m_Position.bottom-2);
		pDC->MoveTo(m_Position.left+30,m_Position.top+2);
		pDC->LineTo(m_Position.left+30,m_Position.bottom-2);
		pDC->MoveTo(m_Position.left+30,m_Position.top+16);
		pDC->LineTo(m_Position.left+48,m_Position.top+16);	
		//写标称：
		s.Format("  C=%G F",m_Value);
		pDC->TextOut(m_Position.left-15,m_Position.top-20,m_Lable+s);
		break;
	case 1:
		//重新确定m_Position：
		m_Position.top=center.y-3*8;
		m_Position.bottom=center.y+3*8;
		m_Position.left=center.x-2*8;
		m_Position.right=center.x+2*8;
		lLinkPoint.x=center.x;
		lLinkPoint.y=center.y-5*8;
		rLinkPoint.x=center.x;
		rLinkPoint.y=center.y+5*8;
		//画引脚：
		pDC->MoveTo(center.x,m_Position.top);
		pDC->LineTo(lLinkPoint);
		pDC->MoveTo(center.x,m_Position.bottom);
		pDC->LineTo(rLinkPoint);
		//标输入端
		pDC->MoveTo(m_Position.left+3,m_Position.top);
		pDC->LineTo(m_Position.left+3,m_Position.top+6);
		pDC->MoveTo(m_Position.left,m_Position.top+3);
		pDC->LineTo(m_Position.left+6,m_Position.top+3);
		//画元件：
		pDC->MoveTo(center.x,m_Position.top);
		pDC->LineTo(m_Position.left+16,m_Position.top+18);
		pDC->MoveTo(m_Position.left+2,m_Position.top+18);
		pDC->LineTo(m_Position.right-2,m_Position.top+18);
		pDC->MoveTo(m_Position.left+2,m_Position.top+30);
		pDC->LineTo(m_Position.right-2,m_Position.top+30);
		pDC->MoveTo(m_Position.left+16,m_Position.top+30);
		pDC->LineTo(m_Position.left+16,m_Position.top+48);	
		
		s.Format("  C=%G F",m_Value);
		pDC->TextOut(m_Position.right+4,(m_Position.top+m_Position.bottom)/2,m_Lable+s);
		break;
	case 2:
		//重新确定m_Position：
		m_Position.top=center.y-2*8;
		m_Position.bottom=center.y+2*8;
		m_Position.left=center.x-3*8;
		m_Position.right=center.x+3*8;
		lLinkPoint.y=center.y;
		lLinkPoint.x=center.x+5*8;
		rLinkPoint.y=center.y;
		rLinkPoint.x=center.x-5*8;
		//画引脚：
		pDC->MoveTo(m_Position.left,center.y);
		pDC->LineTo(rLinkPoint);
		pDC->MoveTo(m_Position.right,center.y);
		pDC->LineTo(lLinkPoint);
		//标输入端
		pDC->MoveTo(m_Position.right-3,m_Position.bottom-6);
		pDC->LineTo(m_Position.right-3,m_Position.bottom);
		pDC->MoveTo(m_Position.right-6,m_Position.bottom-3);
		pDC->LineTo(m_Position.right,m_Position.bottom-3);
		//画元件：
		
		pDC->MoveTo(m_Position.left,center.y);
		pDC->LineTo(m_Position.left+18,m_Position.top+16);
		pDC->MoveTo(m_Position.left+18,m_Position.top+2);
		pDC->LineTo(m_Position.left+18,m_Position.bottom-2);
		pDC->MoveTo(m_Position.left+30,m_Position.top+2);
		pDC->LineTo(m_Position.left+30,m_Position.bottom-2);
		pDC->MoveTo(m_Position.left+30,m_Position.top+16);
		pDC->LineTo(m_Position.left+48,m_Position.top+16);

		s.Format("  C=%G F",m_Value);
		pDC->TextOut(m_Position.left-15,m_Position.top-20,m_Lable+s);
		break;
	case 3:
		//重新确定m_Position：
		m_Position.top=center.y-3*8;
		m_Position.bottom=center.y+3*8;
		m_Position.left=center.x-2*8;
		m_Position.right=center.x+2*8;
		lLinkPoint.x=center.x;
		lLinkPoint.y=center.y+5*8;
		rLinkPoint.x=center.x;
		rLinkPoint.y=center.y-5*8;
		//画引脚：
		pDC->MoveTo(center.x,m_Position.top);
		pDC->LineTo(rLinkPoint);
		pDC->MoveTo(center.x,m_Position.bottom);
		pDC->LineTo(lLinkPoint);
		//标输入端
		pDC->MoveTo(m_Position.right-3,m_Position.bottom-6);
		pDC->LineTo(m_Position.right-3,m_Position.bottom);
		pDC->MoveTo(m_Position.right-6,m_Position.bottom-3);
		pDC->LineTo(m_Position.right,m_Position.bottom-3);
		//画元件：
		pDC->MoveTo(center.x,m_Position.top);
		pDC->LineTo(m_Position.left+16,m_Position.top+18);
		pDC->MoveTo(m_Position.left+2,m_Position.top+18);
		pDC->LineTo(m_Position.right-2,m_Position.top+18);
		pDC->MoveTo(m_Position.left+2,m_Position.top+30);
		pDC->LineTo(m_Position.right-2,m_Position.top+30);
		pDC->MoveTo(m_Position.left+16,m_Position.top+30);
		pDC->LineTo(m_Position.left+16,m_Position.top+48);

		s.Format("  C=%G F",m_Value);
		pDC->TextOut(m_Position.right+4,(m_Position.top+m_Position.bottom)/2,m_Lable+s);
		break;
	}
}
void CCompC::ShowProperties()
{
	CDlgComPro dlg;
	dlg.m_CompLable=m_Lable;
	dlg.m_CompValue=m_Value;
	if (dlg.DoModal()==IDOK)
	{
		m_Value=dlg.m_CompValue;
		m_Lable=dlg.m_CompLable;
	}
}
void CCompC::Serialize(CArchive& ar)
{
	CComp::Serialize(ar);
	if (ar.IsStoring())
	{
		ar<<m_Value;
	} 
	else
	{
		ar>>m_Value;
	}
}



CCompL::CCompL()
{
	int i=rand()%5;
	m_Value=0.1;
	m_Position=CRect(104+8*i,104+8*i,168+8*i,136+8*i);
	m_LeftDir=0;
	m_Type=3;
}
CCompL::CCompL(float value,CRect position)
{
	m_Value=value;
	m_Position=position;
}
void CCompL::Draw(CDC* pDC)
{
	CPoint center;
	center.x=(m_Position.left+m_Position.right)/2;
	center.y=(m_Position.top+m_Position.bottom)/2;
	CString s;

	switch(m_LeftDir)				//m_LeftDir:标记元件逻辑左端的视图方位，0表示左，1表示上，2表示右，3表示下
	{
	case 0:
		//重新确定m_Position：
		m_Position.top=center.y-2*8;
		m_Position.bottom=center.y+2*8;
		m_Position.left=center.x-4*8;
		m_Position.right=center.x+4*8;
		lLinkPoint.x=center.x-6*8;
		lLinkPoint.y=center.y;
		rLinkPoint.x=center.x+6*8;
		rLinkPoint.y=center.y;
		//画引脚：
		pDC->MoveTo(m_Position.left,center.y);
		pDC->LineTo(lLinkPoint);
		pDC->MoveTo(m_Position.right,center.y);
		pDC->LineTo(rLinkPoint);
		//标输入端
		pDC->MoveTo(m_Position.left+3,m_Position.top);
		pDC->LineTo(m_Position.left+3,m_Position.top+6);
		pDC->MoveTo(m_Position.left,m_Position.top+3);
		pDC->LineTo(m_Position.left+6,m_Position.top+3);
		//画元件：
		{
		CRect rect(m_Position.left,center.y-8,m_Position.left+16,center.y+8);
		CPoint end(m_Position.left,center.y);
		CPoint start(m_Position.left+16,center.y);
		while (start.x<=m_Position.right)
		{
			pDC->Arc(rect,start,end);
			rect.left=rect.right;
			rect.right+=16;
			end=start;
			start.x+=16;
		}
		}
		//写标称：
		s.Format("  L=%G H",m_Value);
		pDC->TextOut(m_Position.left-15,m_Position.top-20,m_Lable+s);
		
		break;
	case 1:
		//重新确定m_Position：
		m_Position.top=center.y-4*8;
		m_Position.bottom=center.y+4*8;
		m_Position.left=center.x-2*8;
		m_Position.right=center.x+2*8;
		lLinkPoint.x=center.x;
		lLinkPoint.y=center.y-6*8;
		rLinkPoint.x=center.x;
		rLinkPoint.y=center.y+6*8;
		//画引脚：
		pDC->MoveTo(center.x,m_Position.top);
		pDC->LineTo(lLinkPoint);
		pDC->MoveTo(center.x,m_Position.bottom);
		pDC->LineTo(rLinkPoint);
		//标输入端
		pDC->MoveTo(m_Position.left+3,m_Position.top);
		pDC->LineTo(m_Position.left+3,m_Position.top+6);
		pDC->MoveTo(m_Position.left,m_Position.top+3);
		pDC->LineTo(m_Position.left+6,m_Position.top+3);
		//画元件：
		{
			CRect rect(center.x-8,center.y-32,center.x+8,center.y-16);
			CPoint end(center.x,m_Position.top);
			CPoint start(center.x,m_Position.top+16);
			while (start.y<=m_Position.bottom)
			{
				pDC->Arc(rect,start,end);
				rect.top=rect.bottom;
				rect.bottom+=16;
				end=start;
				start.y+=16;
			}
		}
		
		s.Format("  L=%G H",m_Value);
		pDC->TextOut(m_Position.right+4,(m_Position.top+m_Position.bottom)/2,m_Lable+s);
		
		break;
	case 2:
		//重新确定m_Position：
		m_Position.top=center.y-2*8;
		m_Position.bottom=center.y+2*8;
		m_Position.left=center.x-4*8;
		m_Position.right=center.x+4*8;
		lLinkPoint.y=center.y;
		lLinkPoint.x=center.x+6*8;
		rLinkPoint.y=center.y;
		rLinkPoint.x=center.x-6*8;
		//画引脚：
		pDC->MoveTo(m_Position.left,center.y);
		pDC->LineTo(rLinkPoint);
		pDC->MoveTo(m_Position.right,center.y);
		pDC->LineTo(lLinkPoint);
		//标输入端
		pDC->MoveTo(m_Position.right-3,m_Position.bottom-6);
		pDC->LineTo(m_Position.right-3,m_Position.bottom);
		pDC->MoveTo(m_Position.right-6,m_Position.bottom-3);
		pDC->LineTo(m_Position.right,m_Position.bottom-3);
		//画元件：
		{
			CRect rect(m_Position.left,center.y-8,m_Position.left+16,center.y+8);
			CPoint end(m_Position.left,center.y);
			CPoint start(m_Position.left+16,center.y);
			while (start.x<=m_Position.right)
			{
				pDC->Arc(rect,start,end);
				rect.left=rect.right;
				rect.right+=16;
				end=start;
				start.x+=16;
			}
		}
		
		s.Format("  L=%G H",m_Value);
		pDC->TextOut(m_Position.left-15,m_Position.top-20,m_Lable+s);
		break;
	case 3:
		//重新确定m_Position：
		m_Position.top=center.y-4*8;
		m_Position.bottom=center.y+4*8;
		m_Position.left=center.x-2*8;
		m_Position.right=center.x+2*8;
		lLinkPoint.x=center.x;
		lLinkPoint.y=center.y+6*8;
		rLinkPoint.x=center.x;
		rLinkPoint.y=center.y-6*8;
		//画引脚：
		pDC->MoveTo(center.x,m_Position.top);
		pDC->LineTo(rLinkPoint);
		pDC->MoveTo(center.x,m_Position.bottom);
		pDC->LineTo(lLinkPoint);
		//标输入端
		pDC->MoveTo(m_Position.right-3,m_Position.bottom-6);
		pDC->LineTo(m_Position.right-3,m_Position.bottom);
		pDC->MoveTo(m_Position.right-6,m_Position.bottom-3);
		pDC->LineTo(m_Position.right,m_Position.bottom-3);
		//画元件：
		{
			CRect rect(center.x-8,center.y-32,center.x+8,center.y-16);
			CPoint end(center.x,m_Position.top);
			CPoint start(center.x,m_Position.top+16);
			while (start.y<=m_Position.bottom)
			{
				pDC->Arc(rect,start,end);
				rect.top=rect.bottom;
				rect.bottom+=16;
				end=start;
				start.y+=16;
			}
		}
		
		s.Format("  L=%G H",m_Value);
		pDC->TextOut(m_Position.right+4,(m_Position.top+m_Position.bottom)/2,m_Lable+s);
		break;
	}

}
void CCompL::ShowProperties()
{
	CDlgComPro dlg;
	dlg.m_CompLable=m_Lable;
	dlg.m_CompValue=m_Value;
	if (dlg.DoModal()==IDOK)
	{
		m_Value=dlg.m_CompValue;
		m_Lable=dlg.m_CompLable;
	}
}
void CCompL::Serialize(CArchive& ar)
{
	CComp::Serialize(ar);
	if (ar.IsStoring())
	{
		ar<<m_Value;
	} 
	else
	{
		ar>>m_Value;
	}

}


CCompS::CCompS()
{
	int i=rand()%5;
	m_Volt=5;
	m_Freq=0;
	m_Position=CRect(160+8*i,128+8*i,208+8*i,160+8*i);
	m_LeftDir=0;
	m_Type=4;

}
CCompS::CCompS(double volt, double freq)
{
	m_Volt=volt;
	m_Freq=freq;
	m_Position=CRect(160,128,208,160);
}
void CCompS::Draw(CDC* pDC)
{
	CPoint center;
	center.x=(m_Position.left+m_Position.right)/2;
	center.y=(m_Position.top+m_Position.bottom)/2;
	CString s;

	switch(m_LeftDir)				//m_LeftDir:标记元件逻辑左端的视图方位，0表示左，1表示上，2表示右，3表示下
	{
	case 0:
		//重新确定m_Position：
		m_Position.top=center.y-2*8;
		m_Position.bottom=center.y+2*8;
		m_Position.left=center.x-3*8;
		m_Position.right=center.x+3*8;
		lLinkPoint.x=center.x-5*8;
		lLinkPoint.y=center.y;
		rLinkPoint.x=center.x+5*8;
		rLinkPoint.y=center.y;
		//画引脚：
		pDC->MoveTo(m_Position.left,center.y);
		pDC->LineTo(lLinkPoint);
		pDC->MoveTo(m_Position.right,center.y);
		pDC->LineTo(rLinkPoint);
		//标输入端
		pDC->MoveTo(m_Position.left+3,m_Position.top);
		pDC->LineTo(m_Position.left+3,m_Position.top+6);
		pDC->MoveTo(m_Position.left,m_Position.top+3);
		pDC->LineTo(m_Position.left+6,m_Position.top+3);
		//画元件：
		{
		CRect rect(m_Position.left+8,m_Position.top,m_Position.right-8,m_Position.bottom);
		pDC->MoveTo(m_Position.left,center.y);
		pDC->LineTo(m_Position.left+8,center.y);
		pDC->Ellipse(rect);
		pDC->MoveTo(m_Position.left+10,center.y);
		pDC->LineTo(m_Position.left+18,center.y);
		pDC->MoveTo(m_Position.left+14,center.y-4);
		pDC->LineTo(m_Position.left+14,center.y+4);
		CRect rect1(m_Position.right-28,center.y-12,m_Position.right-12,center.y);
		pDC->Arc(rect1,CPoint(m_Position.right-20,center.y-12),CPoint(m_Position.right-20,center.y));
		
		CRect rect2(m_Position.right-28,center.y,m_Position.right-12,center.y+12);
		pDC->Arc(rect2,CPoint(m_Position.right-20,center.y+12),CPoint(m_Position.right-20,center.y));	
		pDC->MoveTo(m_Position.right-8,center.y);
		pDC->LineTo(m_Position.right,center.y);
		}
		s.Format("  U=%G V,F=%G Hz",m_Volt,m_Freq);
		pDC->TextOut(m_Position.left-15,m_Position.top-20,m_Lable+s);
		break;
	case 1:
		//重新确定m_Position：
		m_Position.top=center.y-3*8;
		m_Position.bottom=center.y+3*8;
		m_Position.left=center.x-2*8;
		m_Position.right=center.x+2*8;
		lLinkPoint.x=center.x;
		lLinkPoint.y=center.y-5*8;
		rLinkPoint.x=center.x;
		rLinkPoint.y=center.y+5*8;
		//画引脚：
		pDC->MoveTo(center.x,m_Position.top);
		pDC->LineTo(lLinkPoint);
		pDC->MoveTo(center.x,m_Position.bottom);
		pDC->LineTo(rLinkPoint);
		//标输入端
		pDC->MoveTo(m_Position.left+3,m_Position.top);
		pDC->LineTo(m_Position.left+3,m_Position.top+6);
		pDC->MoveTo(m_Position.left,m_Position.top+3);
		pDC->LineTo(m_Position.left+6,m_Position.top+3);
		//画元件：
		{
			CRect rect(m_Position.left,m_Position.top+8,m_Position.right,m_Position.bottom-8);
			pDC->MoveTo(center.x,m_Position.top);
			pDC->LineTo(center.x,m_Position.top+8);
			pDC->Ellipse(rect);
			pDC->MoveTo(center.x,m_Position.top+10);
			pDC->LineTo(center.x,m_Position.top+18);
			pDC->MoveTo(center.x-4,m_Position.top+14);
			pDC->LineTo(center.x+4,m_Position.top+14);
			CRect rect1(center.x,m_Position.top+20,center.x+12,m_Position.bottom-12);
			pDC->Arc(rect1,CPoint(center.x+12,m_Position.top+28),CPoint(center.x,m_Position.bottom-20));
			CRect rect2(center.x-12,m_Position.top+20,center.x,m_Position.bottom-12);
			pDC->Arc(rect2,CPoint(center.x-12,m_Position.top+28),CPoint(center.x,m_Position.bottom-20));	
			pDC->MoveTo(center.x,m_Position.bottom-8);
			pDC->LineTo(center.x,m_Position.bottom);
		}
		
		s.Format("  U=%G V,F=%G Hz",m_Volt,m_Freq);
		pDC->TextOut(m_Position.right+4,(m_Position.top+m_Position.bottom)/2,m_Lable+s);
		break;
	case 2:
		//重新确定m_Position：
		m_Position.top=center.y-2*8;
		m_Position.bottom=center.y+2*8;
		m_Position.left=center.x-3*8;
		m_Position.right=center.x+3*8;
		lLinkPoint.y=center.y;
		lLinkPoint.x=center.x+5*8;
		rLinkPoint.y=center.y;
		rLinkPoint.x=center.x-5*8;
		//画引脚：
		pDC->MoveTo(m_Position.left,center.y);
		pDC->LineTo(rLinkPoint);
		pDC->MoveTo(m_Position.right,center.y);
		pDC->LineTo(lLinkPoint);
		//标输入端
		pDC->MoveTo(m_Position.right-3,m_Position.bottom-6);
		pDC->LineTo(m_Position.right-3,m_Position.bottom);
		pDC->MoveTo(m_Position.right-6,m_Position.bottom-3);
		pDC->LineTo(m_Position.right,m_Position.bottom-3);
		//画元件：
		{
		CRect rect(m_Position.left+8,m_Position.top,m_Position.right-8,m_Position.bottom);
		pDC->MoveTo(m_Position.left,center.y);
		pDC->LineTo(m_Position.left+8,center.y);
		pDC->Ellipse(rect);
		pDC->MoveTo(m_Position.left+28,center.y);
		pDC->LineTo(m_Position.left+36,center.y);
		pDC->MoveTo(m_Position.left+32,center.y-4);
		pDC->LineTo(m_Position.left+32,center.y+4);
		CRect rect1(m_Position.right-36,center.y-12,m_Position.right-21,center.y);
		pDC->Arc(rect1,CPoint(m_Position.right-28,center.y-12),CPoint(m_Position.right-28,center.y));
		CRect rect2(m_Position.right-36,center.y,m_Position.right-21,center.y+12);
		pDC->Arc(rect2,CPoint(m_Position.right-28,center.y+12),CPoint(m_Position.right-28,center.y));	
		pDC->MoveTo(m_Position.right-8,center.y);
		pDC->LineTo(m_Position.right,center.y);
		}
		
		s.Format("  U=%G V, F=%G Hz",m_Volt,m_Freq);
		pDC->TextOut(m_Position.left-15,m_Position.top-20,m_Lable+s);
		break;
	case 3:
		//重新确定m_Position：
		m_Position.top=center.y-3*8;
		m_Position.bottom=center.y+3*8;
		m_Position.left=center.x-2*8;
		m_Position.right=center.x+2*8;
		lLinkPoint.x=center.x;
		lLinkPoint.y=center.y+5*8;
		rLinkPoint.x=center.x;
		rLinkPoint.y=center.y-5*8;
		//画引脚：
		pDC->MoveTo(center.x,m_Position.top);
		pDC->LineTo(rLinkPoint);
		pDC->MoveTo(center.x,m_Position.bottom);
		pDC->LineTo(lLinkPoint);
		//标输入端
		pDC->MoveTo(m_Position.right-3,m_Position.bottom-6);
		pDC->LineTo(m_Position.right-3,m_Position.bottom);
		pDC->MoveTo(m_Position.right-6,m_Position.bottom-3);
		pDC->LineTo(m_Position.right,m_Position.bottom-3);
		//画元件：
		{
			CRect rect(m_Position.left,m_Position.top+8,m_Position.right,m_Position.bottom-8);
			pDC->MoveTo(center.x,m_Position.top);
			pDC->LineTo(center.x,m_Position.top+8);
			pDC->Ellipse(rect);
			pDC->MoveTo(center.x,m_Position.top+28);
			pDC->LineTo(center.x,m_Position.top+36);
			pDC->MoveTo(center.x-4,m_Position.top+32);
			pDC->LineTo(center.x+4,m_Position.top+32);
			CRect rect1(center.x,m_Position.top+12,center.x+12,m_Position.bottom-20);
			pDC->Arc(rect1,CPoint(center.x+12,m_Position.top+20),CPoint(center.x,m_Position.bottom-28));
			CRect rect2(center.x-12,m_Position.top+12,center.x,m_Position.bottom-20);
			pDC->Arc(rect2,CPoint(center.x-12,m_Position.top+20),CPoint(center.x,m_Position.bottom-28));	
			pDC->MoveTo(center.x,m_Position.bottom-8);
			pDC->LineTo(center.x,m_Position.bottom);
		}
		
		s.Format("  U=%G V,F=%G Hz",m_Volt,m_Freq);
		pDC->TextOut(m_Position.right+4,(m_Position.top+m_Position.bottom)/2,m_Lable+s);
		break;
	}
	

}
void CCompS::ShowProperties()
{
	CDlgComProS dlg;
	dlg.m_CompLable=m_Lable;
	dlg.m_CompVolt=m_Volt;
	dlg.m_CompFreq=m_Freq;
	if (dlg.DoModal()==IDOK)
	{
		m_Lable=dlg.m_CompLable;
		m_Volt=dlg.m_CompVolt;
		m_Freq=dlg.m_CompFreq;
	}
}

void CCompS::Serialize(CArchive& ar)
{
	CComp::Serialize(ar);
	if (ar.IsStoring())
	{
		ar<<m_Freq;
		ar<<m_Volt;
	} 
	else
	{
		ar>>m_Freq;
		ar>>m_Volt;
	}
}


CLine::CLine()
{
	m_NextComp=NULL;
	m_PreComp=NULL;
	m_Dire=0;
}
CLine::CLine(CPoint start, CPoint end)
{
	m_StartPoint=start;
	m_EndPoint=end;
	m_PreComp=NULL;
	m_NextComp=NULL;
}
void CLine::Draw(CDC* pDC)
{
	pDC->MoveTo(m_StartPoint);
	pDC->LineTo(m_EndPoint);

}
void CLine::Update(CPoint NewPoint)
{
	m_EndPoint=NewPoint;
}
void CLine::Serialize(CArchive& ar)
{
	CObject::Serialize(ar);
	if (ar.IsStoring())
	{
		ar<<m_StartPoint;
		ar<<m_EndPoint;
		ar<<m_PreComp;
		ar<<m_NextComp;
	} 
	else
	{
		ar>>m_StartPoint;
		ar>>m_EndPoint;
		ar>>m_PreComp;
		ar>>m_NextComp;
	}

}

//Serialization Macros
IMPLEMENT_SERIAL(CComp,CObject,1)
IMPLEMENT_SERIAL(CCompR,CComp,1)
IMPLEMENT_SERIAL(CCompC,CComp,1)
IMPLEMENT_SERIAL(CCompL,CComp,1)
IMPLEMENT_SERIAL(CCompS,CComp,1)
IMPLEMENT_SERIAL(CLine,CObject,1)




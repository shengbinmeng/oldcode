// MyEWBDoc.h : interface of the CMyEWBDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MYEWBDOC_H__F70F4F4A_10FB_4329_90AF_33D4C193928A__INCLUDED_)
#define AFX_MYEWBDOC_H__F70F4F4A_10FB_4329_90AF_33D4C193928A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMyEWBDoc : public CDocument
{
protected: // create from serialization only
	CMyEWBDoc();
	DECLARE_DYNCREATE(CMyEWBDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyEWBDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMyEWBDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMyEWBDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYEWBDOC_H__F70F4F4A_10FB_4329_90AF_33D4C193928A__INCLUDED_)

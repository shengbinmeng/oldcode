//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MyEWB.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_MYEWBTYPE                   129
#define IDD_COMPONENTS                  130
#define IDI_ICON_R                      131
#define IDI_MYEWBTYPE                   133
#define IDD_PROPERTY                    140
#define IDD_PROPERTY_S                  141
#define IDR_CONTEXTMENU                 142
#define IDI_ICON_C                      143
#define IDI_ICON_L                      144
#define IDI_ICON_S                      145
#define IDD_VIEWRESULT                  146
#define IDD_RESULTGRAPH                 148
#define IDC_COMP_R                      1003
#define IDC_EDIT_VALUE                  1012
#define IDC_EDIT_LABLE                  1013
#define IDC_EDIT_VOLT                   1015
#define IDC_EDIT_FREQ                   1016
#define IDC_COMP_C                      1017
#define IDC_COMP_L                      1018
#define IDC_COMP_S                      1019
#define IDC_COMBO2                      1029
#define IDC_DISPLAYCOMP                 1030
#define IDC_DISPLAYBRANCH               1031
#define IDC_DISPLAYALL                  1032
#define IDC_COMBO1                      1033
#define IDC_STATIC_U                    1036
#define IDC_STATIC_I                    1037
#define ID_COMP_GEN                     32771
#define ID_COMP_GENE                    32771
#define ID_BUTTON32772                  32772
#define ID_BUTTON32773                  32773
#define ID_BUTTON_C                     32773
#define ID_BUTTON32774                  32774
#define ID_BUTTON_L                     32774
#define ID_BUTTON_R                     32775
#define ID_VIEW_GRID                    32776
#define ID_BUTTON_S                     32777
#define ID_BUTTON_SWITCH                32778
#define IDR_CTXMENU_ROT                 32779
#define ID_CTXMENU_ROT                  32780
#define ID_CTXMENU_DEL                  32781
#define ID_EDIT_CLEARALL                32783
#define ID_EDIT_DEL                     32784
#define ID_CIRCUIT_ANA                  32785
#define ID_HELP_ABOUTLINE               32786
#define ID_BUTTON32787                  32787

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        149
#define _APS_NEXT_COMMAND_VALUE         32789
#define _APS_NEXT_CONTROL_VALUE         1038
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

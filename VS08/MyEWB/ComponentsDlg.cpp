// ComponentsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MyEWB.h"
#include "ComponentsDlg.h"
#include "Comps.h"
#include "MyEWBView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CComponentsDlg dialog

CComponentsDlg::CComponentsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CComponentsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CComponentsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CComponentsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CComponentsDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CComponentsDlg, CDialog)
	//{{AFX_MSG_MAP(CComponentsDlg)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_COMP_R, OnCompR)
	ON_BN_CLICKED(IDC_COMP_C, OnCompC)
	ON_BN_CLICKED(IDC_COMP_L, OnCompL)
	ON_BN_CLICKED(IDC_COMP_S, OnCompS)
	//}}AFX_MSG_MAP
	
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CComponentsDlg message handlers


void CComponentsDlg::OnDestroy() 
{
	// TODO: Add your message handler code here
	CDialog::OnDestroy();
	delete this;	
}


void CComponentsDlg::OnCompR() 
{
	// TODO: Add your control notification handler code here	
	//�鷳��������ԭ������
	CMyEWBView* pMyView=(CMyEWBView*)this->GetParent();
	pMyView->OnButtonR();
	pMyView->SetFocus();
	
}

void CComponentsDlg::OnCompC() 
{
	// TODO: Add your control notification handler code here
	CMyEWBView* pMyView=(CMyEWBView*)this->GetParent();
	pMyView->OnButtonC();
	pMyView->SetFocus();
}

void CComponentsDlg::OnCompL() 
{
	// TODO: Add your control notification handler code here
	CMyEWBView* pMyView=(CMyEWBView*)this->GetParent();
	pMyView->OnButtonL();
	pMyView->SetFocus();
}

void CComponentsDlg::OnCompS() 
{
	// TODO: Add your control notification handler code here
	CMyEWBView* pMyView=(CMyEWBView*)this->GetParent();
	pMyView->OnButtonS();
	pMyView->SetFocus();
}


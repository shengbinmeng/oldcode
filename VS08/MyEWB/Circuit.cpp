#include "StdAfx.h"
#include "Circuit.h"

bool existp(CTypedPtrArray<CObArray,CComp*>& ca,CComp*& p)
{
	for (int i=0;i<ca.GetSize();i++)
	{
		if(ca[i]==p) return true;
	}

	return false;
}

bool existv(CArray<Vertex,Vertex>& ca,Vertex& v)
{
	for (int i=0;i<ca.GetSize();i++)
	{
		int flag=0;
		if(ca[i].Comps.GetSize()!=v.Comps.GetSize()) flag=1;
		else
		{
			int size=v.Comps.GetSize();
			for (int j=0;j<size;j++)
			{
				if(!existp(ca[i].Comps,v.Comps[j])) flag=1;
			}
		}

		if(flag==0) return true;
	}

	return false;
}


int Circuit::Create(const CTypedPtrArray<CObArray,CComp*>& data)
{

	int IDv=1;
	int IDe=1;

	CComp* temp;
	for (int i=0;i<data.GetSize();i++)
	{
		temp=data[i];

		if(temp->m_LeftComps.GetSize()>1)
		{
			Vertex v;
			v.Comps.Add(temp);
			for (int i=0;i<temp->m_LeftComps.GetSize();i++)
			{
				v.Comps.Add(temp->m_LeftComps[i]);
			}

			//如果未被记录过，则是一个新节点
			if (!existv(m_Result.Vertexes,v))
			{

				//填充该新节点信息，主要是出入元件
				v.OutComps.Add(temp);
				for(int j=0;j<temp->m_LeftComps.GetSize();j++)
				{
					CComp* compio=temp->m_LeftComps[j];
					int flag=0;

					for (int k=0;k<v.Comps.GetSize();k++)
					{
						if(!existp(compio->m_LeftComps,v.Comps[k]) && compio!=v.Comps[k]) 
						{
							v.InComps.Add(temp->m_LeftComps[j]);
							flag=1;
							break;
						}
					}

					if(flag==0) v.OutComps.Add(compio);

				}

				v.VertexID=IDv; IDv++;
				m_Result.Vertexes.Add(v);
			}
		}
	}

	for(int i=0;i<m_Result.Vertexes.GetSize();i++)
	{
		Vertex ver=m_Result.Vertexes[i];
		for(int j=0;j<ver.OutComps.GetSize();j++)
		{
			Edge e;
			e.head=ver;
			CComp* comp=ver.OutComps[j];
			e.Comps.Add(comp);
			while(comp->m_RightComps.GetSize()==1)
			{
				comp=comp->m_RightComps[0];
				e.Comps.Add(comp);
			}

			for (int p=0;p<m_Result.Vertexes.GetSize();p++)
			{
				if(existp(m_Result.Vertexes[p].InComps,comp))
				{
					e.tail=m_Result.Vertexes[p];
					break;
				}
			}

			e.EdgeID=IDe; IDe++;
			m_Result.Edges.Add(e);
		}

	}


	for (int i=0;i<data.GetSize();i++)
	{
		if (data[i]->m_Type==4)
		{
			CCompS* pS=(CCompS*)data[i];
			freq=pS->m_Freq;m_Result.freq=freq;
		}
	}
	
	

	return 0;
}


int find(Vertex vtex[],int n,int ID)
{
	for (int i=0;i<n;i++)
	{
		if(vtex[i].VertexID==ID) return i+1;
	}

	return -1;
}


int  Circuit::Analyse()
{

	int nBranch,nNode;
	nBranch=m_Result.Edges.GetSize();
	nNode=m_Result.Vertexes.GetSize();

	if (nNode<3)
	{
		return 1;
	}

	Vertex Vtexs[20];
	for (int i=0;i<nNode;i++)
	{
		Vtexs[i]=m_Result.Vertexes[i];
	}

	IptBranch branch[20];
	for (int i=0;i<nBranch;i++)
	{
		branch[i].head=find(Vtexs,nNode,m_Result.Edges[i].head.VertexID);
		branch[i].tail=find(Vtexs,nNode,m_Result.Edges[i].tail.VertexID);

		Complex admit,source;

		for (int j=0;j<m_Result.Edges[i].Comps.GetSize();j++)
		{
			CComp* comp=m_Result.Edges[i].Comps[j];
			if (comp->m_Type==1)
			{
				CCompR* pR=(CCompR*)(comp);
				admit.real+= 1/(pR->m_Value);
			}
			if (comp->m_Type==2)
			{
				CCompC* pC=(CCompC*)(comp);
				admit.imag+=pC->m_Value*freq;
			}
			if (comp->m_Type==3)
			{
				CCompL* pL=(CCompL*)(comp);
				if(freq==0) admit.imag+=99999999;
				else admit.imag+=1/(pL->m_Value*freq);
			}
			if (comp->m_Type==4)
			{
				CCompS* pS=(CCompS*)(comp);
				source.real+=pS->m_Volt;
				admit.real+=99999999;  //认为电源是一个极小的电阻，因此该支路导纳为正无穷实数
			}
		}

		branch[i].admitance=admit;
		branch[i].source=source;

	}

	OptBranch obranch[20];int n;
	Calculate cal;
	cal.Input(nBranch,freq,branch);
	cal.Form();
	cal.Guass();
	cal.Output(obranch,n);

	for (int i=0;i<n;i++)
	{
		m_Result.Edges[i].Current=obranch[i].Current;
		m_Result.Edges[i].Voltage=obranch[i].Voltage;
		for(int j=0;j<m_Result.Edges[i].Comps.GetSize();j++)
		{
			m_Result.Edges[i].Comps[j]->m_Current=m_Result.Edges[i].Current;

			CComp* comp=m_Result.Edges[i].Comps[j];
			Complex admit;
			if (comp->m_Type==1)
			{
				CCompR* pR=(CCompR*)(comp);
				admit.real= 1/(pR->m_Value);
				comp->m_Voltage=comp->m_Current/admit;
			}
			if (comp->m_Type==2)
			{
				CCompC* pC=(CCompC*)(comp);
				admit.imag=pC->m_Value*freq;
				comp->m_Voltage=comp->m_Current/admit;
			}
			if (comp->m_Type==3)
			{
				CCompL* pL=(CCompL*)(comp);
				if(freq==0) admit.imag=99999999999999;
				else admit.imag=1/(pL->m_Value*freq);
				comp->m_Voltage=comp->m_Current/admit;
			}
			if (comp->m_Type==4)
			{
				CCompS* pS=(CCompS*)(comp);
				comp->m_Voltage.real=pS->m_Volt;
			}
		}
	}
	
	return 0;
}
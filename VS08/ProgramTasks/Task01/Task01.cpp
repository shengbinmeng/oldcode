// Task01.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	char choice;
	while(1)
	{

		cout<<"***Please select:1-Number Syestem  2-Sort  3-整数分离  4-Exit***"<<endl;
		cin>>choice;
		cin.ignore(20,'\n');
		
		switch(choice)
		{
		case '1':
		{

MSG_1:		cout<<"Please input three character(0~9 or A~F)"<<endl;
			char ch[3];
			cin>>ch;

			int num[3]; 
			for(int i=0;i<3;i++)
			{
				if('0'<=ch[i] && ch[i]<='9') num[i]=ch[i]-'0';
				else
				{
					if('A'<=ch[i] && ch[i]<='F') num[i]=ch[i]-'A'+10;
					else{ cout<<"Input Error!"<<endl; goto MSG_1;}
				}
			}

			int num_dec=num[2]+num[1]*16+num[0]*16*16;

			//不同进制输出（利用输出流格式控制；也可以自行编写进制转换代码，但没必要）：
			cout<<"Hex:"<<'0'<<'x'<<ch<<",    Decimal:"<<num_dec<<",   Octal:"<<oct<<num_dec<<dec<<endl;

			break;
		}


		case '2':
		{
			int a[3];
MSG_2:		cout<<"Please input three integers(a,b,c):"<<endl;

			//此处增加了对输入的合法性检查
			//cin.ignore();
			for (int i=0;i<3;i++)
			{	
				//如果输入非整数，则提示出错，然后清除输入流缓冲区内容和错误标志
				if(!(cin>>a[i])){ cout<<"Input Error!"<<endl;cin.sync();cin.clear() ;goto MSG_2;}
			}

			int temp;
			//进行某种排序算法（忘记名字了）
			for (int i=1;i<3;i++)
			{
				for (int j=i;j>0;j--)
				{
					if(a[j]>a[j-1])
					{
						temp=a[j];
						a[j]=a[j-1];
						a[j-1]=temp;
					}
					else break;
				}
			}

			cout<<"由大到小排序后为："<<endl;
			for (int i=0;i<3;i++)
			{
				cout<<a[i]<<"  ";
			}
			cout<<endl;

			break;
		}

		case '3':
		{
MSG_3:		cout<<"Input an integer(1~1000):"<<endl;
			int num;
			cin>>num;
			if (num<1 || num>1000)
			{
				cout<<"Input Error!"<<endl;
				goto MSG_3;
			}
			
			int a[4];
			a[3]=num%10;  //最低位
			a[2]=(num/10)%10;
			a[1]=(num/100)%10;
			a[0]=(num/1000)%10;		//最高位

			bool number_start=false;

			for (int i=0;i<4;i++)
			{
				if (!number_start)
				{
					if(a[i]==0) continue;
					else
					{
						number_start=true;
						cout<<' '<<a[i];
					}
				}
				else cout<<' '<<a[i];

			}
			cout<<endl;


			break;
		}

		case '4':
			exit(0);
			break;

		default:
			cout<<"Input Error!"<<endl;

		}
	}

	return 0;
}


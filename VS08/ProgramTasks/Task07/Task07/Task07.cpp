// Task07.cpp : Defines the entry point for the console application.
//


#include "stdafx.h"

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	cout<<"//EasyPro  Version1.0 \n//Edwin Meng\n"<<endl;        //输出程序名、版本和作者信息
	char ch;
	while(1)
	{
		cout<<"\n请选择：1－反置数    2－逆序排列    3－关键词检索    4－退出 （注：本程序不再进行输入合法性检查，请按要求输入，否则后果自负）"<<endl;
		cin>>ch;
		switch(ch)
		{
		case '1':
			{

				int num1,num2;
				cout<<"输入两个正整数："<<endl;
				cin>>num1>>num2;

				int numv1,numv2;
				numv1=invert(num1); numv2=invert(num2);

				cout<<"这两个数的反置数（依次为"<<numv1<<"、"<<numv2<<"）之和（"<<numv1+numv2<<"）的反置数为："<<invert(numv1+numv2)<<endl;


			}

			break;


		case '2':
			{
				cout<<"输入一组非零整数（不超过20个，输入0表示结束）："<<endl;

				int a[20];
				int i, temp;
				for(i=0;i<20;i++)
				{
					cin>>temp;
					if(temp==0) {cin.sync(); break;}     //读到0之后，就清除缓冲区中余下的可能多输入的数
					a[i]=temp;
				}

				int count=i;   //数组中数据的个数
				if(count==0){cout<<"输入数据个数为0！"<<endl; break;}

				//调用逆序排列函数
				swap(a,count);

				//输出逆序后的结果
				cout<<"数组中的值按逆序存放为："<<endl;
				for(i=0;i<count;i++)
				{
					cout<<a[i]<<"  ";
				}
				cout<<endl;
	

			}

			break;

		case '3':
			{
				char keywords[100];
				char content[1000];

				cout<<"输入关键词："<<endl;
				cin.sync();  //使输入同步；即清空输入缓冲区，避免受之前输入（包括回车符）的影响
				cin.getline(keywords,100);
				cout<<"输入要检查的内容："<<endl;
				cin.sync();  //使输入同步；即清空输入缓冲区，避免受之前输入（包括回车符）的影响
				cin.getline(content,1000);


				//析取句子中所有单词存为字符串，便于用strcmp和strcpy
				char* words[100];       //用于储存各单词（其实可以用已有的高级数据容器的，如Array等；但为了练习基础知识……）
				int p=0, j=0,count=0;
				words[p]=new char[20];

				bool word_start=true;

				for (int i=0;keywords[i]!='\0';i++)
				{
					if(keywords[i]==' ') { word_start=true; words[p][j]='\0'; p++; words[p]=new char[20]; j=0; continue; }
					else
					{
						if(word_start==true)
						{
							count++;
							words[p][j]=keywords[i]; j++;
							word_start=false;

						}
						else{ words[p][j]=keywords[i]; j++; continue;}
					}
				}
				words[p][j]='\0';


				char* contents[100];       //用于储存各单词（其实可以用已有的高级数据容器的，如Array等；但为了练习基础知识……）
				p=0, j=0;
				int count1=0;
				contents[p]=new char[20];


				for (int i=0;content[i]!='\0';i++)
				{
					if(content[i]==' ') { word_start=true; contents[p][j]='\0'; p++; contents[p]=new char[20]; j=0; continue; }
					else
					{
						if(word_start==true)
						{
							count1++;
							contents[p][j]=content[i]; j++;
							word_start=false;

						}
						else{ contents[p][j]=content[i]; j++; continue;}
					}
				}
				contents[p][j]='\0';


				char exist_keywords[20][20];
				char right_keywords[20][20];
				int num=0;
				for (int i=0;i<count;i++)
				{
					for(int j=0;j<count1;j++)
					{
						if(is_keyword(contents[j],words[i])){strcpy(exist_keywords[num],contents[j]); strcpy(right_keywords[num],words[i]);  num++;}
					}
				}

				cout<<"待检查的句子中经过调整的关键词有："<<endl;
				for (int i=0;i<num;i++)
				{
					cout<<exist_keywords[i]<<"  ";
				}
				cout<<"\n它们对应的原关键词依次为："<<endl;
				for (int i=0;i<num;i++)
				{
					cout<<right_keywords[i]<<"  ";
				}

			}

			break;


		case '4':
			exit(0);

		}
	}

	return 0;
}


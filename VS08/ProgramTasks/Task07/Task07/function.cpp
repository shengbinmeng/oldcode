#include "stdafx.h"

//逆序函数
int invert(int num)
{
	int numv=0;
	int m=num,nv[100];

	int flag=0,i=0;
	while(m!=0)
	{
		int p=m%10;     //取最后一位数
		if(flag==0)
		{
			if(p!=0) flag=1;
		}
		if(flag==1){ nv[i]=p;i++;}

		m=(m-p)/10;		//准备取倒数次一位数；m为0则表示已取到最高位
	}

	for(int j=0;j<i;j++)
	{
		numv+=nv[j]*pow(10.0,i-1-j);
	}

	return numv;

}


void swap(int a[],int count)
{
	int temp;
	for (int j=0;j<=(count-1)/2;j++)
	{
		temp=a[j];
		a[j]=a[count-1-j];
		a[count-1-j]=temp;
	}
}


//判断是否是关键词（或字母顺序改变的关键词）
bool is_keyword(char* word,char* keyword)
{
	//如果长度不相等，必然不是
	if(strlen(word)!=strlen(keyword)) return false;

	//定义结构体，储存字母及其出现的次数
	struct Letter_Count
	{
		char letter;
		int count;
	};

	//得到word的各个字母及各字母出现的个数
	Letter_Count lc[20];
	int num=0;
	lc[0].letter=word[0];
	lc[0].count=1;
	num++;
	for(int i=1;word[i]!='\0';i++)
	{
		int flag=0;
		for (int j=0;j<num;j++)
		{
			if(lc[j].letter==word[i]){lc[j].count++; flag=1; break;}
		}
		if(flag==1) continue;

		lc[num].letter=word[i];
		lc[num].count=1;
		num++;		
	}

	//得到keyword的各个字母及各字母出现的个数
	Letter_Count lc1[20];
	int num1=0;
	lc1[0].letter=keyword[0];
	lc1[0].count=1;
	num1++;
	for(int i=1;keyword[i]!='\0';i++)
	{
		int flag=0;
		for (int j=0;j<num1;j++)
		{
			if(lc1[j].letter==keyword[i]){lc1[j].count++; flag=1; break;}
		}
		if(flag==1) continue;

		lc1[num1].letter=keyword[i];
		lc1[num1].count=1;
		num1++;		
	}

	//只有出现的字母相同，且出现次数也相同，才是关键词，返回true；否则返回fasle
	if(num!=num1) return false;
	for (int i=0;i<num;i++)
	{
		int j;
		for (j=0;j<num1;j++)
		{
			if (lc1[j].letter==lc[i].letter)
			{
				if(lc1[j].count!=lc[i].count) return false;
				else break;
			}
		}
		if(j==num1) return false;
	}


	return true;

}



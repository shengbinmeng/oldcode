#if !defined(AFX_COMPONENTSDLG_H__F1DE8355_04C5_4757_9202_02C6E4DDD533__INCLUDED_)
#define AFX_COMPONENTSDLG_H__F1DE8355_04C5_4757_9202_02C6E4DDD533__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ComponentsDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CComponentsDlg dialog

class CComponentsDlg : public CDialog
{
// Construction
public:
	CComponentsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CComponentsDlg)
	enum { IDD = IDD_COMPONENTS };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
	
	CString m_CompName;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CComponentsDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CComponentsDlg)
	afx_msg void OnDestroy();
	afx_msg void OnCompR();
	afx_msg void OnCompC();
	afx_msg void OnCompL();
	afx_msg void OnCompS();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMPONENTSDLG_H__F1DE8355_04C5_4757_9202_02C6E4DDD533__INCLUDED_)

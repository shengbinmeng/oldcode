// Comps.h: interface for the CComps class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_COMPR_H__F924193E_6D38_4EC7_A029_791DF5D85EDD__INCLUDED_)
#define AFX_COMPR_H__F924193E_6D38_4EC7_A029_791DF5D85EDD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Calculate.h"

class CComp : public CObject
{
	DECLARE_SERIAL(CComp)
public:
	CComp();
	virtual ~CComp(){};
	CComp(CRect position);
	virtual void Draw(CDC* pDC);
	virtual void ShowProperties();
	virtual void GetLeftLinkPoint(CPoint& lPoint);
	virtual void GetRightLinkPoint(CPoint& rPoint);
	virtual void Serialize(CArchive& ar);
	
	CRect m_Position;
	CPoint lLinkPoint;
	CPoint rLinkPoint;
	bool bSelected;
	int m_LeftDir;             //标记元件逻辑左端的视图方位，0表示左，1表示上，2表示右，3表示下；0为初始化及默认值
	CString m_Lable;
	int m_ID;
	int m_Type;					//标记元件类型，1－4依次为R、C、L、S，初始化为0
	
	Complex m_Voltage;
	Complex m_Current;

	CTypedPtrArray<CObArray,CComp*> m_LeftComps;
	CTypedPtrArray<CObArray,CComp*> m_RightComps;
};

class CCompR :public CComp 
{
	DECLARE_SERIAL(CCompR)
public:
	CCompR();
	CCompR(float value,CRect position);
	virtual ~CCompR(){};
	virtual void Draw(CDC* pDC);
	virtual void ShowProperties();
	virtual void Serialize(CArchive& ar);

	double m_Value;
};

class CCompC : public CComp
{
	DECLARE_SERIAL(CCompC)
public:
	CCompC();
	CCompC(float value,CRect position);
	virtual ~CCompC(){};
	virtual void Draw(CDC* pDC);
	virtual void ShowProperties();
	virtual void Serialize(CArchive& ar);

	double m_Value;
};

class CCompL : public CComp
{
	DECLARE_SERIAL(CCompL)
public:
	CCompL();
	CCompL(float value,CRect position);
	virtual ~CCompL(){}
	virtual void Draw(CDC* pDC);
	virtual void ShowProperties();
	virtual void Serialize(CArchive& ar);

	double m_Value;
};

class CCompS : public CComp
{
	DECLARE_SERIAL(CCompS)
public:
	CCompS();
	CCompS(double volt, double freq);
	virtual ~CCompS(){}
	virtual void Draw(CDC* pDC);
	virtual void ShowProperties();
	virtual void Serialize(CArchive& ar);

	double m_Volt;
	double m_Freq;
};



//The Line Class
class CLine : public CObject
{
	DECLARE_SERIAL(CLine)
public:
	CLine();
	CLine(CPoint start, CPoint end);
	void Draw(CDC* pDC);
	void Update(CPoint NewPoint);
	virtual void Serialize(CArchive& ar);

	CComp* m_PreComp;
	CComp* m_NextComp;
	CPoint m_StartPoint;
	CPoint m_EndPoint;
	int m_Dire;				//画线时临时记录线的“方向”，顺时针为0，逆则为1；初始化为0
};




#endif // !defined(AFX_COMPR_H__F924193E_6D38_4EC7_A029_791DF5D85EDD__INCLUDED_)

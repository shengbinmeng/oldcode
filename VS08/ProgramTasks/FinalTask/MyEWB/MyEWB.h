// MyEWB.h : main header file for the MYEWB application
//

#if !defined(AFX_MYEWB_H__C53105EE_A5FC_448D_95DF_52123A613BB9__INCLUDED_)
#define AFX_MYEWB_H__C53105EE_A5FC_448D_95DF_52123A613BB9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CMyEWBApp:
// See MyEWB.cpp for the implementation of this class
//

class CMyEWBApp : public CWinApp
{
public:
	CMyEWBApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyEWBApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CMyEWBApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYEWB_H__C53105EE_A5FC_448D_95DF_52123A613BB9__INCLUDED_)

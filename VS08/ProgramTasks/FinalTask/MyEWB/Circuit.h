#include "Comps.h"

bool existp(CTypedPtrArray<CObArray,CComp*>& ca,CComp*& p);


//将用到的数据结构定义：
struct Vertex	//? Node
{
	int VertexID;
	CTypedPtrArray<CObArray,CComp*> Comps;
	CTypedPtrArray<CObArray,CComp*> InComps;
	CTypedPtrArray<CObArray,CComp*> OutComps;

	Vertex(){}
	Vertex(Vertex& v2)
	{
		VertexID=v2.VertexID;

		Comps.RemoveAll();
		Comps.Append(v2.Comps);
		Comps.FreeExtra();

		InComps.RemoveAll();
		InComps.Append(v2.InComps);
		InComps.FreeExtra();

		OutComps.RemoveAll();
		OutComps.Append(v2.OutComps);
		OutComps.FreeExtra();
	}

	const Vertex& operator=(const Vertex& v2)
	{
		if (&v2!=this)
		{
			VertexID=v2.VertexID;

			Comps.RemoveAll();
			Comps.Append(v2.Comps);
			Comps.FreeExtra();

			InComps.RemoveAll();
			InComps.Append(v2.InComps);
			InComps.FreeExtra();

			OutComps.RemoveAll();
			OutComps.Append(v2.OutComps);
			OutComps.FreeExtra();

		}
		return *this;
	}

};

struct Edge		//? Branch
{
	int EdgeID;
	Vertex head;
	Vertex tail;
	CTypedPtrArray<CObArray,CComp*> Comps;
	Complex Voltage;
	Complex Current;

	Edge(){}

	Edge(Edge& e2)
	{
		EdgeID=e2.EdgeID;
		head=e2.head;
		tail=e2.tail;
		Voltage=e2.Voltage;
		Current=e2.Current;

		Comps.RemoveAll();
		Comps.Append(e2.Comps);
		Comps.FreeExtra();

	}

	const Edge& operator=(const Edge& e2)
	{
		if(this!= &e2)
		{
			EdgeID=e2.EdgeID;
			head=e2.head;
			tail=e2.tail;
			Voltage=e2.Voltage;
			Current=e2.Current;

			Comps.RemoveAll();
			Comps.Append(e2.Comps);
			Comps.FreeExtra();
		}

		return *this;
	}
};

struct Result 
{
	CArray<Edge,Edge> Edges;
	CArray<Vertex,Vertex> Vertexes;

	double freq;

	Result(){}
	Result(Result& r2)
	{
		Edges.RemoveAll();
		Edges.Append(r2.Edges);
		Edges.FreeExtra();

		Vertexes.RemoveAll();
		Vertexes.Append(r2.Vertexes);
		Vertexes.FreeExtra();

		freq=r2.freq;

	}

	const Result& operator=(const Result& r2)
	{
		if(this!= &r2)
		{
			Edges.RemoveAll();
			Edges.Append(r2.Edges);
			Edges.FreeExtra();

			Vertexes.RemoveAll();
			Vertexes.Append(r2.Vertexes);
			Vertexes.FreeExtra();

			freq=r2.freq;
		}
		return *this;

	}
};

class Circuit
{
public:
	Circuit(){}
	int Create(const CTypedPtrArray <CObArray,CComp*>& data);
	int Analyse();
	Result GetResult(){return m_Result;}

	double freq;
	Result m_Result;
};
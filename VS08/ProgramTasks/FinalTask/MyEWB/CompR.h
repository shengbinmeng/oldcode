// CompR.h: interface for the CCompR class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_COMPR_H__F924193E_6D38_4EC7_A029_791DF5D85EDD__INCLUDED_)
#define AFX_COMPR_H__F924193E_6D38_4EC7_A029_791DF5D85EDD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CCompR  
{
public:
	CCompR();
	virtual ~CCompR();
private:
	float fValueR;

};

#endif // !defined(AFX_COMPR_H__F924193E_6D38_4EC7_A029_791DF5D85EDD__INCLUDED_)

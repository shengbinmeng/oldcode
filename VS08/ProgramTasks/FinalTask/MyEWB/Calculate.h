/************************************************************************/
/*                  所有元器件以及电路图的的声明                        */
/************************************************************************/

#include <iostream>
#include <fstream>
#include <math.h>
using namespace std;


class Complex//复数
{
public:
	Complex(double x){real=x;imag=0.0;}
	Complex(double x=0.0,double y=0.0){real=x;imag=y;}
	double Re(){return real;}//返回实部
	double Im(){return imag;}//返回虚部
	double Meg();//取模
	double Ang();//取幅角
	double Getreal(){return real;}
	double Getimag(){return imag;}
	Complex operator =(const Complex &);//赋值
	friend Complex operator +(Complex &,Complex &);//复数加法
	friend Complex operator -(Complex &,Complex &);//复数减法
	friend Complex operator *(Complex &,Complex &);//复数乘法
	friend Complex operator /(Complex &,Complex &);//复数除法
	friend istream &operator>>(istream &f,Complex &c);
	friend ostream &operator<<(ostream &f,Complex &c);

	double real,imag;//实部、虚部
};


class Impedance //阻抗，也是图的边
{
public:
	Impedance();
	int Getnfrom(){return nfrom;}
	int Getnto(){return nto;}
	void Setnfrom(int i1){nfrom=i1;}
	void Setnto(int j1){nto=j1;}
	void Setpfrom(Impedance *p){pfrom=p;}
	void Setpto(Impedance *p){pto=p;}
	Impedance *Getpfrom(){return pfrom;}
	Impedance *Getpto(){return pto;}
	bool Getmark(){return mark;}
	void Setmark(bool m){mark=m;}
	void Setyb(Complex &y){yb=y;}
	void Seteb(Complex &e){eb=e;}
	void Setjb(Complex &j){jb=j;}
	Complex &Getyb(){return yb;}
	Complex &Geteb(){return eb;}
	Complex &Getjb(){return jb;}

	int num;//支路的编号
private:
	bool mark;//访问标志
	int nfrom,nto;//该边始、终节点的位置
	Impedance *pfrom,*pto;//分别指向该边始、终节点的另一条未被访问过的边
	Complex yb,eb,jb;//yb是支路导纳，eb是支路独立电压源，jb是支路独立电流源
};


struct Node//节点,也是图的顶点
{
	Complex un;//节点电压
	Impedance *firstedge;//指向第一条边
};

struct IptBranch
{
	int head,tail;
	Complex admitance;
	Complex source;
};

struct OptBranch
{
	int head,tail;
	Complex Voltage;
	Complex Current;	
};


class Calculate //电路图
{
public:
	Calculate();
	~Calculate(){delete[] pnode;}
	int Getnnode(){return nnode;}
	void Input(int nBranch,double Freq,IptBranch branch[]);//创建电路图
	void Form();//形成导纳增广矩阵
	void Guass();//高斯消元
	void Output(OptBranch branch[],int& nBranch);//给出支路电压和电流 

	Complex yna[20][20];//导纳增广矩阵
	Complex Yb[20],Eb[20],Jb[20],Ib[20],Ub[20];//yb是支路导纳，eb是支路独立电压源，jb是支路独立电流源，ib是支路电流，ub是支路电压
	int Nfrom[20],Nto[20];//始终点的编号向量
	bool flage;//方程是否有解

private:
	int nnode,nbr;//独立节点数、支路数
	double omega;//频率
	Node *pnode;//节点数组首地址

};

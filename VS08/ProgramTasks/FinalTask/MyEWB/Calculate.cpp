/************************************************************************/
/*                     所有元器件以及电路图的的实现                     */
/************************************************************************/
#include "StdAfx.h"
#include "Calculate.h"
#include <stdlib.h>

//复数
double Complex::Meg()//取模
{
	double temp;
	temp=sqrt(real*real+imag*imag);
	return  temp;
}
double Complex::Ang()//取幅角
{
	double angle;
	if (real==0&&imag>0) angle=90;
	else if (real==0&&imag<0) angle=-90;
	else if (Meg()==0) angle=0;
	else angle=atan(imag/real)*57.295779;
	if (real<0)angle=angle+180;
	return angle;
}
Complex Complex::operator =(const Complex &c)//赋值
{
	real=c.real;
	imag=c.imag;
	return c;
}
Complex operator +(Complex &c1,Complex &c2)//复数加法
{
	return Complex(c1.real+c2.real,c1.imag+c2.imag);
}
Complex operator -(Complex &c1,Complex &c2)//复数减法
{
	return Complex(c1.real-c2.real,c1.imag-c2.imag);
}
Complex operator *(Complex &c1,Complex &c2)//复数乘法
{
	return Complex(c1.real*c2.real-c1.imag*c2.imag,c1.real*c2.imag+c1.imag*c2.real);
}
Complex operator /(Complex &c1,Complex &c2)//复数除法
{
	double denom=c2.Meg();
	//Edwin(10.6.15):
	//if (!denom){cerr<<"The denominator is zero!"<<endl;exit(1);}
	//else return Complex((c1.real*c2.real+c1.imag*c2.imag)/denom/denom,(c1.imag*c2.real-c1.real*c2.imag)/denom/denom);
	return Complex((c1.real*c2.real+c1.imag*c2.imag)/denom/denom,(c1.imag*c2.real-c1.real*c2.imag)/denom/denom);
}
istream &operator>>(istream &f,Complex &c)//重载“>>”，实现复数输入
{
	double temp1,temp2;
	f>>temp1>>temp2;
	Complex temp(temp1,temp2);
	c=temp;
	return f;
}
ostream &operator<<(ostream &f,Complex &c)//重载“<<”，实现复数输出
{
	f<<c.Re()<<"+i"<<c.Im();
	return f;
}

//类Impedance
Impedance::Impedance()//初始化
{
	mark=0;
	nfrom=0;
	nto=0;
	pto=pfrom=NULL;
}

//类Calculate
Calculate::Calculate()//初始化
{
	flage=1;
	nnode=0;
	nbr=0;
	omega=0;
	pnode=new Node[20];
	for (int i=0;i<20;i++)
	{
		pnode[i].firstedge=NULL;
	}
}

void Calculate::Input(int nBranch,double Freq,IptBranch branch[])//创建图
{
	Complex eb,jb,yb;
	int i,j;
	Impedance *p;
	
	nbr=nBranch;
	omega=Freq;
	for (int k=1;k<=nbr;k++)
	{
		i=branch[k-1].head;
		j=branch[k-1].tail;
	
		yb=branch[k-1].admitance;
		eb=branch[k-1].source;
		//		cout<<"输入该支路的独立电流源电流（复数形式）：";
		//		cin>>jb;

		p=new Impedance;
		if (!p)
		{
			cout<<"内存申请失败！"<<endl;
			exit(0);
		}
		p->num=k;
		p->Seteb(eb);
		p->Setjb(jb);
		p->Setyb(yb);
		p->Setnfrom(i);
		p->Setnto(j);
		p->Setpfrom(pnode[i].firstedge);
		p->Setpto(pnode[j].firstedge);
		pnode[i].firstedge=p;
		pnode[j].firstedge=p;
		if (i>nnode)nnode=i;
		if (j>nnode)nnode=j;
	}
}


void Calculate::Form()//形成导纳增广矩阵yna，支路导纳向量Yb，支路独立电压源向量Eb，支路独立电流源向量Jb
{
	Impedance *p=NULL;
	Complex  temp;
	int k;
	int x1,y1,nn=Getnnode()+1;
	for (int i=1;i<=nnode;i++) for (k=1;k<=nn;k++) yna[i][k]=temp;
	for (int i=0;i<=nnode;i++)
	{
		p=pnode[i].firstedge;
		while (p)
		{
			if (p->Getmark()==0)
			{
				x1=p->Getnfrom();
				y1=p->Getnto();
				k=p->num;
				Jb[k]=p->Getjb();
				Eb[k]=p->Geteb();
				Yb[k]=p->Getyb();
				Nfrom[k]=x1;
				Nto[k]=y1;
				if (x1) yna[x1][x1]=yna[x1][x1]+p->Getyb();
				if (y1) yna[y1][y1]=yna[y1][y1]+p->Getyb();
				if (x1&&y1) 
				{
					yna[x1][y1]=yna[x1][y1]-p->Getyb();
					yna[y1][x1]=yna[y1][x1]-p->Getyb();
				}
				if (x1) yna[x1][nn]=yna[x1][nn]+p->Getjb()-p->Getyb()*p->Geteb();
				if (y1) yna[y1][nn]=yna[y1][nn]-p->Getjb()+p->Getyb()*p->Geteb();
				p->Setmark(1);//访问标记
			}
			if(i==p->Getnfrom())p=p->Getpfrom();
			else p=p->Getpto();
		}	
	}
	for (int i=0;i<=nnode;i++)//还原标志位
	{
		p=pnode[i].firstedge;
		while (p)
		{
			if (p->Getmark()==1)p->Setmark(0);//还原标记
			if(i==p->Getnfrom())p=p->Getpfrom();
			else p=p->Getpto();
		}	
	}
}

void Calculate::Guass()//高斯消元
{
	int i,j,k,n,m,rowindex;
	Complex pivot,temp;
	m=Getnnode();
	n=Getnnode()+1;
	for (i=1;i<=m;i++)
	{
		pivot=yna[i][i];
		rowindex=i;
		for(j=i+1;j<=m;j++) if(yna[j][i].Meg()>pivot.Meg()) {pivot=yna[j][i];rowindex=j;}
		if(pivot.Meg()==0) {flage=0;cout<<"The determinant is zero!";break;}
		if(rowindex!=i) for(k=i;k<=n;k++){temp=yna[i][k];yna[i][k]=yna[rowindex][k];yna[rowindex][k]=temp;}
		for(j=i+1;j<=m;j++) for(k=i+1;k<=n;k++)yna[j][k]=yna[j][k]-yna[j][i]*yna[i][k]/yna[i][i];
	}
	yna[m][n]=yna[m][n]/yna[m][m];
	for (i=m-1;i>=1;i--)
	{
		for (k=i+1;k<=m;k++)yna[i][n]=yna[i][n]-yna[i][k]*yna[k][n];
		yna[i][n]=yna[i][n]/yna[i][i];		
	}
}

void Calculate::Output(OptBranch branch[],int& nBranch)//计算支路电压和电流并输出
{
	int nn=nnode+1;
	Form();
	Guass();
	if(!flage)
	{
		cout<<"No solution!"<<endl;
		exit(0);
	}
	Complex temp;
	yna[0][nn]=temp;

	nBranch=nbr;
	for (int i=1;i<=nbr;i++)Ub[i]=yna[Nfrom[i]][nn]-yna[Nto[i]][nn];
	for (int i=1;i<=nbr;i++)Ib[i]=Yb[i]*(Ub[i]+Eb[i])-Jb[i];
	for(int i=1;i<=nbr;i++)
	{
		branch[i-1].head=Nfrom[i];
		branch[i-1].tail=Nto[i];
		branch[i-1].Voltage=Ub[i];
		branch[i-1].Current=Ib[i];
	}
	
}

// DlgResult.cpp : implementation file
//

#include "stdafx.h"
#include "MyEWB.h"
#include "DlgResult.h"
#include "MyEWBView.h"
#include "DlgGraph.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgResult dialog


CDlgResult::CDlgResult(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgResult::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgResult)
	//}}AFX_DATA_INIT
}


void CDlgResult::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgResult)
	DDX_Control(pDX, IDC_COMBO2, m_Combo2);
	DDX_Control(pDX, IDC_COMBO1, m_Combo1);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDlgResult, CDialog)
	//{{AFX_MSG_MAP(CDlgResult)
	ON_BN_CLICKED(IDC_DISPLAYALL, OnDisplayall)
	ON_BN_CLICKED(IDC_DISPLAYBRANCH, OnDisplaybranch)
	ON_BN_CLICKED(IDC_DISPLAYCOMP, OnDisplaycomp)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgResult message handlers

void CDlgResult::OnDisplayall() 
{
	// TODO: Add your control notification handler code here
	CMyEWBView* pParent=(CMyEWBView*)this->GetParent();
	CString str,str1;
	str+="电路中所有元件的电压电流信息如下：\n\n";
	for (int i=0;i<pParent->m_Data.GetSize();i++)
	{
		str1.Format("两端的电压为：%.4g + j%.4g;   电流为：%.4g + j%.4g",
			pParent->m_Data[i]->m_Voltage.real,pParent->m_Data[i]->m_Voltage.imag,pParent->m_Data[i]->m_Current.real,pParent->m_Data[i]->m_Current.imag);
		str1=pParent->m_Data[i]->m_Lable+str1;
		str=str+str1+" ;\n";
	}

	MessageBox(str);
	//MessageBox("此按钮将显示分析计算得到的所有支路信息，包括每个支路的电压电流，及每个元件的电压电流，等等。");
	
}


BOOL CDlgResult::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CMyEWBView* pParent=(CMyEWBView*)this->GetParent();
	for (int i=0;i<(pParent->m_Data.GetSize());i++)
	{
		m_Combo1.AddString(pParent->m_Data[i]->m_Lable);
	}


	for (int i=0;i<pParent->m_Result.Edges.GetSize();i++)
	{
		int m=pParent->m_Result.Edges[i].Comps.GetSize();

		CString str;str.Format("branch%d (",pParent->m_Result.Edges[i].EdgeID);
		for (int j=0;j<m;j++)
		{
			str+=pParent->m_Result.Edges[i].Comps[j]->m_Lable;
		}
		str+=")";
		m_Combo2.AddString(str);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



void CDlgResult::OnDisplaybranch() 
{
	// TODO: Add your control notification handler code here
	UpdateData();

	CMyEWBView* pParent=(CMyEWBView*)this->GetParent();
	int n=m_Combo2.GetCurSel();
	if (n<0)
	{
		MessageBox("您尚未选择支路！");
		return;
	}


	CString str,display;
	m_Combo2.GetLBText(n,str);

	for (int i=0;i<pParent->m_Result.Edges.GetSize();i++)
	{
		int m=pParent->m_Result.Edges[i].Comps.GetSize();

		CString str1;str1.Format("branch%d (",pParent->m_Result.Edges[i].EdgeID);
		for (int j=0;j<m;j++)
		{
			str1+=pParent->m_Result.Edges[i].Comps[j]->m_Lable;
		}
		str1+=")";

		if (str1==str)
		{
			display.Format("此支路%s电压为：%.4g + j%.4g;    电流为：%.4g + j%.4g",
				str,pParent->m_Result.Edges[n].Voltage.real,pParent->m_Result.Edges[n].Voltage.imag,pParent->m_Result.Edges[n].Current.real,pParent->m_Result.Edges[n].Current.imag);

			CDlgGraph dlg;
			dlg.m_strDisplay=display;
			dlg.m_VoltageR=pParent->m_Result.Edges[i].Voltage.real;
			dlg.m_VoltageI=pParent->m_Result.Edges[i].Voltage.imag;
			dlg.m_CurrentR=pParent->m_Result.Edges[i].Current.real;
			dlg.m_CurrentI=pParent->m_Result.Edges[i].Current.imag;
			dlg.m_Freq=pParent->m_Result.freq;
			dlg.DoModal();
			break; 
		}
	}

	
}


void CDlgResult::OnDisplaycomp() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	CMyEWBView* pParent=(CMyEWBView*)this->GetParent();
	CString str;
	int n=m_Combo1.GetCurSel();
	if (n<0)
	{
		MessageBox("您尚未选择元件！");
		return;
	}
	m_Combo1.GetLBText(n,str);
	for (int i=0;i<pParent->m_Data.GetSize();i++)
	{
		if (pParent->m_Data[i]->m_Lable==str)
		{	
			CString str1; str1.Format("两端的电压为：%.4g + j%.4g;   电流为：%.4g + j%.4g",
				pParent->m_Data[i]->m_Voltage.real,pParent->m_Data[i]->m_Voltage.imag,pParent->m_Data[i]->m_Current.real,pParent->m_Data[i]->m_Current.imag);
			
			CDlgGraph dlg;
			dlg.m_strDisplay=str+str1;
			dlg.m_VoltageR=pParent->m_Result.Edges[i].Voltage.real;
			dlg.m_VoltageI=pParent->m_Result.Edges[i].Voltage.imag;
			dlg.m_CurrentR=pParent->m_Result.Edges[i].Current.real;
			dlg.m_CurrentI=pParent->m_Result.Edges[i].Current.imag;
			dlg.m_Freq=pParent->m_Result.freq;
			dlg.DoModal();
			break;
		}
	}
	
}

void CDlgResult::OnDestroy() 
{
	CDialog::OnDestroy();	
	// TODO: Add your message handler code here
	delete this;
	
}


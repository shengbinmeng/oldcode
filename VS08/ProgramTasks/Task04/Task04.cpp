// Task04.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <conio.h>
using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	char ch;
	while(1)
	{
		cout<<"请选择：1－单词统计   2－大小写转换   3－逆序排列   4－退出 （注：本程序不再进行输入合法性检查，请按要求输入，否则后果自负）"<<endl;
		cin>>ch;
		switch(ch)
		{
		case '1':
			{
				char str[100];
				cout<<"请输入一个英文字符串（长度不超过80）：";
				cin.sync();  //使输入同步；即清空输入缓冲区，避免受之前输入（包括回车符）的影响
				cin.getline(str,100);

				int count=0;
				bool word_start=true;
				for (int i=0;str[i]!='\0';i++)
				{
					if(str[i]==' ') { word_start=true; continue; }
					else
					{
						if(word_start==true){ count++; word_start=false;}
						else continue;
					}
				}

				cout<<"该字符串中单词数为："<<count<<endl;
			}

			break;

		case '2':
			{
				char str[100];
				cout<<"输入一个只含英文字母的字符串（长度不超过20）：";
				cin.sync();
				cin.getline(str,20);

				for (int i=0;str[i]!='\0';i++)
				{
					if('a'<=str[i] && str[i]<='z') str[i]=str[i]+ 'A'-'a';
					else if('A'<=str[i] && str[i]<='Z') str[i]=str[i]+ 'a'-'A';
				}
				
				cout<<"该字符串进行大小写转换后为："<<str<<endl;
			}

			break;

		case '3':
			{
				cout<<"输入一组非零整数（不超过20个，输入0表示结束）："<<endl;

				int a[20];
				int i, temp;
				for(i=0;i<20;i++)
				{
					cin>>temp;
					if(temp==0) {cin.sync(); break;}     //读到0之后，就清除缓冲区中余下的可能多输入的数
					a[i]=temp;
				}

				int count=i;   //数组中数据的个数
				if(count==0){cout<<"输入数据个数为0！"<<endl; break;}

				for (int j=0;j<=(count-1)/2;j++)
				{
					temp=a[j];
					a[j]=a[count-1-j];
					a[count-1-j]=temp;
				}

				cout<<"数组中的值按逆序存放为："<<endl;
				for(i=0;i<count;i++)
				{
					cout<<a[i]<<"  ";
				}
				cout<<endl;

			}

			break;

		case '4':
			exit(0);

		}
	}

	return 0;
}


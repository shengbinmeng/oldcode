// Task02.cpp : Defines the entry point for the console application.

//注：该程序未作输入合法性检查！！！

#include "stdafx.h"
#include <iostream>

using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	while(1)
	{
		char ch;
		cout<<"请选择：1－数据整除   2－图形显示   3－数对   4－退出 （注：本程序不再进行输入合法性检查，请按要求输入，否则后果自负）"<<endl;
		cin>>ch;
		switch(ch)
		{
		case '1':
			{
				int min=0,max=0,factor=0;
				cout<<"输入三个正整数：\n"<<"min:";
				cin>>min;
				cout<<"max:"; cin>>max;
				cout<<"factor:"; cin>>factor;

				for (int i=min;i<=max;i++)
				{
					if(i%factor==0) cout<<i<<"  ";
					else continue;
				}
				cout<<endl;

				break;
			}

		case '2':
			{
				int n=0;
				cout<<"输入行数："; cin>>n;
				for(int i=n;i>0;i--)
				{
					for(int j=0;j<i;j++)  cout<<"*   ";
					cout<<endl;
				}

				break;
			}

		case '3':
			{
				int n=0;
				cout<<"输入一个整数："; cin>>n;

				//只需判断到该整数的一半即可，一半以上不可能是被整除；最后再添上n本身与1相乘
				for (int i=1;i<=n/2;i++)
				{
					if(n%i==0)cout<<i<<" * "<<n/i<<" = "<<n<<endl;
					else continue;
				}
				cout<<n<<" * "<<1<<" = "<<n<<endl;

				break;
			}

		case '4':
			exit(0);

		}
	}

	return 0;
}


// Task05.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	cout<<"//EasyPro  Version1.0 \n//Edwin Meng\n"<<endl;        //输出程序名、版本和作者信息
	char ch;
	while(1)
	{
		cout<<"请选择：1－字符删除   2－出现最多的数   3－Anagrams问题   4－退出 （注：本程序不再进行输入合法性检查，请按要求输入，否则后果自负）"<<endl;
		cin>>ch;
		switch(ch)
		{
		case '1':
			{
				char str[21];
				cout<<"请输入一个ASCII字符串（长度不超过20）：";
				cin.sync();		  //使输入同步；即清空输入缓冲区，避免受之前输入（包括回车符）的影响
				cin.getline(str,21);

				cin.sync(); cin.clear();

				char ch_del, str2[21];
				cout<<"输入要从中删除的字符：";
				//cin>>ch_del;
				cin.get(ch_del);    //用这个可以使输入包括空白字符，如空格

				int j=0;
				for(int i=0;str[i]!='\0';i++)
				{
					if(str[i]!=ch_del) { str2[j]=str[i]; j++;}
				}
				str2[j]='\0';  //需在新数组结尾人为加上NULL，以构成字符串
			
				cout<<"删除该字符后的新字符串为："<<str2<<endl;
			}

			break;

		case '2':
			{
				int N;
				cout<<"输入要输入的数的个数N(<=20)：";
				cin>>N;   cin.sync();
				cout<<"请输入这"<<N<<"个数："<<endl;

				int numbers[20];
				int p, num[20], count[20]={0};
				bool exsit=false;
				for (int i=0;i<N;i++)
				{
					cin>>numbers[i];

					if(i==0){ p=0; num[0]=numbers[i];count[0]=1; continue;}     //第一个数只需记下

					//对以后每一个输入的数，如已经记录过（存在），增加其计数值
					for(int j=0;j<=p;j++)
					{
						if(numbers[i]==num[j])
						{
							exsit=true;
							count[j]++;
							break; 
						}
					}

					//不存在则增加对其的记录
					if(!exsit){ p++; num[p]=numbers[i]; count[p]=1;}
					
				}

				cin.sync();

				//找出对应count值最大的数
				int max_index=0;
				for (int j=0;j<=p;j++)
				{
					if(count[j]>count[max_index]) max_index=j;
					else
					{
						//count值一样则比较数值大小，取较小的
						if(count[j]==count[max_index])
						{
							if(num[j]<num[max_index]) max_index=j;
						}
					}					
				}

				
				cout<<"输入次数最多的（较小的）数为："<<num[max_index]<<endl;
			}

			break;

		case '3':
			{
				cout<<"输入两个单词（每个不超过80字符）："<<endl;
				char word1[80], word2[80];
				cin>>word1>>word2;

				//将大写字母全转换成小写
				for (int i=0;word1[i]!='\0';i++)
				{
					if('A'<=word1[i] && word1[i]<='Z') word1[i]=word1[i]+'a'-'A';
				}
				for (int i=0;word2[i]!='\0';i++)
				{
					if('A'<=word2[i] && word2[i]<='Z') word2[i]=word2[i]+'a'-'A';
				}


				//先认为真，后面对其质询
				bool isAnagrams=true;
				bool isEqualLong=true;

				//分析单词中的字母构成
				char letters1[80], letters2[80];
				int count1[80], count2[80], p1, p2;
				bool exsit1=false, exsit2=false;
				int i=-1;  
				while(1)
				{
					i++;

					//发现长度不相等时退出循环
					if(word1[i]=='\0' && word2[i]!='\0'){isEqualLong=false; break;}
					if(word1[i]!='\0' && word2[i]=='\0'){isEqualLong=false; break;}

					//两字符串结束时退出循环
					if(word1[i]=='\0' && word2[i]=='\0') break;
					

					if(i==0)   //第一个字母只需记下
					{
						p1=p2=0; 
						letters1[0]=word1[i];count1[0]=1;
						letters2[0]=word2[i];count2[0]=1;
						continue;
					}   

					//对以后每一个字母，如已经记录过，增加其计数值
					for(int j=0;j<=p1;j++)
					{
						if(word1[i]==letters1[j])
						{
							exsit1=true;
							count1[j]++;
							break; 
						}
					}
					for(int j=0;j<=p2;j++)
					{
						if(word2[i]==letters2[j])
						{
							exsit2=true;
							count2[j]++;
							break; 
						}
					}

					//否则增加对其的记录
					if(!exsit1){ p1++; letters1[p1]=word1[i]; count1[p1]=1;}
					if(!exsit2){ p2++; letters2[p2]=word2[i]; count2[p2]=1;}

					exsit1=exsit2=false;  //勿忘重置变量，为下一次循环做准备！！

				}


				//退出循环后判断是否Anagrams
				if(!isEqualLong) isAnagrams=false;
				else
				{
					if(p1!=p2) isAnagrams=false;
					else
					{
						for(int i=0;i<=p1;i++)
						{
							bool have_this_letter=false;
							int j;
							for (j=0;j<=p2;j++)
							{
								if(letters2[j]==letters1[i]){ have_this_letter=true; break;}
							}

							if( ! have_this_letter){ isAnagrams=false; break;}
							else
							{
								if(count2[j]!=count1[i]) {isAnagrams=false; break;}
							}

						}
					}
				}

				//呈现结果
				if(isAnagrams) cout<<"这两个单词是Anagrams！"<<endl;
				else cout<<"这两个单词不是Anagrams！"<<endl;
				cout<<"/* “Anagrams”指这样两个单词：两词中每一个英文字母（不区分大小写）所出现的次数都是相同的。*/"<<endl;

			}

			break;

		case '4':
			exit(0);

		}
	}

	return 0;
}


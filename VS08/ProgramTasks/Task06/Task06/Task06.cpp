// Task06.cpp : Defines the entry point for the console application.
//


#include "stdafx.h"
#include <iostream>
#include <cstring>
using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	cout<<"//EasyPro  Version1.0 \n//Edwin Meng\n"<<endl;        //输出程序名、版本和作者信息
	char ch;
	while(1)
	{
		cout<<"\n请选择：1－单词统计    2－句子加密    3－退出 （注：本程序不再进行输入合法性检查，请按要求输入，否则后果自负）"<<endl;
		cin>>ch;
		switch(ch)
		{
		case '1':
			{
				char str[120];
				cout<<"请输入一个英文句子（均小写，长度不超过100）：";
				cin.sync();  //使输入同步；即清空输入缓冲区，避免受之前输入（包括回车符）的影响
				cin.getline(str,100);

				int count=0;
				bool word_start=true;
				
				char* words[100];       //用于储存各单词（其实可以用已有的高级数据容器的，如Array等；但为了练习基础知识……）
				int p=0, j=0;
				words[p]=new char[50];

				//析取句子中所有单词，存在words[p]中
				for (int i=0;str[i]!='\0';i++)
				{
					if(str[i]==' ') { word_start=true; words[p][j]='\0'; p++; words[p]=new char[50]; j=0; continue; }  //是空格，则表明下一字符将要开始一个新单词
					else
					{
						if(word_start==true)	//对新单词的第一个字母进行处理
						{
							count++;
							words[p][j]=str[i]; j++;
							word_start=false;
							
						}
						else{ words[p][j]=str[i]; j++; continue;}
					}
				}
				words[p][j]='\0';	//在最后构成字符串，便于用strcmp来比较


				int diff_count=0;
				bool diff;

				//统计所有单词中不同单词的个数
				for (int p=0;p<count;p++)
				{
					diff=true;
					int i;
					for (i=0;i<p;i++)
					{
						if(strcmp(words[p],words[i])==0){ diff=false; break; }
					}

					if(diff) {diff_count++;}
				}

				cout<<"该字符串中各单词为："<<endl;
				for (int i=0;i<count;i++)
				{
					cout<<words[i]<<endl;
				}

				cout<<"该字符串中单词数为："<<count<<"，其中不同单词数为："<<diff_count<<endl;
			}

			break;

		case '2':
			{
				char str[100];
				cout<<"输入待加密的英文句子（均小写，长度不超过100字符）："<<endl;
				cin.sync();  //使输入同步；即清空输入缓冲区，避免受之前输入（包括回车符）的影响
				cin.getline(str,100);

				char rule[50];
				cout<<"输入加密规则（以“#word1%word2”的形式，表示用word2替换word1）："<<endl;
				cin>>rule;

				//析取替换和被替换的单词
				char replaced[20],replacer[20];
				int flag=0, ed=0, er=0;
				for(int i=0;rule[i]!='\0';i++)
				{
					if(rule[i]=='#'){ flag=1;continue;}
					if(rule[i]=='%'){flag=2;continue;}
					if(flag==1) {replaced[ed]=rule[i];ed++;continue;}
					if(flag==2) {replacer[er]=rule[i];er++;continue;}
				}
				replaced[ed]='\0';replacer[er]='\0';


				//析取句子中所有单词存为字符串，便于用strcmp和strcpy
				char* words[100];       //用于储存各单词（其实可以用已有的高级数据容器的，如Array等；但为了练习基础知识……）
				int p=0, j=0,count=0;
				words[p]=new char[20];

				bool word_start=true;

				for (int i=0;str[i]!='\0';i++)
				{
					if(str[i]==' ') { word_start=true; words[p][j]='\0'; p++; words[p]=new char[20]; j=0; continue; }
					else
					{
						if(word_start==true)
						{
							count++;
							words[p][j]=str[i]; j++;
							word_start=false;

						}
						else{ words[p][j]=str[i]; j++; continue;}
					}
				}
				words[p][j]='\0';


				//将被替换者用替换者替换
				for (int p=0;p<count;p++)
				{
					if(strcmp(words[p],replaced)==0){ strcpy(words[p],replacer);}
				}

				//把替换后的各单词（字符串形式的）转存为一个句子字符串
				char str_coded[200];
				int i=0;
				for(int p=0;p<count;p++)
				{
					for(int j=0;;j++)
					{
						if(words[p][j]=='\0') {str_coded[i]=' ';i++; break;}
						else {str_coded[i]=words[p][j]; i++;}
					}
				}

				str_coded[i-1]='\0';       //把最后一个空格改成字符转结尾符

				//输出结果
				cout<<"加密后的句子为："<<str_coded<<endl;
				
			}

			break;

		

		case '3':
			exit(0);

		}
	}

	return 0;
}


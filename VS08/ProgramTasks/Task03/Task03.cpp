// Task03.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <math.h>
using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	while(1)
	{
		char ch;
		cout<<"请选择：1－栅格打印   2－公安破案   3－阿尔法乘积   4－退出 （注：本程序不再进行输入合法性检查，请按要求输入，否则后果自负）"<<endl;
		cin>>ch;
		switch(ch)
		{
		case '1':
			{
				int height=0,width=0;
				cout<<"输入高度：";
				cin>>height;
				cout<<"输入宽度："; 
				cin>>width;

				for (int i=0;i<height;i++)
				{
					for (int j=0;j<width;j++) cout<<'+'<<'-';
					cout<<'+'<<endl;

					for (int j=0;j<width;j++) cout<<'|'<<' ';
					cout<<'|'<<endl;
				}

				for (int j=0;j<width;j++) cout<<'+'<<'-';
				cout<<'+'<<endl;
			}

			break;

		case '2':
			{
				char* name[4]={"甲","乙","丙","丁"};
				bool isTheif[4]={0};
				int answer[4]={0};
				bool isSolved=0;
				int i;
				
				for (i=0;i<4;i++)
				{
					//假定某一个人为偷窃者
					for (int j=0;j<4;j++)
					{
						if(j==i) isTheif[j]=1;
						else isTheif[j]=0;
					}
					//暂认为问题已解决
					isSolved=1;

					
					answer[0]=(isTheif[1]==0) + (isTheif[3]==1);    //甲的答话，如果完全诚实或完全说谎，则不等于1；下同
					answer[1]=(isTheif[1]==0) + (isTheif[2]==1);
					answer[2]=(isTheif[0]==0) + (isTheif[1]==1);
					answer[3]=(isTheif[3]==0) + (isTheif[3]==0);

	
					//对每人的话进行验证；如果不符合所给条件，则假设不正确，问题没有解决
					for (int j=0;j<4;j++)
					{
						if(answer[j]==1)
						{
							isSolved=0;
							break;
						}
					}

					//如果验证全通过，问题确实解决，退出循环
					if(isSolved) break;
					
				}

				cout<<name[i]<<"是偷窃者。"<<endl;
			}

			break;

		case '3':
			{
				unsigned long n=0;      //此类型可支持更大的整数
				cout<<"输入一个正整数（不超过6,000,000）："; cin>>n;
				
				unsigned long nAlpha=n;

				while(nAlpha>10)
				{
					unsigned long m=nAlpha;
					nAlpha=1;		//准备求新的阿尔法乘积nAlpha

					while(m!=0)
					{
						int p=m%10;     //取最后一位数
						if(p!=0) nAlpha*=p;		//非零则乘上
						m=(m-p)/10;		//准备取倒数次一位数；m为0则表示已取到最高位，nAlpha已是新的阿尔法乘积
					}

				}

				cout<<n<<"的阿尔法乘积为："<<nAlpha<<endl;

			}

			break;

		case '4':
			exit(0);

		}
	}

	return 0;
}


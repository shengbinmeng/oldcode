说明（by孟胜彬）：
1.第2题题目中语句“这四个人的答话，要么完全诚实，要么完全说谎”理解成：“对这四个人中的每个人，其所说的两半句话要么都真，要么都假”。
2.对第3题，用unsigned long类型扩大了可输入整数的上限。（这样，就是用题目中给出的很大的举例数4018224312来验证结果也是没问题的）

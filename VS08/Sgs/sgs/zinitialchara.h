//角色初始化
void initialchara()
{
	for(int i=0;i<4;i++) players[0].weaponflag[i]=0;//没有装备
	players[0].killrange=1;
	players[0].defenserange=0;
	players[0].qrange=1;
	players[0].dead=0;
	players[0].realcard.hcard=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\caocao0.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[0].cardp=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\caocao1.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[0].bighealthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\bighealth.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[0].cardbackground=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\tablecard.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[0].cardbutton=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\caocao.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[0].cardnumber=-1;
	players[0].health=4;
	players[0].healthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\health.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[0].maxhealth=4;
	//players[0].cardp 以后要加
	players[0].country='W';
	players[0].de=de0;
	players[0].maxhealthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\maxhealth.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	strcpy(players[0].name,"曹操");

	for(int i=0;i<4;i++) players[1].weaponflag[i]=0;//没有装备
	players[1].killrange=1;
	players[1].defenserange=0;
	players[1].qrange=1;
	players[1].dead=0;
	players[1].realcard.hcard=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\ganning0.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[1].cardp=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\ganning1.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[1].bighealthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\bighealth.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[1].cardbackground=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\tablecard.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[1].cardbutton=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\ganning.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[1].cardnumber=-1;
	players[1].health=4;
	players[1].healthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\health.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[1].maxhealth=4;
	//players[0].cardp 以后要加
	players[1].country='w';
	players[1].de=de1;
	players[1].maxhealthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\maxhealth.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	strcpy(players[1].name,"甘宁");

	for(int i=0;i<4;i++) players[2].weaponflag[i]=0;//没有装备
	players[2].killrange=1;
	players[2].defenserange=0;
	players[2].qrange=1;
	players[2].dead=0;
	players[2].realcard.hcard=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\guanyu0.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[2].cardp=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\guanyu1.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[2].bighealthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\bighealth.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[2].cardbackground=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\tablecard.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[2].cardbutton=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\guanyu.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[2].cardnumber=-1;
	players[2].health=4;
	players[2].healthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\health.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[2].maxhealth=4;
	//players[0].cardp 以后要加
	players[2].country='s';
	players[2].de=de2;
	players[2].maxhealthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\maxhealth.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	strcpy(players[2].name,"关羽");

	for(int i=0;i<4;i++) players[3].weaponflag[i]=0;//没有装备
	players[3].killrange=1;
	players[3].defenserange=0;
	players[3].qrange=1;
	players[3].dead=0;
	players[3].realcard.hcard=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\liubei0.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[3].cardp=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\liubei1.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[3].bighealthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\bighealth.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[3].cardbackground=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\tablecard.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[3].cardbutton=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\liubei.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[3].cardnumber=-1;
	players[3].health=4;
	players[3].healthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\health.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[3].maxhealth=4;
	//players[0].cardp 以后要加
	players[3].country='s';
	players[3].de=de3;
	players[3].maxhealthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\maxhealth.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	strcpy(players[3].name,"刘备");

	for(int i=0;i<4;i++) players[4].weaponflag[i]=0;//没有装备
	players[4].killrange=1;
	players[4].defenserange=0;
	players[4].qrange=1;
	players[4].dead=0;
	players[4].realcard.hcard=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\machao0.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[4].cardp=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\machao1.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[4].bighealthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\bighealth.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[4].cardbackground=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\tablecard.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[4].cardbutton=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\machao.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[4].cardnumber=-1;
	players[4].health=4;
	players[4].healthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\health.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[4].maxhealth=4;
	//players[0].cardp 以后要加
	players[4].country='s';
	players[4].de=de4;
	players[4].maxhealthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\maxhealth.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	strcpy(players[4].name,"马超");

	for(int i=0;i<4;i++) players[5].weaponflag[i]=0;//没有装备
	players[5].killrange=1;
	players[5].defenserange=0;
	players[5].qrange=1;
	players[5].dead=0;
	players[5].realcard.hcard=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\sunquan0.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[5].cardp=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\sunquan1.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[5].bighealthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\bighealth.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[5].cardbackground=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\tablecard.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[5].cardbutton=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\sunquan.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[5].cardnumber=-1;
	players[5].health=4;
	players[5].healthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\health.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[5].maxhealth=4;
	//players[0].cardp 以后要加
	players[5].country='w';
	players[5].de=de5;
	players[5].maxhealthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\maxhealth.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	strcpy(players[5].name,"孙权");

	for(int i=0;i<4;i++) players[6].weaponflag[i]=0;//没有装备
	players[6].killrange=1;
	players[6].defenserange=0;
	players[6].qrange=1;
	players[6].dead=0;
	players[6].realcard.hcard=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\zhangfei0.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[6].cardp=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\zhangfei1.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[6].bighealthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\bighealth.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[6].cardbackground=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\tablecard.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[6].cardbutton=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\zhangfei.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[6].cardnumber=-1;
	players[6].health=4;
	players[6].healthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\health.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[6].maxhealth=4;
	//players[0].cardp 以后要加
	players[6].country='s';
	players[6].de=de6;
	players[6].maxhealthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\maxhealth.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	strcpy(players[6].name,"张飞");

	for(int i=0;i<4;i++) players[7].weaponflag[i]=0;//没有装备
	players[7].killrange=1;
	players[7].defenserange=0;
	players[7].qrange=1;
	players[7].dead=0;
	players[7].realcard.hcard=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\zhaoyun0.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[7].cardp=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\zhaoyun1.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[7].bighealthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\bighealth.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[7].cardbackground=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\tablecard.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[7].cardbutton=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\zhaoyun.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[7].cardnumber=-1;
	players[7].health=4;
	players[7].healthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\health.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	players[7].maxhealth=4;
	//players[0].cardp 以后要加
	players[7].country='s';
	players[7].de=de7;
	players[7].maxhealthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\maxhealth.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	strcpy(players[7].name,"赵云");

	//为判定牌数赋初值
	for (int i=0;i<8;i++)
	{
		players[i].continuekill=0;
		players[i].judgenum=-1;
	}

	//调试使用
	int i;
	for (i=0;i<8;i++)
	{
		players[i].de=de0;
	}
	//

	//按钮初始化
	queue[0].ox=11; queue[0].oy=246;
	queue[1].ox=11; queue[1].oy=68;
	queue[2].ox=193;queue[2].oy=33;
	queue[3].ox=348;queue[3].oy=33;
	queue[4].ox=503;queue[4].oy=33;
	queue[5].ox=683;queue[5].oy=68;
	queue[6].ox=683;queue[6].oy=246;

	for (i=0;i<7;i++)//角色卡选中初始化
	{
		chara[i].click=0;
		chara[i].top=queue[i].oy-5;
		chara[i].left=queue[i].ox-7;
		chara[i].right=chara[i].left+146;
		chara[i].bottom=chara[i].top+166;
		chara[i].hcard=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\selected.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	}
	chara[7].click=0;//me
	chara[7].top=455;
	chara[7].left=843;
	chara[7].right=chara[7].left+131;
	chara[7].bottom=chara[7].top+123;
	chara[7].hcard=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\selectedme.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));

	//三个按钮初始化
	threebutton[0].cardback=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\confirm.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	threebutton[0].hcard=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\confirmshine.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	threebutton[0].click=0;
	threebutton[0].left=750;
	threebutton[0].top=458;
	threebutton[0].right=threebutton[0].left+58;
	threebutton[0].bottom=threebutton[0].top+69;

	threebutton[1].cardback=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\cancel.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	threebutton[1].hcard=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\cancelshine.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	threebutton[1].click=0;
	threebutton[1].left=749;
	threebutton[1].top=533;
	threebutton[1].right=threebutton[1].left+58;
	threebutton[1].bottom=threebutton[1].top+70;

	threebutton[2].cardback=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\giveup.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	threebutton[2].hcard=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\giveupshine.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	threebutton[2].click=0;
	threebutton[2].left=806;
	threebutton[2].top=495;
	threebutton[2].right=threebutton[2].left+35;
	threebutton[2].bottom=threebutton[2].top+75;

	for (int i=0;i<8;i++) charaavailable[i]=0;//让控件不可用
	for (int i=0;i<3;i++) threeavailable[i]=0;
	for (int i=0;i<=queue[7].cardnumber;i++) queue[7].cards[i].available=0;

	//开场动画
	fourgroup[0].hcard=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\g.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	fourgroup[1].hcard=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\c.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	fourgroup[2].hcard=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\z.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	fourgroup[3].hcard=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\j.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	for (int i=0;i<4;i++)
	{
		fourgroup[i].flash=0;
		fourgroup[i].click=0;
	}
	
	//4种LED灯
	led[0].hcard=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\gled.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	led[0].cardback=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\gback.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));

	led[1].hcard=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\cled.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	led[1].cardback=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\cback.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));

	led[2].hcard=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\jled.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	led[2].cardback=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\jback.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));

	led[3].hcard=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\zled.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	led[3].cardback=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\zback.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));

}



//各阶段函数
void setupcards()//建立卡牌
{
	refuse.cardback=(HBITMAP)(LoadImage(NULL, _T("pic\\card\\back.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	refuse.name='0';

	_finddata_t file;
	long IF;
	string str,dir="pic\\card\\cards\\";
	string weapondir="pic\\card\\weapons\\";

	IF=_findfirst("pic\\card\\cards\\*.*",&file);  //.
	_findnext(IF,&file);	// ..
	for (int i=0;i<104;i++)//104
	{
		if (_findnext(IF,&file)!=0)
		{
			break;
		}
		if(file.attrib ==_A_HIDDEN) continue;

		//分析文件名
		str=file.name;
		string::size_type l,j,k;
		l=str.find_first_of('_');
		j=str.find_last_of('_');
		k=str.find_first_of('.');
		string name=str.substr(0,l);
		string number=str.substr(l+1,j-l-1);
		string huase=str.substr(j+1,k-j-1);

		//读入图片
		readycards[i].hcard=(HBITMAP)(LoadImageA(NULL,(dir+str).data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));

		//name
		if (name=="attack") {readycards[i].name='s'; strcpy(readycards[i].allname,"杀");}
		else if(name=="shun") {readycards[i].name='3';strcpy(readycards[i].allname,"闪");}
		else if(name=="shandian"){ readycards[i].name='!'; strcpy(readycards[i].allname,"闪电");}
		else if(name=="guohe") {readycards[i].name='c';strcpy(readycards[i].allname,"过河拆桥");}
		else if(name=="juedou") {readycards[i].name='d';strcpy(readycards[i].allname,"决斗");}
		else if(name=="wugu"){ readycards[i].name='g';strcpy(readycards[i].allname,"五谷丰登");}
		else if(name=="wanjian"){ readycards[i].name='j';strcpy(readycards[i].allname,"万箭齐发");}
		else if(name=="lebu") {readycards[i].name='l';strcpy(readycards[i].allname,"乐不思蜀");}
		else if(name=="nanman"){ readycards[i].name='m';strcpy(readycards[i].allname,"南蛮入侵");}
		else if(name=="shunshou"){ readycards[i].name='q';strcpy(readycards[i].allname,"顺手牵羊");}
		else if(name=="jiedao") {readycards[i].name='r';strcpy(readycards[i].allname,"借刀杀人");}
		else if(name=="peach"){ readycards[i].name='t';strcpy(readycards[i].allname,"桃");}
		else if(name=="wuxie"){ readycards[i].name='x';strcpy(readycards[i].allname,"无懈可击");}
		else if(name=="taoyuan"){ readycards[i].name='y';strcpy(readycards[i].allname,"桃园结义");}
		else if(name=="wuzhong") {readycards[i].name='z';strcpy(readycards[i].allname,"无中生有");}

		else if(name=="bagua"){
			readycards[i].name='7';  //防具 八卦阵
			strcpy(readycards[i].allname,"八卦阵");
			if(huase=="heitao"){
				readycards[i].weaponbigpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"1_1.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
				readycards[i].weaponsmallpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"1_2.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
			}
			else{
				readycards[i].weaponbigpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"2_1.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
				readycards[i].weaponsmallpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"2_2.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
			}
		}
		else if((name=="dilu")||(name=="jueying")||(name=="zhuahuang")){
			readycards[i].name='8';  //+1马
			strcpy(readycards[i].allname,"+1马");
			readycards[i].weaponbigpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"_1.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
			readycards[i].weaponsmallpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"_2.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
		}
		else if((name=="dawan")||(name=="chitu")||(name=="zixin")){
			strcpy(readycards[i].allname,"-1马");
			readycards[i].name='9';  //-1马
			readycards[i].weaponbigpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"_1.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
			readycards[i].weaponsmallpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"_2.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
		}
		else if(name=="zhuge"){
			readycards[i].name='6';  //武器 诸葛连弩
			strcpy(readycards[i].allname,"诸葛连弩");
			if(huase=="meihua"){
				readycards[i].weaponbigpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"1_1.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
				readycards[i].weaponsmallpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"1_2.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
				readycards[i].weaponrange=1;
			}
			else{
				readycards[i].weaponbigpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"2_1.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
				readycards[i].weaponsmallpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"2_2.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
				readycards[i].weaponrange=1;
			}
		}
		else if(name=="zhangba"){
			strcpy(readycards[i].allname,"丈八蛇矛");
			readycards[i].name='6';  //武器 丈八蛇矛
			readycards[i].weaponbigpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"_1.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
			readycards[i].weaponsmallpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"_2.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
			readycards[i].weaponrange=3;
		}
		else if(name=="qinglong"){
			strcpy(readycards[i].allname,"青龙偃月刀");
			readycards[i].name='6';  //武器 青龙偃月刀
			readycards[i].weaponbigpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"_1.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
			readycards[i].weaponsmallpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"_2.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
			readycards[i].weaponrange=3;
		}
		else if(name=="qinggang"){
			strcpy(readycards[i].allname,"青釭剑");
			readycards[i].name='6';  //武器 青釭剑
			readycards[i].weaponbigpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"_1.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
			readycards[i].weaponsmallpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"_2.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
			readycards[i].weaponrange=2;
		}
		else if(name=="qilin"){
			strcpy(readycards[i].allname,"麒麟弓");
			readycards[i].name='6';  //武器 麒麟弓
			readycards[i].weaponbigpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"_1.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
			readycards[i].weaponsmallpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"_2.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
			readycards[i].weaponrange=5;
		}
		else if(name=="guanshi"){
			strcpy(readycards[i].allname,"贯石斧");
			readycards[i].name='6';  //武器 贯石斧
			readycards[i].weaponbigpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"_1.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
			readycards[i].weaponsmallpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"_2.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
			readycards[i].weaponrange=3;
		}
		else if(name=="fangtian"){
			strcpy(readycards[i].allname,"方天画戟");
			readycards[i].name='6';  //武器 方天画戟
			readycards[i].weaponbigpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"_1.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
			readycards[i].weaponsmallpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"_2.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
			readycards[i].weaponrange=4;
		}
		else if(name=="cixiong"){
			strcpy(readycards[i].allname,"雌雄双股剑");
			readycards[i].name='6';  //武器 雌雄双股剑
			readycards[i].weaponbigpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"_1.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
			readycards[i].weaponsmallpic=(HBITMAP)(LoadImageA(NULL,(weapondir+name+"_2.bmp").data(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
			readycards[i].weaponrange=2;
		}

		//……

		//color
		if(huase=="heitao") readycards[i].color=0;
		else if(huase=="meihua") readycards[i].color=2;
		else if(huase=="hongtao") readycards[i].color=1;
		else if(huase=="fangkuai") readycards[i].color=3;
		else {i--;continue;}

		//number
		if(number == "J")  readycards[i].number=11;
		else if(number == "Q") readycards[i].number=12;
		else if(number == "K") readycards[i].number=13;
		else if(number == "A") readycards[i].number=1;
		else if(number == "X") readycards[i].number=10;
		else{
			readycards[i].number = std::atoi(number.c_str());
		}

		//准备下一个
		readcardsflag++;
	}

	reorder();//洗一下牌

	return;
}

void beforegame()//游戏开始分配身份和位置
{
	char group;
	while(1)//选角色阶段
	{
		drawbegin0();
		update();
		if(fourgroup[0].click==1){group='g'; break;}
		if(fourgroup[1].click==1){group='c'; break;}
		if(fourgroup[2].click==1){group='z'; break;}
		if(fourgroup[3].click==1){group='j'; break;}
	}

	//根据group的情况 整理queue
	player AIzhugong;
	player me;
	if (group=='g')//queue从后面数三个是主公 
	{
		queue[0]=players[1];  queue[1]=players[2];  queue[2]=players[4];
		queue[3]=players[6];  queue[4]=players[7];
		reorderqueue(5);
		queue[5]=players[0];  queue[6]=players[3];  queue[7]=players[5];
		queueflag=8;

		while(1)//选将牌
		{
			int ok=0;
			drawbegin1(group);
			update();
			for(int i=0;i<=7;i++)
				if(queue[i].realcard.click==1)
				{
					me=queue[i];
					me.group='g';
					me.maxhealth++;
					me.health++;

					int count=0;//其它角色入位
					for (int j=0;j<=7;j++)
					{
						if(strcmp(players[j].name,me.name)==0) continue;
						queue[count]=players[j];
						count++;
					}
					//身份安排
					queue[0].group='j'; queue[0].de=de2;
					queue[1].group='c'; queue[1].de=de1;
					queue[2].group='c'; queue[2].de=de1;
					queue[3].group='z'; queue[3].de=de3;
					queue[4].group='z'; queue[4].de=de3;
					queue[5].group='z'; queue[5].de=de3;
					queue[6].group='z'; queue[6].de=de3;
					reorderqueue(7);
					queue[7]=me;//玩家入位
					ok=1;
					break;
				}
				toss=7;
				if(ok) break;
		}
	}
	else//知道AIzhugong 以及queue中有7个可供选择
	{
		queue[0]=players[0]; queue[1]=players[3];  queue[2]=players[5];
		reorderqueue(3);
		AIzhugong=queue[0];
		AIzhugong.group='g';
		AIzhugong.maxhealth++;
		AIzhugong.health++;
		AIzhugong.de=de0;
		int count=0;
		for (int i=0;i<8;i++)
		{
			if(strcmp(AIzhugong.name,players[i].name)==0) continue;
			queue[count]=players[i];
			count++;
		}
		reorderqueue(7);
		queueflag=7;

		while(1)//选将牌
		{
			int ok=0;
			drawbegin1(group);
			update();
			for(int i=0;i<=6;i++)
				if(queue[i].realcard.click==1)
				{
					me=queue[i];
					me.group=group;

					int count=0;//其它角色入位
					for (int j=0;j<=7;j++)
					{
						if(strcmp(players[j].name,me.name)==0) continue;
						if(strcmp(players[j].name,AIzhugong.name)==0) continue;
						queue[count]=players[j];
						count++;
					}
					queue[6]=AIzhugong;

					if (group=='j')
					{
						//queue[0].group='j';//身份安排
						queue[1].group='c'; queue[1].de=de1;
						queue[2].group='c'; queue[2].de=de1;
						queue[3].group='z'; queue[3].de=de3;
						queue[4].group='z'; queue[4].de=de3;
						queue[5].group='z'; queue[5].de=de3;
						queue[0].group='z'; queue[0].de=de3;
						reorderqueue(7);
					}

					if (group=='c')
					{
						//queue[0].group='j';//身份安排
						queue[1].group='c'; queue[1].de=de1;
						queue[2].group='j'; queue[2].de=de2;
						queue[3].group='z'; queue[3].de=de3;
						queue[4].group='z'; queue[4].de=de3;
						queue[5].group='z'; queue[5].de=de3;
						queue[0].group='z'; queue[0].de=de3;
						reorderqueue(7);
					}

					if (group=='z')
					{
						//queue[0].group='j';//身份安排
						queue[1].group='c'; queue[1].de=de1;
						queue[2].group='j'; queue[2].de=de2;
						queue[3].group='c'; queue[3].de=de1;
						queue[4].group='z'; queue[4].de=de3;
						queue[5].group='z'; queue[5].de=de3;
						queue[0].group='z'; queue[0].de=de3;
						reorderqueue(7);
					}

					queue[7]=me;//玩家入位
					ok=1;
					break;
				}
				for (int i=0;i<=7;i++)
				{
					if(strcmp(queue[i].name,AIzhugong.name)==0) 
					{
						toss=i;
						break;
					}
				}
				if(ok) break;
		}
	}

	//读入 脸谱(身份) 图片
	if(queue[7].group == 'g')  queue[7].groupface = (HBITMAP)(LoadImage(NULL, _T("pic\\button\\flagZG.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	else if(queue[7].group == 'c')  queue[7].groupface = (HBITMAP)(LoadImage(NULL, _T("pic\\button\\flagZC.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	else if(queue[7].group == 'j')  queue[7].groupface = (HBITMAP)(LoadImage(NULL, _T("pic\\button\\flagNJ.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	else if(queue[7].group == 'z')  queue[7].groupface = (HBITMAP)(LoadImage(NULL, _T("pic\\button\\flagFZ.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));

	if(queue[7].country == 'W') queue[7].countryface = (HBITMAP)(LoadImage(NULL, _T("pic\\button\\flagWEI.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	else if(queue[7].country == 's') queue[7].countryface = (HBITMAP)(LoadImage(NULL, _T("pic\\button\\flagSHU.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
	else if(queue[7].country == 'w') queue[7].countryface = (HBITMAP)(LoadImage(NULL, _T("pic\\button\\flagWU.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));

	queueflag=8;//当前队列中的玩家数量
	queue[7].de=gamemain;//玩家的决策函数
}

card GetJudgeCard()
{
	if(readcardsflag==-1) reorder();
	card judgecard=readycards[readcardsflag];
	readcardsflag--;
	if (readcardsflag==-1) reorder();
	tablecardsflag++;
	tablecards[tablecardsflag]=judgecard;
	drawdata();
	update();
	for (int i=0;i<=3*Time;i++)
	{
		Sleep(3);
	}
	return judgecard;
}

int JudgeProc()
{
	int chupai=1;
	//判定阶段
	card judgecard;
	card temp;
	for (int i=queue[toss].judgenum; i>=0; i--)
	{
		switch(queue[toss].judgecards[i].name)
		{
		case 'l':
			if(for_X(toss,toss)==1)//被无懈可击了
			{
				moveCardtoDeath();
				card temp=queue[toss].judgecards[i];
				queue[toss].judgenum--;
				moveCardtoTable(temp,toss);
				continue;
			}
			moveCardtoDeath();
			judgecard=GetJudgeCard();
			who=toss;
			if (judgecard.color!=1)
			{
				chupai=0;//若不为红桃 则跳过
				lbss=1;
				drawdata();update();
				for (int i=0;i<Time*7;i++) Sleep(2);
				lbss=0;
			}else
			{
				lbss=2;
				drawdata();update();
				for (int i=0;i<Time*7;i++) Sleep(2);
				lbss=0;
			}

			temp=queue[toss].judgecards[i];
			queue[toss].judgenum--;
			moveCardtoTable(temp,toss);
			break;
		case '!':
			if(for_X(toss,toss)==1)//被无懈可击了
			{
				moveCardtoDeath();
				card temp=queue[toss].judgecards[i];
				queue[toss].judgenum--;
				int next=toss-1;
				if(next==-1) next=7;
				while(1)
				{
					if (queue[next].dead==1) next--;
					if(next<0) next+=8;
					if(queue[next].dead==0 && next>=0) break;
				}
				for (int i=0;i<=queue[next].judgenum;i++)//下家有闪电跳过
					if(queue[next].judgecards[i].name=='!')
					{
						next--;
						if(next==-1) next=7;
					}
				queue[next].judgenum++;
				queue[next].judgecards[queue[next].judgenum]=temp;
				continue;
			}
			moveCardtoDeath();
			judgecard=GetJudgeCard();
			if(judgecard.color==0 && judgecard.number>=2 &&judgecard.number<=9)//中闪电了
			{
				mciSendString(_T("Play sound//中闪电了.wav"),NULL,0,NULL);//播放声音
				who=toss;
				sd=1;
				drawdata();update();
				for (int i=0;i<Time*7;i++) Sleep(2);
				sd=0;

				if(queue[toss].health>0)minusHealthAnimation(toss);
				else queue[toss].health--;
				if(queue[toss].health>0)minusHealthAnimation(toss);
				else queue[toss].health--;
				if(queue[toss].health>0)minusHealthAnimation(toss);
				else queue[toss].health--;
				card temp=queue[toss].judgecards[queue[toss].judgenum];
				queue[toss].judgenum--;
				moveCardtoTable(temp,toss);

				if(queue[toss].health<=0)Peach_for_Death(toss,toss,0-queue[toss].health+1,-1);
			}else//没中闪电
			{
				who=toss;
				sd=2;
				drawdata();update();
				for (int i=0;i<Time*7;i++) Sleep(2);
				sd=0;

				card temp=queue[toss].judgecards[i];
				queue[toss].judgenum--;
				int next=toss-1;
				if(next==-1) next=7;
				while(1)
				{
					if (queue[next].dead==1) next--;
					if(next<0) next+=8;
					if(queue[next].dead==0 && next>=0) break;
				}
				for (int i=0;i<=queue[next].judgenum;i++)//下家有闪电跳过
					if(queue[next].judgecards[i].name=='!')
					{
						next--;
						if(next==-1) next=7;
					}
				queue[next].judgenum++;
				queue[next].judgecards[queue[next].judgenum]=temp;
			}
			break;
		}
	}
	
	moveCardtoDeath();
	return chupai;
}

void beforeDe(int toss)
{
	queue[toss].continuekill=0;//可以出一张杀
}

void readyforplayerAnimation(int toss, int xtoget)//摸牌动画 谁摸牌  摸几张
{
	drawdata();
	update();
	for (int i=0;i<Time;i++)
	{
		Sleep(3);
		drawdata();
		update();
	}

	int dstx,dsty,srcx,srcy;
	srcx=365,srcy=235;//屏幕中央
	if(toss==7){dstx=queue[toss].ox; dsty=queue[toss].oy;}//目标位置
	else{dstx=queue[toss].ox+10; dsty=queue[toss].oy+10;}
	int xnow,ynow;//每帧中牌的位置

	int i,j;
	for (i=0;i<=3*Time;i++)
	{
		xnow=(dstx-srcx)*1.0*i/3/Time+srcx;
		ynow=(dsty-srcy)*1.0*i/3/Time+srcy;
		drawdata();
		for (j=0;j<xtoget;j++) drawtodcMem(refuse.cardback,xnow+j*15,ynow);
		update();
		Sleep(1);
	}
	for (int k=0;k<Time;k++)
	{
		Sleep(1);
		drawdata();
		for (j=0;j<xtoget;j++) drawtodcMem(refuse.cardback,xnow+j*15,ynow);
		update();
	}
}

void readyforplayer(int toss,int xtoget)//摸牌阶段
{
	readyforplayerAnimation(toss,xtoget);//摸牌动画

	//玩家摸牌
	int i;
	for (i=0;i<xtoget;i++)
	{
		if (readcardsflag<=-1) reorder();
		queue[toss].cardnumber++;
		queue[toss].cards[queue[toss].cardnumber]=readycards[readcardsflag];
		queue[toss].cards[queue[toss].cardnumber].click=0;
		readcardsflag--;
		if (readcardsflag<=-1) reorder();
	}

	drawdata();
	update();
	for (int i=0;i<Time;i++)
	{
		Sleep(1);
		drawdata();
		update();
	}
}

void discards(int toss)//弃牌阶段
{
	card temp;
	while (1)
	{
		if (queue[toss].cardnumber+1<=queue[toss].health) break;
		qiuqipainum=queue[toss].cardnumber+1-queue[toss].health;
		temp=queue[toss].de(2,refuse,toss);
		for (int i=0;i<=queue[toss].cardnumber;i++)
		{
			if (queue[toss].cards[i].click==1)
			{
				queue[toss].cards[i].click=0;
				if (queue[toss].cardnumber+1<=queue[toss].health) break;
				temp=queue[toss].cards[i];
				SearchCard(toss,temp,1);
				drawdata();
				update();
				moveCardtoTable(temp,toss);
				i=-1;
			}
		}
		if(temp.name=='0') continue;
		drawdata();
		update();
		for (int i=0;i<2*Time;i++)
		{
			Sleep(1);
			drawdata();
			update();
		}
	}
	for (int i=0;i<=queue[toss].cardnumber;i++) queue[toss].cards[i].click=0;
	moveCardtoDeath();
}

void reorderqueue(int playersnum)//洗角色
{
	//生成随机数
	srand((unsigned)time(NULL));
	//进行牌混洗
	random_shuffle(queue,queue+playersnum);
}
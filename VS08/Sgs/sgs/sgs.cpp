// sgs.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "sgs.h"
#include <MMSystem.h>//声音使用的文件
#pragma  comment(lib, "Winmm.lib")//声音使用的文件

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);

// 我的头文件

//自带头文件
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <algorithm>
#include <math.h>
using std::random_shuffle;
#include <io.h>
#include <iostream>
#include <string>
using namespace std;
#include <atlimage.h>
//
/************************************************************************/
/* 我的头文件                                                           */
/************************************************************************/
//类头文件
#include "card.h"//卡片
#include "character.h"//角色
//

//全局变量头文件
#include "globalVariable.h"
//

//函数声明头文件
#include "de.h"//全局变量和一些决策都在里面
#include "initialchara.h"//角色初始化
#include "procedure.h"//各阶段函数
#include "cardfunction.h"//卡牌函数
#include "draw.h"//绘图函数
#include "gamemain.h"//交互头文件
//
//函数声明cpp
#include "zcardfunction.h"
#include "zde.h"
#include "zdraw.h"
#include "zinitialchara.h"
#include "zprocedure.h"
#include "zgamemain.h"
//

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_SGS, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}
	
	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_SGS));

	
	/*************part1 双缓冲结构搭建******************/
	hdc = GetDC(hWnd);
	RECT rectWnd;//用于双缓冲
	GetClientRect(hWnd, &rectWnd);
	dcMem=CreateCompatibleDC(hdc);
	bmpMem=CreateCompatibleBitmap(hdc, rectWnd.right, rectWnd.bottom);
	SelectObject(dcMem, bmpMem);
	dcBmp=CreateCompatibleDC(dcMem);
	/****************part1 end***************************/
	//前面有几个全局变量  以后使用dcMem绘画



	/****************消息循环***************************/
	hThread  = CreateThread(NULL,0,(LPTHREAD_START_ROUTINE) ThreadFunction,NULL,0,&ThreadID);//创建游戏线程
	PlaySound(_T("sound//background.wav"),NULL,SND_ASYNC|SND_LOOP);

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	/****************消息循环结束***************************/
	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style			= CS_HREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_SGS));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	//wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_SGS);
	wcex.lpszMenuName	=0;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
 //HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   //
   SetWindowLong(hWnd,GWL_STYLE, WS_DLGFRAME &~WS_CAPTION);
   SetWindowPos(hWnd,NULL,120,80,1007,608,SWP_SHOWWINDOW);
   //

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	////我加的变量
	int px,py;
	////
	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_LBUTTONDOWN:		//鼠标左击，记录
		mouseX=LOWORD(lParam);
		mouseY=HIWORD(lParam);
		clickdown();
		// TODO: Add any drawing code here...
		break;
	case WM_LBUTTONUP:
		mouseX=LOWORD(lParam);
		mouseY=HIWORD(lParam);
		//clickup();
	case WM_MOUSEMOVE:			//鼠标移动，记录位置
		px=LOWORD(lParam);
		py=HIWORD(lParam);
		moveflash(px,py);
		break;
	case WM_KEYDOWN:
		if(wParam==_T('D'))
		{
			if(keyd==0) keyd=1;
			else keyd=0;
		}
		if (wParam==VK_RIGHT)
		{
			if(Time>10) Time-=2;
		}
		if (wParam==VK_LEFT)
		{
			if(210-Time>0) Time+=2;
		}
		break;
	case WM_PAINT:	
		hdc = BeginPaint(hWnd, &ps);
		//在这里可以画图
		{
			//Rectangle(hdc,10,20,70,40);
		}
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

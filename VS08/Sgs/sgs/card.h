#ifndef CLASS_H
#define CLASS_H

class card{
public:
	int number;
	int color;
	char name;//装备分为四种 武器6 防具7 +1马8 -1马9
	char allname[20];//方天之类的名字来区别
	HBITMAP hcard;
	HBITMAP cardback;
	int src;
	int dst;

	//武器装备
	int weaponrange;
	HBITMAP weaponbigpic;
	HBITMAP weaponsmallpic;

	int flash;//移动标记
	int click;//0未选中 1选中
	int left,top,bottom,right;//每个控件的坐标范围
	int available;//点击允许
};

class Info{
public:
	card c;
	int src;
	int dstnum;
	int dst[8];
};

#endif


HDC dcMem;//用于双缓冲
HBITMAP bmpMem;//用于双缓冲
HWND hWnd;//窗口句柄
HDC dcBmp;//用于双缓冲
HDC hdc;//窗口dc
HBITMAP table=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\background.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));//面板
HBITMAP overtable=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\background.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));//面板
HBITMAP bombsmall=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\Bombsmall.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));//爆炸效果
HBITMAP bombbig=(HBITMAP)(LoadImage(NULL, _T("pic\\playerbutton\\Bombbig.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));//爆炸效果
//判定标记的图片：
HBITMAP flagL=(HBITMAP)(LoadImage(NULL, _T("pic\\card\\flagL.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));//乐的标记
HBITMAP flagSD=(HBITMAP)(LoadImage(NULL, _T("pic\\card\\flagSD.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));//闪电的标记
HBITMAP healthcolor=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\health.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
int overclick=0;
int Time=60;//游戏速度

card led[4];//四种LED灯
int gameover=0;
HBITMAP zhonchen=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\zhongchen.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));//乐的标记
HBITMAP neijian=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\neijian.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));//乐的标记
HBITMAP fanzei=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\fanzei.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));//乐的标记


//开场选将
card fourgroup[4];
HBITMAP smallboard=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\smallboard.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
HBITMAP bigboard=(HBITMAP)(LoadImage(NULL, _T("pic\\button\\bigboard.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));

//字体设置
HFONT h1=CreateFont(16,0,0,0,FW_BLACK,false,false,false,
					GB2312_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,
					DEFAULT_QUALITY,FIXED_PITCH,_T("宋体"));
HFONT hw=CreateFont(12,0,0,0,FW_BLACK,false,false,false,
					GB2312_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,
					DEFAULT_QUALITY,FIXED_PITCH,_T("宋体"));


card readycards[108]; int readcardsflag=-1;//可用牌
card wastecards[108]; int wastecardsflag=-1;//废牌
card tablecards[108]; int tablecardsflag=-1;//桌面弃牌
card wugufengdeng[8]; int wugufengdengflag=-1;//五谷丰登
Info infobox[90000];//消息框
int infoboxnum=-1;//消息当前下标
card refuse;

player players[8];//分配25名角色
player queue[9];//游戏中的8个人
int queueflag;
int toss=7;//令牌 其实就是queue的下标 将来toss的初始化应该给主公

//交互使用
int mouseX=0;
int mouseY=0;
//每个角色卡用一个card代替 每张牌用一个card代替 没个按钮用一个card代替  0表示未选中 1表示选中
card chara[8];//角色卡控件
card threebutton[3];//三个按钮控件
int charaavailable[8];//八个角色的按钮闸门
int threeavailable[3];//三个按钮的闸门

//全局变量
HANDLE hThread;  //线程的句柄
DWORD ThreadID;  //线程的ID


//提示语言开关
int dangqian=0;
int wuxie=0; int wuxiesrc=-1;
int qiutao=0; int qiutaosrc=-1; int howmanytao=0;
int qiusha=0; int qiushasrc=-1;
int	qiushan=0; int qiushansrc=-1;
int qiuqipai=0;int qiuqipainum=0;
int bagua=0;
int wugu=0;
int keyd=0;

//五谷丰登 顺拆面板开关
int wgfdctrl=0;//面板控制
int wgfdclickctrl=0;//点击控制
int scctrl=0;//顺拆面板开关
int scdst;//顺拆目标编号

int who=-1;
int lbss=0;//乐不思蜀关闭0 生效1 失效2
int sd=0;//闪电关闭0 生效1 失效2
int bg=0;//八卦关闭0 生效1 失效2


//游戏结束画面
HBITMAP endpic=(HBITMAP)(LoadImage(NULL, _T("pic\\over\\endpic.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
HBITMAP cend=(HBITMAP)(LoadImage(NULL, _T("pic\\over\\cend.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
HBITMAP gend=(HBITMAP)(LoadImage(NULL, _T("pic\\over\\gend.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
HBITMAP jend=(HBITMAP)(LoadImage(NULL, _T("pic\\over\\jend.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
HBITMAP zend=(HBITMAP)(LoadImage(NULL, _T("pic\\over\\zend.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
HBITMAP fail=(HBITMAP)(LoadImage(NULL, _T("pic\\over\\fail.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
HBITMAP victory=(HBITMAP)(LoadImage(NULL, _T("pic\\over\\victory.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
HBITMAP playerend=(HBITMAP)(LoadImage(NULL, _T("pic\\over\\player.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));
HFONT hend=CreateFont(14,0,0,0,FW_ULTRALIGHT,false,false,false,
					GB2312_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,
					DEFAULT_QUALITY,FIXED_PITCH,_T("宋体"));
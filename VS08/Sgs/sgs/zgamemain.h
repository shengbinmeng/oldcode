card gamemain(int state,card c,int toss)//管理员维护
{
	int i,j;
	drawdata();
	update();
	int charamark=0; 
	int charai[2]={0,0};
	int cardmark=-1;
	for(int i=0;i<3;i++) threebutton[i].click=0;

	while(1)
	{
		/**********///点击辅助效果
		if (state!=2)//不是弃牌过程 只能有一张牌被点
		{
			for (int i=0;i<=queue[toss].cardnumber;i++)
			{
				if (cardmark==-1 && queue[toss].cards[i].click==1)//未被点过
				{
					cardmark=i;
					break;
				}else if (cardmark!=-1 && queue[toss].cards[i].click==1 && i!=cardmark)
				{
					queue[toss].cards[cardmark].click=0;
					cardmark=i;
					break;
				}
			}	
		}
		/**********/
		for (int i=0;i<=queue[7].cardnumber;i++) queue[7].cards[i].available=1;//允许点击
		for (int i=0;i<3;i++) threeavailable[i]=1;
		for (int i=0;i<8;i++) charaavailable[i]=1;
		if (state>0 && state!=2)//求的过程
		{
			for (int i=0;i<8;i++) charaavailable[i]=0;
			threeavailable[2]=0;
			if (threebutton[1].click==1) threebutton[2].click=1;
		}

		if(state==0)//当前回合
		{
			dangqian=1;//打开提示开关
			//charaavailable[0]=1;charaavailable[6]=1;//能打到的人
			if(charamark<=1)
			for (int i=0;i<=7;i++)//判断是否点击目标
			{
				if(chara[i].click==1)
				{
					if(charamark==0)
					{
						charai[0]=i;
						charamark++;
						break;
					}else if(i!=charai[0])
					{
						charai[1]=i;
						charamark++;
						break;
					}
				}
			}

			if(threebutton[0].click==1)//如果点击确定了
			{
				threebutton[0].click=0;//先消去确定的标记

				/************************///判断是否杀人
				bool cardmark=0; int cardi;//牌的编号
				for (int i=0;i<=queue[7].cardnumber;i++)//判断是否选杀牌了
				{
					if(queue[7].cards[i].click==1)
					{
						cardmark=1;
						cardi=i;
						break;
					}
				}
				if(cardmark && charamark) 
				{
					dangqian=0;
					if(queue[7].cards[cardi].name == 's'){
						kill(7,charai[0],queue[7].cards[cardi]);
					}
					else if(queue[7].cards[cardi].name == 'q'){
						for_Q(7,queue[7].cards[cardi],charai[0],0,0);  //顺手牵羊
					}
					else if(queue[7].cards[cardi].name == 'c'){
						for_C(7,queue[7].cards[cardi],charai[0],0,0);  //过河拆桥
					}
					else if(queue[7].cards[cardi].name == 'd'){
						for_D(7,charai[0],queue[7].cards[cardi]);  //决斗
					}
					else if(queue[7].cards[cardi].name == 'l'){
						queue[7].cards[cardi].click=0;
						for_L(7,charai[0],queue[7].cards[cardi]);  //乐不思蜀
					}else if (queue[7].cards[cardi].name == 'r'	&& charamark==2)
					{
						queue[7].cards[cardi].click=0;
						for_R(7,charai[0],charai[1],queue[7].cards[cardi]);
					}

					threebutton[1].click=1;
				}
				/************************///判断是否出借刀杀				


				/************************///判断是否出桃
				for (int i=0;i<=queue[7].cardnumber;i++)//判断是否选牌了
				{
					if(queue[7].cards[i].click==1)
					{
						dangqian=0;
						Peach_for_1(7,queue[7].cards[i]);
						threebutton[1].click=1;
						break;
					}
				}

				/************************///判断是否无中生有
				for (int i=0;i<=queue[7].cardnumber;i++)//判断是否选牌了
				{
					if(queue[7].cards[i].click==1)
					{
						dangqian=0;
						for_Z(toss,queue[7].cards[i]);
						threebutton[1].click=1;
						break;
					}
				}

				/************************///判断是否万箭齐发
				for (int i=0;i<=queue[7].cardnumber;i++)
				{
					if(queue[7].cards[i].click==1)
					{
						dangqian=0;
						for_J(toss,queue[7].cards[i]);
						threebutton[1].click=1;
						break;
					}
				}

				/************************///判断是否南蛮入侵
				for (int i=0;i<=queue[7].cardnumber;i++)
				{
					if(queue[7].cards[i].click==1)
					{
						dangqian=0;
						for_M(toss,queue[7].cards[i]);
						threebutton[1].click=1;
						break;
					}
				}
				
				/************************///判断是否五谷丰登
				for (int i=0;i<=queue[7].cardnumber;i++)
				{
					if(queue[7].cards[i].click==1)
					{
						dangqian=0;
						for_G(toss,queue[7].cards[i]);
						threebutton[1].click=1;
						break;
					}
				}

				/************************///判断是否桃园结义
				for (int i=0;i<=queue[7].cardnumber;i++)
				{
					if(queue[7].cards[i].click==1)
					{
						dangqian=0;
						for_Y(toss,queue[7].cards[i]);
						threebutton[1].click=1;
						break;
					}
				}

				/************************///判断装备武器
				for (int i=0;i<=queue[7].cardnumber;i++)
				{
					if(queue[7].cards[i].click==1)
					{
						dangqian=0;
						arm(toss,queue[7].cards[i]);  //装备武器函数
						threebutton[1].click=1;
						break;
					}
				}

				/************************///判断是闪电
				for (int i=0;i<=queue[7].cardnumber;i++)
				{
					if(queue[7].cards[i].click==1)
					{
						dangqian=0;
						for_SD(7,queue[7].cards[i]);  //闪电
						threebutton[1].click=1;
						break;
					}
				}

			}
		}
		
		if(state==1)//求闪模式
		{
			qiushan=1;//打开开关
			qiushansrc=c.src;//谁求闪
			if(threebutton[0].click==1)//如果点击确定了
			{
				threebutton[0].click=0;//先消去确定的标记
				for (int i=0;i<=queue[7].cardnumber;i++)
				{
					qiushan=0;//关闭开关
					if(queue[7].cards[i].click==1)
					{
						queue[7].cards[i].click=0;
						for (int i=0;i<3;i++) threeavailable[i]=0;
						for (int i=0;i<8;i++)chara[i].click=0;
						return queue[7].cards[i];
					}
				}
			}
		}

		if(state==2)//弃牌模式
		{
			qiuqipai=1;
			if(threebutton[0].click==1)//如果点击确定了
			{
				threebutton[0].click=0;//先消去确定的标记
				qiuqipai=0;

				for (int i=0;i<3;i++) threeavailable[i]=0;
				for (int i=0;i<8;i++)chara[i].click=0;
				return refuse;
			}
		}

		if(state==3)//求桃模式
		{
			qiutao=1;
			qiutaosrc=c.dst;
			if(threebutton[0].click==1)//如果点击确定了
			{
				threebutton[0].click=0;//先消去确定的标记
				for (int i=0;i<=queue[7].cardnumber;i++)
				{
					qiutao=0;
					if(queue[7].cards[i].click==1) 
					{
						queue[7].cards[i].click=0;
						for (int i=0;i<3;i++) threeavailable[i]=0;
						for (int i=0;i<8;i++)chara[i].click=0;
						return queue[7].cards[i];
					}
				}
			}
		}

		if(state==4)//求无懈可击
		{
			wuxie=1;
			wuxiesrc=c.src;
			if(threebutton[0].click==1)//如果点击确定了
			{
				threebutton[0].click=0;//先消去确定的标记
				for (int i=0;i<=queue[7].cardnumber;i++)
				{
					wuxie=0;
					if(queue[7].cards[i].click==1) 
					{
						queue[7].cards[i].click=0;
						for (int i=0;i<3;i++) threeavailable[i]=0;
						for (int i=0;i<8;i++)chara[i].click=0;
						return queue[7].cards[i];
					}
				}
			}
		}

		if(state==5)//求杀
		{
			qiusha=1;
			qiushasrc=c.src;
			if(threebutton[0].click==1)//如果点击确定了
			{
				threebutton[0].click=0;//先消去确定的标记
				for (int i=0;i<=queue[7].cardnumber;i++)
				{
					qiusha=0;
					if(queue[7].cards[i].click==1) 
					{
						queue[7].cards[i].click=0;
						for (int i=0;i<3;i++) threeavailable[i]=0;
						for (int i=0;i<8;i++)chara[i].click=0;
						return queue[7].cards[i];
					}
				}
			}
		}

		if(state==6)//求五谷
		{
			wugu=1;
			wgfdclickctrl=1;
			for (int i=0;i<=wugufengdengflag;i++)
			{
				if(wugufengdeng[i].click==1)
				{
					wgfdclickctrl=0;
					wugu=0;
					wugufengdeng[i].click=0;
					for (int i=0;i<3;i++) threeavailable[i]=0;
					for (int i=0;i<8;i++)chara[i].click=0;
					return wugufengdeng[i];
				}
			}
		}

		if(state==8)//是否判定八卦阵
		{
			bagua=1;
			if(threebutton[0].click==1)//如果点击确定了
			{
				bagua=0;
				threebutton[0].click=0;//先消去确定的标记
				{
					for (int i=0;i<3;i++) threeavailable[i]=0;
					for (int i=0;i<8;i++)chara[i].click=0;
					return queue[7].weapon[1];
				}
			}
		}

		/**************按钮的一些操作************/
		if (threebutton[1].click==1)//点击取消后 取消选牌
		{
			for (i=0;i<=queue[7].cardnumber;i++)
			{
				queue[7].cards[i].click=0;
			}
			for (i=0;i<8;i++)chara[i].click=0;
			threebutton[1].click=0;
			charamark=0;
			cardmark=-1;
		}

		if (threebutton[2].click==1)//点击弃牌后 消去所有标记 并退出循环
		{
			bagua=0;
			dangqian=0;
			wuxie=0;
			qiusha=0;
			qiutao=0;
			qiushan=0;
			qiuqipai=0;
			wgfdclickctrl=0;
			wugu=0;
			charamark=0;
			
			threebutton[2].click=0;
			for (i=0;i<=queue[7].cardnumber;i++)
			{
				queue[7].cards[i].click=0;
			}
			for (i=0;i<8;i++)
			{
				chara[i].click=0;
			}
			break;
		}

		drawdata();
		update();
	}

	for (int i=0;i<8;i++) charaavailable[i]=0;//让控件不可用
	for (int i=0;i<3;i++) threeavailable[i]=0;
	for (int i=0;i<=queue[7].cardnumber;i++) queue[7].cards[i].available=0;

	return refuse;
}

void moveflash(int px,int py)
{
	int i;
	for (i=0;i<4;i++)//分配角色
	{
		if (px>fourgroup[i].left+7 && px<fourgroup[i].right-7 && 
			py>fourgroup[i].top+5  && py<fourgroup[i].bottom-5)
		{
			fourgroup[i].flash=1;
		}
		else fourgroup[i].flash=0;
	}

	for (i=0;i<queueflag;i++)//分配角色
	{
		if (px>queue[i].realcard.left+7 && px<queue[i].realcard.right-7 && 
			py>queue[i].realcard.top+5  && py<queue[i].realcard.bottom-5)
		{
			queue[i].realcard.flash=1;
		}
		else queue[i].realcard.flash=0;
	}

	if(scctrl==1 || wgfdctrl==1) for(int i=0;i<=7;i++) charaavailable[i]=0;
	for (i=0;i<8;i++)//角色卡
	{
		if (charaavailable[i]==0) continue;
		if (px>chara[i].left+7 && px<chara[i].right-7 && 
			py>chara[i].top+5  && py<chara[i].bottom-5)
		{
			chara[i].flash=1;
		}
		else chara[i].flash=0;
	}

	for (i=0;i<3;i++)
	{
		if(threeavailable[i]==0) continue;
		if (px>threebutton[i].left+10 && px<threebutton[i].right-10 && 
			py>threebutton[i].top+10  && py<threebutton[i].bottom-10)
		{
			threebutton[i].flash=1;
		}
		else threebutton[i].flash=0;
	}

	for (i=0;i<=queue[7].cardnumber;i++)
	{
		card temp=queue[7].cards[i];
		if (px>temp.left && px<temp.right && 
			py>temp.top  && py<temp.bottom)
		{
			queue[7].cards[i].flash=1;
		}
		else queue[7].cards[i].flash=0;
	}

	if (scctrl)//顺拆面板
	{
		for (i=0;i<=queue[scdst].cardnumber;i++)
		{
			card temp=queue[scdst].cards[i];
			if (px>temp.left && px<temp.right && 
				py>temp.top  && py<temp.bottom)
			{
				queue[scdst].cards[i].flash=1;
			}
			else queue[scdst].cards[i].flash=0;
		}
		
		for (i=0;i<=3;i++)
		{
			if(queue[scdst].weaponflag[i]==0) continue;
			card temp=queue[scdst].weapon[i];
			if (px>temp.left && px<temp.right && 
				py>temp.top  && py<temp.bottom)
			{
				queue[scdst].weapon[i].flash=1;
			}
			else queue[scdst].weapon[i].flash=0;
		}

		for (i=0;i<=queue[scdst].judgenum;i++)
		{
			card temp=queue[scdst].judgecards[i];
			if (px>temp.left && px<temp.right && 
				py>temp.top  && py<temp.bottom)
			{
				queue[scdst].judgecards[i].flash=1;
			}
			else queue[scdst].judgecards[i].flash=0;
		}
	}

	if (wgfdctrl)//五谷丰登面板
	{
		for (i=0;i<=wugufengdengflag;i++)
		{
			card temp=wugufengdeng[i];
			if (px>temp.left && px<temp.right && 
				py>temp.top  && py<temp.bottom)
			{
				wugufengdeng[i].flash=1;
			}
			else wugufengdeng[i].flash=0;
		}
	}
}

void clickdown()
{
	int i;
	int px=mouseX;
	int py=mouseY;

	//游戏结束控制
	if (overclick)
	{
		exit(0);
	}
	//

	for (i=0;i<4;i++)//分配角色
	{
		if (px>fourgroup[i].left+7 && px<fourgroup[i].right-7 && 
			py>fourgroup[i].top+5  && py<fourgroup[i].bottom-5)
		{
			fourgroup[i].click=1;
		}
		else fourgroup[i].click=0;
	}

	for (i=0;i<queueflag;i++)//分配角色
	{
		if (px>queue[i].realcard.left+7 && px<queue[i].realcard.right-7 && 
			py>queue[i].realcard.top+5  && py<queue[i].realcard.bottom-5)
		{
			queue[i].realcard.click=1;
		}
		else queue[i].realcard.click=0;
	}

	if(scctrl==1 || wgfdctrl==1) for(int i=0;i<=7;i++) charaavailable[i]=0;
	for (i=0;i<8;i++)//角色卡
	{
		if (charaavailable[i]==0) continue;
		if (px>chara[i].left+7 && px<chara[i].right-7 && 
			py>chara[i].top+5  && py<chara[i].bottom-5)
		{
			chara[i].click=1;
		}
	}

	for (i=0;i<3;i++)
	{
		if(threeavailable[i]==0) continue;
		if (px>threebutton[i].left+10 && px<threebutton[i].right-10 && 
			py>threebutton[i].top+10  && py<threebutton[i].bottom-10)
		{
			threebutton[i].click=1;
		}
	}

	for (i=0;i<=queue[7].cardnumber;i++)
	{
		if(queue[7].cards[i].available==0) continue;
		card temp=queue[7].cards[i];
		if (px>temp.left && px<temp.right && 
			py>temp.top  && py<temp.bottom)
		{
			queue[7].cards[i].click=1;
		}
	}

	if (scctrl)//顺拆面板
	{
		for (i=0;i<=queue[scdst].cardnumber;i++)
		{
			card temp=queue[scdst].cards[i];
			if (px>temp.left && px<temp.right && 
				py>temp.top  && py<temp.bottom)
			{
				queue[scdst].cards[i].click=1;
			}
			else queue[scdst].cards[i].click=0;
		}

		for (i=0;i<=3;i++)
		{
			if(queue[scdst].weaponflag[i]==0)continue;
			card temp=queue[scdst].weapon[i];
			if (px>temp.left && px<temp.right && 
				py>temp.top  && py<temp.bottom)
			{
				queue[scdst].weapon[i].click=1;
			}
			else queue[scdst].weapon[i].click=0;
		}

		for (i=0;i<=queue[scdst].judgenum;i++)
		{
			card temp=queue[scdst].judgecards[i];
			if (px>temp.left && px<temp.right && 
				py>temp.top  && py<temp.bottom)
			{
				queue[scdst].judgecards[i].click=1;
			}
			else queue[scdst].judgecards[i].click=0;
		}
	}

	if (wgfdclickctrl)//五谷丰登面板
	{
		for (i=0;i<=wugufengdengflag;i++)
		{
			card temp=wugufengdeng[i];
			if (px>temp.left && px<temp.right && 
				py>temp.top  && py<temp.bottom)
			{
				wugufengdeng[i].click=1;
			}
			else wugufengdeng[i].click=0;
		}
	}

	//结束后复位
	mouseX=0;
	mouseY=0;
}


DWORD WINAPI ThreadFunction(LPVOID lpParam)//副线程函数
{
	int i,j;
	/*****************part2 建牌 建立角色******************/
	setupcards();//建立卡牌
	initialchara();//载入武将属性	
	beforegame();//游戏开始分配身份和位置,从武将中选出8名装入queue toss给主公
	/************************part2 end**************************/
	
	/*card rrr;
	card sss;
	card www;
	card s3;
	card ll;
	for (int i=0;i<104;i++)
	{
		if(readycards[i].name=='r') rrr=readycards[i];
		if(readycards[i].name=='s') sss=readycards[i];
		if(readycards[i].name=='6') www=readycards[i];
		if(readycards[i].name=='!') s3=readycards[i];
		if(readycards[i].name=='7') ll=readycards[i];
	}
	for (int i=0;i<20;i++)	readycards[i]=sss;
	for (int i=20;i<30;i++)	readycards[i]=sss;
	for (int i=30;i<35;i++)	readycards[i]=ll;
	for (int i=35;i<50;i++)	readycards[i]=ll;
	for (int i=50;i<54;i++)	readycards[i]=ll;
	readcardsflag=53;
	reorder();*/

	/********************part3 游戏开始****************************/
	for (int i=0;i<=7;i++)//每个人发4张牌
	{
		int temp=toss-i;
		if(temp<0) temp+=8;
		readyforplayer(temp,4);
	}

	while(1)//这里要加判断函数 如果阵亡就跳过他的所有阶段
	{
		//校验总牌数
		int cardsum = 0;
		for(int i=0;i<8;i++){
			int weaponcardsum = 0;
			for(int j=0;j<4;j++){
				weaponcardsum = weaponcardsum + queue[i].weaponflag[j];
			}
			cardsum = cardsum + queue[i].cardnumber+1 + weaponcardsum + queue[i].judgenum+1;
		}
		cardsum = cardsum + readcardsflag+1 + wastecardsflag+1 + tablecardsflag+1;
		if(cardsum !=104){
			MessageBoxA(hWnd,"Card Number Error !","Debug",1);
		}
		////////////校验结束
	
		if (queue[toss].dead==1) //死者过
		{
			toss--;
			if(toss==-1) toss=7;
			continue;
		}
		int chupai=JudgeProc();//判定阶段
		if (queue[toss].dead==1) //被劈死就过
		{
			toss--;
			if(toss==-1) toss=7;
			continue;
		}
		readyforplayer(toss,2);//摸牌阶段
		beforeDe(toss);//决策之前
		if(chupai!=0) queue[toss].de(0,refuse,toss);//出牌阶段 其它阶段会调用你的1模式 你做出回应即可
		discards(toss);//弃牌阶段
		//回合结束阶段

		toss--;//循环到开始那个人
		if(toss==-1) toss=7;
	}
	/**************************part3 end**************************/
	return 0;
}
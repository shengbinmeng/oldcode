//卡牌函数,return(0)表示杀不到，return(1)表示已经出杀完毕
int kill(int src,int dst,card c)
{
	int judge = JudgeKillRange(src, dst)&&JudgeCardisRight(c,'s');//这张牌是杀 且能杀到
	if(judge == 0)return(0);
	if(src == dst)return(0);//且不是自杀
	if(queue[src].weaponflag[0]==0 && queue[src].continuekill==1) return 0;
	if(queue[src].weaponflag[0]==1)
		if(strcmp(queue[src].weapon[0].allname,"诸葛连弩")!=0 && queue[src].continuekill==1) return 0;
	queue[src].continuekill=1;
	
	mciSendString(_T("Play sound//attack_1.wav"),NULL,0,NULL);//播放声音
	
	Info temp;//发送信息
	temp.c=c; temp.src=src; temp.dstnum=1;temp.dst[0]=dst;
	SendInfo(temp);
	c.src=src;c.dst=dst;//牌中蕴含的伤害来源
    
	SearchCard(src,c,1);//搜索
	moveCardtoTable(c,src);//将牌放到桌子上
	for (int i=0;i<Time;i++)
	{
		Sleep(1);
		drawdata();
		update();
	}

	//求闪函数
	judge=AskFor3(src,dst,c);
	if(judge==0)
	{
		minusHealthAnimation(dst);//没闪就掉血
		if(queue[dst].health<=0) Peach_for_Death(toss,dst,1,src);
	}
	moveCardtoDeath();//放入弃牌堆

	drawdata();
	update();
	return(1);
}

/************************************************************************/
/* 判断是否可以杀到的函数，可以杀，返回1，否则返回0                                                                     */
/************************************************************************/
int JudgeKillRange(int src, int dst){
	if(queue[dst].dead == 1)return(0);
	int i,temp;
	int length_left = 0;
	int length_right = 0;
	int length;
	for(i = 1;i<8;i++){
		temp = (src+i)%8;
		if(queue[temp].dead==1)continue;
		length_left++;
		if(temp == dst)break;
	}
	for(i = 1;i<8;i++){
		temp = (dst+i)%8;
		if(queue[temp].dead==1)continue;
		length_right++;
		if(temp == src)break;
	}
	length = min(length_left, length_right) + queue[dst].defenserange;

	if(length > queue[src].killrange)  return(0);
	return(1);
}

int JudgeCardisRight(card c, char name){
	if(c.name == name)return(1);
	return(0);
}

int AskFor3(int src,int dst,card c)//谁发动 针对谁 什么牌
{
	if(queue[dst].weaponflag[1] == 1)  //防具只有八卦阵
	{
		card baguajudge = queue[dst].de(8,c,dst);
		if(baguajudge.name == '7')  //返回 card八卦阵 代表判定
		{
			card judgecard = GetJudgeCard();
			if((judgecard.color==1)||(judgecard.color==3))
			{
				who=dst;
				bg=1;
				drawdata();update();
				for (int i=0;i<Time*7;i++) Sleep(2);
				bg=0;

				return 1;  //八卦阵判定生效，等效于出闪
			}else
			{
				who=dst;
				bg=2;
				drawdata();update();
				for (int i=0;i<Time*7;i++) Sleep(2);
				bg=0;
			}
		}
	}

	c.src=src;
	card answer=queue[dst].de(1,c,dst);
	if (!JudgeCardisRight(answer,'3')) return 0;
	mciSendString(_T("Play sound//shun_1.wav"),NULL,0,NULL);//播放声音
	Info temp;//发送信息
	temp.c=answer; temp.src=dst; temp.dstnum=-1;
	SendInfo(temp);

	SearchCard(dst,answer,1);//将牌放到桌子上
	moveCardtoTable(answer,dst);//将牌放到弃牌堆
	for (int i=0;i<Time;i++)
	{
		Sleep(1);
		drawdata();
		update();
	}
	return 1;
}

//求杀函数
int AskForS(int src,int dst,card c)//谁发动 针对谁 什么牌
{
	c.src=src;
	card answer=queue[dst].de(5,c,dst);
	if (!JudgeCardisRight(answer,'s')) return 0;

	mciSendString(_T("Play sound//attack_1.wav"),NULL,0,NULL);//播放声音
	Info temp;//发送信息
	temp.c=answer; temp.src=dst; temp.dstnum=-1;
	SendInfo(temp);

	SearchCard(dst,answer,1);//将牌放到桌子上
	moveCardtoTable(answer,dst);//将牌放到弃牌堆
	for (int i=0;i<Time;i++)
	{
		Sleep(10);
		drawdata();
		update();
	}
	return 1;
}

//对自己使用桃
int Peach_for_1(int src, card c){
	if(queue[src].health == queue[src].maxhealth)return(0);
	if(JudgeCardisRight(c,'t') == 0)return(0);

	queue[src].health++;
	Info temp;//发送信息
	temp.c=c; temp.src=src; temp.dstnum=-1;
	SendInfo(temp);

	SearchCard(src,c,1);//搜索
	moveCardtoTable(c,src);

	drawdata();
	update();
	moveCardtoDeath();
	return(1);

}

/************************************************************************/
/* toss指的是某个人当前回合，dst指被伤害的角色                                                                     */
/************************************************************************/
int Peach_for_Death(int toss, int dst,int howmany,int killer)//state==3 这里应该标记是谁杀的
{
	int i=0;int j;
	int temp;
	card temp_card1;
	card answer;
	temp_card1.src = killer;//杀人者
	temp_card1.dst = dst;//求桃者
	int count=0;

	for(i=0;i<8;i++)
	{
		temp = toss-i;
		if(temp<0)temp = temp+8;
		if(queue[temp].dead==1) continue;
		if(howmany==count) break;

		while (1)
		{
			if(howmany==count) break;
			//显示 求几个桃
			howmanytao=howmany-count;
			answer = queue[temp].de(3, temp_card1, temp);
			if(answer.name == 't') 
			{
				count++;
				queue[dst].health++;
				Info temp1;//发送信息
				temp1.c=answer; temp1.src=temp; temp1.dstnum=1;temp1.dst[0]=dst;
				SendInfo(temp1);

				SearchCard(temp,answer,1);
				drawdata();
				update();
				moveCardtoTable(answer, temp);
			}else break;
		}
	}

	if(queue[dst].health<=0)//该角色阵亡
	{
		if (strcmp(queue[dst].name,"曹操")==0) mciSendString(_T("Play sound//曹操dead.wav"),NULL,0,NULL);//播放声音
		if (strcmp(queue[dst].name,"甘宁")==0) mciSendString(_T("Play sound//甘宁dead.wav"),NULL,0,NULL);//播放声音
		if (strcmp(queue[dst].name,"赵云")==0) mciSendString(_T("Play sound//赵云dead.wav"),NULL,0,NULL);//播放声音
		if (strcmp(queue[dst].name,"刘备")==0) mciSendString(_T("Play sound//刘备dead.wav"),NULL,0,NULL);//播放声音
		if (strcmp(queue[dst].name,"孙权")==0) mciSendString(_T("Play sound//孙权dead.wav"),NULL,0,NULL);//播放声音
		if (strcmp(queue[dst].name,"张飞")==0) mciSendString(_T("Play sound//张飞dead.wav"),NULL,0,NULL);//播放声音
		if (strcmp(queue[dst].name,"关羽")==0) mciSendString(_T("Play sound//关羽dead.wav"),NULL,0,NULL);//播放声音

		queue[dst].dead=1;
		queueflag--;
		while(queue[dst].cardnumber!=-1)
		{
			card temp=queue[dst].cards[0];
			SearchCard(dst,queue[dst].cards[0],1);
			moveCardtoTable(temp, dst);
		}	
		while(queue[dst].judgenum!=-1)
		{
			queue[dst].judgenum--;
			moveCardtoTable(queue[dst].judgecards[queue[dst].judgenum+1], dst);
		}
		for (int i=0;i<4;i++)
		{
			if (queue[dst].weaponflag[i]==1)
			{
				card temp=queue[dst].weapon[i];
				queue[dst].weaponflag[i]=0;
				moveCardtoTable(temp,dst);
			}
		}
		
		if(killer==-1) return 1;//被闪电劈死的
		if(queue[dst].group=='z') readyforplayer(killer,3);//杀死反贼拿3张
		if (queue[dst].group=='c' && queue[killer].group=='g')//主公杀死忠臣
		{
			while(queue[killer].cardnumber!=-1)
			{
				card temp=queue[killer].cards[0];
				SearchCard(killer,queue[killer].cards[0],1);
				moveCardtoTable(temp,killer);
			}	
			for (int i=0;i<4;i++)
			{
				if (queue[killer].weaponflag[i]==1)
				{		
					card temp=queue[killer].weapon[i];
					loseweapon(killer,i+6);
					moveCardtoTable(temp,killer);
				}
			}
		}
	}

	return(1);
}


/************************************************************************/
/* 无中生有。0代表因为牌不对无效，1代表生效，2代表被无懈可击了                                               */
/************************************************************************/
int for_Z(int toss, card card_temp2)
{
	if(card_temp2.name != 'z') return(0);
	else
	{
		Info temp;//发送信息
		temp.c=card_temp2; temp.src=toss; temp.dstnum=-1;
		SendInfo(temp);

		mciSendString(_T("Play sound//无中生有.wav"),NULL,0,NULL);//播放声音
		SearchCard(toss,card_temp2,1);
		moveCardtoTable(card_temp2,toss);
		drawdata();
		update();

		if(for_X(toss, toss) == 1)//求无懈
		{
			moveCardtoDeath();
			drawdata();
			update();
			return(2);
		}
		else
		{
			readyforplayer(toss,2);
			moveCardtoDeath();
			drawdata();
			update();
			return(1);
		}
	}
}

/************************************************************************/
/* toss表示是谁的当前回合，src表示是谁向你求无懈可击   */
/*return(0)代表没有无邪，return(1)表示无邪成功         */
/************************************************************************/
int for_X(int toss, int src)//state==4
{
	int count = 0;//当前无懈可击转圈计数
	int count_x = 0;//0 1标记
	int temp=toss+1;
	card card_temp1, answer;
	int i;
	card_temp1.src = src;

	while(1)
	{
		if(count == queueflag)break;

		temp --;
		if(temp<0)temp = temp+8;
		if(queue[temp].dead==1) continue;

		//没有无懈可击直接跳过
		int judge=0;
		for (int i=0;i<=queue[temp].cardnumber;i++)
			if(queue[temp].cards[i].name=='x') judge=1;
		
		if(judge) answer = queue[temp].de(4, card_temp1, temp);
		else answer=refuse;

		if(answer.name != 'x')
		{
			count++;
			continue;
		}
		else
		{
			card_temp1.src=temp;
			Info temp1;//发送信息
			temp1.c=answer; temp1.src=temp; temp1.dstnum=-1;
			SendInfo(temp1);

			mciSendString(_T("Play sound//无懈可击.wav"),NULL,0,NULL);//播放声音
			SearchCard(temp,answer,1);
			moveCardtoTable(answer,temp);
			drawdata();
			update();
			for (int i=0;i<3*Time;i++)
			{
				Sleep(2);
				drawdata();
				update();
			}
			if(count_x == 0)count_x = 1;
			else count_x = 0;
			count = 0;
			temp++;
		}
	}
	return(count_x);
}

/************************************************************************/
/* 万箭齐发。return 0代表打出的牌不是万箭齐发，return 1代表是       */
/************************************************************************/
int for_J(int toss, card card_temp)
{
	int judge;
	if(card_temp.name != 'j') return(0);
	else
	{
		Info temp;//发送信息
		temp.c=card_temp; temp.src=toss; temp.dstnum=-1;
		SendInfo(temp);

		mciSendString(_T("Play sound//万箭齐发.wav"),NULL,0,NULL);//播放声音
		SearchCard(toss,card_temp,1);
		moveCardtoTable(card_temp,toss);
		drawdata();
		update();

		for(int i=1;i<8;i++)
		{
			int temptoss = (toss-i+8)%8;
			if(queue[temptoss].dead==1) continue;
			if(for_X(toss,temptoss) == 1)//有人无懈
			{
				drawdata();
				update();
			}
			else
			{
				//求闪函数
				judge=AskFor3(toss,temptoss,card_temp);
				if(judge==0)
				{
					minusHealthAnimation(temptoss);//没闪就掉血
					if(queue[temptoss].health<=0) Peach_for_Death(toss,temptoss,1,toss);
				}
				drawdata();
				update();
			}
		}
		moveCardtoDeath();
		drawdata();
		update();
		return(1);
	}
}

/************************************************************************/
/* 南蛮入侵。return 0代表打出的牌不是南蛮入侵，return 1代表是      */
/************************************************************************/
int for_M(int toss, card card_temp)
{
	int judge;
	if(card_temp.name != 'm') return(0);
	else
	{
		Info temp;//发送信息
		temp.c=card_temp; temp.src=toss; temp.dstnum=-1;
		SendInfo(temp);

		mciSendString(_T("Play sound//南蛮入侵.wav"),NULL,0,NULL);//播放声音
		SearchCard(toss,card_temp,1);
		moveCardtoTable(card_temp,toss);
		drawdata();
		update();

		for(int i=1;i<8;i++)
		{
			int temptoss = (toss-i+8)%8;
			if(queue[temptoss].dead==1) continue;
			if(for_X(toss,temptoss) == 1)//有人无懈
			{
				drawdata();
				update();
			}
			else
			{
				//求杀函数
				judge=AskForS(toss,temptoss,card_temp);
				if(judge==0)
				{
					minusHealthAnimation(temptoss);//没闪就掉血
					if(queue[temptoss].health<=0) Peach_for_Death(toss,temptoss,1,toss);
				}
				drawdata();
				update();
			}
		}
		moveCardtoDeath();
		drawdata();
		update();
		return(1);
	}
}

int for_G(int src,card G)
{
	if(G.name != 'g') return(0);//判断是不是五谷丰登
	else
	{
		Info temp;//发送信息
		temp.c=G; temp.src=src; temp.dstnum=-1;
		SendInfo(temp);

		mciSendString(_T("Play sound//五谷丰登.wav"),NULL,0,NULL);//播放声音
		SearchCard(src,G,1);
		moveCardtoTable(G,src);
		drawdata();
		update();

		if(readcardsflag+1<queueflag) reorder();
		for (int i=0;i<queueflag;i++)//queueflag张牌进入五谷丰登队列
		{
			wugufengdengflag++;
			wugufengdeng[wugufengdengflag]=readycards[readcardsflag];
			wugufengdeng[wugufengdengflag].click=0;
			readcardsflag--;
		}
		wgfdctrl=1;//打开五谷丰登面板
		wgfdclickctrl=0;

		for(int i=0;i<8;i++)
		{
			int temptoss = (src-i+8)%8;
			if(queue[temptoss].dead==1) continue;
			if(for_X(src,temptoss) == 1)//有人无懈
			{
				drawdata();
				update();
			}
			else
			{
				card choose=queue[temptoss].de(6,refuse,temptoss);//求玩家的五谷选择
				int wugufengdengflagtemp=wugufengdengflag;//暂时记录五谷丰登牌数
				for (int i=0;i<=wugufengdengflag;i++)
				{
					if(choose.name==wugufengdeng[i].name)//如果有重复的
					{
						for (int j=i+1;j<=wugufengdengflag;j++) wugufengdeng[j-1]=wugufengdeng[j];
						wugufengdengflag--;//五谷丰登牌变少一张
						FromSrctoDstAnimation(8,temptoss,choose,0);//动画效果
						queue[temptoss].cardnumber++;//目标手牌数增加
						queue[temptoss].cards[queue[temptoss].cardnumber]=choose;//目标手牌增加
						break;
					}
				}
				if (wugufengdengflag==wugufengdengflagtemp)//如果牌数没有任何变化，说明选手选错了
				{
					choose=wugufengdeng[wugufengdengflag];
					wugufengdengflag--;
					FromSrctoDstAnimation(8,temptoss,choose,0);//动画效果
					queue[temptoss].cardnumber++;//目标手牌数增加
					queue[temptoss].cards[queue[temptoss].cardnumber]=choose;//目标手牌增加
				}
				drawdata();
				update();
			}
		}

		for (int i=0;i<=wugufengdengflag;i++)
		{
			wastecardsflag++;
			wastecards[wastecardsflag]=wugufengdeng[i];
		}

		wgfdclickctrl=0;
		wgfdctrl=0;//关闭面板
		wugufengdengflag=-1;
		moveCardtoDeath();
		drawdata();
		update();
		return(1);
	}
}


int for_D(int src,int dst,card card_D)//决斗
{
	int judge_1,judge_2;
	if(card_D.name != 'd') return(0);
	if(src==dst) return 0;
	else
	{
		if(queue[dst].dead == 1) return(0);//

		Info temp;//发送信息
		temp.c=card_D; temp.src=src; temp.dstnum=1; temp.dst[0]=dst;
		SendInfo(temp);

		mciSendString(_T("Play sound//决斗.wav"),NULL,0,NULL);//播放声音
		SearchCard(src,card_D,1);
		moveCardtoTable(card_D,src);
		drawdata();
		update();

		if(for_X(src,dst) == 1)//有人无懈
		{
			moveCardtoDeath();
			drawdata();
			update();
			return(2);//返回2代表被无懈
		}
		else
		{
			while(1)
			{
				judge_1 = AskForS(src,dst,card_D);
				if(judge_1 == 0)
				{
					minusHealthAnimation(dst);//没杀就掉血
					if(queue[dst].health<=0) Peach_for_Death(toss,dst,1,src);
					drawdata();
					update();
					break;
				}
				judge_2 = AskForS(dst,src,card_D);
				if(judge_2 == 0)
				{
					minusHealthAnimation(src);//没闪就掉血
					if(queue[src].health<=0) Peach_for_Death(toss,src,1,dst);
					drawdata();
					update();
					break;
				}
			}
			moveCardtoDeath();
			drawdata();
			update();
			return(1);
		}
	}
}

/************************************************************************/
/* 桃园结义。return 0代表打出的牌不是桃园结义，return 1代表是       */
/************************************************************************/
int for_Y(int toss, card card_temp)
{
	int judge;
	if(card_temp.name != 'y') return(0);
	else
	{
		Info temp;//发送信息
		temp.c=card_temp; temp.src=toss; temp.dstnum=-1; 
		SendInfo(temp);

		mciSendString(_T("Play sound//桃园结义.wav"),NULL,0,NULL);//播放声音
		SearchCard(toss,card_temp,1);
		moveCardtoTable(card_temp,toss);
		drawdata();
		update();

		for(int i=0;i<8;i++)  //从玩家开始结算
		{
			int temptoss = (toss-i+8)%8;
			if(queue[temptoss].dead==1) continue;
			if(queue[temptoss].health == queue[temptoss].maxhealth) continue;
			if(for_X(toss,temptoss) == 1)//有人无懈
			{
				drawdata();
				update();
			}
			else
			{
				queue[temptoss].health++;
				drawdata();
				update();
			}
		}
		moveCardtoDeath();
		drawdata();
		update();
		return(1);
	}
}

int for_Q(int src,card card_Q,int dst,int kinds,int which)//顺手牵羊 kinds==1 手牌 2装备 3判定牌 which表明是那张牌 装备按照武器 防具 +1 -1的顺序 判定按照来的顺序
//如果是玩家调用,kinds which无效 函数内部判断 如果src==7则调用AskForQ,返回一个数组KW,KW[0]就是kinds KW[1]就二十which
{
	if(card_Q.name != 'q') return(0);
	if(queue[dst].dead == 1) return(0);
	if(src==dst)return 0;
	if(src==7)
	{
		if(queue[dst].cardnumber == -1 &&
			queue[dst].judgenum==-1 &&
			queue[dst].weaponflag[0]==0 &&
			queue[dst].weaponflag[1]==0 &&
			queue[dst].weaponflag[2]==0 &&
			queue[dst].weaponflag[3]==0) return 0;
	}
	else
	{
		if(queue[dst].cardnumber == -1 && kinds==1) return 0;
		if(kinds==3	&& queue[dst].judgenum==-1) return 0;
		if(kinds==2	&& queue[dst].weaponflag[which]==0) return(0);
	}

	if(JudgeQRange(src,dst) == 0) return(0);  //顺手牵羊 牵不到

	Info temp;//发送信息
	temp.c=card_Q; temp.src=src; temp.dstnum=1; temp.dst[0]=dst;
	SendInfo(temp);

	mciSendString(_T("Play sound//顺手牵羊.wav"),NULL,0,NULL);//播放声音
	SearchCard(src,card_Q,1);
	moveCardtoTable(card_Q,src);
	drawdata();
	update();

	if(for_X(src,dst) == 1)//有人无懈
	{
		moveCardtoDeath();
		drawdata();
		update();
		return(2);//返回2代表被无懈
	}
	else
	{
		if(src == 7)  //玩家
		{
			int kw[2];
			AskForQ(dst,kw);  //
			kinds = kw[0];
			which = kw[1];
		}

		if(kinds == 1)//拆手牌
		{
			card tempcard = queue[dst].cards[which];
			SearchCard(dst,tempcard,1);	
			FromSrctoDstAnimation(dst,src,tempcard,1);
			queue[src].cardnumber++;
			queue[src].cards[queue[src].cardnumber] = tempcard;
		}
		if(kinds == 2)//拆装备牌
		{
			card tempcard = queue[dst].weapon[which];
			SearchCard(dst,tempcard,2);	
			FromSrctoDstAnimation(dst,src,tempcard,0);
			queue[src].cardnumber++;
			queue[src].cards[queue[src].cardnumber] = tempcard;
		}
		if(kinds == 3)//拆判定牌
		{
			card tempcard = queue[dst].judgecards[which];
			SearchCard(dst,tempcard,3);
			FromSrctoDstAnimation(dst,src,tempcard,0);
			queue[src].cardnumber++;
			queue[src].cards[queue[src].cardnumber] = tempcard;
		}

		moveCardtoDeath();
		drawdata();
		update();
		return(1);
	}
}

int*AskForQ(int dst,int*KW)//顺手牵羊交互函数
{
	scctrl=1;
	scdst=dst;

	int ok=0;
	while(1)
	{
		drawdata();
		update();
		for (int i=0;i<=queue[dst].cardnumber;i++)
		{
			if (queue[dst].cards[i].click==1)
			{
				queue[dst].cards[i].click=0;
				(*KW)=1; *(KW+1)=i;
				ok=1;
				break;
			}
		}
		if(ok) break;

		for (int i=0;i<=3;i++)
		{
			if(queue[dst].weaponflag[i]==0)continue;
			if (queue[dst].weapon[i].click==1)
			{
				queue[dst].weapon[i].click=0;
				(*KW)=2; *(KW+1)=i;
				ok=1;
				break;
			}
		}
		if(ok) break;

		for (int i=0;i<=queue[dst].judgenum;i++)
		{
			if (queue[dst].judgecards[i].click==1)
			{
				queue[dst].judgecards[i].click=0;
				(*KW)=3; *(KW+1)=i;
				ok=1;
				break;
			}
		}
		if(ok) break;
	}

	scdst=8;
	scctrl=0;
	return KW;
}

/************************************************************************/
/* 判断是否可以顺手牵羊(或 兵粮寸断)到的函数，可以牵，返回1，否则返回0                                                                     */
/************************************************************************/
int JudgeQRange(int src, int dst){
	if(queue[dst].dead == 1) return(0);
	int i,temp;
	int length_left = 0;
	int length_right = 0;
	int length;
	for(i = 1;i<8;i++){
		temp = (src+i)%8;
		if(queue[temp].dead==1)continue;
		length_left++;
		if(temp == dst)break;
	}
	for(i = 1;i<8;i++){
		temp = (dst+i)%8;
		if(queue[temp].dead==1)continue;
		length_right++;
		if(temp == src)break;
	}
	length = min(length_left, length_right) + queue[dst].defenserange;

	if(length >queue[src].qrange) return(0);
	return(1);
}

int for_C(int src,card card_C,int dst,int kinds,int which)//过河拆桥 kinds==1 手牌 2装备 3判定牌 which表明是那张牌 装备按照武器 防具 +1 -1的顺序 判定按照来的顺序
//如果是玩家调用,kinds which无效 函数内部判断 如果src==7则调用AskForQ,返回一个数组KW,KW[0]就是kinds KW[1]就二十which
{
	if(card_C.name != 'c') return(0);
	if(queue[dst].dead == 1) return(0);//
	if(src==dst)return 0;
	
	if(src==7)
	{
		if(queue[dst].cardnumber == -1 &&
		   queue[dst].judgenum==-1 &&
		   queue[dst].weaponflag[0]==0 &&
		   queue[dst].weaponflag[1]==0 &&
		   queue[dst].weaponflag[2]==0 &&
		   queue[dst].weaponflag[3]==0) return 0;
	}
	else
	{
		if(queue[dst].cardnumber == -1 && kinds==1) return 0;
		if(kinds==3	&& queue[dst].judgenum==-1) return 0;
		if(kinds==2	&& queue[dst].weaponflag[which]==0) return(0);
	}

	Info temp;//发送信息
	temp.c=card_C; temp.src=src; temp.dstnum=1; temp.dst[0]=dst;
	SendInfo(temp);

	mciSendString(_T("Play sound//过河拆桥.wav"),NULL,0,NULL);//播放声音
	SearchCard(src,card_C,1);
	moveCardtoTable(card_C,src);
	drawdata();
	update();

	if(for_X(src,dst) == 1)//有人无懈
	{
		moveCardtoDeath();
		drawdata();
		update();
		return(2);//返回2代表被无懈
	}
	else
	{
		if(src == 7)  //玩家
		{
			int kw[2];
			AskForQ(dst,kw);  //
			kinds = kw[0];
			which = kw[1];
		}

		if(kinds == 1)//拆手牌
		{
			card tempcard = queue[dst].cards[which];
			SearchCard(dst,tempcard,1);	
			moveCardtoTable(tempcard,dst);
		}
		if(kinds == 2)//拆装备牌
		{
			card tempcard = queue[dst].weapon[which];
			SearchCard(dst,tempcard,2);	
			moveCardtoTable(tempcard,dst);
		}
		if(kinds == 3)//拆判定牌
		{
			card tempcard = queue[dst].judgecards[which];
			SearchCard(dst,tempcard,3);
			moveCardtoTable(tempcard,dst);
		}

		moveCardtoDeath();
		drawdata();
		update();
		return(1);
	}
}

int for_R(int src,int mid,int dst,card R)//借刀杀人
{
	if(R.name!='r') return 0;//牌不对
	if(queue[mid].weaponflag[0]==0) return 0;//没有武器 死人没武器 不用考虑
	if(JudgeKillRange(mid, dst)==0) return 0;//mid 杀不到dst
	if(src==mid || mid==dst) return 0;
	//以下说明使用成功

	Info temp;//发送信息
	temp.c=R; temp.src=src; temp.dstnum=2; temp.dst[0]=mid; temp.dst[1]=dst;
	SendInfo(temp);
	R.src=src;R.dst=mid;

	mciSendString(_T("Play sound//借刀杀人.wav"),NULL,0,NULL);//播放声音
	SearchCard(src,R,1);
	moveCardtoTable(R,src);
	drawdata();
	update();

	if(for_X(src,mid) == 1)//有人无懈
	{
		moveCardtoDeath();
		drawdata();
		update();
		return(2);//返回2代表被无懈
	}else
	{
		R.src=src; R.dst=dst;//告诉mid src让你杀dst
		int judge=AskForS(src,mid,R);//向mid求出杀
		if(judge==0)//如果没杀
		{
			queue[mid].weaponflag[0]=0;//卸掉武器
			queue[mid].killrange=1+queue[mid].weaponflag[3];//考虑-1马
			card tempweapon=queue[mid].weapon[0];
			FromSrctoDstAnimation(mid,src,tempweapon,0);//移牌动画
			queue[src].cardnumber++;
			queue[src].cards[queue[src].cardnumber]=tempweapon;//给牌至使用借刀杀人者
		}else//使用杀了
		{
			R.src=src; R.dst=mid;//告诉dst src让mid杀你
			int ju=AskFor3(mid,dst,R);//求闪
			if(ju==0)
			{
				minusHealthAnimation(dst);//没闪就掉血
				if(queue[dst].health<=0) Peach_for_Death(src,dst,1,mid);
			}
		}
		drawdata();
		update();
	}
	moveCardtoDeath();
	return 1;
}

/************************************************************************/
/* 乐不思蜀  return 0代表打出的牌无效       */
/************************************************************************/
int for_L(int src,int dst,card card_L)
{
	if(card_L.name != 'l') return(0);  //不是这张牌
	if(queue[dst].dead == 1) return(0);//目标已死；
	if(src==dst)return 0;
	for (int i=0;i<=queue[dst].judgenum;i++)
	{
		if(queue[dst].judgecards[i].name=='l') return 0;//有乐不能再挂了
	}

	Info temp;//发送信息
	temp.c=card_L; temp.src=src; temp.dstnum=1; temp.dst[0]=dst;
	SendInfo(temp);

	mciSendString(_T("Play sound//乐不思蜀.wav"),NULL,0,NULL);//播放声音
	SearchCard(src,card_L,1);  //已经从使用者手牌中去除；
	FromSrctoDstAnimation(src,8,card_L,0);
	drawdata();
	update();

	//乐使用成功
	//增加对目标的标记
	queue[dst].judgenum++;
	queue[dst].judgecards[queue[dst].judgenum]=card_L;
	//绘制标记在draw函数中完成

	moveCardtoDeath(); //已做过标记
	drawdata();
	update();
	return 1;
}


//闪电
int for_SD(int src,card card_SD)
{
	if(card_SD.name != '!') return(0);  //不是这张牌
	for (int i=0;i<=queue[src].judgenum;i++)
	{
		if(queue[src].judgecards[i].name=='!') return 0;//有闪电不能再挂了
	}

	card_SD.click=0;

	Info temp;//发送信息
	temp.c=card_SD; temp.src=src; temp.dstnum=-1;
	SendInfo(temp);

	mciSendString(_T("Play sound//闪电.wav"),NULL,0,NULL);//播放声音
	SearchCard(src,card_SD,1);  //已经从使用者手牌中去除
	FromSrctoDstAnimation(src,8,card_SD,0);
	drawdata();
	update();

	//使用成功，增加对目标的标记
	queue[src].judgenum++;
	queue[src].judgecards[queue[src].judgenum]=card_SD;

	moveCardtoDeath(); //已做过标记，
	drawdata();
	update();
	return 1;

}

int arm(int toss,card card_weapon)  //装备武器函数
{
	card_weapon.click=0;
	if(card_weapon.name=='6'){

		Info temp;//发送信息
		temp.c=card_weapon; temp.src=toss; temp.dstnum=-1;
		SendInfo(temp);
		mciSendString(_T("Play sound//装备武器.wav"),NULL,0,NULL);//播放声音

		if(queue[toss].weaponflag[0]==1) 
		{
			card temp=loseweapon(toss,6);  //原来有装备，则先失去装备
			moveCardtoTable(temp,toss);
		}
		queue[toss].weapon[0] = card_weapon;
		queue[toss].weaponflag[0] = 1;
		queue[toss].killrange = card_weapon.weaponrange + queue[toss].weaponflag[3]; //同时考虑-1马
		SearchCard(toss,card_weapon,1);  //删除玩家手中的牌
	}
	else if(card_weapon.name=='7'){

		Info temp;//发送信息
		temp.c=card_weapon; temp.src=toss; temp.dstnum=-1;
		SendInfo(temp);
		mciSendString(_T("Play sound//装备武器.wav"),NULL,0,NULL);//播放声音

		if(queue[toss].weaponflag[1]==1)
		{
			card temp=loseweapon(toss,7);  //原来有装备，则先失去装备
			moveCardtoTable(temp,toss);
		}
		queue[toss].weapon[1] = card_weapon;
		queue[toss].weaponflag[1] = 1;
		SearchCard(toss,card_weapon,1);  //删除玩家手中的牌
	}
	else if(card_weapon.name=='8'){

		Info temp;//发送信息
		temp.c=card_weapon; temp.src=toss; temp.dstnum=-1;
		SendInfo(temp);
		mciSendString(_T("Play sound//装备武器.wav"),NULL,0,NULL);//播放声音

		if(queue[toss].weaponflag[2]==1)  
		{
			card temp=loseweapon(toss,8);  //原来有装备，则先失去装备
			moveCardtoTable(temp,toss);
		}
		queue[toss].weapon[2] = card_weapon;
		queue[toss].weaponflag[2] = 1;
		queue[toss].defenserange = 1;
		SearchCard(toss,card_weapon,1);  //删除玩家手中的牌
	}
	else if(card_weapon.name=='9'){

		Info temp;//发送信息
		temp.c=card_weapon; temp.src=toss; temp.dstnum=-1;
		SendInfo(temp);
		mciSendString(_T("Play sound//装备武器.wav"),NULL,0,NULL);//播放声音

		if(queue[toss].weaponflag[3]==1)  
		{
			card temp=loseweapon(toss,9);  //原来有装备，则先失去装备
			moveCardtoTable(temp,toss);
		}
		queue[toss].weapon[3] = card_weapon;
		queue[toss].weaponflag[3] = 1;
		queue[toss].qrange = 2;  //顺手牵羊的距离
		queue[toss].killrange++;
		SearchCard(toss,card_weapon,1);  //删除玩家手中的牌
	}
	else return(0);

	moveCardtoDeath();
	return(1);
}

card loseweapon(int toss,int num)  //失去装备
{
	if(num == 6)  queue[toss].killrange=1+queue[toss].weaponflag[3];
	else if(num == 7) ;
	else if(num == 8)  queue[toss].defenserange=0;
	else if(num == 9) { queue[toss].qrange=1; queue[toss].killrange--; }
	num = num - 6;

	card temp = queue[toss].weapon[num];
	queue[toss].weaponflag[num]=0;
	return temp;
}

/************************************************************************/
/* search                                                                     */
/************************************************************************/

void moveCardtoTable(card c,int toss)//将这张牌放到桌面上 有待扩展 显示来源和目标 有动画
{
	//先更新一下
	drawdata();
	update();

	int dstx,dsty,srcx,srcy;
	dstx=365,dsty=235;//屏幕中央
	if(toss==7){srcx=queue[toss].ox; srcy=queue[toss].oy;}//目标位置
	else{srcx=queue[toss].ox+10; srcy=queue[toss].oy+10;}
	int xnow,ynow;//每帧中牌的位置

	int i,j;
	for (i=0;i<=3*Time;i++)
	{
		xnow=(dstx-srcx)*1.0*i/3/Time+srcx;
		ynow=(dsty-srcy)*1.0*i/3/Time+srcy;
		drawdata();
		drawtodcMem(c.hcard,xnow,ynow);
		update();
		Sleep(1);
	}

	tablecardsflag++;
	tablecards[tablecardsflag]=c;
}

void FromSrctoDstAnimation(int src,int dst,card c,int front)//front==0 正面 1 反面   src or dst取8时表示是桌面
{
	//先更新一下
	drawdata();
	update();

	int dstx,dsty,srcx,srcy;
	srcx=queue[src].ox;  srcy=queue[src].oy;
	dstx=queue[dst].ox;  dsty=queue[dst].oy;

	if(src!=7){ srcx+=10; srcy+=10; }
	if(dst!=7){ dstx+=10; dsty+=10; }
	int xnow,ynow;//每帧中牌的位置
	
	int i,j;
	for (i=0;i<=3*Time;i++)
	{
		xnow=(dstx-srcx)*1.0*i/3/Time+srcx;
		ynow=(dsty-srcy)*1.0*i/3/Time+srcy;
		drawdata();
		if(front==0) drawtodcMem(c.hcard,xnow,ynow);
		else drawtodcMem(refuse.cardback,xnow,ynow);
		update();
		Sleep(1);
	}
	for (int i=0;i<3*Time;i++) Sleep(2);
}

void moveCardtoDeath()//将桌面牌放入弃牌堆
{
	for (int i=0;i<3*Time;i++)
	{
		Sleep(1);
		drawdata();
		update();
	}
	int i;
	for (i=0;i<=tablecardsflag;i++)
	{
		wastecardsflag++;
		wastecards[wastecardsflag]=tablecards[i];
	}
	tablecardsflag=-1;
}

void reorder()
{
	int i;
	//将两者合并
	for(i=readcardsflag+1;i<=readcardsflag+wastecardsflag+1;i++)readycards[i] = wastecards[i-readcardsflag-1];

	readcardsflag = readcardsflag+wastecardsflag+1;
	wastecardsflag = -1;

	//生成随机数
	srand((unsigned)time(NULL));
	//进行牌混洗
	random_shuffle(readycards,readycards+readcardsflag+1);
}

card SearchCard(int toss,card c,int kinds)//搜牌的时候要考虑点数和花色
{
	int i,j;
	if(kinds==1)
	{
		for (i=queue[toss].cardnumber;i>=0;i--)
		{
			if (queue[toss].cards[i].name==c.name && queue[toss].cards[i].color==c.color && queue[toss].cards[i].number==c.number)	break;
		}

		for (j=i+1;j<=queue[toss].cardnumber;j++) queue[toss].cards[j-1]=queue[toss].cards[j];
		queue[toss].cardnumber--;
	}
	
	if (kinds==2)
	{
		if(queue[toss].weapon[0].name==c.name && queue[toss].weapon[0].color==c.color && queue[toss].weapon[0].number==c.number) loseweapon(toss,6);
		if(queue[toss].weapon[1].name==c.name && queue[toss].weapon[1].color==c.color && queue[toss].weapon[1].number==c.number) loseweapon(toss,7);
		if(queue[toss].weapon[2].name==c.name && queue[toss].weapon[2].color==c.color && queue[toss].weapon[2].number==c.number) loseweapon(toss,8);
		if(queue[toss].weapon[3].name==c.name && queue[toss].weapon[3].color==c.color && queue[toss].weapon[3].number==c.number) loseweapon(toss,9);
	}

	if(kinds==3)
	{
		for (i=queue[toss].judgenum;i>=0;i--)
		{
			if (queue[toss].judgecards[i].name==c.name && queue[toss].judgecards[i].color==c.color && queue[toss].judgecards[i].number==c.number)	break;
		}

		for (j=i+1;j<=queue[toss].judgenum;j++) queue[toss].judgecards[j-1]=queue[toss].judgecards[j];
		queue[toss].judgenum--;
	}

	return c;
}

void minusHealthAnimation(int toss)
{
	int i;
	queue[toss].health--;

	mciSendString(_T("Play sound//掉血.wav"),NULL,0,NULL);//播放声音
	if (toss==7) 
	{
		int bighealthwid=28;
		int x=971;
		int y=458+queue[toss].health*bighealthwid;

		for (i=0;i<10;i++)
		{
			drawdata();
			SelectObject(dcBmp,bombbig);
			BitBlt(dcMem,x,y,21, 22, dcBmp,(i+5)*21, 0, SRCCOPY);
			update();
			Sleep(Time*1.5);
		}
	}

	if (toss!=7) 
	{
		int bighealthwid=16;
		int x=queue[toss].ox+49+16*queue[toss].health;
		int y=queue[toss].oy+75;

		for (i=0;i<10;i++)
		{
			drawdata();
			SelectObject(dcBmp,bombsmall);
			BitBlt(dcMem,x,y,16,16, dcBmp,(i+5)*16, 0, SRCCOPY);
			update();
			Sleep(Time*1.5);
		}
	}

	drawdata();
	update();
}
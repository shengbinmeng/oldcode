void drawdata()
{
	int i;
	//基本图片
	SelectObject(dcBmp, table);
	BitBlt(dcMem, 0, 0, 1007, 608, dcBmp, 0, 0, SRCCOPY);//面板	
	DrawInfo();
	queue[8].ox=365;queue[8].oy=235;//屏幕中央

	//画4种LED灯 并判断游戏结束条件
	int countj=0; int countz=0;int countc=0;
	for (int i=0;i<=7;i++)
	{
		if(queue[i].group=='g' && queue[i].dead==1) gameover=1;//主公死亡游戏结束
		if(queue[i].group=='c' && queue[i].dead==0) countc++;
		if(queue[i].group=='j' && queue[i].dead==0) countj++;
		if(queue[i].group=='z' && queue[i].dead==0) countz++;
	}
	//判断游戏结束条件
	if(countj==0 && countz==0) gameover=1;
	if(gameover==1)
	{
		if(countj==0 && countz==0) GameOver('g');//主公赢了
		if(countj>0 && countz==0) GameOver('j');//内奸赢了
		if(countz>0) GameOver('z');//反贼赢了
	}

	int ledwid=20;
	int ledx0=835; int ledy0=50;
	//画主公
	int ledx=ledx0;
	drawtodcMem(led[0].hcard,ledx,ledy0);
	ledx+=ledwid; 
	//画忠臣
	for (int i=0;i<2;i++)
	{
		if(countc)//如果有活的
		{
			drawtodcMem(led[1].hcard,ledx,ledy0);
			ledx+=ledwid; 
			countc--;
		}else
		{
			drawtodcMem(led[1].cardback,ledx,ledy0);
			ledx+=ledwid; 
		}
	}
	//画内奸
	for (int i=0;i<1;i++)
	{
		if(countj)//如果有活的
		{
			drawtodcMem(led[2].hcard,ledx,ledy0);
			ledx+=ledwid; 
			countj--;
		}else
		{
			drawtodcMem(led[2].cardback,ledx,ledy0);
			ledx+=ledwid; 
		}
	}
	//画反贼
	for (int i=0;i<4;i++)
	{
		if(countz)//如果有活的
		{
			drawtodcMem(led[3].hcard,ledx,ledy0);
			ledx+=ledwid; 
			countz--;
		}else
		{
			drawtodcMem(led[3].cardback,ledx,ledy0);
			ledx+=ledwid; 
		}
	}

	//三个按钮
	for (i=0;i<3;i++)
	{
		if(threeavailable[i]==0) continue;
		if (threebutton[i].click==0 && threebutton[i].flash==1 )
			drawtodcMem(threebutton[i].hcard,threebutton[i].left,threebutton[i].top);
		else drawtodcMem(threebutton[i].cardback,threebutton[i].left,threebutton[i].top);
	}

	//面板字体设置
	SetBkMode(dcMem,TRANSPARENT);
	SetTextColor(dcMem,RGB(255,255,0));
	//

	//初始化各个玩家面板位置
	queue[0].ox=11; queue[0].oy=246;
	queue[1].ox=11; queue[1].oy=68;
	queue[2].ox=193;queue[2].oy=33;
	queue[3].ox=348;queue[3].oy=33;
	queue[4].ox=503;queue[4].oy=33;
	queue[5].ox=683;queue[5].oy=68;
	queue[6].ox=683;queue[6].oy=246;
	queue[7].ox=530;queue[7].oy=467;
	//

	//图片偏移量参数
	int cardbutton[2]={7,29};
	int maxcolor[2]={49,75};
	int maxcolorwid=15;
	int cardnumber[2]={4,74};
	//


	//画前7个玩家的图片
	int j;
	for (i=0;i<7;i++)
	{
		if (chara[i].click==1 || chara[i].flash==1)//画选中
		drawtodcMem(chara[i].hcard,chara[i].left,chara[i].top);

		SelectObject(dcBmp, queue[i].cardbackground);
		BitBlt(dcMem, queue[i].ox, queue[i].oy, 131, 154, dcBmp, 0, 0, SRCCOPY);//画角色面板

		SelectObject(dcBmp, queue[i].cardbutton);
		BitBlt(dcMem, queue[i].ox+cardbutton[0], queue[i].oy+cardbutton[1], 117, 44, dcBmp, 0, 0, SRCCOPY);//画角色图

		for (j=0;j<queue[i].maxhealth;j++)//画血槽
		{
			SelectObject(dcBmp, queue[i].maxhealthcolor);
			BitBlt(dcMem, queue[i].ox+maxcolor[0]+j*maxcolorwid, queue[i].oy+maxcolor[1], 15, 16, dcBmp, 0, 0, SRCCOPY);
		}

		for (j=0;j<queue[i].health;j++)//画血量
		{
			SelectObject(dcBmp, healthcolor);
			BitBlt(dcMem, queue[i].ox+maxcolor[0]+maxcolorwid*j, queue[i].oy+maxcolor[1], 15, 16, dcBmp, 0, 0, SRCCOPY);
		}

		char str[]="00";//画手牌数量
		str[0]=(queue[i].cardnumber+1)/10+'0';
		if(str[0]=='0')str[0]=' ';
		str[1]=(queue[i].cardnumber+1)%10+'0';
		str[2]=0;
		SelectObject(dcMem,h1);
		TextOutA(dcMem,queue[i].ox+4,queue[i].oy+74,str,strlen(str));

		//画角色身份
		if(keyd)
		{
			SelectObject(dcMem,hw);//选择汉字字体
			if(queue[i].group=='g')	TextOutA(dcMem,queue[i].ox+5,queue[i].oy+3,"主公",strlen("主公"));
			if(queue[i].group=='c')	TextOutA(dcMem,queue[i].ox+5,queue[i].oy+3,"忠臣",strlen("忠臣"));
			if(queue[i].group=='j')	TextOutA(dcMem,queue[i].ox+5,queue[i].oy+3,"内奸",strlen("内奸"));
			if(queue[i].group=='z')	TextOutA(dcMem,queue[i].ox+5,queue[i].oy+3,"反贼",strlen("反贼"));
			SelectObject(dcMem,h1);//选择数字字体
		}
		
		//画其他玩家的装备
		for(int k=0;k<4;k++){
			if(queue[i].weaponflag[k]==1) drawtodcMem(queue[i].weapon[k].weaponsmallpic,queue[i].ox+1,queue[i].oy+k*15+91);
		}

		//画判定标记
		for (int j=0;j<=queue[i].judgenum;j++)
		{
			switch(queue[i].judgecards[j].name)
			{
			case 'l':
				SelectObject(dcBmp,flagL);
				TransparentBlt(dcMem,queue[i].ox+j*30,queue[i].oy+150,24,25,dcBmp,0,0,24,25,RGB(255,255,255));
				break;
			case '!':
				SelectObject(dcBmp,flagSD);
				TransparentBlt(dcMem,queue[i].ox+j*30,queue[i].oy+150,24,25,dcBmp,0,0,24,25,RGB(255,255,255));
				break;
			}
		}

		//画武将名字
		SelectObject(dcMem,hw);//选择汉字字体
		TextOutA(dcMem,queue[i].ox+50,queue[i].oy+10,queue[i].name,strlen(queue[i].name));

		//显示角色手牌
		if(i!=7 && keyd==1)
		{
			char strcard[100];
			strcard[0]=0;
			char *p=strcard;
			for (int j=0;j<=queue[i].cardnumber;j++)
			{
				if(queue[i].cards[j].name=='s') strcpy(p,"杀");
				if(queue[i].cards[j].name=='3') strcpy(p,"闪");
				if(queue[i].cards[j].name=='t') strcpy(p,"桃");
				if(queue[i].cards[j].name=='z') strcpy(p,"中");
				if(queue[i].cards[j].name=='x') strcpy(p,"懈");
				if(queue[i].cards[j].name=='j') strcpy(p,"剑");
				if(queue[i].cards[j].name=='m') strcpy(p,"蛮");
				if(queue[i].cards[j].name=='g') strcpy(p,"谷");
				if(queue[i].cards[j].name=='d') strcpy(p,"斗");
				if(queue[i].cards[j].name=='q') strcpy(p,"顺");
				if(queue[i].cards[j].name=='c') strcpy(p,"拆");
				if(queue[i].cards[j].name=='y') strcpy(p,"义");
				if(queue[i].cards[j].name=='l') strcpy(p,"乐");
				if(queue[i].cards[j].name=='!') strcpy(p,"电");
				if(queue[i].cards[j].name=='r') strcpy(p,"借");
				if(queue[i].cards[j].name=='6') strcpy(p,"武");
				if(queue[i].cards[j].name=='7') strcpy(p,"防");
				if(queue[i].cards[j].name=='8') strcpy(p,"加");
				if(queue[i].cards[j].name=='9') strcpy(p,"减");
				p+=2;
			}
			p++;
			(*p)=0;
			TextOutA(dcMem,queue[i].ox,queue[i].oy+156,strcard,strlen(strcard));
			SelectObject(dcMem,h1);//选择数字字体
		}

		//画阵亡图片
		if(queue[i].dead==1 && i!=7)
		{
			if(queue[i].group=='c')
			{
				SelectObject(dcBmp,zhonchen);
				TransparentBlt(dcMem,queue[i].ox+2,queue[i].oy+35,127,70,dcBmp,0,0,146,94,RGB(255,255,255));
			}
			if(queue[i].group=='j')
			{
				SelectObject(dcBmp,neijian);
				TransparentBlt(dcMem,queue[i].ox+2,queue[i].oy+35,127,70,dcBmp,0,0,146,94,RGB(255,255,255));
			}
			if(queue[i].group=='z')
			{
				SelectObject(dcBmp,fanzei);
				TransparentBlt(dcMem,queue[i].ox+2,queue[i].oy+35,127,70,dcBmp,0,0,146,94,RGB(255,255,255));
			}
		}
		/************************************************************************/
		/*   将来要加画装备                                                     */
		/************************************************************************/
	}
	//

	//画人的信息

	//人的手牌
	int x,y;
	x=170;y=465;//人的手牌起始坐标
	int wid;//牌和牌之间的间隔
	if(queue[7].cardnumber<5)	wid=93;
	else wid=(550-50)/(queue[7].cardnumber+3);

	for (i=0;i<=queue[7].cardnumber;i++) 
	{
		if (queue[7].cards[i].click==1) queue[7].cards[i].top=y-20;
		else queue[7].cards[i].top=y;

		if (queue[7].cards[i].flash==0)
		{
			queue[7].cards[i].left=x;
			queue[7].cards[i].right=x+wid;
			if (i==queue[7].cardnumber) queue[7].cards[i].right=x+92;
			queue[7].cards[i].bottom=y+130;
			drawtodcMem(queue[7].cards[i].hcard,queue[7].cards[i].left,queue[7].cards[i].top);//操作者手牌
			x+=wid;
		}
		else
		{
			x+=5;
			queue[7].cards[i].left=x;
			queue[7].cards[i].bottom=y+130;
			drawtodcMem(queue[7].cards[i].hcard,queue[7].cards[i].left,queue[7].cards[i].top);//操作者手牌
			
			if (queue[7].cardnumber<5)
			{
				queue[7].cards[i].right=x+92;
				x+=98;
			}else
			{
				if (92*0.8>wid)x+=92*0.8;
				else x+=wid+10;
				queue[7].cards[i].right=x;
			}
		}
	}

	//画人的血量
	int bighealthwid=28;
	for (i=0;i<queue[7].health;i++)	drawtodcMem(queue[7].bighealthcolor,971,458+i*bighealthwid);

	if (chara[7].click==1 || chara[7].flash==1)//画me选中
		drawtodcMem(chara[7].hcard,chara[7].left,chara[7].top);

	//画人图
	SelectObject(dcBmp,queue[7].cardp);
	BitBlt(dcMem, 846, 458, 115, 108,dcBmp, 0, 0, SRCCOPY);

	//画身份、魏 蜀 吴
	drawtodcMem(queue[7].groupface,810,456);
	drawtodcMem(queue[7].countryface,810,570);

	//画阵亡信息
	if(queue[7].dead==1)
	{
		if(queue[7].group=='c') 
		{
			SelectObject(dcBmp,zhonchen);
			TransparentBlt(dcMem,queue[7].ox-130,queue[7].oy+10,167,92,dcBmp,0,0,146,94,RGB(255,255,255));
		}
		if(queue[7].group=='j')
		{
			SelectObject(dcBmp,neijian);
			TransparentBlt(dcMem,queue[7].ox-130,queue[7].oy+10,167,92,dcBmp,0,0,146,94,RGB(255,255,255));
		}
		if(queue[7].group=='z')
		{
			SelectObject(dcBmp,fanzei);
			TransparentBlt(dcMem,queue[7].ox-130,queue[7].oy+10,167,92,dcBmp,0,0,146,94,RGB(255,255,255));
		}
	}

	//画当前牌堆牌数
	char str[]="000";
	str[0]=(readcardsflag+1)/100+'0';
	if(str[0]=='0')str[0]=' ';
	str[1]=((readcardsflag+1)/10)%10+'0';
	if(str[1]=='0')str[1]=' ';
	str[2]=(readcardsflag+1)%10+'0';
	str[3]=0;
	SelectObject(dcMem,h1);
	TextOutA(dcMem,767,38,str,strlen(str));

	//画桌面弃牌
	x=365;y=235;//桌面弃牌起始坐标
	if(tablecardsflag<5) wid=100;
	else wid=(480-50)/(tablecardsflag+1);
	x-=tablecardsflag*1.0/2*wid;
	for (i=0;i<=tablecardsflag;i++) drawtodcMem(tablecards[i].hcard,x+i*wid,y);//桌面弃牌牌

	//画人的装备
	for(int k=0;k<4;k++){
		if(queue[7].weaponflag[k]==1)  drawtodcMem(queue[7].weapon[k].weaponbigpic,13,k*32+470);
	}

	//画人的判定标志
	int judgex=queue[7].ox-337;
	for (int j=0;j<=queue[7].judgenum;j++)
	{
		switch(queue[7].judgecards[j].name)
		{
		case 'l':
			SelectObject(dcBmp,flagL);
			TransparentBlt(dcMem,judgex+j*30,queue[7].oy-18,24,25,dcBmp,0,0,24,25,RGB(255,255,255));
			break;
		case '!':
			SelectObject(dcBmp,flagSD);
			TransparentBlt(dcMem,judgex+j*30,queue[7].oy-18,24,25,dcBmp,0,0,24,25,RGB(255,255,255));
			break;
		}
	}

	//画提示语句
	if (qiushan)
	{
		char str1[10];
		strcpy(str1,queue[qiushansrc].name);
		TextOutA(dcMem,170,435,str1,strlen(str1));

		char str2[]="向你求出闪";
		TextOutA(dcMem,210,435,str2,strlen(str2));
	}

	if (qiusha)
	{
		char str1[10];
		strcpy(str1,queue[qiushasrc].name);
		TextOutA(dcMem,170,435,str1,strlen(str1));

		char str2[]="向你求出杀";
		TextOutA(dcMem,210,435,str2,strlen(str2));
	}

	if (qiutao)
	{
		char str1[10];
		strcpy(str1,queue[qiutaosrc].name);
		TextOutA(dcMem,170,435,str1,strlen(str1));

		char str2[]="向你求";
		TextOutA(dcMem,210,435,str2,strlen(str2));

		char str3[20];
		str3[0]=howmanytao+' 0';
		str3[1]=0;
		strcat(str3,"个桃");
		TextOutA(dcMem,265,435,str3,strlen(str3));
	}

	if (wuxie)
	{
		char str1[10];
		strcpy(str1,queue[wuxiesrc].name);
		TextOutA(dcMem,170,435,str1,strlen(str1));

		char str2[]="向你求无懈可击";
		TextOutA(dcMem,210,435,str2,strlen(str2));
	}

	if (wugu)
	{
		char str0[]="你可以选一张五谷丰登牌";
		TextOutA(dcMem,180,435,str0,strlen(str0));
	}

	if (bagua)
	{
		char str0[]="你是否判定八卦阵";
		TextOutA(dcMem,180,435,str0,strlen(str0));
	}

	if (qiuqipai)
	{
		char str0[]="你需要弃掉";
		TextOutA(dcMem,180,435,str0,strlen(str0));

		char str1[]="0";
		str1[0]='0'+qiuqipainum;
		TextOutA(dcMem,280,435,str1,strlen(str1));

		char str2[]="张牌";
		TextOutA(dcMem,290,435,str2,strlen(str2));
	}

	if (dangqian)
	{
		char str0[]="现在是你的当前回合";
		TextOutA(dcMem,180,435,str0,strlen(str0));
	}
	
	//画顺手牵羊 过河拆桥 五谷丰登的面板
	if (scctrl)
	{
		SelectObject(dcBmp,bigboard);
		BitBlt(dcMem, 210,70,406, 447,dcBmp, 0, 0, SRCCOPY);//画大面板
		//这里先只考虑手牌 装备和判定将来再加
		int x=222; int y=82;
		int wid;//牌和牌之间的间隔
		if(queue[scdst].cardnumber<4)	wid=93;
		else wid=(406-92*2)/(queue[scdst].cardnumber+1);

		for (i=0;i<=queue[scdst].cardnumber;i++) 
		{
			queue[scdst].cards[i].top=y;
			if (queue[scdst].cards[i].flash==0)
			{
				queue[scdst].cards[i].left=x;
				queue[scdst].cards[i].right=x+wid;
				if (i==queue[scdst].cardnumber) queue[scdst].cards[i].right=x+92;
				queue[scdst].cards[i].bottom=y+130;
				drawtodcMem(refuse.cardback,queue[scdst].cards[i].left,queue[scdst].cards[i].top);//操作者手牌
				x+=wid;
			}
			else
			{
				x+=5;
				queue[scdst].cards[i].left=x;
				queue[scdst].cards[i].bottom=y+130;
				drawtodcMem(refuse.cardback,queue[scdst].cards[i].left,queue[scdst].cards[i].top);//操作者手牌

				if (queue[scdst].cardnumber<5)
				{
					queue[scdst].cards[i].right=x+92;
					x+=98;
				}else
				{
					if (92*0.8>wid)x+=92*0.8;
					else x+=wid+10;
					queue[scdst].cards[i].right=x;
				}
			}
		}
		
		//画装备
		x=222; y=82+130;
		wid=93;
		for (i=0;i<=3;i++) 
		{
			if(queue[scdst].weaponflag[i]==0) continue;
			queue[scdst].weapon[i].top=y;
			if (queue[scdst].weapon[i].flash==0)
			{
				queue[scdst].weapon[i].left=x;
				queue[scdst].weapon[i].right=x+wid;
				queue[scdst].weapon[i].bottom=y+130;
				drawtodcMem(queue[scdst].weapon[i].hcard,queue[scdst].weapon[i].left,queue[scdst].weapon[i].top);//操作者手牌
				x+=wid;
			}
			else
			{
				x+=5;
				queue[scdst].weapon[i].left=x;
				queue[scdst].weapon[i].bottom=y+130;
				drawtodcMem(queue[scdst].weapon[i].hcard,queue[scdst].weapon[i].left,queue[scdst].weapon[i].top);

				queue[scdst].weapon[i].right=x+92;
				x+=98;
			}
		}

		//画判定牌
		x=222; y=82+130*2;
		wid=93;
		for (i=0;i<=queue[scdst].judgenum;i++) 
		{
			queue[scdst].judgecards[i].top=y;
			if (queue[scdst].judgecards[i].flash==0)
			{
				queue[scdst].judgecards[i].left=x;
				queue[scdst].judgecards[i].right=x+wid;
				queue[scdst].judgecards[i].bottom=y+130;
				drawtodcMem(queue[scdst].judgecards[i].hcard,queue[scdst].judgecards[i].left,queue[scdst].judgecards[i].top);//操作者手牌
				x+=wid;
			}
			else
			{
				x+=5;
				queue[scdst].judgecards[i].left=x;
				queue[scdst].judgecards[i].bottom=y+130;
				drawtodcMem(queue[scdst].judgecards[i].hcard,queue[scdst].judgecards[i].left,queue[scdst].judgecards[i].top);

				queue[scdst].judgecards[i].right=x+92;
				x+=98;
			}
		}
	}

	if(wgfdctrl)
	{
		SelectObject(dcBmp,smallboard);
		BitBlt(dcMem, 210, 110,406, 295,dcBmp, 0, 0, SRCCOPY);//画大面板
		
		int up1,up2;//考虑还有几张牌
		if(wugufengdengflag>3){up1=3;up2=wugufengdengflag;}
		else {up1=wugufengdengflag;up2=-1;}

		int x=226; int y=124;
		int wid=95;
		for (int i=0;i<=up1;i++)
		{
			wugufengdeng[i].top=y;
			if(wugufengdeng[i].flash==1)wugufengdeng[i].top-=8;
			wugufengdeng[i].left=x;
			wugufengdeng[i].right=wugufengdeng[i].left+92;
			wugufengdeng[i].bottom=wugufengdeng[i].top+134;
			drawtodcMem(wugufengdeng[i].hcard,x,wugufengdeng[i].top);
			x+=wid;
		}
		x=226; y=264;
		for (int i=4;i<=up2;i++)
		{
			wugufengdeng[i].top=y;
			if(wugufengdeng[i].flash==1)wugufengdeng[i].top-=8;
			wugufengdeng[i].left=x;
			wugufengdeng[i].right=wugufengdeng[i].left+92;
			wugufengdeng[i].bottom=wugufengdeng[i].top+134;
			drawtodcMem(wugufengdeng[i].hcard,x,wugufengdeng[i].top);
			x+=wid;
		}
	}

	//画判定生效失效部分
	int xcenter=325;
	int ycenter=295;
	if (lbss==1)//乐不思蜀生效
	{
		char str[50];
		strcpy(str,queue[who].name);
		strcat(str,"的乐不思蜀判定");
		SetTextColor(dcMem,RGB(250,250,160));//设置颜色
		TextOutA(dcMem,xcenter-3,ycenter,str,strlen(str));

		SetTextColor(dcMem,RGB(0,255,0));//设置颜色
		TextOutA(dcMem,xcenter+153,ycenter,"生效",strlen("生效"));
	}
	if (lbss==2)//乐不思蜀失效
	{
		char str[50];
		strcpy(str,queue[who].name);
		strcat(str,"的乐不思蜀判定");
		SetTextColor(dcMem,RGB(250,250,160));//设置颜色
		TextOutA(dcMem,xcenter-3,ycenter,str,strlen(str));

		SetTextColor(dcMem,RGB(255,0,0));//设置颜色
		TextOutA(dcMem,xcenter+153,ycenter,"失效",strlen("失效"));
	}
	if (sd==1)//闪电生效
	{
		char str[50];
		strcpy(str,queue[who].name);
		strcat(str,"的闪电判定");
		SetTextColor(dcMem,RGB(250,250,160));//设置颜色
		TextOutA(dcMem,xcenter+15,ycenter,str,strlen(str));

		SetTextColor(dcMem,RGB(0,255,0));//设置颜色
		TextOutA(dcMem,xcenter+135,ycenter,"生效",strlen("生效"));
	}
	if (sd==2)//闪电失效
	{
		char str[50];
		strcpy(str,queue[who].name);
		strcat(str,"的闪电判定");
		SetTextColor(dcMem,RGB(250,250,160));//设置颜色
		TextOutA(dcMem,xcenter+15,ycenter,str,strlen(str));

		SetTextColor(dcMem,RGB(255,0,0));//设置颜色
		TextOutA(dcMem,xcenter+135,ycenter,"失效",strlen("失效"));
	}
	if (bg==1)//八卦阵生效
	{
		char str[50];
		strcpy(str,queue[who].name);
		strcat(str,"的八卦阵判定");
		SetTextColor(dcMem,RGB(250,250,160));//设置颜色
		TextOutA(dcMem,xcenter+7,ycenter,str,strlen(str));

		SetTextColor(dcMem,RGB(0,255,0));//设置颜色
		TextOutA(dcMem,xcenter+143,ycenter,"生效",strlen("生效"));
	}
	if (bg==2)//八卦阵失效
	{
		char str[50];
		strcpy(str,queue[who].name);
		strcat(str,"的八卦阵判定");
		SetTextColor(dcMem,RGB(250,250,160));//设置颜色
		TextOutA(dcMem,xcenter+7,ycenter,str,strlen(str));

		SetTextColor(dcMem,RGB(255,0,0));//设置颜色
		TextOutA(dcMem,xcenter+143,ycenter,"失效",strlen("失效"));
	}

	SetTextColor(dcMem,RGB(255,255,0));//设置颜色
}

void update()
{
	BitBlt(hdc, 0, 0, 1007, 608, dcMem, 0, 0, SRCCOPY);
}

void drawtodcMem(HBITMAP p,int x,int y)//画图用的函数
{
	SelectObject(dcBmp,p);
	BitBlt(dcMem, x, y, 200, 200,dcBmp, 0, 0, SRCCOPY);
}

void drawbegin0()//选角色
{
	SelectObject(dcBmp, table);
	BitBlt(dcMem, 0, 0, 1007, 608, dcBmp, 0, 0, SRCCOPY);//面板
	SelectObject(dcBmp, smallboard);
	BitBlt(dcMem, 210, 110, 406, 295, dcBmp, 0, 0, SRCCOPY);//小面板

	int x=320; int y=120;
	int wid=95;
	for (int i=0;i<2;i++)
	{
		fourgroup[i].top=y;
		if(fourgroup[i].flash==1)fourgroup[i].top-=8;
		fourgroup[i].left=x;
		fourgroup[i].right=fourgroup[i].left+92;
		fourgroup[i].bottom=fourgroup[i].top+134;
		drawtodcMem(fourgroup[i].hcard,x,fourgroup[i].top);
		x+=wid;
	}
	x=320; y=260;
	for (int i=2;i<4;i++)
	{
		fourgroup[i].top=y;
		if(fourgroup[i].flash==1)fourgroup[i].top-=8;
		fourgroup[i].left=x;
		fourgroup[i].right=fourgroup[i].left+92;
		fourgroup[i].bottom=fourgroup[i].top+134;
		drawtodcMem(fourgroup[i].hcard,x,fourgroup[i].top);
		x+=wid;
	}

}

void drawbegin1(char group)//选将牌
{
	SelectObject(dcBmp, table);
	BitBlt(dcMem, 0, 0, 1007, 608, dcBmp, 0, 0, SRCCOPY);//面板
	SelectObject(dcBmp, smallboard);
	BitBlt(dcMem, 210, 110, 406, 295, dcBmp, 0, 0, SRCCOPY);//小面板
	
	if (group=='g')//如果是主公
	{
		int x=265; int y=120;
		int wid=105;
		for (int i=5;i<=7;i++)
		{
			queue[i].realcard.top=y;
			if(queue[i].realcard.flash==1)queue[i].realcard.top-=8;
			queue[i].realcard.left=x;
			queue[i].realcard.right=queue[i].realcard.left+92;
			queue[i].realcard.bottom=queue[i].realcard.top+134;
			drawtodcMem(queue[i].realcard.hcard,x,queue[i].realcard.top);
			x+=wid;
		}
		x=315; y=260;
		for (int i=0;i<=1;i++)
		{
			queue[i].realcard.top=y;
			if(queue[i].realcard.flash==1)queue[i].realcard.top-=8;
			queue[i].realcard.left=x;
			queue[i].realcard.right=queue[i].realcard.left+92;
			queue[i].realcard.bottom=queue[i].realcard.top+134;
			drawtodcMem(queue[i].realcard.hcard,x,queue[i].realcard.top);
			x+=wid;
		}
	}else//如果是其它
	{
		int x=265; int y=195;
		int wid=105;
		for (int i=0;i<=2;i++)
		{
			queue[i].realcard.top=y;
			if(queue[i].realcard.flash==1)queue[i].realcard.top-=8;
			queue[i].realcard.left=x;
			queue[i].realcard.right=queue[i].realcard.left+92;
			queue[i].realcard.bottom=queue[i].realcard.top+134;
			drawtodcMem(queue[i].realcard.hcard,x,queue[i].realcard.top);
			x+=wid;
		}
	}
}

void SendInfo(Info information)//发送到消息框
{
	infoboxnum++;
	infobox[infoboxnum]=information;
}

void DrawInfo()
{
	int x0=830; int y0=103;//左上角坐标
	int maxlength=26;//每行的最大字符串长度
	int heightwid=13;//高度间距
	SelectObject(dcMem,hw);//选择文字字体
	
	int j=14;
	for (int i=infoboxnum;i>=0;i--)
	{
		Info temp=infobox[i];
		char str[80];//一条信息
		strcpy(str,queue[temp.src].name);//输入来源
		char srcstr[20];
		strcpy(srcstr,queue[temp.src].name);//输入来源
		char midstr[20];//中间者
		char dststr[20];//目标

		if (temp.dstnum==-1)//没有敌人
		{
			strcat(str,"打出  ");
			strcat(str,temp.c.allname);	
		}
		else//有敌人
		{
			strcat(str,"对");
			strcat(str,queue[temp.dst[0]].name);
			strcpy(midstr,queue[temp.dst[0]].name);
			if(temp.dstnum==2)
			{
				strcat(str,"杀");
				strcat(str,queue[temp.dst[1]].name);
				strcpy(dststr,queue[temp.dst[1]].name);
			}
			strcat(str,"打出  ");
			strcat(str,temp.c.allname);
		}

		int length=strlen(str);//考虑文字长度超过一行
		int two=0; char str1[50];
		if(length>26)
		{
			two=1;
			strcpy(str1,str+26);
			str[26]=0;
		}
		
		if (two)
		{
			TextOutA(dcMem,x0,y0+j*heightwid,str1,strlen(str1));
			j--;
			if(j<0) break;
		}
		TextOutA(dcMem,x0,y0+j*heightwid,str,strlen(str));
		
		SetTextColor(dcMem,RGB(0,255,0));
		TextOutA(dcMem,x0,y0+j*heightwid,srcstr,strlen(srcstr));
		if(temp.dstnum==1 || temp.dstnum==2) TextOutA(dcMem,x0+(strlen(srcstr)+1)*7.8,y0+j*heightwid,midstr,strlen(midstr));
		if(temp.dstnum==2) TextOutA(dcMem,x0+(strlen(srcstr)+strlen(midstr)+2)*7.8,y0+j*heightwid,dststr,strlen(dststr));
		SetTextColor(dcMem,RGB(255,255,0));
		
		j--;
		if(j<0) break;
	}
	
	char instru[50]="按左右键调整游戏速度";
	TextOutA(dcMem,830,305,instru,strlen(instru));
	char instru1[50]="当前游戏速度是:";
	TextOutA(dcMem,830,319,instru1,strlen(instru1));
	char speed[20];
	itoa(210-Time,speed,10);
	TextOutA(dcMem,930,319,speed,strlen(speed));

	SelectObject(dcMem,h1);//选择数字字体
}

void GameOver(char who)//谁赢了
{
	SelectObject(dcBmp,endpic);
	BitBlt(dcMem, 200, 100, 1007, 608, dcBmp, 0, 0, SRCCOPY);//结束面板

	SetTextColor(dcMem,RGB(228,213,160));//设置颜色
	SelectObject(dcMem,hend);
	SetBkMode(dcMem,TRANSPARENT);

	overclick=1;
	int wid,x,y;
	while(1)
	{
		wid=23;
		x=248;
		y=191;
		if (who=='g')
		{
			//画主公
			for (int i=7;i>=0;i--)
				if (queue[i].group=='g')//找到主公
				{
					drawtodcMem(gend,x,y-5);//画身份标识
					TextOutA(dcMem,x+130,y+2,queue[i].name,strlen(queue[i].name));//画武将名字
					drawtodcMem(victory,x+254,y);
					if(i==7)drawtodcMem(playerend,x+392,y);//如果是玩家
				}
			//画忠臣
			for (int i=7;i>=0;i--)
				if (queue[i].group=='c')//找到忠臣
				{
					y+=wid;
					drawtodcMem(cend,x,y-2);//画身份标识
					TextOutA(dcMem,x+130,y+2,queue[i].name,strlen(queue[i].name));//画武将名字
					drawtodcMem(victory,x+254,y);
					if(i==7)drawtodcMem(playerend,x+392,y);//如果是玩家
				}
			y+=10;
			//画内奸
			for (int i=7;i>=0;i--)
				if (queue[i].group=='j')//找到忠臣
				{
					y+=wid;
					drawtodcMem(jend,x,y-4);//画身份标识
					TextOutA(dcMem,x+130,y+2,queue[i].name,strlen(queue[i].name));//画武将名字
					drawtodcMem(fail,x+254,y);
					if(i==7)drawtodcMem(playerend,x+392,y);//如果是玩家					}
				}

			y+=10;
			//画反贼
			for (int i=7;i>=0;i--)
				if (queue[i].group=='z')//找到忠臣
				{
					y+=wid;
					drawtodcMem(zend,x,y-4);//画身份标识
					TextOutA(dcMem,x+130,y+2,queue[i].name,strlen(queue[i].name));//画武将名字
					drawtodcMem(fail,x+254,y);
					if(i==7)drawtodcMem(playerend,x+392,y);//如果是玩家					}
				}
		}

		if (who=='j')
		{
			//画内奸
			for (int i=7;i>=0;i--)
				if (queue[i].group=='j')//找到主公
				{
					drawtodcMem(jend,x,y-4);//画身份标识
					TextOutA(dcMem,x+130,y+2,queue[i].name,strlen(queue[i].name));//画武将名字
					drawtodcMem(victory,x+254,y);
					if(i==7)drawtodcMem(playerend,x+392,y);//如果是玩家
				}

				y+=10;
				//画主公
				for (int i=7;i>=0;i--)
					if (queue[i].group=='g')//找到忠臣
					{
						y+=wid;
						drawtodcMem(gend,x,y-5);//画身份标识
						TextOutA(dcMem,x+130,y+2,queue[i].name,strlen(queue[i].name));//画武将名字
						drawtodcMem(fail,x+254,y);
						if(i==7)drawtodcMem(playerend,x+392,y);//如果是玩家
					}
					//画忠臣
					for (int i=7;i>=0;i--)
						if (queue[i].group=='c')//找到忠臣
						{
							y+=wid;
							drawtodcMem(cend,x,y-2);//画身份标识
							TextOutA(dcMem,x+130,y+2,queue[i].name,strlen(queue[i].name));//画武将名字
							drawtodcMem(fail,x+254,y);
							if(i==7)drawtodcMem(playerend,x+392,y);//如果是玩家					}
						}

						y+=10;
						//画反贼
						for (int i=7;i>=0;i--)
							if (queue[i].group=='z')//找到忠臣
							{
								y+=wid;
								drawtodcMem(zend,x,y-4);//画身份标识
								TextOutA(dcMem,x+130,y+2,queue[i].name,strlen(queue[i].name));//画武将名字
								drawtodcMem(fail,x+254,y);
								if(i==7)drawtodcMem(playerend,x+392,y);//如果是玩家					}
							}
		}

		if (who=='z')
		{
			//画反贼
			y-=wid;
			for (int i=7;i>=0;i--)
				if (queue[i].group=='z')//找到反贼
				{
					y+=wid;
					drawtodcMem(zend,x,y-4);//画身份标识
					TextOutA(dcMem,x+130,y+2,queue[i].name,strlen(queue[i].name));//画武将名字
					drawtodcMem(victory,x+254,y);
					if(i==7)drawtodcMem(playerend,x+392,y);//如果是玩家
				}

				y+=10;
				//画内奸
				for (int i=7;i>=0;i--)
					if (queue[i].group=='j')//找到忠臣
					{
						y+=wid;
						drawtodcMem(jend,x,y-4);//画身份标识
						TextOutA(dcMem,x+130,y+2,queue[i].name,strlen(queue[i].name));//画武将名字
						drawtodcMem(fail,x+254,y);
						if(i==7)drawtodcMem(playerend,x+392,y);//如果是玩家
					}

					y+=10;
					//画主公
					for (int i=7;i>=0;i--)
						if (queue[i].group=='g')//找到忠臣
						{
							y+=wid;
							drawtodcMem(gend,x,y-5);//画身份标识
							TextOutA(dcMem,x+130,y+2,queue[i].name,strlen(queue[i].name));//画武将名字
							drawtodcMem(fail,x+254,y);
							if(i==7)drawtodcMem(playerend,x+392,y);//如果是玩家					}
						}
						//画忠臣
						for (int i=7;i>=0;i--)
							if (queue[i].group=='c')//找到忠臣
							{
								y+=wid;
								drawtodcMem(cend,x,y-2);//画身份标识
								TextOutA(dcMem,x+130,y+2,queue[i].name,strlen(queue[i].name));//画武将名字
								drawtodcMem(fail,x+254,y);
								if(i==7)drawtodcMem(playerend,x+392,y);//如果是玩家					}
							}
		}
		update();
	}
}